-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 01, 2019 lúc 03:59 CH
-- Phiên bản máy phục vụ: 10.1.21-MariaDB
-- Phiên bản PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `education_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_activities`
--

CREATE TABLE `mn_activities` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_activities`
--

INSERT INTO `mn_activities` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(33, 'Thông tin liên hệ', NULL, '<p>info</p>\r\n', NULL, '<p>info</p>\r\n', NULL, '', NULL, NULL, 31, 0, 0, 0, 0, NULL, 1546065952, NULL, NULL, NULL, NULL, 'thong-tin-lien-he'),
(34, 'test 1', NULL, '<p>t1</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362125.png', 26, 0, 0, 1, 0, NULL, 1546142757, NULL, NULL, NULL, NULL, 'test-1'),
(35, 'test 2', NULL, '<p>t2</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'banner_web_mba-15456224621.jpg', 26, 0, 0, 0, 0, NULL, 1546142776, NULL, NULL, NULL, NULL, 'test-2'),
(36, 'test 3', NULL, '<p>t3</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'banner_lien_thong_dai_hoc_quoc_te-15456224481.png', 26, 0, 0, 0, 0, NULL, 1546142792, NULL, NULL, NULL, NULL, 'test-3'),
(37, 'test 4', NULL, '<p>test 4</p>\r\n', NULL, '<p>test 4</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362128.png', 26, 0, 0, 0, 0, NULL, 1546231804, NULL, NULL, NULL, NULL, 'test-4'),
(38, 'test 5', NULL, '<p>test 5</p>\r\n', NULL, '<p>test 5</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362129.png', 26, 0, 0, 0, 0, NULL, 1546231827, NULL, NULL, NULL, NULL, 'test-5'),
(39, 'test 6', NULL, '<p>test 6</p>\r\n', NULL, '<p>test 6</p>\r\n', NULL, '', NULL, NULL, 26, 0, 0, 0, 0, NULL, 1546231851, NULL, NULL, NULL, NULL, 'test-6'),
(40, 'test 7', NULL, '<p>test 7</p>\r\n', NULL, '<p>test 7</p>\r\n', NULL, '', NULL, 'noel_hutech-154563621210.png', 26, 0, 0, 0, 0, NULL, 1546231908, NULL, NULL, NULL, NULL, 'test-7');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_admin`
--

CREATE TABLE `mn_admin` (
  `Id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(5) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` tinyint(4) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_admin`
--

INSERT INTO `mn_admin` (`Id`, `username`, `password`, `email`, `level`, `time`, `fullname`, `note`, `address`, `ticlock`, `date`) VALUES
(1, 'admin', 'c25837af2de5370a97a75830c4fe9e7a', 'admin@admin.vn', 1, '0000-00-00 00:00:00', '', '       ', '', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_adminmenu`
--

CREATE TABLE `mn_adminmenu` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `ticlock` tinyint(4) NOT NULL DEFAULT '0',
  `route` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_adminmenu`
--

INSERT INTO `mn_adminmenu` (`Id`, `title_vn`, `images`, `sort`, `parentid`, `ticlock`, `route`) VALUES
(1, 'Cấu hình site', 'icon-48-cauhinh.png', 0, 1, 0, 'admincp/website'),
(2, 'Admin', 'icon-48-user.png', 0, 1, 0, 'admincp/admin'),
(3, 'Logo web', 'icon-48-image.png', 0, 1, 0, 'admincp/flash'),
(4, 'Thành viên', 'icon-48-nhom.png', 13, 3, 1, 'admincp/user'),
(6, 'Banner home', 'icon-48-camera.png', 3, 3, 0, 'admincp/weblink'),
(66, 'Ngành đào tạo', 'icon-catagory-sale.png', 4, 45, 0, 'admincp/provinces'),
(11, 'Menu Giới thiệu', 'icon-48-square.png', 0, 2, 1, 'admincp/catnews'),
(12, 'Bài viết giới thiệu', 'icon-48-chi.png', 9, 2, 0, 'admincp/news'),
(13, 'Trang', 'icon-48-templates.png', 10, 2, 1, 'admincp/pagehtml'),
(15, 'Admin menu', 'icon-48-menu.png', 0, 1, 0, 'admincp/adminmenu'),
(18, 'Sản Phẩm', 'icon-48-product.png', 5, 3, 0, 'admincp/product'),
(19, 'Danh mục', 'icon-48-doituong.png', 4, 3, 0, 'admincp/catelog'),
(20, 'Gọi lại', 'phone.png', 10, 3, 0, 'admincp/contact'),
(21, 'Hỗ trợ', 'icon-48-support.png', 11, 3, 1, 'admincp/support'),
(41, 'Menu Thư viện', 'icon-48-square.png', 0, 41, 0, 'admincp/catlibrary'),
(24, 'Menu Ngang', 'icon-48-menuitem_bak.png', 5, 1, 0, 'admincp/menu'),
(47, 'Website', 'icon-sale.png', 0, 47, 0, 'admin/webplus'),
(45, 'Menu Tuyển Sinh', 'icon-48-square.png', 0, 45, 0, 'admincp/catadmissions'),
(46, 'Menu Sinh Viên', 'icon-48-square.png', 0, 46, 0, 'admincp/catstudent'),
(61, 'Bài viết sinh viên', 'icon-48-nhom.png', 1, 46, 0, 'admincp/student'),
(32, 'Banner Slide Home', 'icon-48-camera.png', 6, 1, 0, 'admincp/bmenu'),
(59, 'Dữ liệu thư viện', 'icon-48-book.png', 1, 41, 0, 'admincp/library'),
(64, 'Bài viết, Thông tin tuyển sinh', 'icon-order-sale.png', 1, 45, 0, 'admincp/admissions'),
(65, 'Tuyển sinh Online', 'icon-48-chuyenmuc.png', 6, 45, 0, 'admincp/admonline'),
(57, 'Bài viết hoạt động Khoa, Trung tâm', 'icon-48-paper.png', 1, 44, 0, 'admincp/activities'),
(44, 'Menu Hoạt động Khoa, Trung tâm', 'icon-48-square.png', 0, 44, 0, 'admincp/catactivities'),
(43, 'Bài viết  hợp tác doanh nghiệp', 'icon-48-paper.png', 1, 43, 0, 'admincp/cooperation'),
(54, 'Menu hợp tác doanh nghiệp', 'icon-48-square.png', 0, 43, 0, 'admincp/catcooperation'),
(53, 'Menu Media', 'icon-48-square.png', 0, 42, 0, 'admincp/catmedia'),
(42, 'Media', 'icon-48-youtube.png', 0, 42, 0, 'admincp/media'),
(67, 'Phương thức xét tuyển', 'icon-48-menuitem_bak.png', 5, 45, 0, 'admincp/catadmonline');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_admissions`
--

CREATE TABLE `mn_admissions` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_admissions`
--

INSERT INTO `mn_admissions` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(35, 'Tại sao chọn trường Tài chính - Kế toán', NULL, '<p>des</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'noel_hutech-154563621214.png', 25, 0, 0, 1, 0, NULL, 1546260141, NULL, NULL, NULL, NULL, 'tai-sao-chon-truong-tai-chinh-ke-toan'),
(36, 'Cơ sở vật chất', NULL, '<p>des</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'PC-new2_jpg.gif', 25, 0, 0, 0, 0, NULL, 1546260270, NULL, NULL, NULL, NULL, 'co-so-vat-chat'),
(38, 'Tuyể sinh test', NULL, '<p>desc</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'PC-new2_jpg6.gif', 30, 0, 0, 0, 0, NULL, 1546327856, NULL, NULL, NULL, NULL, 'tuye-sinh-test');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_admonline`
--

CREATE TABLE `mn_admonline` (
  `Id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `graduation_year` tinyint(4) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticlock` tinyint(4) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `idprovin` int(11) DEFAULT NULL,
  `score1` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score2` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score3` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zalo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sbd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtb12` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kind` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tohop` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_bmenu`
--

CREATE TABLE `mn_bmenu` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(200) NOT NULL,
  `link` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `ticlock` int(11) NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_bmenu`
--

INSERT INTO `mn_bmenu` (`Id`, `title_vn`, `images`, `link`, `sort`, `ticlock`, `parentid`, `style`) VALUES
(111, NULL, 'b49e875d90505f35982a2ee0a2794a3b.jpg', '', 0, 0, 0, 3),
(112, NULL, 'a79dead1ac2b5dfef26bc5418c5970dc.png', '', 0, 0, 0, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_captcha`
--

CREATE TABLE `mn_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_captcha`
--

INSERT INTO `mn_captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(489, 1474692425, '127.0.0.1', 'sEKUqP'),
(490, 1474692446, '127.0.0.1', '8yUGaz'),
(491, 1474692513, '127.0.0.1', 'iZCN0l'),
(492, 1474692611, '127.0.0.1', 'eUm4GK'),
(493, 1474692635, '127.0.0.1', 'MLPbAg'),
(494, 1474692672, '127.0.0.1', 'cZVOb3'),
(495, 1474692677, '127.0.0.1', 'i12LHB'),
(496, 1474692682, '127.0.0.1', 'EK0STx'),
(497, 1474692697, '127.0.0.1', 'wgjeTE'),
(498, 1474692736, '127.0.0.1', '7eZy28'),
(499, 1474692776, '127.0.0.1', 'pHJtNR'),
(500, 1474692795, '127.0.0.1', 'xBZSQj'),
(501, 1474692826, '127.0.0.1', 'eEBpAw'),
(502, 1474949518, '127.0.0.1', 'NCi8hE'),
(503, 1475027512, '127.0.0.1', 'kfynSL'),
(504, 1475032787, '127.0.0.1', 'dfJKEB'),
(505, 1475035745, '127.0.0.1', 'L7gi6V'),
(506, 1475035931, '127.0.0.1', 'OG95v6'),
(507, 1487578693, '210.245.33.131', 'YSTvI6'),
(508, 1487738909, '118.69.226.181', 'daUiBr'),
(509, 1488111230, '58.186.53.77', 'WXopa6'),
(510, 1488174641, '113.161.91.49', '9ynClT'),
(511, 1488295460, '42.118.138.238', 'ygzZG3'),
(512, 1488448002, '210.245.33.131', 'pRa05B'),
(513, 1488448018, '210.245.33.131', 'oTjVdw'),
(514, 1488448039, '210.245.33.131', '1CKOXY'),
(515, 1488520207, '210.245.33.131', '85qMdR'),
(516, 1488521844, '210.245.33.131', 'I8LMTP'),
(517, 1489024755, '210.245.33.131', 'Mvwp7m'),
(518, 1489108855, '123.21.125.11', 'tG60qv');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catactivities`
--

CREATE TABLE `mn_catactivities` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `home_sub` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catactivities`
--

INSERT INTO `mn_catactivities` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`, `home_sub`) VALUES
(24, 'Giới thiệu', 0, 0, 1, 0, NULL, '', '', 'gioi-thieu', 1546065568, NULL, 0),
(25, 'Giảng viên', 0, 0, 2, 0, NULL, '', '', 'giang-vien', 1546065578, NULL, 0),
(26, 'Sinh viên', 1, 0, 3, 0, NULL, '', '', 'sinh-vien', 1546065588, NULL, 1),
(27, 'Đoàn hội', 0, 0, 4, 0, NULL, '', '', 'doan-hoi', 1546065597, NULL, 0),
(28, 'Doanh nghiệp tuyển dụng', 0, 0, 5, 0, NULL, '', '', 'doanh-nghiep-tuyen-dung', 1546065605, NULL, 0),
(29, 'Nghiên cứu khoa học', 0, 0, 6, 0, NULL, '', '', 'nghien-cuu-khoa-hoc', 1546065613, NULL, 0),
(30, 'Thư viện', 0, 0, 7, 0, NULL, '', '', 'thu-vien', 1546065625, NULL, 0),
(31, 'Liên hệ', 0, 0, 8, 0, NULL, '', '', 'lien-he', 1546065638, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catadmissions`
--

CREATE TABLE `mn_catadmissions` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catadmissions`
--

INSERT INTO `mn_catadmissions` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`) VALUES
(25, 'Môi trường học tập', 0, 0, 0, 0, NULL, '', '', 'moi-truong-hoc-tap', 1546258872, NULL),
(26, 'Hợp tác doanh nghiệp', 0, 25, 0, 0, NULL, '', '', 'hop-tac-doanh-nghiep', 1546258891, NULL),
(27, 'Thông báo tuyển sinh', 0, 0, 0, 0, NULL, '', '', 'thong-bao-tuyen-sinh', 1546258931, NULL),
(30, 'Tư vấn tuyển sinh', 0, 0, 0, 0, NULL, '', '', 'tu-van-tuyen-sinh', 1546259016, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catadmonline`
--

CREATE TABLE `mn_catadmonline` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catadmonline`
--

INSERT INTO `mn_catadmonline` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`) VALUES
(24, 'Xét điểm thi học bạ', 0, 0, 0, 0, NULL, NULL, NULL, 'xet-diem-thi-hoc-ba', 1546332625, NULL),
(25, 'Xét điểm thi THPT', 0, 0, 0, 0, NULL, NULL, NULL, 'xet-diem-thi-thpt', 1546332647, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catalogue`
--

CREATE TABLE `mn_catalogue` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) NOT NULL,
  `file_vn` varchar(255) NOT NULL,
  `file_en` varchar(255) NOT NULL,
  `ticlock` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catcooperation`
--

CREATE TABLE `mn_catcooperation` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catcooperation`
--

INSERT INTO `mn_catcooperation` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`) VALUES
(24, 'Giới thiệu doanh nghiệp', 0, 0, 1, 0, NULL, '', '', 'gioi-thieu-doanh-nghiep', 1546060171, NULL),
(25, 'Ngày hội việc làm', 0, 0, 2, 0, NULL, '', '', 'ngay-hoi-viec-lam', 1546060182, NULL),
(26, 'Ứng viên TCKT', 0, 0, 3, 0, NULL, '', '', 'ung-vien-tckt', 1546060190, NULL),
(27, 'Khởi nghiệp', 1, 0, 4, 0, NULL, '', '', 'khoi-nghiep', 1546060199, NULL),
(28, 'Học cùng doanh nghiệp', 0, 0, 5, 0, NULL, '', '', 'hoc-cung-doanh-nghiep', 1546060209, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catdeal`
--

CREATE TABLE `mn_catdeal` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` float DEFAULT NULL,
  `percentab` float DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catelog`
--

CREATE TABLE `mn_catelog` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` float DEFAULT NULL,
  `percentab` float DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` text COLLATE utf8_unicode_ci,
  `short_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catelog`
--

INSERT INTO `mn_catelog` (`Id`, `title_vn`, `description_vn`, `images`, `images1`, `parentid`, `sort`, `ticlock`, `alias`, `meta_keyword`, `meta_description`, `home`, `style`, `title`, `icon`, `percent`, `percentab`, `date`, `tag`, `short_title`) VALUES
(268, 'Chăm sóc da', NULL, 'cham-soc-da-1545470118.png', NULL, 265, 0, 0, 'cham-soc-da', '', '', NULL, '', '', NULL, NULL, NULL, 1545470118, '', ''),
(265, 'Beauty', NULL, 'beauty-1545469998.png', NULL, 0, 0, 0, 'beauty', '', '', NULL, '', '', NULL, NULL, NULL, 1545469998, '', ''),
(266, 'Tinh Dầu', NULL, 'tinh-dau-1545470032.png', NULL, 0, 0, 0, 'tinh-dau', '', '', NULL, '', '', NULL, NULL, NULL, 1545470032, '', ''),
(267, 'Thiết Bị Gia Đình', NULL, 'thiet-bi-gia-dinh-1545470076.jpg', NULL, 0, 0, 0, 'thiet-bi-gia-dinh', '', '', NULL, '', '', NULL, NULL, NULL, 1545470076, '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catlibrary`
--

CREATE TABLE `mn_catlibrary` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catmedia`
--

CREATE TABLE `mn_catmedia` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catmedia`
--

INSERT INTO `mn_catmedia` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`) VALUES
(24, 'Video giới thiệu trường', 0, 0, 0, 0, NULL, '', '', 'video-gioi-thieu-truong', 1546050009, NULL),
(25, 'Video các hoạt động', 1, 0, 0, 0, NULL, '', '', 'video-cac-hoat-dong', 1546051406, NULL),
(26, 'Khoa học thường thức', 1, 0, 0, 0, NULL, '', '', 'khoa-hoc-thuong-thuc', 1546051564, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catnews`
--

CREATE TABLE `mn_catnews` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catnews`
--

INSERT INTO `mn_catnews` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`) VALUES
(1, 'Tổng quan về trường', 0, NULL, 1, 0, NULL, '', '', 'tong-quan-ve-truong', 1546011512, NULL),
(12, 'Sơ đồ tổ chức', 0, 0, 3, 0, NULL, NULL, NULL, 'so-do', 1546011512, NULL),
(11, 'Lịch sử phát triển', 0, 0, 2, 0, NULL, NULL, NULL, 'lich-su-phat-trien', 1546011512, NULL),
(13, 'Hội đồng quản trị', 0, 0, 4, 0, NULL, NULL, NULL, 'hoi-dong-quan-tri', 1546011512, NULL),
(14, 'Ban giam hiệu', 0, 0, 5, 0, NULL, NULL, NULL, 'ban-giam-hieu', 1546011512, NULL),
(15, 'Thành tích đạt được', 0, 0, 6, 0, NULL, NULL, NULL, 'thanh-tich-dat-duoc', 1546011512, NULL),
(16, 'Hệ thống nhận dạng và ý nghĩa', 0, 0, 7, 0, NULL, NULL, NULL, 'he-thong-nhan-dang-va-y-nghia', 1546011512, NULL),
(17, 'Báo cáo 3 công khai', 0, 0, 8, 0, NULL, NULL, NULL, 'bao-cao-3-cong-khai', 1546011512, NULL),
(18, 'Chính sách học phí', 0, 0, 9, 0, NULL, NULL, NULL, 'chinh-sach-hoc-phi', 1546011512, NULL),
(19, 'Chính sách học bổng', 0, 0, 10, 0, NULL, NULL, NULL, 'chinh-sach-hoc-bong', 1546011512, NULL),
(20, 'Thư viện hình ảnh', 0, 0, 11, 0, NULL, NULL, NULL, 'thu-vien-hinh-anh', 1546011512, NULL),
(21, 'Văn hóa và Truyền thống', 0, 0, 12, 0, NULL, NULL, NULL, 'van-hoa-truyen-thong', 1546011512, NULL),
(22, 'Đường đến trường', 0, 0, 13, 0, NULL, NULL, NULL, 'duong-den-truong', 1546011512, NULL),
(25, 'Giới thiệu', 0, 0, 0, 0, NULL, '', '', 'gioi-thieu', 1546083350, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catpercent`
--

CREATE TABLE `mn_catpercent` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `percent` float DEFAULT NULL,
  `percentab` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_catstudent`
--

CREATE TABLE `mn_catstudent` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` int(1) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `home_sub` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_catstudent`
--

INSERT INTO `mn_catstudent` (`Id`, `title_vn`, `home`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `date`, `description_vn`, `home_sub`) VALUES
(24, 'Giới thiệu', 0, 0, 1, 0, NULL, '', '', 'gioi-thieu', 1546224618, NULL, 0),
(25, 'Rèn luyện', 0, 0, 2, 0, NULL, '', '', 'ren-luyen', 1546224626, NULL, 0),
(26, 'Hoạt động sinh viên', 1, 0, 3, 0, NULL, '', '', 'hoat-dong-sinh-vien', 1546224633, NULL, 1),
(27, 'Hỗ trợ sinh viên', 0, 0, 4, 0, NULL, '', '', 'ho-tro-sinh-vien', 1546224641, NULL, 0),
(28, 'Câu lạc bộ', 0, 0, 5, 0, NULL, '', '', 'cau-lac-bo', 1546224650, NULL, 0),
(29, 'Thi đua', 0, 0, 6, 0, NULL, '', '', 'thi-dua', 1546224657, NULL, 0),
(30, 'Hình ảnh', 0, 0, 7, 0, NULL, '', '', 'hinh-anh', 1546224666, NULL, 0),
(31, 'Liên hệ', 0, 0, 8, 0, NULL, '', '', 'lien-he', 1546224675, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_code`
--

CREATE TABLE `mn_code` (
  `iduser` int(11) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `IP` varchar(255) DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `Id` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_code`
--

INSERT INTO `mn_code` (`iduser`, `date`, `IP`, `idcat`, `Id`, `link`) VALUES
(4, 1468034805, '116.102.107.80', NULL, 1, 'http://www.mada.vn/'),
(4, 1468221624, '116.102.107.80', NULL, 2, 'http://www.mada.vn/'),
(4, 1468221628, '116.102.107.80', NULL, 3, 'http://www.mada.vn/'),
(4, 1468221663, '116.102.107.80', NULL, 4, 'http://www.mada.vn/'),
(4, 1468221687, '116.102.107.80', NULL, 5, 'http://www.mada.vn/'),
(4, 1468221707, '116.102.107.80', NULL, 6, 'http://www.mada.vn/'),
(4, 1468221731, '116.102.107.80', NULL, 7, 'http://www.mada.vn/');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_code_discount`
--

CREATE TABLE `mn_code_discount` (
  `id` int(11) NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_day` datetime NOT NULL,
  `end_day` datetime NOT NULL,
  `created` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `ticlock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_color`
--

CREATE TABLE `mn_color` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_color`
--

INSERT INTO `mn_color` (`Id`, `title_vn`, `color`, `ticlock`, `sort`) VALUES
(1, 'Hổ phách', '#FFBF00', 0, 0),
(2, 'Ametit', '#9966CC', 0, 0),
(3, 'Xanh berin', '#7FFFD4', 0, 0),
(4, 'Xanh da trời', '#007FFF', 0, 0),
(5, 'Be', '#F5F5DC', 0, 0),
(6, 'Nâu sẫm', '#3D2B1F', 0, 0),
(7, 'Đen', '#000000', 0, 0),
(8, 'Xanh lam', '	#0000FF', 0, 0),
(9, 'Nâu', '#964B00', 0, 0),
(10, 'Da bò', '#F0DC82', 0, 0),
(11, 'Cam cháy', '#CC5500', 0, 0),
(12, 'Hồng y', '#C41E3A', 0, 0),
(13, 'Đỏ yên chi', '#960018', 0, 0),
(14, 'Men ngọc', '#ACE1AF', 0, 0),
(15, 'Anh đào', '#DE3163', 0, 0),
(16, 'Xanh hoàng hôn', '#007BA7', 0, 0),
(17, 'Xanh nõn chuối', '	#7FFF00', 0, 0),
(18, 'Xanh cô ban', '#0047AB', 0, 0),
(19, 'Đồng', '#B87333', 0, 0),
(20, 'San hô', '#FF7F50', 0, 0),
(21, 'Kem', '#FFFDD0', 0, 0),
(22, 'Đỏ thắm', '#DC143C', 0, 0),
(23, 'Xanh lơ (cánh chả)', '#00FFFF', 0, 0),
(24, 'Lục bảo', '#50C878', 0, 0),
(25, 'Vàng kim loại', '#FFD700', 0, 0),
(26, 'Xám', '#808080', 0, 0),
(27, 'Xanh lá cây', '#00FF00', 0, 0),
(28, 'Vòi voi', '#DF73FF', 0, 0),
(29, 'Chàm', '#4B0082', 0, 0),
(30, 'Ngọc thạch', '#00A86B', 0, 0),
(31, 'Kaki', '#C3B091', 0, 0),
(32, 'Oải hương', '#E6E6FA', 0, 0),
(33, 'Vàng chanh', '#CCFF00', 0, 0),
(34, 'Hồng sẫm', '#FF00FF', 0, 0),
(35, 'Hạt dẻ', '#800000', 0, 0),
(36, 'Cẩm quỳ', '#993366', 0, 0),
(37, 'Hoa cà', '#c8a2c8', 0, 0),
(38, 'Lam sẫm', '#000080', 0, 0),
(39, 'Ochre', '#CC7722', 0, 0),
(40, 'Ô liu', '#808000', 0, 0),
(41, 'Da cam', '#FF7F00', 0, 0),
(42, 'Lan tím', '#DA70D6', 0, 0),
(43, 'Lòng đào', '#FFE5B4	', 0, 0),
(44, 'Dừa cạn', '#CCCCFF', 0, 0),
(45, 'Hồng', '#FFC0CB', 0, 0),
(46, 'Mận', '#660066', 0, 0),
(47, 'Xanh thủy tinh', '#003399', 0, 0),
(48, 'Hồng đất', '#CC8899', 0, 0),
(49, 'Tía', '#660099', 0, 0),
(50, 'Đỏ', '#FF0000', 0, 0),
(51, 'Cá hồi', '#FF8C69', 0, 0),
(52, 'Đỏ tươi', '#FF2400', 0, 0),
(53, 'Nâu đen', '#704214', 0, 0),
(54, 'Bạc', '#C0C0C0', 0, 0),
(55, 'Nâu tanin', '#D2B48C', 0, 0),
(56, 'Mòng két', '#008080', 0, 0),
(57, 'Xanh Thổ', '#30D5C8', 0, 0),
(58, 'Đỏ son', '#FF4D00', 0, 0),
(59, 'Tím', '#BF00BF', 0, 0),
(60, 'Xanh crôm', '	#40826D', 0, 0),
(61, 'Trắng', '#FFFFFF', 0, 0),
(62, 'Vàng', '#FFFF00', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_comment`
--

CREATE TABLE `mn_comment` (
  `id` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `content` text,
  `date` int(11) DEFAULT NULL,
  `idpro` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ticlock` tinyint(4) DEFAULT NULL,
  `star` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_comment_payment`
--

CREATE TABLE `mn_comment_payment` (
  `Id` int(11) NOT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `content` text,
  `date` int(11) DEFAULT NULL,
  `idcustommer` int(11) DEFAULT NULL,
  `ticlock` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_contact`
--

CREATE TABLE `mn_contact` (
  `Id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ticlock` tinyint(4) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `view` tinyint(4) NOT NULL DEFAULT '0',
  `idpro` int(11) DEFAULT NULL,
  `deal` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_cooperation`
--

CREATE TABLE `mn_cooperation` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_cooperation`
--

INSERT INTO `mn_cooperation` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(33, 'Khởi nghiệp', NULL, '<p>des</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'noel_hutech-154563621211.png', 27, 0, 0, 1, 0, 1, 1546060846, NULL, NULL, NULL, NULL, 'khoi-nghiep'),
(34, 'Test 1', NULL, '<p>des</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'banner_lien_thong_dai_hoc_quoc_te-1545622448.png', 27, 0, 0, 0, 0, NULL, 1546138081, NULL, NULL, NULL, NULL, 'test-1'),
(35, 'Test 2', NULL, '<p>des</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362124.png', 25, 0, 0, 0, 0, NULL, 1546138212, NULL, NULL, NULL, NULL, 'test-2');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_customer`
--

CREATE TABLE `mn_customer` (
  `Id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `fullname` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `methodpay` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `date` int(11) DEFAULT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `agree` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idmanu` int(11) DEFAULT NULL,
  `idprovinces` int(11) DEFAULT NULL,
  `iddistrict` int(11) DEFAULT NULL,
  `tongdonhang` int(11) NOT NULL DEFAULT '0',
  `idgioithieu` int(11) NOT NULL DEFAULT '0',
  `transfee` tinytext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_customer_cart`
--

CREATE TABLE `mn_customer_cart` (
  `idcustomer` int(11) NOT NULL,
  `idpro` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `idmanu` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `deal` int(11) NOT NULL DEFAULT '0',
  `idpercent` int(11) NOT NULL DEFAULT '0',
  `idgioithieu` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_customer_reasoncancel`
--

CREATE TABLE `mn_customer_reasoncancel` (
  `Id` int(11) NOT NULL,
  `idcustomer` int(11) NOT NULL,
  `idreasoncancel` int(11) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_customer_reasoncancel`
--

INSERT INTO `mn_customer_reasoncancel` (`Id`, `idcustomer`, `idreasoncancel`, `content`, `date`) VALUES
(1, 226, 4, '', 1487843868);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_deal`
--

CREATE TABLE `mn_deal` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci NOT NULL,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `idcat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idmanufacturer` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warranty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `images2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `images3` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `images4` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `images5` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `codepro` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `ticnew` int(1) NOT NULL DEFAULT '0',
  `ticlock` int(1) DEFAULT '0',
  `hot` tinyint(1) NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_price` int(11) NOT NULL DEFAULT '0',
  `date` int(11) DEFAULT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kmai` tinyint(1) DEFAULT NULL,
  `kygui` tinyint(4) NOT NULL DEFAULT '0',
  `empty` tinyint(4) NOT NULL DEFAULT '0',
  `view` int(11) DEFAULT NULL,
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `titlepage` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `kichthuoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chatlieu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag` text COLLATE utf8_unicode_ci,
  `linkgoc` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `info_vn` text COLLATE utf8_unicode_ci,
  `trash` tinyint(4) NOT NULL DEFAULT '0',
  `khuyenmai` text COLLATE utf8_unicode_ci,
  `kodau` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `oder` int(11) NOT NULL DEFAULT '0',
  `admin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idpercent` int(11) NOT NULL DEFAULT '0',
  `expired` int(11) DEFAULT NULL,
  `vat` int(11) DEFAULT NULL,
  `deal` int(11) NOT NULL DEFAULT '0',
  `discount_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `code_type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_discount`
--

CREATE TABLE `mn_discount` (
  `id` int(11) NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `voucher` int(11) NOT NULL,
  `created` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `modifield` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_discount`
--

INSERT INTO `mn_discount` (`id`, `code`, `value`, `status`, `user_id`, `prod_id`, `voucher`, `created`, `modifield`) VALUES
(2, 'u2k6LKgk1P', 6, 0, 111, 0, 0, '1469415616', ''),
(3, 'QIhnBoEokp', 5, 0, 112, 0, 1, '1469417188', ''),
(4, 'cR9y71c91d', 5, 0, 113, 0, 1, '1469417487', ''),
(5, 'LkmtCCu5om', 5, 0, 114, 0, 1, '1469417667', ''),
(6, 'oBISv8v8hO', 5, 0, 115, 0, 1, '1469418515', ''),
(7, 'ke1XC4QB9X', 5, 0, 116, 0, 1, '1469418541', ''),
(8, 'eN6iF6uXBF', 4, 0, 117, 0, 1, '1469418563', ''),
(9, 'k0HN9LNM9V', 3, 0, 118, 0, 1, '1469419969', ''),
(10, 'DT1RqNlP0A', 5, 0, 119, 0, 1, '1469420210', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_district`
--

CREATE TABLE `mn_district` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `free` int(1) NOT NULL DEFAULT '0',
  `ticlock` tinyint(4) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `ship` int(11) DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_district`
--

INSERT INTO `mn_district` (`Id`, `title_vn`, `free`, `ticlock`, `sort`, `parentid`, `ship`, `idcat`) VALUES
(1, 'Quận 3', 1, 0, 3, NULL, 10000, 1),
(2, 'Quận 6', 1, 0, 6, NULL, 10000, 1),
(3, 'Quận 1', 1, 0, 1, NULL, 10000, 1),
(4, 'Quận 2', 0, 0, 2, NULL, 18000, 1),
(5, 'Quận 4', 1, 0, 4, NULL, 10000, 1),
(6, 'Quận 5', 1, 0, 5, NULL, 10000, 1),
(7, 'Quận 7', 1, 0, 7, NULL, 10000, 1),
(8, 'Quận 8', 1, 0, 8, NULL, 10000, 1),
(9, 'Quận 9', 0, 0, 9, NULL, 18000, 1),
(10, 'Quận 10', 1, 0, 10, NULL, 10000, 1),
(11, 'Quận 11', 1, 0, 11, NULL, 10000, 1),
(12, 'Quận 12', 1, 0, 12, NULL, 18000, 1),
(13, 'Bình Chánh', 0, 0, 15, NULL, 18000, 1),
(14, 'Bình Tân', 1, 0, 14, NULL, 10000, 1),
(15, 'Bình Thạnh', 1, 0, 13, NULL, 10000, 1),
(16, 'Cần Giờ', 0, 0, 17, NULL, 18000, 1),
(17, 'Củ Chi', 0, 0, 16, NULL, 18000, 1),
(18, 'Gò Vấp', 1, 0, 18, NULL, 10000, 1),
(19, 'Hóc Môn', 0, 0, 19, NULL, 18000, 1),
(20, 'Nhà Bè', 0, 0, 20, NULL, 18000, 1),
(21, 'Phú Nhuận', 1, 0, 21, NULL, 10000, 1),
(22, 'Huyện Cờ Đỏ', 0, 0, 0, NULL, 23000, 5),
(23, 'Quận Ba Đình', 0, 0, 0, NULL, 23000, 3),
(24, 'Quận Bắc Từ Liêm', 0, 0, 0, NULL, 23000, 3),
(25, 'Quận Hoàn Kiếm', 0, 0, 0, NULL, 23000, 3),
(26, 'Quận Hai Bà Trưng', 0, 0, 0, NULL, 23000, 3),
(27, 'Quận Cầu Giấy', 0, 0, 0, NULL, 23000, 3),
(28, 'Quận Thanh Xuân', 0, 0, 0, NULL, 23000, 3),
(29, 'Quận Đống Đa', 0, 0, 0, NULL, 23000, 3),
(30, 'Quận Tây Hồ', 0, 0, 0, NULL, 23000, 3),
(31, 'Quận Hoàng Mai', 0, 0, 0, NULL, 23000, 3),
(32, 'Quận Long Biên', 0, 0, 0, NULL, 23000, 3),
(33, 'Quận Hà Đông', 0, 0, 0, NULL, 23000, 3),
(34, 'Huyện Ba Vì', 0, 0, 0, NULL, 25000, 3),
(35, 'Huyện Đan Phượng', 0, 0, 0, NULL, 25000, 3),
(36, 'Huyện Chương Mỹ', 0, 0, 0, NULL, 25000, 3),
(37, 'Huyện Gia Lâm', 0, 0, 0, NULL, 25000, 3),
(38, 'Huyện Hoài Đức', 0, 0, 0, NULL, 25000, 3),
(39, 'Huyện Mê Linh', 0, 0, 0, NULL, 25000, 3),
(40, 'Huyện Mỹ Đức', 0, 0, 0, NULL, 25000, 3),
(41, 'Huyện Phú Xuyên', 0, 0, 0, NULL, 25000, 3),
(42, 'Huyện Phúc Thọ', 0, 0, 0, NULL, 25000, 3),
(43, 'Huyện Quốc Oai', 0, 0, 0, NULL, 25000, 3),
(44, 'Huyện Sóc Sơn', 0, 0, 0, NULL, 25000, 3),
(45, 'Huyện Thạch Thất', 0, 0, 0, NULL, 25000, 3),
(46, 'Huyện Thanh Oai', 0, 0, 0, NULL, 25000, 3),
(47, 'Huyện Thanh Trì', 0, 0, 0, NULL, 25000, 3),
(48, 'Huyện Thường Tín', 0, 0, 0, NULL, 25000, 3),
(49, 'Quận Nam Từ Liêm', 0, 0, 0, NULL, 23000, 3),
(50, 'Huyện Ứng Hòa', 0, 0, 0, NULL, 25000, 3),
(51, 'Huyện Đông Anh', 0, 0, 0, NULL, 25000, 3),
(52, 'Thành Phố Sơn Tây', 0, 0, 0, NULL, 27000, 3),
(53, 'Huyện Hoàng Sa', 0, 0, 0, NULL, 25000, 4),
(54, 'Huyện Hòa Vang', 0, 0, 0, NULL, 25000, 4),
(55, 'Quận Cẩm Lệ', 0, 0, 0, NULL, 23000, 4),
(56, 'Quận Hải Châu', 0, 0, 0, NULL, 23000, 4),
(57, 'Quận Liên Chiểu', 0, 0, 0, NULL, 23000, 4),
(58, 'Quận Ngũ Hành Sơn', 0, 0, 0, NULL, 23000, 4),
(59, 'Quận Sơn Trà', 0, 0, 0, NULL, 23000, 4),
(60, 'Quận Thanh Khê', 0, 0, 0, NULL, 23000, 4),
(61, 'Thành Phố Đà Nẵng', 0, 0, 0, NULL, 30000, 4),
(62, 'Huyện Phong Điền', 0, 0, 0, NULL, 23000, 5),
(63, 'Huyện Thới Lai', 0, 0, 0, NULL, 23000, 5),
(64, 'Huyện Vĩnh Thạnh', 0, 0, 0, NULL, 23000, 5),
(65, 'Quận Bình Thủy', 0, 0, 0, NULL, 23000, 5),
(66, 'Quận Cái Răng', 0, 0, 0, NULL, 23000, 5),
(67, 'Quận Ninh Kiều', 0, 0, 0, NULL, 23000, 5),
(68, 'Quận Ô Môn', 0, 0, 0, NULL, 23000, 5),
(69, 'Quận Thốt Nốt', 0, 0, 0, NULL, 23000, 5),
(70, 'Quận Dương Kinh', 0, 0, 0, NULL, 25000, 6),
(71, 'Quận Hải An', 0, 0, 0, NULL, 25000, 6),
(72, 'Quận Hồng Bàng', 0, 0, 0, NULL, 25000, 6),
(73, 'Quận Kiến An', 0, 0, 0, NULL, 25000, 6),
(74, 'Quận Lê Chân', 0, 0, 0, NULL, 25000, 6),
(75, 'Quận Ngô Quyền', 0, 0, 0, NULL, 25000, 6),
(76, 'Quận Đồ Sơn', 0, 0, 0, NULL, 25000, 6),
(77, 'Huyện Bạch Long Vĩ', 0, 0, 0, NULL, 25000, 6),
(78, 'Huyện An Dương', 0, 0, 0, NULL, 25000, 6),
(79, 'Huyện An Lão', 0, 0, 0, NULL, 25000, 6),
(80, 'Huyện Cát Hải', 0, 0, 0, NULL, 25000, 6),
(81, 'Huyện Kiến Thụy', 0, 0, 0, NULL, 25000, 6),
(82, 'Huyện Thủy Nguyên', 0, 0, 0, NULL, 25000, 6),
(83, 'Huyện Tiên Lãng', 0, 0, 0, NULL, 25000, 6),
(84, 'Huyện Vĩnh Bảo', 0, 0, 0, NULL, 25000, 6),
(85, 'Huyện Khánh Sơn', 0, 0, 0, NULL, 25000, 7),
(86, 'Huyện Trường Sa', 0, 0, 0, NULL, 25000, 7),
(87, 'Huyện Can Lâm', 0, 0, 0, NULL, 25000, 7),
(88, 'Huyện Diên Khánh', 0, 0, 0, NULL, 25000, 7),
(89, 'Huyện Khánh Vĩnh', 0, 0, 0, NULL, 25000, 7),
(90, 'Huyện Ninh Hòa', 0, 0, 0, NULL, 25000, 7),
(91, 'Huyện Vạn Ninh', 0, 0, 0, NULL, 25000, 7),
(92, 'Thành Phố Cam Ranh', 0, 0, 0, NULL, 25000, 7),
(93, 'Thành Phố Nha Trang', 0, 0, 0, NULL, 25000, 7),
(94, 'Huyện Châu Đức', 0, 0, 0, NULL, 23000, 8),
(95, 'Huyện Côn Đảo', 0, 0, 0, NULL, 23000, 8),
(96, 'Huyện Long Điền', 0, 0, 0, NULL, 23000, 8),
(97, 'Huyện Tân Thành', 0, 0, 0, NULL, 23000, 8),
(98, 'Huyện Xuyên Mộc', 0, 0, 0, NULL, 23000, 8),
(99, 'Huyện Đất Đỏ', 0, 0, 0, NULL, 23000, 8),
(100, 'Thành Phố Bà Rịa', 0, 0, 0, NULL, 23000, 8),
(102, 'Thành Phố Vũng Tàu', 0, 0, 0, NULL, 23000, 8),
(104, 'Huyện Cẩm Mỹ', 0, 0, 0, NULL, 23000, 9),
(105, 'Huyện Long Thành', 0, 0, 0, NULL, 23000, 9),
(106, 'Huyện Nhơn Trạch', 0, 0, 0, NULL, 23000, 9),
(107, 'Huyện Tân Phú', 0, 0, 0, NULL, 23000, 9),
(108, 'Huyện Thống Nhất', 0, 0, 0, NULL, 23000, 9),
(109, 'Huyện Trảng Bom', 0, 0, 0, NULL, 23000, 9),
(110, 'Huyện Vĩnh Cửu', 0, 0, 0, NULL, 23000, 9),
(111, 'Huyện Xuân Lộc', 0, 0, 0, NULL, 23000, 9),
(112, 'Huyện Định Quán', 0, 0, 0, NULL, 23000, 9),
(113, 'Thành Phố Biên Hòa', 0, 0, 0, NULL, 23000, 9),
(114, 'Thị Xã Long Khánh', 0, 0, 0, NULL, 23000, 9),
(115, 'Huyện Đăk Nông', 0, 0, 0, NULL, 23000, 10),
(116, 'Thành Phố Buôn Ma Thuột', 0, 0, 0, NULL, 23000, 10),
(117, 'Thị Xã Buôn Hồ', 0, 0, 0, NULL, 23000, 10),
(118, 'Huyện Cư Jút', 0, 0, 0, NULL, 23000, 10),
(119, 'Huyện Đak Rlấp', 0, 0, 0, NULL, 23000, 10),
(120, 'Huyện Krông Nô', 0, 0, 0, NULL, 23000, 10),
(121, 'Huyện Krông Pắc', 0, 0, 0, NULL, 23000, 10),
(122, 'Huyện Buôn Đôn', 0, 0, 0, NULL, 23000, 10),
(123, 'Huyện Cư M\'gar', 0, 0, 0, NULL, 23000, 10),
(124, 'Huyện Đak Mil', 0, 0, 0, NULL, 23000, 10),
(125, 'Huyện Ea Kar', 0, 0, 0, NULL, 23000, 10),
(126, 'Huyện Ea H\'leo', 0, 0, 0, NULL, 23000, 10),
(127, 'Huyện Ea Súp', 0, 0, 0, NULL, 23000, 10),
(128, 'Huyện Krông Ana', 0, 0, 0, NULL, 23000, 10),
(129, 'Huyện Krông Bông', 0, 0, 0, NULL, 23000, 10),
(130, 'Huyện Krông Buk', 0, 0, 0, NULL, 23000, 10),
(131, 'Huyện Krông Năng', 0, 0, 0, NULL, 23000, 10),
(132, 'Huyện Lak', 0, 0, 0, NULL, 23000, 10),
(133, 'Huyện M\'đrak', 0, 0, 0, NULL, 23000, 10),
(134, 'Huyện An Phú', 0, 0, 0, NULL, 23000, 11),
(135, 'Huyện Châu Phú', 0, 0, 0, NULL, 23000, 11),
(136, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 11),
(137, 'Huyện Chợ Mới', 0, 0, 0, NULL, 23000, 11),
(138, 'Huyện Phú Tân', 0, 0, 0, NULL, 23000, 11),
(139, 'Huyện Tân Châu', 0, 0, 0, NULL, 23000, 11),
(140, 'Huyện Thoại Sơn', 0, 0, 0, NULL, 23000, 11),
(141, 'Huyện Tịnh Biên', 0, 0, 0, NULL, 23000, 11),
(142, 'Huyện Tri Tôn', 0, 0, 0, NULL, 23000, 11),
(143, 'Thành Phố Long Xuyên', 0, 0, 0, NULL, 23000, 11),
(144, 'Thị Xã Châu Đốc', 0, 0, 0, NULL, 23000, 11),
(145, 'Huyện Lạng Giang', 0, 0, 0, NULL, 25000, 12),
(146, 'Huyện Lục Ngạn', 0, 0, 0, NULL, 25000, 12),
(147, 'Huyện Yên Dũng', 0, 0, 0, NULL, 25000, 12),
(148, 'Huyện Hiệp Hòa', 0, 0, 0, NULL, 25000, 12),
(149, 'Huyện Lục Nam', 0, 0, 0, NULL, 25000, 12),
(150, 'Huyện Sơn Động', 0, 0, 0, NULL, 25000, 12),
(151, 'Huyện Tân Uyên', 0, 0, 0, NULL, 25000, 12),
(152, 'Huyện Việt Yên', 0, 0, 0, NULL, 25000, 12),
(153, 'Thành Phố Bắc Giang', 0, 0, 0, NULL, 25000, 12),
(154, 'Huyện Yên Thế', 0, 0, 0, NULL, 25000, 12),
(155, 'Huyện Bạch Thông', 0, 0, 0, NULL, 28000, 13),
(156, 'Huyện Ba Bể', 0, 0, 0, NULL, 28000, 13),
(157, 'Huyện Chợ Mới', 0, 0, 0, NULL, 28000, 13),
(158, 'Huyện Chợ Đồn', 0, 0, 0, NULL, 28000, 13),
(159, 'Huyện Na Rì', 0, 0, 0, NULL, 28000, 13),
(160, 'Huyện Ngân Sơn', 0, 0, 0, NULL, 28000, 13),
(161, 'Huyện Pác Nặm', 0, 0, 0, NULL, 28000, 13),
(163, 'Thị Xã Bắc Kạn', 0, 0, 0, NULL, 28000, 13),
(164, 'Huyện Giá Rai', 0, 0, 0, NULL, 23000, 14),
(165, 'Huyện Hòa Bình', 0, 0, 0, NULL, 23000, 14),
(166, 'Huyện Hồng Dân', 0, 0, 0, NULL, 23000, 14),
(167, 'Huyện Phước Long', 0, 0, 0, NULL, 23000, 14),
(168, 'Huyện Vĩnh Lợi', 0, 0, 0, NULL, 23000, 14),
(169, 'Huyện Đông Hải', 0, 0, 0, NULL, 23000, 14),
(170, 'Thành Phố Bạc Liêu', 0, 0, 0, NULL, 23000, 14),
(171, 'Huyện Bến Cát', 0, 0, 0, NULL, 23000, 15),
(172, 'Huyện Dầu Tiếng', 0, 0, 0, NULL, 23000, 15),
(173, 'Huyện Phú Giáo', 0, 0, 0, NULL, 23000, 15),
(174, 'Huyện Tân Uyên', 0, 0, 0, NULL, 23000, 15),
(175, 'Thành Phố Thủ Dầu Một', 0, 0, 0, NULL, 23000, 15),
(176, 'Thị Xã Dĩ An', 0, 0, 0, NULL, 23000, 15),
(177, 'Thị Xã Thuận An', 0, 0, 0, NULL, 23000, 15),
(178, 'Thị Xã Bình Long', 0, 0, 0, NULL, 23000, 16),
(179, 'Huyện Bù Gia Mập', 0, 0, 0, NULL, 23000, 16),
(180, 'Huyện Bù Đăng', 0, 0, 0, NULL, 23000, 16),
(181, 'Huyện Bù Đốp', 0, 0, 0, NULL, 23000, 16),
(182, 'Huyện Chơn Thành', 0, 0, 0, NULL, 23000, 16),
(183, 'Huyện Hớn Quản', 0, 0, 0, NULL, 23000, 16),
(184, 'Huyện Lộc Ninh', 0, 0, 0, NULL, 23000, 16),
(185, 'Huyện Đồng Phú', 0, 0, 0, NULL, 23000, 16),
(186, 'Thị Xã Phước Long', 0, 0, 0, NULL, 23000, 16),
(187, 'Thị Xã Đồng Xoài', 0, 0, 0, NULL, 23000, 16),
(188, 'Huyện Bắc Bình', 0, 0, 0, NULL, 23000, 17),
(189, 'Huyện Hàm Tân', 0, 0, 0, NULL, 23000, 17),
(190, 'Huyện Hàm Thuận Bắc', 0, 0, 0, NULL, 23000, 17),
(191, 'Huyện Hàm Thuận Nam', 0, 0, 0, NULL, 23000, 17),
(192, 'Huyện Phú Quý', 0, 0, 0, NULL, 23000, 17),
(193, 'Huyện Tánh Linh', 0, 0, 0, NULL, 23000, 17),
(194, 'Huyện Tuy Phong', 0, 0, 0, NULL, 23000, 17),
(195, 'Huyện Đức Linh', 0, 0, 0, NULL, 23000, 17),
(196, 'Thị Xã La Gi', 0, 0, 0, NULL, 23000, 17),
(197, 'Thành Phố Phan Thiết', 0, 0, 0, NULL, 23000, 17),
(198, 'Huyện Phú Mỹ', 0, 0, 0, NULL, 25000, 18),
(199, 'Huyện Tây Sơn', 0, 0, 0, NULL, 25000, 18),
(200, 'Huyện An Lão', 0, 0, 0, NULL, 25000, 18),
(201, 'Huyện Hoài Ân', 0, 0, 0, NULL, 25000, 18),
(202, 'Huyện Hoài Nhơn', 0, 0, 0, NULL, 25000, 18),
(203, 'Huyện Phù Cát', 0, 0, 0, NULL, 25000, 18),
(204, 'Huyện Tuy Phước', 0, 0, 0, NULL, 25000, 18),
(205, 'Huyện Vân Canh', 0, 0, 0, NULL, 25000, 18),
(206, 'Huyện Vĩnh Thạnh', 0, 0, 0, NULL, 25000, 18),
(207, 'Thành Phố Quy Nhơn', 0, 0, 0, NULL, 25000, 18),
(208, 'Thị Xã An Nhơn', 0, 0, 0, NULL, 25000, 18),
(209, 'Huyện Thuận Thành', 0, 0, 0, NULL, 25000, 19),
(211, 'Huyện Gia Bình', 0, 0, 0, NULL, 25000, 19),
(212, 'Huyện Lương Tài', 0, 0, 0, NULL, 25000, 19),
(213, 'Huyện Quế Võ', 0, 0, 0, NULL, 25000, 19),
(214, 'Huyện Tiên Du', 0, 0, 0, NULL, 25000, 19),
(215, 'Huyện Yên Phong', 0, 0, 0, NULL, 25000, 19),
(216, 'Thành Phố Bắc Ninh', 0, 0, 0, NULL, 25000, 19),
(217, 'Thị Xã Từ Sơn', 0, 0, 0, NULL, 25000, 19),
(218, 'Huyện Cái Ninh', 0, 0, 0, NULL, 23000, 20),
(219, 'Huyện Năm Căn', 0, 0, 0, NULL, 23000, 20),
(220, 'Huyện Ngọc Hiển', 0, 0, 0, NULL, 23000, 20),
(221, 'Huyện Phú Tân', 0, 0, 0, NULL, 23000, 20),
(222, 'Huyện Thới Bình', 0, 0, 0, NULL, 23000, 20),
(223, 'Huyện Trần Văn Thời', 0, 0, 0, NULL, 23000, 20),
(224, 'Huyện U Minh', 0, 0, 0, NULL, 23000, 20),
(225, 'Huyện Đầm Dơi', 0, 0, 0, NULL, 23000, 20),
(226, 'Thành Phố Cà Mau', 0, 0, 0, NULL, 23000, 20),
(227, 'Huyện Hòa An', 0, 0, 0, NULL, 28000, 21),
(228, 'Huyện Phục Hòa', 0, 0, 0, NULL, 28000, 21),
(229, 'Huyện Bảo Lạc', 0, 0, 0, NULL, 28000, 21),
(230, 'Huyện Bảo Lâm', 0, 0, 0, NULL, 28000, 21),
(231, 'Huyện Hạ Lang', 0, 0, 0, NULL, 28000, 21),
(232, 'Huyện Hà Quảng', 0, 0, 0, NULL, 28000, 21),
(233, 'Huyện Nguyên Bình', 0, 0, 0, NULL, 28000, 21),
(234, 'Huyện Quảng Uyên', 0, 0, 0, NULL, 28000, 21),
(235, 'Huyện Thạch An', 0, 0, 0, NULL, 28000, 21),
(236, 'Huyện Thông Nông', 0, 0, 0, NULL, 28000, 21),
(237, 'Huyện Trà Lĩnh', 0, 0, 0, NULL, 28000, 21),
(238, 'Huyện Trùng Khánh', 0, 0, 0, NULL, 28000, 21),
(239, 'Thị Xã Cao Bằng', 0, 0, 0, NULL, 28000, 21),
(240, 'Huyện Ba Tri', 0, 0, 0, NULL, 23000, 22),
(241, 'Huyện Bình Đại', 0, 0, 0, NULL, 23000, 22),
(242, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 22),
(243, 'Huyện Chợ Lách', 0, 0, 0, NULL, 23000, 22),
(244, 'Huyện Giồng Trôm', 0, 0, 0, NULL, 23000, 22),
(245, 'Huyện Mỏ Cày Nam', 0, 0, 0, NULL, 23000, 22),
(246, 'Huyên Thạnh Phú', 0, 0, 0, NULL, 23000, 22),
(247, 'Thành Phố Bến Tre', 0, 0, 0, NULL, 23000, 22),
(248, 'Huyện Cư Jút', 0, 0, 0, NULL, 23000, 23),
(249, 'Huyện Krông Nô', 0, 0, 0, NULL, 23000, 23),
(250, 'Huyện Tuy Đức', 0, 0, 0, NULL, 23000, 23),
(251, 'Huyện Đắk Glong', 0, 0, 0, NULL, 23000, 23),
(252, 'Huyện Đak Mil', 0, 0, 0, NULL, 23000, 23),
(253, 'Huyện Đăk Rlấp', 0, 0, 0, NULL, 23000, 23),
(254, 'Huyện Đăk Song', 0, 0, 0, NULL, 23000, 23),
(255, 'Thị Xã Gia Nghĩa', 0, 0, 0, NULL, 23000, 23),
(256, 'Huyện Điện Biên', 0, 0, 0, NULL, 28000, 24),
(257, 'Huyện Mường Ảng', 0, 0, 0, NULL, 28000, 24),
(258, 'Huyện Mường Chà', 0, 0, 0, NULL, 28000, 24),
(259, 'Huyện Mường Nhé', 0, 0, 0, NULL, 28000, 24),
(260, 'Huyện Nầm Pồ', 0, 0, 0, NULL, 28000, 24),
(261, 'Huyện Tủa Chùa', 0, 0, 0, NULL, 28000, 24),
(262, 'Huyện Tuần Giáo', 0, 0, 0, NULL, 28000, 24),
(263, 'Huyện Điện Biên Đông', 0, 0, 0, NULL, 28000, 24),
(264, 'Thành Phố Điện Biên', 0, 0, 0, NULL, 28000, 24),
(265, 'Thị Xã Mường Lay', 0, 0, 0, NULL, 28000, 24),
(266, 'Thị Xã Hồng Ngự', 0, 0, 0, NULL, 23000, 25),
(267, 'Huyện Cao Lãnh', 0, 0, 0, NULL, 23000, 25),
(268, 'Huyện Thuận Thành', 0, 0, 0, NULL, 23000, 25),
(269, 'Huyện Lai Vung', 0, 0, 0, NULL, 23000, 25),
(270, 'Huyện Lấp Vò', 0, 0, 0, NULL, 23000, 25),
(271, 'Huyện Tam Nông', 0, 0, 0, NULL, 23000, 25),
(272, 'Huyện Tân Hồng', 0, 0, 0, NULL, 23000, 25),
(273, 'Huyện Thanh Bình', 0, 0, 0, NULL, 23000, 25),
(274, 'Huyện Tháp Mười', 0, 0, 0, NULL, 23000, 25),
(275, 'Thành Phố Cao Lãnh', 0, 0, 0, NULL, 23000, 25),
(276, 'Thị Xã Sa Đéc', 0, 0, 0, NULL, 23000, 25),
(277, 'Huyện Hồng Ngự', 0, 0, 0, NULL, 23000, 25),
(278, 'Huyện Chư Păh', 0, 0, 0, NULL, 25000, 26),
(279, 'Huyện Chư Prông', 0, 0, 0, NULL, 25000, 26),
(280, 'Huyện Chư Pưh', 0, 0, 0, NULL, 25000, 26),
(281, 'Huyện Chư Sê', 0, 0, 0, NULL, 25000, 26),
(282, 'Huyện La Grai', 0, 0, 0, NULL, 25000, 26),
(283, 'Huyện La Pa', 0, 0, 0, NULL, 25000, 26),
(284, 'Huyện Kbang', 0, 0, 0, NULL, 25000, 26),
(285, 'Huyện Kông Chro', 0, 0, 0, NULL, 25000, 26),
(286, 'Huyện Krông Pa', 0, 0, 0, NULL, 25000, 26),
(287, 'Huyện Mang Yang', 0, 0, 0, NULL, 25000, 26),
(288, 'Huyện Phú Thiện', 0, 0, 0, NULL, 25000, 26),
(289, 'Huyện Đak Pơ', 0, 0, 0, NULL, 25000, 26),
(290, 'Huyện Đăk Đoa', 0, 0, 0, NULL, 25000, 26),
(291, 'Huyện Đức Cơ', 0, 0, 0, NULL, 25000, 26),
(292, 'Thành Phố Pleiku', 0, 0, 0, NULL, 25000, 26),
(293, 'Thị Xã An Khê', 0, 0, 0, NULL, 25000, 26),
(294, 'Thị Xã Ayun Pa', 0, 0, 0, NULL, 25000, 26),
(295, 'Huyện Bắc Mê', 0, 0, 0, NULL, 28000, 27),
(296, 'Huyện Bắc Quang', 0, 0, 0, NULL, 28000, 27),
(297, 'Huyện Hoàng Su Phì', 0, 0, 0, NULL, 28000, 27),
(298, 'Huyện Mèo Vạc', 0, 0, 0, NULL, 28000, 27),
(299, 'Huyện Quản Bạ', 0, 0, 0, NULL, 28000, 27),
(300, 'Huyện Quang Bình', 0, 0, 0, NULL, 28000, 27),
(301, 'Huyện Vị Xuyên', 0, 0, 0, NULL, 28000, 27),
(302, 'Huyện Xín Mần', 0, 0, 0, NULL, 28000, 27),
(303, 'Huyện Yên Minh', 0, 0, 0, NULL, 28000, 27),
(304, 'Huyện Đồng Văn', 0, 0, 0, NULL, 28000, 27),
(305, 'Thành Phố Hà Giang', 0, 0, 0, NULL, 28000, 27),
(306, 'Huyện Bình Lục', 0, 0, 0, NULL, 25000, 28),
(307, 'Huyện Duy Tiên', 0, 0, 0, NULL, 25000, 28),
(308, 'Huyện Kim Bảng', 0, 0, 0, NULL, 25000, 28),
(309, 'Huyện Lý Nhân', 0, 0, 0, NULL, 25000, 28),
(310, 'Huyện Thanh Liêm', 0, 0, 0, NULL, 25000, 28),
(311, 'Thành Phố Phủ Lý', 0, 0, 0, NULL, 25000, 28),
(312, 'Huyện Cẩm Xuyên', 0, 0, 0, NULL, 25000, 29),
(313, 'Huyện Vũ Quang', 0, 0, 0, NULL, 25000, 29),
(314, 'Huyện Can Lộc', 0, 0, 0, NULL, 25000, 29),
(315, 'Huyện Hương Khê', 0, 0, 0, NULL, 25000, 29),
(316, 'Huyện Hương Sơn', 0, 0, 0, NULL, 25000, 29),
(317, 'Huyện Kỳ Anh', 0, 0, 0, NULL, 25000, 29),
(318, 'Huyện Lộc Hà', 0, 0, 0, NULL, 25000, 29),
(319, 'Huyện Nghi Xuân', 0, 0, 0, NULL, 25000, 29),
(320, 'Huyện Thạch Hà', 0, 0, 0, NULL, 25000, 29),
(321, 'Huyện Đức Thọ', 0, 0, 0, NULL, 25000, 29),
(322, 'Thành Phố Hà Tĩnh', 0, 0, 0, NULL, 25000, 29),
(323, 'Thị Xã Hồng Lĩnh', 0, 0, 0, NULL, 25000, 29),
(324, 'Huyện Bình Giang', 0, 0, 0, NULL, 25000, 30),
(325, 'Huyện Gia Lộc', 0, 0, 0, NULL, 25000, 30),
(326, 'Huyện Cẩm Giàng', 0, 0, 0, NULL, 25000, 30),
(327, 'Huyện Kim Thành', 0, 0, 0, NULL, 25000, 30),
(328, 'Huyện Kim Môn', 0, 0, 0, NULL, 25000, 30),
(329, 'Huyện Nam Sách', 0, 0, 0, NULL, 25000, 30),
(330, 'Huyện Ninh Giang', 0, 0, 0, NULL, 25000, 30),
(331, 'Huyện Thanh Hà', 0, 0, 0, NULL, 25000, 30),
(332, 'Huyện Thanh Miện', 0, 0, 0, NULL, 25000, 30),
(333, 'Huyện Tứ Kỳ', 0, 0, 0, NULL, 25000, 30),
(334, 'Thành Phố Hải Dương', 0, 0, 0, NULL, 25000, 30),
(335, 'Thị Xã Chí Linh', 0, 0, 0, NULL, 25000, 30),
(336, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 31),
(337, 'Huyện Châu Thành A', 0, 0, 0, NULL, 23000, 31),
(338, 'Huyện Long Mỹ', 0, 0, 0, NULL, 23000, 31),
(339, 'Huyện Phụng Hiệp', 0, 0, 0, NULL, 23000, 31),
(340, 'Huyện Vị Thủy', 0, 0, 0, NULL, 23000, 31),
(341, 'Thành Phố Vị Thanh', 0, 0, 0, NULL, 23000, 31),
(342, 'Thị Xã Ngã Bảy', 0, 0, 0, NULL, 23000, 31),
(343, 'Huyện Cao Phong', 0, 0, 0, NULL, 25000, 32),
(344, 'Huyện Kim Bôi', 0, 0, 0, NULL, 25000, 32),
(345, 'Huyện Kỳ Sơn', 0, 0, 0, NULL, 25000, 32),
(346, 'Huyện Lạc Sơn', 0, 0, 0, NULL, 25000, 32),
(347, 'Huyện Lạc Thủy', 0, 0, 0, NULL, 25000, 32),
(348, 'Huyện Lương Sơn', 0, 0, 0, NULL, 25000, 32),
(349, 'Huyện Mai Châu', 0, 0, 0, NULL, 25000, 32),
(350, 'Huyện Tân Lạc', 0, 0, 0, NULL, 25000, 32),
(351, 'Huyện Yên Thủy', 0, 0, 0, NULL, 25000, 32),
(352, 'Huyện Đà Bắc', 0, 0, 0, NULL, 25000, 32),
(353, 'Thành Phố Hòa Bình', 0, 0, 0, NULL, 25000, 32),
(354, 'Huyện Phù Cừ', 0, 0, 0, NULL, 25000, 33),
(355, 'Huyện Ân Thi', 0, 0, 0, NULL, 25000, 33),
(356, 'Huyện Khoái Châu', 0, 0, 0, NULL, 25000, 33),
(357, 'Huyện Kim Động', 0, 0, 0, NULL, 25000, 33),
(358, 'Huyện Mỹ Hào', 0, 0, 0, NULL, 25000, 33),
(359, 'Huyện Tiên Lữ', 0, 0, 0, NULL, 25000, 33),
(360, 'Huyện Văn Giang', 0, 0, 0, NULL, 25000, 33),
(361, 'Huyện Văn Lâm', 0, 0, 0, NULL, 25000, 33),
(362, 'Huyện Yên Mỹ', 0, 0, 0, NULL, 25000, 33),
(363, 'Thành Phố Hưng Yên', 0, 0, 0, NULL, 25000, 33),
(364, 'Huyện Giồng Riềng', 0, 0, 0, NULL, 23000, 34),
(365, 'Huyện Gò Quao', 0, 0, 0, NULL, 23000, 34),
(366, 'Huyện An Biên', 0, 0, 0, NULL, 23000, 34),
(367, 'Huyện An Minh', 0, 0, 0, NULL, 23000, 34),
(368, 'Huyện Châu Thành', 0, 0, 0, NULL, 25000, 12),
(369, 'Huyện Giang Thành', 0, 0, 0, NULL, 23000, 34),
(370, 'Huyện Hòn Đất', 0, 0, 0, NULL, 23000, 34),
(371, 'Huyện Kiên Hải', 0, 0, 0, NULL, 23000, 34),
(372, 'Huyện Kiên Lương', 0, 0, 0, NULL, 23000, 34),
(373, 'Huyện Phú Quốc', 0, 0, 0, NULL, 23000, 34),
(374, 'Huyện Tân Hiệp', 0, 0, 0, NULL, 23000, 34),
(375, 'Huyện U Minh Thượng', 0, 0, 0, NULL, 23000, 34),
(376, 'Huyện Vĩnh Thuận', 0, 0, 0, NULL, 23000, 34),
(377, 'Thành Phố Rạch Giá', 0, 0, 0, NULL, 23000, 34),
(378, 'Thị Xã Hà Tiên', 0, 0, 0, NULL, 23000, 34),
(379, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 34),
(380, 'Huyện Kon Rẩy', 0, 0, 0, NULL, 25000, 35),
(381, 'Huyện Kon Plong', 0, 0, 0, NULL, 25000, 35),
(382, 'Huyện Ngọc Hồi', 0, 0, 0, NULL, 25000, 35),
(383, 'Huyện Sa Thầy', 0, 0, 0, NULL, 25000, 35),
(384, 'Huyện Tu Mơ Rông', 0, 0, 0, NULL, 25000, 35),
(385, 'Huyện Đak Glêi', 0, 0, 0, NULL, 25000, 35),
(386, 'Huyện Đak Hà', 0, 0, 0, NULL, 25000, 35),
(387, 'Huyện Đak Tô', 0, 0, 0, NULL, 25000, 35),
(388, 'Thành Phố Kon Tum', 0, 0, 0, NULL, 25000, 35),
(389, 'Huyện Nậm Nhùn', 0, 0, 0, NULL, 28000, 36),
(390, 'Huyện Mường Tè', 0, 0, 0, NULL, 28000, 36),
(391, 'Huyện Phong Thổ', 0, 0, 0, NULL, 28000, 36),
(392, 'Huyện Sìn Hồ', 0, 0, 0, NULL, 28000, 36),
(393, 'Huyện Tam Đường', 0, 0, 0, NULL, 28000, 36),
(394, 'Huyện Tân Uyên', 0, 0, 0, NULL, 28000, 36),
(395, 'Huyện Than Uyên', 0, 0, 0, NULL, 28000, 36),
(396, 'Thị Xã Lai Châu', 0, 0, 0, NULL, 28000, 36),
(397, 'Huyện Cát Tiên', 0, 0, 0, NULL, 23000, 37),
(398, 'Huyện Bảo Lâm', 0, 0, 0, NULL, 23000, 37),
(399, 'Huyện Di Linh', 0, 0, 0, NULL, 23000, 37),
(400, 'Huyện Lạc Dương', 0, 0, 0, NULL, 23000, 37),
(401, 'Huyện Lâm Hà', 0, 0, 0, NULL, 23000, 37),
(402, 'Huyện Đạ Huoai', 0, 0, 0, NULL, 23000, 37),
(403, 'Huyện Đạ Tẻh', 0, 0, 0, NULL, 23000, 37),
(404, 'Huyện Đam Rông', 0, 0, 0, NULL, 23000, 37),
(405, 'Huyện Đơn Dương', 0, 0, 0, NULL, 23000, 37),
(406, 'Huyện Đức Trọng', 0, 0, 0, NULL, 23000, 37),
(407, 'Thành Phố Bảo Lộc', 0, 0, 0, NULL, 23000, 37),
(408, 'Thành Phố Đà Lạt', 0, 0, 0, NULL, 23000, 37),
(409, 'Huyện Chi Lăng', 0, 0, 0, NULL, 25000, 38),
(410, 'Huyện Tràng Định', 0, 0, 0, NULL, 25000, 38),
(411, 'Huyện Đình Lập', 0, 0, 0, NULL, 25000, 38),
(412, 'Huyện Bắc Sơn', 0, 0, 0, NULL, 25000, 38),
(413, 'Huyện Bình Gia', 0, 0, 0, NULL, 25000, 38),
(414, 'Huyện Cao Lộc', 0, 0, 0, NULL, 25000, 38),
(415, 'Huyện Hữu Lũng', 0, 0, 0, NULL, 25000, 38),
(416, 'Huyện Lộc Bình', 0, 0, 0, NULL, 25000, 38),
(417, 'Huyện Văn Lãng', 0, 0, 0, NULL, 25000, 38),
(418, 'Huyện Văn Quan', 0, 0, 0, NULL, 25000, 38),
(419, 'Thành Phố Lạng Sơn', 0, 0, 0, NULL, 25000, 38),
(420, 'Huyện Bắc Hà', 0, 0, 0, NULL, 28000, 39),
(421, 'Huyện Bảo Thắng', 0, 0, 0, NULL, 28000, 39),
(422, 'Huyện Bảo Yên', 0, 0, 0, NULL, 28000, 39),
(423, 'Huyện Bát Xát', 0, 0, 0, NULL, 28000, 39),
(424, 'Huyện Mường Khương', 0, 0, 0, NULL, 28000, 39),
(425, 'Huyện Sa Pa', 0, 0, 0, NULL, 28000, 39),
(426, 'Huyện Si Ma Cai', 0, 0, 0, NULL, 28000, 39),
(427, 'Huyện Văn Bàn', 0, 0, 0, NULL, 28000, 39),
(428, 'Thành Phố Lào Cai', 0, 0, 0, NULL, 28000, 39),
(429, 'Huyện Bến Lức', 0, 0, 0, NULL, 23000, 40),
(430, 'Huyện Cần Giuộc', 0, 0, 0, NULL, 23000, 40),
(431, 'Huyện Cần Đước', 0, 0, 0, NULL, 23000, 40),
(432, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 40),
(433, 'Huyện Mộc Hóa', 0, 0, 0, NULL, 23000, 40),
(434, 'Huyện Tân Hưng', 0, 0, 0, NULL, 23000, 40),
(435, 'Huyện Tân Thạnh', 0, 0, 0, NULL, 23000, 40),
(436, 'Huyện Tân Trụ', 0, 0, 0, NULL, 23000, 40),
(437, 'Huyện Thạnh Hóa', 0, 0, 0, NULL, 23000, 40),
(438, 'Huyện Thủ Thừa', 0, 0, 0, NULL, 23000, 40),
(439, 'Huyện Vĩnh Hưng', 0, 0, 0, NULL, 23000, 40),
(440, 'Huyện Đức Hòa', 0, 0, 0, NULL, 23000, 40),
(441, 'Huyện Đức Huệ', 0, 0, 0, NULL, 23000, 40),
(442, 'Thành Phố Tân An', 0, 0, 0, NULL, 23000, 40),
(443, 'Huyện Nam Trực', 0, 0, 0, NULL, 25000, 41),
(444, 'Huyện Vụ Bản', 0, 0, 0, NULL, 25000, 41),
(445, 'Huyện Giao Thủy', 0, 0, 0, NULL, 25000, 41),
(446, 'Huyện Hải Hậu', 0, 0, 0, NULL, 25000, 41),
(447, 'Huyện Mỹ Lộc', 0, 0, 0, NULL, 25000, 41),
(448, 'Huyện Nghĩa Hưng', 0, 0, 0, NULL, 25000, 41),
(449, 'Huyện Trực Ninh', 0, 0, 0, NULL, 25000, 41),
(450, 'Huyện Xuân Trường', 0, 0, 0, NULL, 25000, 41),
(451, 'Huyện Ý Yên', 0, 0, 0, NULL, 25000, 41),
(452, 'Thành Phố Nam Định', 0, 0, 0, NULL, 25000, 41),
(453, 'Huyện Anh Sơn', 0, 0, 0, NULL, 25000, 42),
(454, 'Huyện Con Cuông', 0, 0, 0, NULL, 25000, 42),
(455, 'Huyện Diễn Châu', 0, 0, 0, NULL, 25000, 42),
(456, 'Huyện Hưng Nguyên', 0, 0, 0, NULL, 25000, 42),
(457, 'Huyện Kỳ Sơn', 0, 0, 0, NULL, 25000, 42),
(458, 'Huyện Nam Đàn', 0, 0, 0, NULL, 25000, 42),
(459, 'Huyện Nghi Lộc', 0, 0, 0, NULL, 25000, 42),
(460, 'Huyện Nghĩa Đàn', 0, 0, 0, NULL, 25000, 42),
(461, 'Huyện Quế Phong', 0, 0, 0, NULL, 25000, 42),
(462, 'Huyện Quỳ Châu', 0, 0, 0, NULL, 25000, 42),
(463, 'Huyện Quỳ Hợp', 0, 0, 0, NULL, 25000, 42),
(464, 'Huyện Quỳnh Lưu', 0, 0, 0, NULL, 25000, 42),
(465, 'Huyện Tân Kỳ', 0, 0, 0, NULL, 25000, 42),
(466, 'Huyện Thành Chương', 0, 0, 0, NULL, 25000, 42),
(467, 'Huyện Tương Dương', 0, 0, 0, NULL, 25000, 42),
(468, 'Huyện Yên Thành', 0, 0, 0, NULL, 25000, 42),
(469, 'Huyện Đô Lương', 0, 0, 0, NULL, 25000, 42),
(470, 'Thành Phố Vinh', 0, 0, 0, NULL, 25000, 42),
(471, 'Thị Xã Cửa Lò', 0, 0, 0, NULL, 25000, 42),
(472, 'Thị Xã Thái Hòa', 0, 0, 0, NULL, 25000, 42),
(473, 'Huyện Gia Viễn', 0, 0, 0, NULL, 25000, 43),
(474, 'Huyện Hoa Lư', 0, 0, 0, NULL, 25000, 43),
(475, 'Huyện Kim Sơn', 0, 0, 0, NULL, 25000, 43),
(476, 'Huyện Nho Quan', 0, 0, 0, NULL, 25000, 43),
(477, 'Huyện Yên Khánh', 0, 0, 0, NULL, 25000, 43),
(478, 'Huyện Yên Mô', 0, 0, 0, NULL, 25000, 43),
(479, 'Thành Phố Ninh Bình', 0, 0, 0, NULL, 25000, 43),
(480, 'Thị Xã Tam Điệp', 0, 0, 0, NULL, 25000, 43),
(481, 'Huyện Bác Ái', 0, 0, 0, NULL, 23000, 44),
(482, 'Huyện Ninh Hải', 0, 0, 0, NULL, 23000, 44),
(483, 'Huyện Ninh Phước', 0, 0, 0, NULL, 23000, 44),
(484, 'Huyện Ninh Sơn', 0, 0, 0, NULL, 23000, 44),
(485, 'Huyện Thuận Bắc', 0, 0, 0, NULL, 23000, 44),
(486, 'Huyện Thuận Nam', 0, 0, 0, NULL, 23000, 44),
(487, 'Thành Phố Phan Rang', 0, 0, 0, NULL, 23000, 44),
(488, 'Huyện Cẩm Khê', 0, 0, 0, NULL, 25000, 45),
(489, 'Huyện Hạ Hòa', 0, 0, 0, NULL, 25000, 45),
(490, 'Huyện Lâm Thao', 0, 0, 0, NULL, 25000, 45),
(491, 'Huyện Phù Ninh', 0, 0, 0, NULL, 25000, 45),
(492, 'Huyện Tam Nông', 0, 0, 0, NULL, 25000, 45),
(493, 'Huyên Tân Sơn', 0, 0, 0, NULL, 25000, 45),
(494, 'Huyện Thanh Ba', 0, 0, 0, NULL, 25000, 45),
(495, 'Huyện Thanh Sơn', 0, 0, 0, NULL, 25000, 45),
(496, 'Huyện Thanh Thủy', 0, 0, 0, NULL, 25000, 45),
(497, 'Huyện Yên Lập', 0, 0, 0, NULL, 25000, 45),
(498, 'Huyện Đoan Hùng', 0, 0, 0, NULL, 25000, 45),
(499, 'Thành Phố Việt Trì', 0, 0, 0, NULL, 25000, 45),
(500, 'Thị Xã Phú Thọ', 0, 0, 0, NULL, 25000, 45),
(501, 'Huyện Phú Hòa', 0, 0, 0, NULL, 25000, 46),
(502, 'Huyện Sơn Hòa', 0, 0, 0, NULL, 25000, 46),
(503, 'Huyện Sông Cầu', 0, 0, 0, NULL, 25000, 46),
(504, 'Huyện Sông Hinh', 0, 0, 0, NULL, 25000, 46),
(505, 'Huyện Tây Hòa', 0, 0, 0, NULL, 25000, 46),
(506, 'Huyện Tuy An', 0, 0, 0, NULL, 25000, 46),
(507, 'Huyện Đông Hòa', 0, 0, 0, NULL, 25000, 46),
(508, 'Huyện Đồng Xuân', 0, 0, 0, NULL, 25000, 46),
(509, 'Thành Phố Tuy Hòa', 0, 0, 0, NULL, 25000, 46),
(510, 'Thị Xã Sông Cầu', 0, 0, 0, NULL, 25000, 46),
(511, 'Huyện Bố Trạch', 0, 0, 0, NULL, 25000, 47),
(512, 'Huyện Lệ Thủy', 0, 0, 0, NULL, 25000, 47),
(513, 'Huyện Minh Hóa', 0, 0, 0, NULL, 25000, 47),
(514, 'Huyện Quảng Ninh', 0, 0, 0, NULL, 25000, 47),
(515, 'Huyện Quảng Trạch', 0, 0, 0, NULL, 25000, 47),
(516, 'Huyện Tuyên Hóa', 0, 0, 0, NULL, 25000, 47),
(517, 'Thành Phố Đồng Hới', 0, 0, 0, NULL, 25000, 47),
(518, 'Huyện Phú Ninh', 0, 0, 0, NULL, 25000, 48),
(519, 'Huyện Núi Thành', 0, 0, 0, NULL, 25000, 48),
(520, 'Huyện Bắc Trà My', 0, 0, 0, NULL, 25000, 48),
(521, 'Huyện Duy Xuyên', 0, 0, 0, NULL, 25000, 48),
(522, 'Huyện Hiệp Đức', 0, 0, 0, NULL, 25000, 48),
(523, 'Huyện Nam Giang', 0, 0, 0, NULL, 25000, 48),
(524, 'Huyện Nam Trà My', 0, 0, 0, NULL, 25000, 48),
(525, 'Huyện Nông Sơn', 0, 0, 0, NULL, 25000, 48),
(526, 'Huyện Phước Sơn', 0, 0, 0, NULL, 25000, 48),
(527, 'Huyện Quế Sơn', 0, 0, 0, NULL, 25000, 48),
(528, 'Huyện Tây Giang', 0, 0, 0, NULL, 25000, 48),
(529, 'Huyện Thăng Bình', 0, 0, 0, NULL, 25000, 48),
(530, 'Huyện Tiên Phước', 0, 0, 0, NULL, 25000, 48),
(531, 'Huyện Đại Lộc', 0, 0, 0, NULL, 25000, 48),
(532, 'Huyện Điện Bàn', 0, 0, 0, NULL, 25000, 48),
(533, 'Huyện Đông Giang', 0, 0, 0, NULL, 25000, 48),
(534, 'Thành Phố Hội An', 0, 0, 0, NULL, 25000, 48),
(535, 'Thành Phố Tam Kỳ', 0, 0, 0, NULL, 25000, 48),
(536, 'Huyện Tây Trà', 0, 0, 0, NULL, 25000, 49),
(537, 'Huyện Ba Tơ', 0, 0, 0, NULL, 25000, 49),
(538, 'Huyện Bình Sơn', 0, 0, 0, NULL, 25000, 49),
(539, 'Huyện Lý Sơn', 0, 0, 0, NULL, 25000, 49),
(540, 'Huyện Minh Long', 0, 0, 0, NULL, 25000, 49),
(541, 'Huyện Mộ Đức', 0, 0, 0, NULL, 25000, 49),
(542, 'Huyện Nghĩa Hành', 0, 0, 0, NULL, 25000, 49),
(543, 'Huyện Sơn Hà', 0, 0, 0, NULL, 25000, 49),
(544, 'Huyện Sơn Tây', 0, 0, 0, NULL, 25000, 49),
(545, 'Huyện Sơn Tịnh', 0, 0, 0, NULL, 25000, 49),
(546, 'Huyện Trà Bồng', 0, 0, 0, NULL, 25000, 49),
(547, 'Huyện Tư Nghĩa', 0, 0, 0, NULL, 25000, 49),
(548, 'Huyện Đức Phổ', 0, 0, 0, NULL, 25000, 49),
(549, 'Thành Phố Quảng Ngãi', 0, 0, 0, NULL, 25000, 49),
(562, 'Thành Phố Móng Cái', 0, 0, 0, NULL, 25000, 50),
(598, 'Huyện Bến Cầu', 0, 0, 0, NULL, 23000, 54),
(599, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 54),
(600, 'Huyện Dương Minh Châu', 0, 0, 0, NULL, 23000, 54),
(601, 'Huyện Gò Dầu', 0, 0, 0, NULL, 23000, 54),
(606, 'Thị Xã Tây Ninh', 0, 0, 0, NULL, 23000, 54),
(607, 'Huyện Tiền Hải', 0, 0, 0, NULL, 25000, 55),
(608, 'Huyện Hưng Hà', 0, 0, 0, NULL, 25000, 55),
(609, 'Huyện Kiến Xương', 0, 0, 0, NULL, 25000, 55),
(610, 'Huyện Quỳnh Phụ', 0, 0, 0, NULL, 25000, 55),
(611, 'Huyện Thái Thụy', 0, 0, 0, NULL, 25000, 55),
(612, 'Huyện Vũ Thư', 0, 0, 0, NULL, 25000, 55),
(613, 'Huyện Đông Hưng', 0, 0, 0, NULL, 25000, 55),
(614, 'Thành Phố Thái Bình', 0, 0, 0, NULL, 25000, 55),
(615, 'Huyện Phú Bình', 0, 0, 0, NULL, 25000, 56),
(616, 'Huyện Phổ Yên', 0, 0, 0, NULL, 25000, 56),
(617, 'Huyện Phú Lương', 0, 0, 0, NULL, 25000, 56),
(618, 'Huyện Võ Nhai', 0, 0, 0, NULL, 25000, 56),
(619, 'Huyện Đại Từ', 0, 0, 0, NULL, 25000, 56),
(620, 'Huyện Định Hóa', 0, 0, 0, NULL, 25000, 56),
(621, 'Huyện Đồng Hỷ', 0, 0, 0, NULL, 25000, 56),
(622, 'Thành Phố Thái Nguyên', 0, 0, 0, NULL, 25000, 56),
(623, 'Thị Xã Sông Công', 0, 0, 0, NULL, 25000, 56),
(624, 'Huyện Đông Sơn', 0, 0, 0, NULL, 25000, 57),
(625, 'Huyện Bá Thước', 0, 0, 0, NULL, 25000, 57),
(626, 'Huyện Cẩm Thủy', 0, 0, 0, NULL, 25000, 57),
(627, 'Huyện Hà Trung', 0, 0, 0, NULL, 25000, 57),
(628, 'Huyện Hậu Lộc', 0, 0, 0, NULL, 25000, 57),
(629, 'Huyện Hoằng Hóa', 0, 0, 0, NULL, 25000, 57),
(630, 'Huyện Lang Chánh', 0, 0, 0, NULL, 25000, 57),
(631, 'Huyện Mường Lát', 0, 0, 0, NULL, 25000, 57),
(632, 'Huyện Nga Sơn', 0, 0, 0, NULL, 25000, 57),
(633, 'Huyện Ngọc Lặc', 0, 0, 0, NULL, 25000, 57),
(635, 'Huyện Như Thanh', 0, 0, 0, NULL, 25000, 57),
(636, 'Huyện Như Xuân', 0, 0, 0, NULL, 25000, 57),
(637, 'Huyện Nông Cống', 0, 0, 0, NULL, 25000, 57),
(638, 'Huyện Quan Hóa', 0, 0, 0, NULL, 25000, 57),
(639, 'Huyện Quan Sơn', 0, 0, 0, NULL, 25000, 57),
(640, 'Huyện Quảng Xương', 0, 0, 0, NULL, 25000, 57),
(641, 'Huyện Thạch Thành', 0, 0, 0, NULL, 25000, 57),
(642, 'Huyện Thiệu Hóa', 0, 0, 0, NULL, 25000, 57),
(643, 'Huyện Thọ Xuân', 0, 0, 0, NULL, 25000, 57),
(644, 'Huyện Thường Xuân', 0, 0, 0, NULL, 25000, 57),
(645, 'Huyện Tĩnh Gia', 0, 0, 0, NULL, 25000, 57),
(646, 'Huyện Triêu Sơn', 0, 0, 0, NULL, 25000, 57),
(647, 'Huyện Vĩnh Lộc', 0, 0, 0, NULL, 25000, 57),
(648, 'Huyện Yên Định', 0, 0, 0, NULL, 25000, 57),
(649, 'Thành Phố Thanh Hóa', 0, 0, 0, NULL, 25000, 57),
(650, 'Thị Xã Bỉm Sơn', 0, 0, 0, NULL, 25000, 57),
(651, 'Thị Xã Sầm Sơn', 0, 0, 0, NULL, 25000, 57),
(652, 'Huyện Phong Điền', 0, 0, 0, NULL, 25000, 58),
(653, 'Huyện A Lưới', 0, 0, 0, NULL, 25000, 58),
(654, 'Huyện Nam Đông', 0, 0, 0, NULL, 25000, 58),
(655, 'Huyện Phú Lộc', 0, 0, 0, NULL, 25000, 58),
(656, 'Huyện Phú Vang', 0, 0, 0, NULL, 25000, 58),
(657, 'Huyện Quảng Điền', 0, 0, 0, NULL, 25000, 58),
(658, 'Thị Xã Hương Thủy', 0, 0, 0, NULL, 25000, 58),
(659, 'Thị Xã Hương Trà', 0, 0, 0, NULL, 25000, 58),
(660, 'Thành Phố Huế', 0, 0, 0, NULL, 25000, 58),
(661, 'Huyện Cai Lậy', 0, 0, 0, NULL, 23000, 59),
(662, 'Huyện Cái Bè', 0, 0, 0, NULL, 23000, 59),
(663, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 59),
(664, 'Huyện Chợ Gạo', 0, 0, 0, NULL, 23000, 59),
(665, 'Huyện Gò Công Tây', 0, 0, 0, NULL, 23000, 59),
(666, 'Huyện Gò Công Đông', 0, 0, 0, NULL, 23000, 59),
(667, 'Huyện Tân Phú Đông', 0, 0, 0, NULL, 23000, 59),
(668, 'Huyện Tân Phước', 0, 0, 0, NULL, 23000, 59),
(669, 'Thành Phố Mỹ Tho', 0, 0, 0, NULL, 23000, 59),
(670, 'Thị Xã Gò Công', 0, 0, 0, NULL, 23000, 59),
(671, 'Huyện Càng Long', 0, 0, 0, NULL, 23000, 60),
(672, 'Huyện Cầu Kè', 0, 0, 0, NULL, 23000, 60),
(673, 'Huyện Cầu Ngang', 0, 0, 0, NULL, 23000, 60),
(674, 'Huyện Châu Thành', 0, 0, 0, NULL, 23000, 60),
(675, 'Huyện Duyên Hải', 0, 0, 0, NULL, 23000, 60),
(676, 'Huyện Tiểu Cần', 0, 0, 0, NULL, 23000, 60),
(677, 'Huyện Trà Cú', 0, 0, 0, NULL, 23000, 60),
(678, 'Thành Phố Trà Vinh', 0, 0, 0, NULL, 23000, 60),
(679, 'Huyện Chiêm Hóa', 0, 0, 0, NULL, 28000, 61),
(680, 'Huyện Hàm Yên', 0, 0, 0, NULL, 28000, 61),
(681, 'Huyện Lâm Bình', 0, 0, 0, NULL, 28000, 61),
(682, 'Huyện Na Hang', 0, 0, 0, NULL, 28000, 61),
(683, 'Huyện Sơn Dương', 0, 0, 0, NULL, 28000, 61),
(684, 'Huyện Yên Sơn', 0, 0, 0, NULL, 28000, 61),
(685, 'Thành Phố Tuyên Quang', 0, 0, 0, NULL, 28000, 61),
(686, 'Huyện Bình Minh', 0, 0, 0, NULL, 23000, 62),
(687, 'Huyện Vũng Liêm', 0, 0, 0, NULL, 23000, 62),
(688, 'Huyện Bình Tân', 0, 0, 0, NULL, 23000, 62),
(689, 'Huyện Long Hồ', 0, 0, 0, NULL, 23000, 62),
(690, 'Huyện Mang Thít', 0, 0, 0, NULL, 23000, 62),
(691, 'Huyện Tam Bình', 0, 0, 0, NULL, 23000, 62),
(692, 'Huyện Trà Ôn', 0, 0, 0, NULL, 23000, 62),
(693, 'Thành Phố Vĩnh Long', 0, 0, 0, NULL, 23000, 62),
(694, 'Huyện Tam Đảo', 0, 0, 0, NULL, 25000, 63),
(695, 'Huyện Bình Xuyên', 0, 0, 0, NULL, 25000, 63),
(696, 'Huyện Lập Thạch', 0, 0, 0, NULL, 25000, 63),
(697, 'Huyện Sông Thô', 0, 0, 0, NULL, 25000, 63),
(698, 'Huyện Tam Dương', 0, 0, 0, NULL, 25000, 63),
(699, 'Huyện Vĩnh Tường', 0, 0, 0, NULL, 25000, 63),
(700, 'Huyện Yên Lạc', 0, 0, 0, NULL, 25000, 63),
(701, 'Thành Phố Vĩnh Yên', 0, 0, 0, NULL, 25000, 63),
(702, 'Thị Xã Phúc Yên', 0, 0, 0, NULL, 25000, 63),
(703, 'Huyện Lục Yên', 0, 0, 0, NULL, 25000, 64),
(704, 'Huyện Mù Cang Chải', 0, 0, 0, NULL, 25000, 64),
(705, 'Huyện Trạm Tấu', 0, 0, 0, NULL, 25000, 64),
(706, 'Huyện Trấn Yên', 0, 0, 0, NULL, 25000, 64),
(707, 'Huyện Văn Chấn', 0, 0, 0, NULL, 25000, 64),
(708, 'Huyện Văn Yên', 0, 0, 0, NULL, 25000, 64),
(709, 'Huyện Yên Bình', 0, 0, 0, NULL, 25000, 64),
(710, 'Thành Phố Yên Bái', 0, 0, 0, NULL, 25000, 64),
(711, 'Thị Xã Nghĩa Lộ', 0, 0, 0, NULL, 25000, 64),
(712, 'Thủ Đức', 0, 0, 0, NULL, 18000, 1),
(713, 'Tân Bình', 1, 0, 0, NULL, 10000, 1),
(714, 'Tân Phú', 1, 0, 0, NULL, 10000, 1),
(715, 'Huyện Mỏ Cày Bắc', 0, 0, 0, NULL, 23000, 22);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_flash`
--

CREATE TABLE `mn_flash` (
  `Id` int(11) NOT NULL,
  `location` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_vn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `style` tinyint(1) NOT NULL,
  `ticlock` tinyint(4) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_flash`
--

INSERT INTO `mn_flash` (`Id`, `location`, `file_vn`, `file_en`, `link`, `width`, `height`, `sort`, `style`, `ticlock`, `color`, `position`) VALUES
(67, '2', 'logo1.png', '', '', '', '', 0, 2, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_follow`
--

CREATE TABLE `mn_follow` (
  `Id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idpro` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `view` tinyint(4) NOT NULL DEFAULT '0',
  `ticlock` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_follow`
--

INSERT INTO `mn_follow` (`Id`, `iduser`, `idpro`, `date`, `view`, `ticlock`) VALUES
(1, 2, 224, 0, 0, 0),
(2, 2, 286, 0, 0, 0),
(3, 19, 360, 0, 0, 0),
(4, 2, 404, 0, 0, 0),
(7, 28, 376, 0, 0, 0),
(8, 16, 1769, 0, 0, 0),
(9, 1, 1976, 0, 0, 0),
(13, 1, 2030, 0, 0, 0),
(14, 16, 2029, 0, 0, 0),
(15, 16, 1990, 0, 0, 0),
(16, 16, 1989, 0, 0, 0),
(17, 26, 1981, 0, 0, 0),
(24, 16, 1785, 0, 0, 0),
(25, 16, 2091, 0, 0, 0),
(26, 22, 1785, 0, 0, 0),
(27, 22, 2091, 0, 0, 0),
(28, 43, 2613, 0, 0, 0),
(29, 43, 2607, 0, 0, 0),
(30, 82, 1787, 0, 0, 0),
(31, 96, 1938, 0, 0, 0),
(34, 814, 2492, 0, 0, 0),
(35, 814, 2623, 0, 0, 0),
(37, 832, 2331, 0, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_gift`
--

CREATE TABLE `mn_gift` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `ip_address` int(11) NOT NULL,
  `start_day` int(11) NOT NULL,
  `end_day` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_gift`
--

INSERT INTO `mn_gift` (`id`, `id_user`, `type`, `status`, `ip_address`, `start_day`, `end_day`, `created`) VALUES
(12, 828, 6, 1, 0, 0, 0, 1474076943);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_guid`
--

CREATE TABLE `mn_guid` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_library`
--

CREATE TABLE `mn_library` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_location`
--

CREATE TABLE `mn_location` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `ticlock` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_location`
--

INSERT INTO `mn_location` (`id`, `title`, `sort`, `ticlock`, `created`, `modified`) VALUES
(1, 'Hồ Chí Minh', 1, 0, 1476068554, 1476068616),
(2, 'Hà Nội', 2, 0, 1476068627, 0),
(3, 'Đà nẵng', 3, 0, 1476068637, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_manufacturer`
--

CREATE TABLE `mn_manufacturer` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `description_vn` text CHARACTER SET utf8,
  `titlepage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8,
  `meta_keyword` text CHARACTER SET utf8,
  `images` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_manufacturer`
--

INSERT INTO `mn_manufacturer` (`Id`, `title_vn`, `idcat`, `ticlock`, `description_vn`, `titlepage`, `meta_description`, `meta_keyword`, `images`, `sort`, `alias`, `date`) VALUES
(117, 'Antipodes', 0, 0, '', NULL, NULL, NULL, 'Antipodes.png', 0, 'antipodes', 1545466287),
(118, 'GoodHealth', 0, 0, '', NULL, NULL, NULL, 'Goodhealth.png', 0, 'goodhealth', 1545466340);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_media`
--

CREATE TABLE `mn_media` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_media`
--

INSERT INTO `mn_media` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(35, 'Giới thiệu', NULL, NULL, NULL, 'https://www.youtube.com/embed/C5rDmPIAA40', NULL, '', NULL, 'noel_hutech-15456362123.png', 25, 0, 0, 0, 0, NULL, 1546061373, NULL, NULL, NULL, NULL, 'gioi-thieu'),
(36, 'Test 1', NULL, NULL, NULL, 'https://www.youtube.com/embed/C5rDmPIAA40', NULL, '', NULL, 'noel_hutech-15456362122.png', 26, 0, 0, 1, 0, NULL, 1546094665, NULL, NULL, NULL, NULL, 'test-1'),
(37, 'Test 2', NULL, NULL, NULL, 'https://www.youtube.com/embed/C5rDmPIAA40', NULL, '', NULL, 'noel_hutech-15456362121.png', 25, 0, 0, 0, 0, NULL, 1546094683, NULL, NULL, NULL, NULL, 'test-2'),
(38, 'Test 3', NULL, NULL, NULL, 'https://www.youtube.com/embed/C5rDmPIAA40', NULL, '', NULL, 'noel_hutech-1545636212.png', 26, 0, 0, 0, 0, NULL, 1546094699, NULL, NULL, NULL, NULL, 'test-3');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_menu`
--

CREATE TABLE `mn_menu` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `catelog` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_news`
--

CREATE TABLE `mn_news` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_news`
--

INSERT INTO `mn_news` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(43, 'Đường đến trường', NULL, '<p>Đường đến trường</p>\r\n', NULL, '<p>Đường đến trường</p>\r\n', NULL, '', NULL, NULL, 25, 13, 0, 0, 0, NULL, 1546085249, NULL, NULL, NULL, NULL, 'duong-den-truong'),
(42, 'Văn hóa và Truyền thống', NULL, '<p>Văn h&oacute;a v&agrave; Truyền thống</p>\r\n', NULL, '<p>Văn h&oacute;a v&agrave; Truyền thống</p>\r\n', NULL, '', NULL, NULL, 25, 12, 0, 0, 0, NULL, 1546085234, NULL, NULL, NULL, NULL, 'van-hoa-va-truyen-thong'),
(31, 'Giới thiệu tổng quan', NULL, '<p>Giới thiệu</p>\r\n', NULL, '<p>Giới thiệu</p>\r\n', NULL, '', NULL, '', 25, 1, 0, 0, 0, NULL, 1546011512, NULL, NULL, NULL, NULL, 'gioi-thieu-tong-quan'),
(32, 'Sơ đồ tổ chức', NULL, '<p>Sơ đồ tổ chức</p>\r\n', NULL, '<p>Sơ đồ tổ chức</p>\r\n', NULL, '', NULL, NULL, 25, 3, 0, 0, 0, NULL, 1546016038, NULL, NULL, NULL, NULL, 'so-do-to-chuc'),
(33, 'Lịch sử phát triển', NULL, '<p>content</p>\r\n', NULL, '<p>content</p>\r\n', NULL, '', NULL, NULL, 25, 2, 0, 0, 0, NULL, 1546084955, NULL, NULL, NULL, NULL, 'lich-su-phat-trien'),
(34, 'Hội đồng quản trị', NULL, '<p>Hội đồng quản trị</p>\r\n', NULL, '<p>Hội đồng quản trị</p>\r\n', NULL, '', NULL, NULL, 25, 4, 0, 0, 0, NULL, 1546085074, NULL, NULL, NULL, NULL, 'hoi-dong-quan-tri'),
(35, 'Ban giam hiệu', NULL, '<p>Ban giam hiệu</p>\r\n', NULL, '<p>Ban giam hiệu</p>\r\n', NULL, '', NULL, NULL, 25, 5, 0, 0, 0, NULL, 1546085093, NULL, NULL, NULL, NULL, 'ban-giam-hieu'),
(36, 'Thành tích đạt được', NULL, '<p>Th&agrave;nh t&iacute;ch đạt được</p>\r\n', NULL, '<p>Th&agrave;nh t&iacute;ch đạt được</p>\r\n', NULL, '', NULL, NULL, 25, 6, 0, 0, 0, NULL, 1546085121, NULL, NULL, NULL, NULL, 'thanh-tich-dat-duoc'),
(37, 'Hệ thống nhận dạng và ý nghĩa', NULL, '<p>Hệ thống nhận dạng v&agrave; &yacute; nghĩa</p>\r\n', NULL, '<p>Hệ thống nhận dạng v&agrave; &yacute; nghĩa</p>\r\n', NULL, '', NULL, NULL, 25, 7, 0, 0, 0, NULL, 1546085141, NULL, NULL, NULL, NULL, 'he-thong-nhan-dang-va-y-nghia'),
(38, 'Báo cáo 3 công khai', NULL, '<p style=\"margin-left:42.55pt;\">B&aacute;o c&aacute;o 3 c&ocirc;ng khai</p>\r\n', NULL, '<p style=\"margin-left:42.55pt;\">B&aacute;o c&aacute;o 3 c&ocirc;ng khai</p>\r\n', NULL, '', NULL, NULL, 25, 8, 0, 0, 0, NULL, 1546085163, NULL, NULL, NULL, NULL, 'bao-cao-3-cong-khai'),
(39, 'Chính sách học phí', NULL, '<p>Ch&iacute;nh s&aacute;ch học ph&iacute;</p>\r\n', NULL, '<p>Ch&iacute;nh s&aacute;ch học ph&iacute;</p>\r\n', NULL, '', NULL, NULL, 25, 9, 0, 0, 0, NULL, 1546085179, NULL, NULL, NULL, NULL, 'chinh-sach-hoc-phi'),
(40, 'Chính sách học bổng', NULL, '<p>Ch&iacute;nh s&aacute;ch học bổng</p>\r\n', NULL, '<p>Ch&iacute;nh s&aacute;ch học bổng</p>\r\n', NULL, '', NULL, NULL, 25, 10, 0, 0, 0, NULL, 1546085197, NULL, NULL, NULL, NULL, 'chinh-sach-hoc-bong'),
(41, 'Thư viện hình ảnh', NULL, '<p>Thư viện h&igrave;nh ảnh</p>\r\n', NULL, '<p>Thư viện h&igrave;nh ảnh</p>\r\n', NULL, '', NULL, NULL, 25, 11, 0, 0, 0, NULL, 1546085220, NULL, NULL, NULL, NULL, 'thu-vien-hinh-anh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_pagehtml`
--

CREATE TABLE `mn_pagehtml` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_pagehtml`
--

INSERT INTO `mn_pagehtml` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `sort`, `ticlock`, `alias`) VALUES
(8, 'Thông tin footer', NULL, NULL, NULL, '<div class=\"col-right\">\r\n<div class=\"title\">\r\nLive Chat\r\n</div>\r\n<div class=\"subtitle\">\r\nMon - Fri\r\n<br>9.30am - 5pm NZST\r\n</div>\r\n</div>\r\n', NULL, NULL, '', 'thong-tin-footer');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_partners`
--

CREATE TABLE `mn_partners` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  `idcat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_product`
--

CREATE TABLE `mn_product` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci NOT NULL,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `digital` text COLLATE utf8_unicode_ci,
  `idcat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idmanufacturer` int(11) DEFAULT NULL,
  `status` int(255) NOT NULL DEFAULT '0',
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images1` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images4` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images5` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `codepro` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `hot` tinyint(1) DEFAULT NULL,
  `xlimit` int(1) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_price` int(11) NOT NULL DEFAULT '0',
  `date` int(11) DEFAULT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `empty` tinyint(4) NOT NULL DEFAULT '0',
  `view` int(11) DEFAULT NULL,
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `titlepage` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `tag` text COLLATE utf8_unicode_ci,
  `trash` tinyint(4) NOT NULL DEFAULT '0',
  `iduser` int(11) DEFAULT NULL,
  `oder` int(11) NOT NULL DEFAULT '0',
  `admin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idpercent` int(11) NOT NULL DEFAULT '0',
  `expired` int(11) DEFAULT NULL,
  `xh` int(1) NOT NULL DEFAULT '0',
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_type` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_product`
--

INSERT INTO `mn_product` (`Id`, `title_vn`, `description_vn`, `content_vn`, `digital`, `idcat`, `idmanufacturer`, `status`, `images`, `images1`, `images2`, `images3`, `images4`, `images5`, `price`, `codepro`, `sort`, `ticlock`, `hot`, `xlimit`, `alias`, `sale_price`, `date`, `meta_keyword`, `meta_description`, `empty`, `view`, `home`, `titlepage`, `tag`, `trash`, `iduser`, `oder`, `admin`, `idpercent`, `expired`, `xh`, `linkgoc`, `discount_code`, `code_type`) VALUES
(1, 'Viên Uống Giảm Cân ', '<span style=\"font-size: 14px; line-height: 20.8px;\">Excellence Losing Weight Capsules giúp làm giảm cảm giác thèm ăn, thúc đẩy sự phân hủy và đốt cháy chất béo, có tác dụng cải thiện bệnh béo phì.</span>', '<p><span style=\"font-size:14px\">Vi&ecirc;n uống giảm c&acirc;n Excellence Losing Weight Capsules hỗ trợ giảm c&acirc;n t&iacute;ch cực, gi&uacute;p cả nam v&agrave; nữ nhanh ch&oacute;ng lấy lại v&oacute;c&nbsp;d&aacute;ng thon gọn, săn chắc m&agrave; cần nhờ đến những b&agrave;i tập mệt nhọc hay những bữa ăn ki&ecirc;ng khổ cực.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\">Excellence Losing Weight Capsules gi&uacute;p l&agrave;m giảm cảm gi&aacute;c th&egrave;m ăn, th&uacute;c đẩy sự ph&acirc;n hủy v&agrave; đốt ch&aacute;y chất b&eacute;o, c&oacute; t&aacute;c dụng cải thiện bệnh b&eacute;o ph&igrave;.<br />\r\n- Đặc biệt t&aacute;c động t&iacute;ch cực đến v&ugrave;ng bụng, chuy&ecirc;n d&ugrave;ng cho v&ograve;ng bụng to, giảm nhanh v&ograve;ng bụng.<br />\r\n- Hỗ trợ giảm b&eacute;o, ngăn ngừa bệnh b&eacute;o ph&igrave;, chống lại sự t&iacute;ch tụ mỡ trong cơ thể đặc biệt l&agrave; ở v&ugrave;ng thắt lưng, h&ocirc;ng v&agrave; bụng dưới, chống t&aacute;o b&oacute;n v&agrave; kh&ocirc;ng hề g&acirc;y t&aacute;c dụng phụ.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần:&nbsp;</strong><br />\r\n- Vitamin A - 650 IU, vitamin B1 -400 IU, vitamin B6 - 200 IU, Vitamin 12 - 100 IU, calcium - 250mg, iron - 15mg, L-Cartime - 20mg, kiwi fruit 68.8mg, apple - 57.4mg, orange 48.6mg, guarana fruit - 50.1mg, lotus leaf - 48.8mg.<br />\r\n<strong>C&aacute;ch d&ugrave;ng:</strong><br />\r\n- Ng&agrave;y uống 02 vi&ecirc;n. Để c&oacute; kết quả tốt nhất, uống v&agrave;o buổi s&aacute;ng, l&uacute;c bụng đ&oacute;i; uống 1 vi&ecirc;n trước sau đ&oacute; 2 đến 3 giờ sau th&igrave; uống vi&ecirc;n thứ hai.<br />\r\n<strong>Bảo quản:</strong><br />\r\n- Bảo quản ở nhiệt độ ph&ograve;ng.<br />\r\n<strong>Quy c&aacute;ch đ&oacute;ng g&oacute;i</strong>:&nbsp;60 vi&ecirc;n<br />\r\n<strong>Lưu &yacute;:</strong><br />\r\n- Sản phẩm d&ugrave;ng cho cả nam v&agrave; nữ trong độ tuổi: 18 - 60. Kh&ocirc;ng d&ugrave;ng cho phụ nữ c&oacute; thai v&agrave; đang cho con b&uacute;. Những người bị bệnh về m&aacute;u, tiểu đường, xuất huyết ở mức nguy hiểm được khuyến c&aacute;o kh&ocirc;ng n&ecirc;n d&ugrave;ng.&nbsp;</span></p>\r\n', NULL, '28', 117, 0, 'bo-tai-nghe-kem-gia-do-da-nang-hello-kitty-kute.jpg', 'thuc-pham-chuc-nang-vien-uong-giam-can-excellence-losing-weight-capsules-hop-60-vien-01.jpg', '', '', '', '', '1200000', '0', 0, 0, 1, 1, 'thuc-pham-chuc-nang-vien-uong-giam-can-excellence-losing-weight-capsules-hop-60-vien', 900000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 2, 'admin', 0, NULL, 0, NULL, '', 0),
(2, 'Breast Enlargemen', '<span style=\"font-size:14px;\">Làm tăng và điều hòa các yếu tố nội tiết nữ,&nbsp;</span><span style=\"font-size:14px;\">thúc đẩy sự giãn nở của các mạch máu nuôi dưỡng bầu ngực,&nbsp;</span><span style=\"font-size:14px;\">tạo nên sự đàn hồi và săn chắc tự nhiên cho bộ ngực, giúp bộ ngực tăng đều, nở tròn tự nhiên.</span>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">V&ograve;ng 1 đẹp l&agrave; điều m&agrave; c&aacute;c chị em phụ nữ đều&nbsp;mong muốn. Vi&ecirc;n uống nở ngực&nbsp;No.1 Breast Enlargenment USA với chiết xuất từ&nbsp;tự nhi&ecirc;n an to&agrave;n v&agrave; hiệu quả&nbsp;tạo n&ecirc;n sự đ&agrave;n hồi v&agrave; săn chắc tự nhi&ecirc;n cho bộ ngực, gi&uacute;p bộ ngực tăng đều, nở tr&ograve;n tự nhi&ecirc;n.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\">-&nbsp;Gi&uacute;p điều h&ograve;a kinh nguyệt, hoạt huyết.<br />\r\n-<strong>&nbsp;</strong>L&agrave;m tăng v&agrave; điều h&ograve;a c&aacute;c yếu tố nội tiết nữ (d&ugrave;ng cho tất cả c&aacute;c chị em phụ nữ. kh&ocirc;ng ph&acirc;n biệt sắc d&acirc;n), v&agrave; l&agrave;m giảm những triệu chứng hậu đoạn kinh.<br />\r\n- L&agrave;m giảm những triệu chứng đau ngực trong thời kỳ kinh nguyệt.<br />\r\n- L&agrave;m tăng c&aacute;c tế b&agrave;o m&ocirc; biểu tại v&ugrave;ng ngực, tạo n&ecirc;n sự đ&agrave;n hồi v&agrave; săn chắc tự nhi&ecirc;n cho bộ ngực, gi&uacute;p bộ ngực tăng đều, nở tr&ograve;n tự nhi&ecirc;n.<br />\r\n- Th&uacute;c đẩy sự gi&atilde;n nở của c&aacute;c mạch m&aacute;u nu&ocirc;i dưỡng bầu ngực, tăng cường lưu lượng m&aacute;u nu&ocirc;i dưỡng bầu ngực.<br />\r\n- X&oacute;a bỏ c&aacute;c vết th&acirc;m, đen, n&aacute;m ở v&ugrave;ng ngực, l&agrave;m da trắng mịn, l&agrave;m hồng nhũ hoa.<br />\r\n- Kh&ocirc;ng g&acirc;y k&iacute;ch ứng, mẩn đỏ v&agrave; bất kỳ t&aacute;c dụng phụ g&acirc;y hại n&agrave;o.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần:</strong><br />\r\n-&nbsp;Chiết xuất củ m&agrave;i, chiết xuất quả c&acirc;y Saw Palmetto, chiết xuất hạt cỏ thảo linh lăng, chiết xuất c&acirc;y dương quy, chiết xuất Damiana, chiết xuất th&igrave; l&agrave;, chiết xuất l&aacute; &iacute;ch mẫu, chiết xuất c&acirc;y rẽ bồ c&ocirc;ng anh, chiết xuất l&aacute; c&acirc;y Blessed Thisle, chiết xuất l&aacute; c&acirc;y chaste Berry, chiết xuất nh&acirc;n s&acirc;m ch&acirc;u &Aacute;, chiết xuất rể c&acirc;y Black Cohosh Extract, chiết xuất hạt đậu n&agrave;nh, chiết xuất c&acirc;y Piger nigrum ...<br />\r\n<strong>Đối tượng sử dụng:</strong><br />\r\n-&nbsp;Sử dụng cho nữ từ 12 tuổi trở l&ecirc;n c&oacute; nhu cầu nở ngực. Kh&ocirc;ng d&ugrave;ng cho phụ nữ đang mang thai hoặc cho con b&uacute;.<br />\r\n<strong>Hướng dẫn sử dụng:</strong><br />\r\n-&nbsp;Ng&agrave;y uống 2 lần, mỗi lần 1 vi&ecirc;n. Kh&ocirc;ng d&ugrave;ng sản phẩm chung với đồ uống c&oacute; gas hoặc c&agrave; ph&ecirc;.</span><br />\r\n<span style=\"font-size:14px\"><strong>Hạn sử dụng:</strong><br />\r\n- Thời hạn sử dụng: 4 năm kể từ ng&agrave;y sản xuất.<br />\r\n- Ng&agrave;y sản xuất v&agrave; hạn sử dụng được in tr&ecirc;n bao b&igrave;.<br />\r\n<strong>Bảo quản:</strong><br />\r\n-&nbsp;Bảo quản nơi kh&ocirc; r&aacute;o, tho&aacute;t m&aacute;t, tr&aacute;nh &aacute;nh s&aacute;ng trực tiếp.<br />\r\n<strong>Chất liệu bao b&igrave; v&agrave; quy c&aacute;ch đ&oacute;ng g&oacute;i:</strong><br />\r\n- Sản phẩm được đựng trong chai nhựa.<br />\r\n- Bao b&igrave; đạt chất lượng d&ugrave;ng trong thực phẩm.<br />\r\n<strong>Xuất xứ:</strong><br />\r\n-&nbsp;Sản xuất v&agrave; ph&acirc;n phối bởi H.A.Herbal LLC., (Mỹ)</span></p>\r\n', NULL, '6', 118, 0, 'balo-3d-sieu-nhe-hinh-chu-cong-oops-1.png', 'vien-uong-no-nguc-no-1-breast-enlargement-usa-hop-10g-01.jpg', '', '', '', '', '1500000', '0', 0, 0, 1, 1, 'thuc-pham-chuc-nang-vien-uong-no-nguc-no-1-breast-enlargement-usa-hop-10g', 1200000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 5, 'honghue', 0, NULL, 0, NULL, '', 0),
(12, 'Viên Sữa Ong Chúa', '<font size=\"3\">Viên sữa ong chúa Golden Care Royal Jelly<strong></strong>&nbsp;giúp tăng cường khả năng miễn dịch của cơ thể, làm chậm quá trình lão hóa, làm đẹp da.</font>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">Vi&ecirc;n sữa ong ch&uacute;a&nbsp;Golden Care Royal Jelly l&agrave; loại&nbsp;thực phẩm bổ sung rất&nbsp;được ưa chuộng, kh&ocirc;ng những gi&uacute;p cơ thể khỏe mạnh m&agrave; c&ograve;n cải thiện l&agrave;n da sần s&ugrave;i, th&ocirc; nh&aacute;m gi&uacute;p bạn ng&agrave;y c&agrave;ng xinh đẹp v&agrave; tự tin hơn.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Đặc điểm nổi bật:</strong></u></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- <strong>Vi&ecirc;n sữa ong ch&uacute;a Golden Care Royal Jelly</strong>&nbsp;gi&uacute;p tăng cường khả năng miễn dịch của cơ thể, l&agrave;m chậm qu&aacute; tr&igrave;nh l&atilde;o h&oacute;a, l&agrave;m đẹp da, hỗ trợ sức khỏe, tốt cho tim mạch v&agrave; huyết &aacute;p.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Th&ocirc;ng tin sản phẩm:</strong></u></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần:</strong><br />\r\n-&nbsp;Sữa ong ch&uacute;a (Royal Jelly Lyophilised dry) 1000mg.<br />\r\n-&nbsp;Gelatin, glycerol, nước tinh khiết.<br />\r\n<strong>C&ocirc;ng dụng:</strong><br />\r\n- <strong>Vi&ecirc;n sữa ong ch&uacute;a Golden Care Royal Jelly </strong>gi&uacute;p tăng cường khả năng miễn dịch của cơ thể, l&agrave;m chậm qu&aacute; tr&igrave;nh l&atilde;o h&oacute;a, l&agrave;m đẹp da, hỗ trợ sức khỏe, tốt cho tim mạch v&agrave; huyết &aacute;p.<br />\r\n<strong>Đối tượng sử dụng:</strong><br />\r\n- Từ 12 tuổi trở l&ecirc;n.<br />\r\n- Phụ nữ mang thai n&ecirc;n sử dụng theo sự tư vấn của b&aacute;c sĩ hoặc dược sĩ trước khi d&ugrave;ng.<br />\r\n<strong>Hướng dẫn sử dụng:</strong><br />\r\n- Ng&agrave;y uống 1 vi&ecirc;n hoặc theo sự hướng dẫn của chuy&ecirc;n gia y tế.<br />\r\n<strong>Bảo quản:</strong><br />\r\n- Nơi kh&ocirc; r&aacute;o, tho&aacute;ng m&aacute;t v&agrave; tr&aacute;nh &aacute;nh nắng.<br />\r\n<strong>Khối lượng tịnh:</strong>&nbsp;1448 mg/vi&ecirc;n.<br />\r\n<strong>Quy c&aacute;ch đ&oacute;ng g&oacute;i:</strong>&nbsp;Lọ 120 vi&ecirc;n hoặc lọ 365 vi&ecirc;n.<br />\r\n<strong>Xuất xứ:</strong>&nbsp;Australia</span></p>\r\n', NULL, '6', 24, 0, 'balo-3d-sieu-nhe-hinh-nhim-con-oops.png', 'vien-sua-ong-chua-golden-care-1000mg-hop-365-vien-01.jpg', '', '', '', '', '0', '0', 0, 0, 1, 1, 'vien-sua-ong-chua-golden-care-1000mg-hop-365-vien', 640000, 2016, '', '', 0, NULL, 0, '', '', 0, 0, 2, 'nguyen', 0, NULL, 0, NULL, '', 0),
(3, 'Supreme Royal ', '<p>Vi&ecirc;n sữa ong ch&uacute;a Supreme Royal Jelly được d&ugrave;ng như một chất kh&aacute;ng khuẩn, kh&aacute;ng si&ecirc;u vi, kh&aacute;ng sinh. Với h&agrave;m lượng dinh dưỡng cao, hỗ trợ chống l&atilde;o h&oacute;a, hỗ trợ l&agrave;m mờ n&aacute;m da v&agrave; suy nhược cơ thể.</p>\r\n\r\n<div>Thương hiệu: H.A.Herbal</div>\r\n', '<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Vi&ecirc;n sữa ong ch&uacute;a Supreme Royal Jelly</strong> được đặc chế dưới&nbsp;dạng vi&ecirc;n n&eacute;n gi&uacute;p bổ sung chất dinh dưỡng cho cơ thể, d&ugrave;ng được cho cả nam v&agrave; nữ.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Đặc điểm nổi bật:</strong></u></span><br />\r\n<span style=\"font-size:14px\">-<strong> Vi&ecirc;n sữa&nbsp;ong ch&uacute;a&nbsp;Supreme Royal Jelly</strong> được d&ugrave;ng như một chất kh&aacute;ng khuẩn, kh&aacute;ng si&ecirc;u vi, kh&aacute;ng sinh. Với h&agrave;m lượng dinh dưỡng cao, hỗ trợ chống l&atilde;o h&oacute;a, hỗ trợ l&agrave;m mờ n&aacute;m da v&agrave; suy nhược cơ thể. Sữa ong ch&uacute;a l&agrave; một hợp chất thi&ecirc;n nhi&ecirc;n gi&uacute;p cơ thể tạo chất keo cần thiết cho xương, khớp xương, g&acirc;n v&agrave; da. N&oacute; chứa đầy đủ dưỡng chất cần thiết l&agrave;m cho mắt được tinh tường, tăng khả năng t&igrave;nh dục cho cả hai ph&aacute;i nam v&agrave; nữ, gi&uacute;p cơ thể dẻo dai, bền bỉ hơn, v&agrave; tăng cường sinh lực.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\">Th&agrave;nh phần <strong>vi&ecirc;n sữa&nbsp;ong ch&uacute;a&nbsp;Supreme Royal Jelly</strong> bao gồm: Royal jelly, Milk thiste extract, reishi mushroom, Bee pollen, Bee propolis,asian ginseng, Eleuthero, ashwagandham, passion flower&hellip; &nbsp;<br />\r\n<strong>Quy&nbsp;c&aacute;ch</strong>: 1 hộp c&oacute; 60 vi&ecirc;n<br />\r\n<strong>Trọng lượng:</strong>&nbsp;10g<br />\r\n<strong>K&iacute;ch thước:</strong>&nbsp;12x12cm<br />\r\n<strong>Xuất xứ:</strong><br />\r\n- Được sản xuất trực tiếp bởi tập đo&agrave;n H.A HERBAL LLC. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />\r\n<strong>C&aacute;ch d&ugrave;ng:</strong><br />\r\n- Mỗi ng&agrave;y sử dụng 2 lần, mỗi lần 1 vi&ecirc;n ( v&agrave;o s&aacute;ng v&agrave; tối sau bữa ăn ).</span></p>\r\n', '', '29', 19, 0, 'balo-3d-sieu-nhe-hinh-rua-con-oops.png', 'sua-vien-ong-chua-supreme-royal-jelly-hop-60-vien-01.jpg', '', '', '', '', '0', NULL, 0, 0, 0, 1, 'sua-vien-ong-chua-supreme-royal-jelly-hop-60-vien', 1000000, 2016, '', '', 0, NULL, 0, NULL, '', 1, 0, 1, 'nguyen', 0, NULL, 0, NULL, '', 0),
(4, 'Viên Uống Giảm Cân ', '<span style=\"font-size:14px;\">Giúp giảm cân, giảm mỡ trong máu, chống lão hóa, giúp da mịn màng và tươi trẻ.</span>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">Bạn đang lo lắng về c&acirc;n nặng, v&oacute;c d&aacute;ng của bạn kh&ocirc;ng được thon thả bạn cần một giải ph&aacute;p cho c&acirc;n nặng của m&igrave;nh h&atilde;y đến với&nbsp;vi&ecirc;n giảm c&acirc;n Rich Slim. Với th&agrave;nh phần từ c&aacute;c nguy&ecirc;n liệu tự nhi&ecirc;n sẽ gi&uacute;p bạn c&oacute; một v&oacute;c d&aacute;ng thon gọn.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\">- Vi&ecirc;n uống giảm c&acirc;n&nbsp;Rich Slim&nbsp;gi&uacute;p giảm c&acirc;n, giảm mỡ trong m&aacute;u, chống l&atilde;o h&oacute;a, gi&uacute;p da mịn m&agrave;ng v&agrave; tươi trẻ.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh Phần:</strong><br />\r\n-&nbsp;Chiết xuất tr&agrave; xanh, chiết xuất tầm xu&acirc;n, glycerin, m&agrave;u caramel,&nbsp;m&agrave;u thực vật, stearate.<br />\r\n<strong>Đối tượng sử dụng:</strong>&nbsp;<br />\r\n-&nbsp;Vi&ecirc;n uống giảm c&acirc;n&nbsp;Rich Slim&nbsp;th&iacute;ch hợp cho cả nam v&agrave; nữ.<br />\r\n- Kh&ocirc;ng sử dụng cho phụ nữ mang thai v&agrave; cho con b&uacute;, người bị rối loạn tinh thần hay nghiện ma t&uacute;y, rượu, người suy giảm chức năng gan, thận.<br />\r\n<strong>Hướng dẫn sử dụng:</strong>&nbsp;<br />\r\n-&nbsp;Uống 02 vi&ecirc;n / ng&agrave;y, uống trước bữa ăn s&aacute;ng v&agrave; ăn trưa.<br />\r\n<strong>Chỉ ti&ecirc;u chất lượng ch&iacute;nh:</strong><br />\r\n-<strong>&nbsp;</strong>&nbsp;Năng lượng &le; 50 kcal/vi&ecirc;n.<br />\r\n<strong>Khối lượng tịnh:</strong>&nbsp;<br />\r\n-&nbsp;1500 &plusmn; 10% mg/vi&ecirc;n, 60 vi&ecirc;n/hộp.<br />\r\n<strong>Ng&agrave;y SX:&nbsp;</strong><br />\r\n-<strong>&nbsp;</strong>Xem tr&ecirc;n bao b&igrave;<br />\r\n<strong>Hạn Sử Dụng:</strong><br />\r\n-&nbsp;Xem tr&ecirc;n bao b&igrave;<br />\r\n<strong>Bảo Quản:</strong>&nbsp;<br />\r\n-&nbsp;Bảo quản nơi kh&ocirc; r&aacute;o, tho&aacute;ng m&aacute;t.<br />\r\n<strong>Xuất Xứ:</strong>&nbsp;<br />\r\n-&nbsp;Sản xuất v&agrave; ph&acirc;n phối bởi H.A .HERBAL LLC.,</span><br />\r\n&nbsp;</p>\r\n', NULL, '29', 19, 0, 'balo-3d-sieu-nhe-hinh-rung-cay-oops.png', 'vien-uong-giam-can-rich-slim-hop-60-vien-01.jpg', '', '', '', '', '1500000', '0', 0, 0, 1, 1, 'vien-uong-giam-can-rich-slim-hop-60-vien', 900000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 1, 'honghue', 0, NULL, 0, NULL, '', 0),
(5, ' Royal Jelly ', '<span style=\"font-size:14px;\">Viên sữa ong chúa Lifespring Royal Jelly chứa những chất dinh dưỡng có giá trị tuyệt vời bao gồm 22 acid amin và các vitamins quan trọng cần thiết cho cơ thể</span><div><span style=\"font-size:14px;\">Thương hiệu: LifeSpring</span></div>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Vi&ecirc;n sữa&nbsp;ong ch&uacute;a Lifespring Royal Jelly</strong> được đặc chế dưới dạng vi&ecirc;n n&eacute;n, gi&uacute;p hỗ trợ chất dinh dưỡng tối đa cho cơ thể, d&ugrave;ng được dưới dạng nước hoặc trộn trực tiếp với thức ăn sẽ đem đến cho bạn một sức khỏe tuyệt vời.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Đặc điểm nổi bật:</strong></u></span><br />\r\n<span style=\"font-size:14px\"><strong>Vi&ecirc;n sữa&nbsp;ong ch&uacute;a Lifespring Royal Jelly</strong> chứa những chất dinh dưỡng c&oacute; gi&aacute; trị tuyệt vời bao gồm 22 acid amin v&agrave; c&aacute;c vitamins quan trọng cần thiết cho cơ thể<br />\r\n-&nbsp;L&agrave;m giảm triệu chứng mệt mỏi, mất ngủ.<br />\r\n-&nbsp;L&agrave;m giảm triệu chứng xơ vữa động mạch v&agrave; cao cholesterol trong m&aacute;u.<br />\r\n-&nbsp;L&agrave;m chậm qu&aacute; tr&igrave;nh l&atilde;o ho&aacute;, hỗ trợ trị n&aacute;m da, đồi mồi v&agrave; t&agrave;n nhang, cho l&agrave;n da mịn m&agrave;ng khỏe mạnh<br />\r\n-&nbsp;Tăng cường hệ miễn dịch.<br />\r\n-&nbsp;Gi&uacute;p ngăn ngừa chứng vi&ecirc;m thấp khớp v&agrave; bệnh đa xơ cứng.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Th&ocirc;ng tin sản phẩm:</strong></u></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần:</strong><br />\r\nChứa 18% protein, 10-17% glucid, 5,5% lipid, khoảng 20 loại acid amin quan trọng, trong đ&oacute;, nhiều loại cơ thể con người kh&ocirc;ng tổng hợp được, nhất l&agrave; cystein, c&aacute;c acid hữu cơ, c&aacute;c vitamin&nbsp;B1, B2 (riboflavin), niacin, B5 (pantothenic acid), B6, biotin, folic acid, B12, inositol, v&agrave; choline.&nbsp;Sữa ong ch&uacute;a&nbsp;cũng c&oacute; một số lượng nhỏ của vitamin A, C, D, v&agrave; E c&ugrave;ng những kho&aacute;ng chất như canxi, đồng, chất sắt, photpho, kali, silic, lưu huỳnh.<br />\r\n<strong>C&aacute;ch d&ugrave;ng:</strong><br />\r\nUống 1 vi&ecirc;n/lần, c&oacute; thể d&ugrave;ng 1 đến 2 vi&ecirc;n mỗi ng&agrave;y. C&oacute; thể trộn trong nước, trộn với thức ăn hoặc chỉ đơn giản l&agrave; uống với nước.&nbsp;<br />\r\n<strong>Chống chỉ định:</strong><br />\r\n- Kh&ocirc;ng d&ugrave;ng cho người dưới 18 tuổi<br />\r\n- Kh&ocirc;ng d&ugrave;ng cho những người bị hen suyễn v&agrave; bị dị ứng với phấn hoa.<br />\r\n<strong>Bảo quản</strong><br />\r\n- Bảo quản ở nhiệt độ ph&ograve;ng.<br />\r\n<strong>Quy c&aacute;ch đ&oacute;ng g&oacute;i:</strong>&nbsp;365 vi&ecirc;n/ 1 lọ</span></p>\r\n', '', '11', 23, 0, 'balo-3d-sieu-nhe-hinh-rung-cay-oops-1.png', 'bo-tai-nghe-kem-gia-do-da-nang-hello-kitty-kute.jpg', '', '', '', '', '0', NULL, 0, 0, 0, 1, 'thuc-pham-chuc-nang-sua-ong-chua-lifespring-royal-jelly-1000mg-lo-365-vien', 3069000, 2016, '', '', 0, NULL, 0, NULL, '', 1, 0, 1, 'nguyen', 0, NULL, 0, NULL, '', 0),
(6, 'Enlargement USA', '<h2 style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(54, 52, 50); margin-top: 0px; margin-bottom: 10px; background-color: rgb(255, 255, 255);\"><span style=\"font-family: \'Times New Roman\', serif; font-size: 18px; line-height: 20.7px;\">Kem có tác dụng giúp vòng 1 căng tròn, săn chắc cho những phụ nữ có vòng ngực khiêm tốn, lấy lại sự tự tin cho những ai có bầu ngực nhỏ hoặc không có ngực.</span></h2>', '<p style=\"text-align: justify;\"><span style=\"font-size:14px\">V&ograve;ng 1 l&atilde;o h&oacute;a chảy xệ l&agrave;m mất đi sự tự tin của phụ nữ, kem nở ngực&nbsp;No. 1 Breast Enlargement USA &nbsp;gi&uacute;p định h&igrave;nh n&acirc;ng đỡ to&agrave;n bộ cấu tr&uacute;c da v&ugrave;ng&nbsp;ngực l&agrave;m săn chắc nhũ hoa,&nbsp;giảm c&aacute;c vết nhăn nhanh hữu hiệu mang đến cho người phụ nữ sự tự tin v&agrave; quyến rũ.</span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\">- Tăng k&iacute;ch thước v&agrave; l&agrave;m mở rộng cấu tr&uacute;c tế b&agrave;o của bộ ngực một c&aacute;ch đều đặn.&nbsp;<br />\r\n- Th&uacute;c đẩy sự ph&aacute;t triển của c&aacute;c hốc v&agrave; c&aacute;c thuỳ trong ngực, tăng t&iacute;nh đ&agrave;n hồi của c&aacute;c m&ocirc; ngực.<br />\r\n- Cảm nhận được kết quả chỉ trong 4 - 8 ng&agrave;y sử dụng!<br />\r\n- 100% sản phẩm tự nhi&ecirc;n sẽ đi đ&ocirc;i với việc tr&ocirc;ng thấy v&agrave; cảm nhận bộ ngực tự nhi&ecirc;n.<br />\r\n- Sử dụng cho tất cả c&aacute;c chị em phụ nữ, kh&ocirc;ng ph&acirc;n biệt sắc d&acirc;n.<br />\r\n- Gi&uacute;p điều h&ograve;a h&oacute;c m&ocirc;n nữ v&agrave; giảm c&aacute;c triệu chứng đau ngực trong thời kỳ kinh nguyệt.<br />\r\n- Cải thiện h&igrave;nh d&aacute;ng v&agrave; k&iacute;ch thước của bộ ngực để bạn hấp dẫn hơn<br />\r\n- K&iacute;ch th&iacute;ch m&aacute;u nu&ocirc;i dưỡng da v&ugrave;ng ngực tốt hơn<br />\r\n- Chống chảy sệ v&agrave; l&agrave;m săn chắc bộ ngực của bạn.<br />\r\n- Kh&ocirc;ng g&acirc;y tăng c&acirc;n, an to&agrave;n &amp; hiệu quả, kh&ocirc;ng c&oacute; t&aacute;c dụng phụ g&acirc;y hại.</span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần:</strong><br />\r\nWater, octyl palmitate, Glyceryl Stearate, Cetearyl Alcohol, PEG-100 Stearate, Caprylic/Cpric Triglyceride, Dimethiocne, Saw Palmetto Extract, Dong Quai Estract, Dandelion Dxtract, Blessed Thistle Extract, Kava Kava Extract, Wild Yam Extract, Mother&#39;s Wart Extract, Fenugreek Extract, Black Cohosh Extract, Fennel Extract, Cumin Extract, Myristyl Myristate, Glycerin, Trienthanolamin, Carbomer, Methylparaben, Phenoxyethanol.&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<br />\r\n<strong>C&aacute;ch d&ugrave;ng:</strong><br />\r\n<em>-&nbsp;</em>Thoa đều kem v&agrave;o v&ugrave;ng ngực sau khi tắm rửa sạch sẽ ng&agrave;y 02 lần v&agrave;o buổi s&aacute;ng v&agrave; tối. Thoa nhẹ nh&agrave;ng cho tới khi kem ngấm hết.<br />\r\n<strong>&nbsp;Đối tượng sử dụng:&nbsp;</strong><br />\r\n- Sản phẩm sử dụng cho mọi phụ nữ từ 12 tuổi trở l&ecirc;n c&oacute; nhu cầu nở ngực.<br />\r\n<strong>Thời hạn sử dụng:</strong><br />\r\n- 4 năm kể từ ng&agrave;y sản xuất<br />\r\n<strong>&nbsp; Hạn sử dụng:</strong><br />\r\n- Xem tr&ecirc;n bao b&igrave;<br />\r\n<strong>Hướng dẫn bảo quản:</strong><br />\r\n- Bảo quản nơi kh&ocirc; r&aacute;o, tho&aacute;ng m&aacute;t.&nbsp;<br />\r\n- Tr&aacute;nh &aacute;nh s&aacute;ng trực tiếp.<br />\r\n<strong>Đ&oacute;ng g&oacute;i:<br />\r\n-&nbsp;&nbsp;</strong>50 gram/lọ.<br />\r\n<strong>Xuất xứ:</strong>&nbsp;<br />\r\n-&nbsp;Tập đo&agrave;n H.A HERBAL LLC., 1701 Berkshire Eve. Dr., Duluth, GA 30097 USA (Hoa Kỳ)&nbsp;</span></p>\r\n', NULL, '2', 19, 0, 'balo-3d-sieu-nhe-hinh-thanh-pho-cun-con-oops-.png', 'bo-tai-nghe-kem-gia-do-da-nang-hello-kitty-kute.jpg', '', '', '', '', '0', '0', 0, 0, 1, 1, 'kem-no-nguc-no-1-breast-enlargement-usa-hop-10g', 1500000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 1, 'honghue', 0, NULL, 0, NULL, '', 0),
(7, 'Thực Phẩm ', '<span style=\"text-align: justify; background-color: rgb(255, 255, 255);\"><font color=\"#363432\" face=\"Times New Roman\"><span style=\"font-size: 18px; line-height: 25.7143px;\">Thực Phẩm Hỗ Trợ Tăng Cường Sinh Lý Nam Ngọc Đế Hoàn 10g giúp gia tăng khả năng tình dục nam giới. Hỗ trợ chống liệt dương, hỗ trợ chống xuất tinh sớm. Hỗ trợ phục hồi khả năng sinh lý nhanh, giúp cơ thể cường tráng khỏe mạnh.</span></font></span>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">Thực Phẩm Hỗ Trợ Tăng Cường Sinh L&yacute; Nam Ngọc Đế Ho&agrave;n 10g&nbsp;l&agrave;&nbsp;sản phẩm&nbsp;gi&uacute;p tăng cường sinh l&yacute; cho nam giới, gi&uacute;p nam giới tăng cường sức khỏe trong cuộc sống cũng như trong sinh hoạt giường chiếu.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Đặc điểm nổi bật:</strong></u></span><br />\r\n<span style=\"font-size:14px\">- Gia tăng khả năng t&igrave;nh dục nam giới. K&iacute;ch th&iacute;ch cơ quan sinh dục cương cứng nhanh.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- K&eacute;o d&agrave;i thời gian giao hợp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hỗ trợ chống liệt dương, hỗ trợ chống xuất tinh sớm.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hỗ trợ phục hồi khả năng sinh l&yacute; nhanh, gi&uacute;p cơ thể cường tr&aacute;ng khỏe mạnh.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Gi&uacute;p bổ thận, mạnh g&acirc;n cốt, hỗ trợ điều trị sinh l&yacute; yếu.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng g&acirc;y phản ứng phụ.&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Th&agrave;nh phần:</strong>&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Đ&ocirc;ng tr&ugrave;ng hạ thảo, nh&acirc;n s&acirc;m, c&acirc;u kỷ tử, nhục thung dung, d&acirc;m dương hoắc v&agrave; nhiều loại dược thảo qu&iacute; hiếm kh&aacute;c.&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>C&aacute;ch d&ugrave;ng:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Nhai v&agrave; nuốt từ &frac12; đến 1 vi&ecirc;n với nước ấm 30 ph&uacute;t trước khi quan hệ cho kết quả tức thời khi giao hợp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Uống 2 ng&agrave;y 1/2 vi&ecirc;n trong v&ograve;ng 3 th&aacute;ng hỗ trợ liệt dương, yếu sinh l&yacute;, xuất tinh sớm...&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng n&ecirc;n d&ugrave;ng vượt qu&aacute; liều chỉ dẫn.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Sau khi d&ugrave;ng sản phẩm, xin vui l&ograve;ng uống nước m&aacute;t nếu trong trường hợp kh&oacute; xuất tinh.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng n&ecirc;n d&ugrave;ng qu&aacute; 1 vi&ecirc;n trong 24 giờ.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Chất lượng bao b&igrave;:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Sản phẩm đựng trong bao nhựa, b&ecirc;n ngo&agrave;i l&agrave; hộp giấy.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Bao b&igrave; đạt chất lượng d&ugrave;ng trong thực phẩm.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Quy c&aacute;ch đ&oacute;ng g&oacute;i:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- 9g/vi&ecirc;n.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hộp nhỏ: 9g x 03 ho&agrave;n; Hộp lớn: 9g x 12 ho&agrave;n.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Xuất xứ:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Sản xuất v&agrave; ph&acirc;n phối bởi H.A.Herbal LLC., (Mỹ).&nbsp;</span></p>\r\n', NULL, '15', 19, 0, 'balo-day-rut-thoi-trang.jpg', '', '', '', '', '', '0', '0', 0, 0, 1, 1, 'thuc-pham-ho-tro-tang-cuong-sinh-ly-nam-ngoc-de-hoan-10g', 1300000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 1, 'kimanh', 0, NULL, 0, NULL, '', 0),
(8, 'Viên Sữa Ong ', '<span style=\"font-size:14px;\">Viên sữa ong chúa&nbsp;Rebirth Platinum Royal Jelly 1000 mg là loại thực phẩm chức năng cực kì tốt cho cơ thể, là sự lựa chọn tối ưu cho cả nam và nữ</span>', '<p><span style=\"font-size:14px\"><strong>Vi&ecirc;n sữa ong ch&uacute;a&nbsp;Rebirth Platinum Royal Jelly 1000 mg</strong> được đặc chế dưới dạng vi&ecirc;n n&eacute;n l&agrave; lựa chọn tối ưu cho những ai đang cần bổ sung chất dinh dưỡng to&agrave;n diện cho cơ thể, hiệu quả đối với cả nam lẫn nữ.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Vi&ecirc;n sữa ong ch&uacute;a&nbsp;Rebirth Platinum Royal Jelly 1000 mg </strong>c&oacute; c&ocirc;ng dụng:</span><br />\r\n<span style=\"font-size:14px\">- Tăng cường hệ miễn dịch, l&agrave;m chậm qu&aacute; tr&igrave;nh l&atilde;o h&oacute;a, Trị n&aacute;m, l&agrave;m đẹp da.<br />\r\n- Hỗ&nbsp;trợ tốt trong qu&aacute; tr&igrave;nh trị n&aacute;m da, đồi mồi v&agrave; t&agrave;n nhang, cho l&agrave;n da mịn m&agrave;ng, khỏe mạnh<br />\r\n- Ngăn ngừa rụng t&oacute;c, cho một m&aacute;i t&oacute;c su&ocirc;n mượt v&agrave; c&aacute;c m&oacute;ng tay, ch&acirc;n khỏe mạnh<br />\r\n- Giảm triệu chứng xơ vữa động mạch v&agrave; cholesterol cao trong m&aacute;u<br />\r\n- Tăng cường sinh lực, giảm stress<br />\r\n- Gi&uacute;p ngăn ngừa chứng vi&ecirc;m thấp khớp v&agrave; bệnh đa xơ cứng.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần vi&ecirc;n sữa ong ch&uacute;a&nbsp;</strong><strong>Rebirth Platinum Royal Jelly 1000 mg​</strong><br />\r\n-&nbsp;Chứa 18% protein, 10-17% glucid, 5,5% lipid, khoảng 20 loại acid amin quan trọng, trong đ&oacute;, nhiều loại cơ thể con người kh&ocirc;ng tổng hợp được, nhất l&agrave; cystein, c&aacute;c acid hữu cơ, c&aacute;c vitamin A, B1, B2, B6, B12 , PP, c&aacute;c k&iacute;ch th&iacute;ch tố, muối kho&aacute;ng, một số enzym, hormon hướng sinh dục.&nbsp;<br />\r\n<strong>Trọng lượng:</strong>&nbsp;10g<br />\r\n<strong>K&iacute;ch thước:</strong>&nbsp;12x12cm<br />\r\n<strong>Quy c&aacute;ch đ&oacute;ng g&oacute;i:</strong>&nbsp;60 vi&ecirc;n nang/hộp<br />\r\n<strong>C&aacute;ch sử dụng:</strong><br />\r\n-&nbsp;Uống 1 vi&ecirc;n/lần, c&oacute; thể d&ugrave;ng 1 đến 2 vi&ecirc;n mỗi ng&agrave;y. C&oacute; thể trộn trong nước, trộn với thức ăn hoặc chỉ đơn giản l&agrave; uống với nước.<br />\r\n<strong>Chống chỉ định:</strong><br />\r\n-&nbsp;Kh&ocirc;ng d&ugrave;ng cho người dưới 18 tuổi<br />\r\n-&nbsp;Kh&ocirc;ng d&ugrave;ng cho những người bị hen suyễn v&agrave; bị dị ứng với phấn hoa.<br />\r\n<strong>Bảo quản</strong><br />\r\n-&nbsp;Bảo quản ở nhiệt độ ph&ograve;ng.</span></p>\r\n', NULL, '11', 22, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1-2.jpg', 'vien-sua-ong-chua-rebirth-platinum-royal-jelly-1000-mg-hop-60-vien-nang-01.jpg', '', '', '', '', '650000', '0', 0, 0, 1, 1, 'thuc-pham-chuc-nang-sua-ong-chua-rebirth-platinum-royal-jelly-1000-mg-hop-60-vien-nang', 520000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 1, 'nguyen', 0, NULL, 0, NULL, '', 0),
(9, 'Thực Phẩm Hỗ ', '<font size=\"2\">Thực Phẩm Hỗ Trợ &nbsp;Sinh Lý Nam Cường Dương Hoàng Đế Hoàn&nbsp;10g&nbsp;giúp cường dương, bổ thận, kéo&nbsp;dài thời gian giao hợp, kéo dài thời gian cương cứng của dương vật, kích thích và thúc đẩy sự sinh sản các hoóc môn nam giới.</font>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">Thực Phẩm Hỗ Trợ &nbsp;Sinh L&yacute; Nam Cường Dương Ho&agrave;ng Đế Ho&agrave;n l&agrave; b&agrave;i thuốc qu&yacute; hiếm cổ xưa, xuất ph&aacute;t từ v&ugrave;ng đất T&acirc;y Tạng huyền b&iacute; c&oacute; kh&iacute; hậu khắc nghiệt. B&agrave;i thuốc n&agrave;y đ&atilde; được lưu truyền qua c&aacute;c thế hệ thổ d&acirc;n du mục với c&ocirc;ng dụng bồi bổ sức khỏe, tăng sức chống chịu của cơ thể, tăng sinh l&yacute; v&agrave; duy tr&igrave; n&ograve;i giống.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Đặc điểm nổi bật:</strong></u></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Cường dương, bổ thận, k&eacute;o d&agrave;i thời gian giao hợp.&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- K&eacute;o d&agrave;i thời gian cương cứng của dương vật.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- K&iacute;ch th&iacute;ch v&agrave; th&uacute;c đẩy sự sinh sản c&aacute;c ho&oacute;c m&ocirc;n nam giới.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><u><strong>Th&ocirc;ng tin sản phẩm:</strong></u></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Th&agrave;nh phần:&nbsp;</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Đ&ocirc;ng tr&ugrave;ng hạ thảo, nh&acirc;n s&acirc;m, c&acirc;u kỷ tử, nhục thung dung, d&acirc;m dương hoắc v&agrave; nhiều loại dược thảo qu&yacute;&nbsp;hiếm kh&aacute;c.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Sử dụng:</strong><br />\r\n- Nhai v&agrave; nuốt từ &frac12; đến 1 vi&ecirc;n với nước ấm 30 ph&uacute;t trước khi quan hệ cho kết quả tức thời khi giao hợp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- D&ugrave;ng hỗ trợ điều trị uống 2 ng&agrave;y 1/2 vi&ecirc;n trong v&ograve;ng 3 th&aacute;ng&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Lưu &yacute;:&nbsp;</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng n&ecirc;n d&ugrave;ng vượt qu&aacute; liều chỉ dẫn.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Sau khi d&ugrave;ng sản phẩm, xin vui l&ograve;ng uống nước m&aacute;t nếu trong trường hợp kh&oacute; xuất tinh.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng n&ecirc;n d&ugrave;ng qu&aacute; 1 vi&ecirc;n trong 24 giờ.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Đ&oacute;ng g&oacute;i:</strong></span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hộp nhỏ: 9g x 03 vi&ecirc;n ho&agrave;n; Hộp lớn: 9g x 18 vi&ecirc;n ho&agrave;nhi tiết</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Xuất xứ:</strong>&nbsp;</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Mỹ</span></p>\r\n', NULL, '11', -1, 0, 'bo-tap-an-pigeon-buoc-2-.jpg', 'thuc-pham-chuc-nang-ho-tro-sinh-ly-nam-cuong-duong-hoang-de-hoan-10g-01.jpg', '', '', '', '', '0', '0', 0, 0, 1, 1, 'thuc-pham-ho-tro-sinh-ly-nam-cuong-duong-hoang-de-hoan-10g', 1949000, 2016, '', '', 0, NULL, 0, '', '', 1, 0, 1, 'kimanh', 0, NULL, 0, NULL, '', 0),
(10, 'Kem Dưỡng Làm Trắng ', '<span style=\"font-size:14px;\">Ngăn ngừa sự hình thành sắc tố làm tối da, levorotatory VC làm da trắng, xóa vết đồi mồi, trị mụn, chống viêm, làm sáng da chống nắng, tinh chất lô hội và auxin trong táo xanh giúp cải thiện màu sắc tự nhiên cho làn da, làm sạch, mềm mịn, trắng hồng, khỏe mạnh.</span>', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">L&agrave; phụ nữ ai cũng mong muốn m&igrave;nh c&oacute; l&agrave;n da trắng hồng tự nhi&ecirc;n nhưng kh&ocirc;ng phải ai cũng được trời ph&uacute; cho l&agrave;n da đẹp ho&agrave;n hảo. V&igrave; vậy ch&uacute;ng ta cần t&igrave;m đến những sản phẩm l&agrave;m trắng da v&agrave; kem l&agrave;m trắng da&nbsp;Best Body&nbsp;Whitening Cream&nbsp;l&agrave; một trong số đ&oacute;.&nbsp;kem l&agrave;m trắng da&nbsp;Best Body&nbsp;Whitening Cream&nbsp;với th&agrave;nh phần tự nhi&ecirc;n cho bạn l&agrave;n da trắng hồng an to&agrave;n v&agrave; hiệu quả.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Đặc điểm nổi bật:</u></strong></span><br />\r\n<span style=\"font-size:14px\">- Kem dưỡng&nbsp;l&agrave;m trắng da Best Body Whitening cream gi&uacute;p&nbsp;</span><span style=\"font-size:14px\">ngăn ngừa sự h&igrave;nh th&agrave;nh sắc tố l&agrave;m tối da, levorotatory VC l&agrave;m da trắng, x&oacute;a vết đồi mồi, trị mụn, chống vi&ecirc;m, l&agrave;m s&aacute;ng da chống nắng, tinh chất l&ocirc; hội v&agrave; auxin trong t&aacute;o xanh gi&uacute;p cải thiện m&agrave;u sắc tự nhi&ecirc;n cho l&agrave;n da, l&agrave;m sạch, mềm mịn, trắng hồng, khỏe mạnh.<br />\r\n- Giữ độ ẩm cho da đặc biệt l&agrave; sau khi d&ugrave;ng kem tắm trắng, tạo sự đ&agrave;n hồi, k&iacute;ch th&iacute;ch t&aacute;i sinh tế b&agrave;o mới chống lại t&igrave;nh trạng biến đổi sắc tố do l&atilde;o ho&aacute;.<br />\r\n-&nbsp;Chống oxy h&oacute;a v&agrave; kh&aacute;ng khuẩn cao sẽ bảo vệ da khỏi t&igrave;nh trạng vi&ecirc;m nhiễm, cũng như t&aacute;i tạo l&agrave;n da trắng mịn v&agrave; trẻ trung bất ngờ khi sữ dụng. Đ&oacute; l&agrave; l&iacute; do v&igrave; sao, bạn n&ecirc;n d&ugrave;ng kết hợp c&ugrave;ng kem tắm trắng để nhanh ch&oacute;ng sở hữu l&agrave;n da s&aacute;ng hồng mịn m&agrave;ng.<br />\r\n-&nbsp;L&agrave;m trắng da, gi&uacute;p l&agrave;n da mịn m&agrave;ng, giảm vết nhăn cho da, ngăn ngừa l&atilde;o h&oacute;a v&agrave; săn chắc cơ mặt.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:16px\"><strong><u>Th&ocirc;ng tin sản phẩm:</u></strong></span><br />\r\n<span style=\"font-size:14px\"><strong>Th&agrave;nh phần</strong><br />\r\n- Tranexamic acid<br />\r\n- Th&agrave;nh phần Vitamin A &amp; C&nbsp;<br />\r\n- Tinh chất tr&agrave; xanh&nbsp;<br />\r\n- Bột ngọc trai tự nhi&ecirc;n<br />\r\n<strong>C&aacute;ch sử dụng:</strong><br />\r\n-&nbsp;Thoa đều kem v&agrave;o da sau khi tắm rửa sạch sẽ. Sau đ&oacute; massage nhẹ nh&agrave;ng cho đến khi kem ngấm hết v&agrave;o da.<br />\r\n<strong>Bảo quản</strong>:&nbsp;<br />\r\n-&nbsp;Bảo quản ở nhiệt độ ph&ograve;ng.<br />\r\n<strong>Xuất xứ:</strong> USA</span></p>\r\n', NULL, '2', 28, 0, 'bo-tap-an-pigeon-buoc-2-.jpg', 'kem-duong-lam-trang-da-toan-than-best-body-whitening-cream-hop-10g-01.jpg', '', '', '', '', '3000000', '0', 0, 0, 1, 1, 'kem-duong-lam-trang-da-toan-than-best-body-whitening-cream-hop-10g', 2850000, 2016, '', '', 0, NULL, 0, '', '', 0, 0, 2, 'honghue', 0, NULL, 0, NULL, '', 0),
(11, 'King Of ', '<p><font size=\"2\">Vi&ecirc;n Uống Hỗ Trợ Tăng Cường Sinh L&yacute; Nam Giới The King Of Xmen Hộp 12 vi&ecirc;n c&oacute; t&aacute;c dụng hỗ trợ qu&aacute; tr&igrave;nh hoạt động sinh l&yacute; đ&agrave;n &ocirc;ng&nbsp; trong c&aacute;c trường hợp&nbsp;yếu sinh l&yacute;, cường dương k&eacute;m, giảm ham muốn, sức bền k&eacute;m, ch&acirc;n tay yếu mềm...</font></p>\r\n', '<p style=\"text-align:justify\"><span style=\"font-size:14px\">Vi&ecirc;n Uống Hỗ Trợ Tăng Cường Sinh L&yacute; Nam Giới The King Of Xmen Hộp 12 vi&ecirc;n l&agrave; sản phẩm tăng cường sinh l&yacute; cho nam giới, th&agrave;nh phần l&agrave; những dược liệu được chiết xuất ho&agrave;n to&agrave;n từ tự nhi&ecirc;n, kh&ocirc;ng c&oacute; th&agrave;nh phần h&oacute;a học v&agrave; chất g&acirc;y nghiện.</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><u><strong><span style=\"font-size:16px\">Đặc điểm nổi bật:</span></strong></u></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Vi&ecirc;n Uống Hỗ Trợ Tăng Cường Sinh L&yacute; Nam Giới The King Of Xmen gi&uacute;p&nbsp;tăng cường sinh l&iacute; nam.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Hỗ trợ qu&aacute; tr&igrave;nh hoạt động sinh l&yacute; đ&agrave;n &ocirc;ng&nbsp;trong c&aacute;c trường hợp&nbsp;yếu sinh l&yacute;, cường dương k&eacute;m.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Giảm ham muốn, sức bền k&eacute;m, ch&acirc;n tay yếu mềm...</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><u><strong><span style=\"font-size:16px\">Th&ocirc;ng tin sản phẩm:</span></strong></u></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\"><strong>Đối tượng sử dụng:</strong> </span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Tất cả đối tượng l&agrave; nam giới trưởng th&agrave;nh v&agrave; kh&ocirc;ng mẫn cảm với c&aacute;c th&agrave;nh phần của sản phẩm.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">Ch&uacute; &yacute;:</span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng sử dụng cho người c&oacute; vấn đề về tim mạch hay tăng huyết &aacute;p.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Kh&ocirc;ng sử dụng qu&aacute; 01 vi&ecirc;n trong v&ograve;ng 24 giờ.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Để xa tầm tay trẻ em.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">C&aacute;ch d&ugrave;ng: </span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Uống 1 vi&ecirc;n với nước ấm trước khi giao hợp 30 &ndash; 45 ph&uacute;t.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">Bảo quản: </span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Bảo quản nơi sạch sẽ, tho&aacute;ng m&aacute;t, tr&aacute;nh tiếp x&uacute;c trực tiếp với &aacute;nh nắng mặt trời.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">Khối lượng tịnh:</span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- 500mg/1 vi&ecirc;n, 12 vi&ecirc;n/ hộp.</span></p>\r\n\r\n<p style=\"text-align:justify\"><strong><span style=\"font-size:14px\">Xuất xứ: </span></strong></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"font-size:14px\">- Mỹ _&nbsp;Sản xuất tại C&ocirc;ng ty K.T.N Group LLC</span></p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n', '', '19', -1, 0, 'bo-tap-an-pigeon-buoc-2--3.jpg', 'vien-uong-ho-tro-tang-cuong-sinh-ly-nam-gioi-the-king-of-xmen-hop-12-vien-01.jpg', '', '', '', '', '0', NULL, 0, 0, 0, 1, 'vien-uong-ho-tro-tang-cuong-sinh-ly-nam-gioi-the-king-of-xmen-hop-12-vien', 1300000, 2016, '', '', 0, NULL, 0, NULL, '', 1, 0, 1, 'kimanh', 0, NULL, 0, NULL, '', 0),
(13, 'Viên Uống  Weekend', '<div><font size=\"2\">Viên Uống Hỗ Trợ Tăng Cường Sinh Lý Nam Giới Super Long Weekend Hộp 12 Viên&nbsp;có tác dụng hỗ trợ tăng cường sinh lý, giúp tăng hiệu suất và khoái cảm trong việc chăn gối</font></div>', '<p><span style=\"font-size:14px\">Vi&ecirc;n Uống Hỗ Trợ Tăng Cường Sinh L&yacute; Nam Giới Super Long Weekend Hộp 12 Vi&ecirc;n&nbsp;chiết xuất từ c&aacute;c loại thảo dược tự nhi&ecirc;n gi&uacute;p hỗ trợ tăng cường chức năng sinh l&yacute;, gi&uacute;p tăng hiệu suất v&agrave; kho&aacute;i cảm trong việc chăn gối v&agrave; tăng cường chức năng sinh l&yacute;, trị yếu sinh l&yacute; v&agrave; rối loạn cường dương.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong><span style=\"font-size:16px\">Đặc điểm nổi b&acirc;t:</span></strong></u><br />\r\n<span style=\"font-size:14px\">- Vi&ecirc;n n&eacute;n Super Long Weekend 500mg d&ugrave;ng cho nam giới gi&uacute;p bổ sung dưỡng chất.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">- Hỗ trợ tăng cường sinh l&yacute;, gi&uacute;p tăng hiệu suất v&agrave; kho&aacute;i cảm trong việc chăn gối.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u><span style=\"font-size:16px\">Th&ocirc;ng tin sản phẩm:</span></u></strong></p>\r\n\r\n<p><span style=\"font-size:14px\"><strong>Quy c&aacute;ch:&nbsp;</strong><br />\r\n- 12 vi&ecirc;n / hộp.<br />\r\n<strong>Hướng dẫn sử dụng:</strong><br />\r\n- D&ugrave;ng trước khi quan hệ 30 ph&uacute;t&nbsp;<br />\r\n- Kh&ocirc;ng d&ugrave;ng qu&aacute; 1 vi&ecirc;n trong 24 giờ.<br />\r\nCảnh b&aacute;o thận trọng:<br />\r\n- Kh&ocirc;ng sử dụng nếu bạn bị tim mạch v&agrave; huyết &aacute;p cao.<br />\r\n- Kh&ocirc;ng d&ugrave;ng cho người dưới 18 tuổi.<br />\r\n<strong>Bảo quản: </strong></span></p>\r\n\r\n<p><span style=\"font-size:14px\">- Bảo quản nơi kh&ocirc; r&aacute;o v&agrave; tho&aacute;ng m&aacute;t.&nbsp;<br />\r\n<strong>Trọng lượng: </strong></span></p>\r\n\r\n<p><span style=\"font-size:14px\">- 10g / hộp<br />\r\n<strong>K&iacute;ch thước: </strong></span></p>\r\n\r\n<p><span style=\"font-size:14px\">- 12x12cm</span></p>\r\n', '', '19', -1, 0, 'bo-tap-an-pigeon-buoc-2--3.jpg', 'vien-uong-ho-tro-tang-cuong-sinh-ly-nam-gioi-super-long-weekend-hop-12-vien-01.jpg', '', '', '', '', '0', NULL, 0, 0, 0, 1, 'vien-uong-ho-tro-tang-cuong-sinh-ly-nam-gioi-super-long-weekend-hop-12-vien', 700000, 2016, '', '', 0, NULL, 0, NULL, '', 1, 0, 1, 'kimanh', 0, NULL, 0, NULL, '', 0),
(3388, 'Kính Mát Nam Velocity VL 5973 901 g', '<p style=\"text-align: justify;\">Thiết kế hiện đại, tinh tế, chất liệu cao cấp bền đẹp d&agrave;i l&acirc;u, gam m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng.</p>\r\n', '<p style=\"text-align: justify;\">K&iacute;nh M&aacute;t Nam Velocity VL 5973 901 Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng m&oacute;n phụ kiện gi&uacute;p ph&aacute;i mạnh th&ecirc;m sang trọng v&agrave; lịch l&atilde;m. Kiểu d&aacute;ng mắt k&iacute;nh phi c&ocirc;ng với lối c&aacute;ch điệu nhẹ nh&agrave;ng, tinh tế. L&agrave; gam m&agrave;u gi&uacute;p bạn dễ d&agrave;ng phối hợp với c&aacute;c phong c&aacute;ch thời trang, c&aacute;c kiểu quần &aacute;o kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Đặc điểm nổi bật</strong></u></h2>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5973 901 Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng được thiết kế với kiểu d&aacute;ng hiện đại, dựa tr&ecirc;n đường n&eacute;t của chiếc mắt k&iacute;nh phi c&ocirc;ng phổ biến. Lối c&aacute;ch điệu nhẹ nh&agrave;ng, thanh lịch, gi&uacute;p qu&yacute; &ocirc;ng tr&ocirc;ng sang trọng v&agrave; lịch l&atilde;m hơn. Sản phẩm được trau chuốt trong từng đường n&eacute;t, c&aacute;c g&oacute;c bo, khớp nối linh hoạt.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5973 901 Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng được gia c&ocirc;ng từ những chất liệu cao cấp, giữ được sự bền đẹp d&agrave;i l&acirc;u. Tr&ograve;ng k&iacute;nh polycarbonate với lớp phủ chống tia UV400, đ&aacute;p ứng những khuyến c&aacute;o bảo vệ mắt từ Hiệp hội nh&atilde;n khoa thế giới. Gọng k&iacute;nh hợp kim bền chắc được phủ lớp xi mạ chống gỉ kh&ocirc;ng bong tr&oacute;c theo thời gian.</p>\r\n\r\n<p style=\"text-align: justify;\">- Gam m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng. Đ&acirc;y cũng l&agrave; gam m&agrave;u gi&uacute;p bạn dễ d&agrave;ng phối hợp với c&aacute;c phong c&aacute;ch thời trang, c&aacute;c kiểu quần &aacute;o kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Th&ocirc;ng tin sản phẩm</strong></u></h2>\r\n\r\n<table cellspacing=\"0\" class=\"table table-bordered table-detail table-striped\" id=\"chi-tiet\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Velocity</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Xuất xứ thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">&Yacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Sản xuất tại</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Model</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">VL 5973 901</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Giới t&iacute;nh</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">K&iacute;ch cỡ</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Chiều d&agrave;i 16.5, chiều cao 7, chiều rộng 8.5 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '9', -1, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1.jpg', 'kinh-mat-nam-velocity-vl-5973-901-den-trong-mau-khoi-chinh-hang-1.jpg', 'kinh-mat-nam-velocity-vl-5973-901-den-trong-mau-khoi-chinh-hang-2.jpg', NULL, NULL, NULL, '480000', NULL, 0, 0, 0, 0, 'kinh-mat-nam-velocity-vl-5973-901-den-trong-mau-khoi-chinh-hang', 320000, 1489378684, '', '', 0, 4, 0, NULL, '', 1, NULL, 0, 'thanhdieu', 0, NULL, 0, NULL, NULL, NULL);
INSERT INTO `mn_product` (`Id`, `title_vn`, `description_vn`, `content_vn`, `digital`, `idcat`, `idmanufacturer`, `status`, `images`, `images1`, `images2`, `images3`, `images4`, `images5`, `price`, `codepro`, `sort`, `ticlock`, `hot`, `xlimit`, `alias`, `sale_price`, `date`, `meta_keyword`, `meta_description`, `empty`, `view`, `home`, `titlepage`, `tag`, `trash`, `iduser`, `oder`, `admin`, `idpercent`, `expired`, `xh`, `linkgoc`, `discount_code`, `code_type`) VALUES
(3389, 'Kính Mát Nam Velocity VL 5973 904 Đen Tròng Màu Xanh Chính Hãng', '<p style=\"text-align: justify;\">Thiết kế hiện đại, tinh tế, chất liệu cao cấp bền đẹp d&agrave;i l&acirc;u, gam m&agrave;u xanh nổi bật, bắt mắt.</p>\r\n', '<p style=\"text-align: justify;\">K&iacute;nh M&aacute;t Nam Velocity VL 5973 904 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng lối c&aacute;ch điệu nhẹ nh&agrave;ng, tinh tế. Tr&ograve;ng k&iacute;nh vu&ocirc;ng vắn, l&agrave;m nổi bật gương mặt g&oacute;c cạnh của ph&aacute;i mạnh.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Đặc điểm nổi bật</strong></u></h2>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5973 904 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng thiết kế đơn giản, nhưng kh&ocirc;ng hề đơn điệu. Tr&ograve;ng k&iacute;nh vu&ocirc;ng vắn, tạo sự c&acirc;n đối v&agrave; h&agrave;i h&ograve;a cho gương mặt. Mặt k&iacute;nh hơi chếch về ph&iacute;a sau tạo n&eacute;t c&aacute; t&iacute;nh mạnh mẽ.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>Sản phẩm được gia c&ocirc;ng từ những chất liệu cao cấp, giữ được sự bền đẹp d&agrave;i l&acirc;u. Tr&ograve;ng k&iacute;nh polycarbonate với lớp phủ chống tia UV400, đ&aacute;p ứng những khuyến c&aacute;o bảo vệ mắt từ Hiệp hội nh&atilde;n khoa thế giới. Gọng k&iacute;nh hợp kim bền chắc được phủ lớp xi mạ chống gỉ kh&ocirc;ng bong tr&oacute;c theo thời gian.</p>\r\n\r\n<p style=\"text-align: justify;\">- K&iacute;nh M&aacute;t Nam Velocity VL 5973 904 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng sở hữu t&ocirc;ng m&agrave;u xanh nổi bật, bắt mắt. Gọng đen hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng. Đ&acirc;y cũng l&agrave; gam m&agrave;u gi&uacute;p bạn dễ d&agrave;ng phối hợp với c&aacute;c phong c&aacute;ch thời trang, c&aacute;c kiểu quần &aacute;o kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Th&ocirc;ng tin sản phẩm</strong></u></h2>\r\n\r\n<table cellspacing=\"0\" class=\"table table-bordered table-detail table-striped\" id=\"chi-tiet\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Velocity</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Xuất xứ thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">&Yacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Sản xuất tại</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Model</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">VL 5973 904</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Giới t&iacute;nh</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">K&iacute;ch cỡ</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Chiều d&agrave;i 16.5, chiều cao 7, chiều rộng 8.5 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '3', 118, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1.jpg', 'kinh-mat-nam-velocity-vl-5973-904-den-trong-mau-xanh-chinh-hang-1.jpg', 'kinh-mat-nam-velocity-vl-5973-904-den-trong-mau-xanh-chinh-hang-2.jpg', NULL, NULL, NULL, '480000', NULL, 0, 0, 0, 0, 'kinh-mat-nam-velocity-vl-5973-904-den-trong-mau-xanh-chinh-hang', 320000, 1489379236, '', '', 0, 6, 0, NULL, '', 1, NULL, 0, 'thanhdieu', 0, NULL, 0, NULL, NULL, NULL),
(3390, 'Kính Mát Nam Velocity VL 5973 026 Đen Tròng Màu Xanh Chính Hãng', '<p style=\"text-align: justify;\">Tr&ograve;ng k&iacute;nh Polycarbonat si&ecirc;u nhẹ, tr&ograve;ng k&iacute;nh vu&ocirc;ng vắn c&aacute; t&iacute;nh, mạnh mẽ, gam m&agrave;u xanh nổi bật, bắt mắt.</p>\r\n', '<p style=\"text-align: justify;\">K&iacute;nh M&aacute;t Nam Velocity VL 5973 026 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng gi&uacute;p bạn nam th&ecirc;m phần sang trọng, lịch l&atilde;m, nhất l&agrave; c&aacute;c bạn tuổi từ 20-30. Trong những chuyến đi du lịch, d&atilde; ngoại, đ&acirc;y l&agrave; phụ kiện kh&ocirc;ng thể thiếu. Bạn cũng n&ecirc;n sử dụng k&iacute;nh m&aacute;t mỗi khi ra ngo&agrave;i v&agrave;o ban ng&agrave;y, tr&aacute;nh để tia UV, khỏi bụi g&acirc;y tổn hại đ&ocirc;i mắt.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Đặc điểm nổi bật</strong></u></h2>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5973 026 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng thiết kế đơn giản, nhưng kh&ocirc;ng hề đơn điệu. Tr&ograve;ng k&iacute;nh vu&ocirc;ng vắn, tạo sự c&acirc;n đối v&agrave; h&agrave;i h&ograve;a cho gương mặt. Mặt k&iacute;nh hơi chếch về ph&iacute;a sau tạo n&eacute;t c&aacute; t&iacute;nh mạnh mẽ.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>Sản phẩm được gia c&ocirc;ng từ những chất liệu cao cấp bền đẹp. Tr&ograve;ng k&iacute;nh Polycarbonat si&ecirc;u nhẹ, c&oacute; khả năng chịu lực va đập cao. Lớp phủ chống tia UV 400, đạt ti&ecirc;u chuẩn chỉ số chống tia cực t&iacute;m theo khuyến c&aacute;o của c&aacute;c hiệp hội nh&atilde;n khoa thế giới. Gọng hợp kim cao cấp, c&oacute; phủ lớp xi mạ chống gỉ.</p>\r\n\r\n<p style=\"text-align: justify;\">- K&iacute;nh M&aacute;t Nam Velocity VL 5973 026 Đen Tr&ograve;ng M&agrave;u Xanh Ch&iacute;nh H&atilde;ng sở hữu t&ocirc;ng m&agrave;u xanh nổi bật, bắt mắt. Gọng đen hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng. Đ&acirc;y cũng l&agrave; gam m&agrave;u gi&uacute;p bạn dễ d&agrave;ng phối hợp với c&aacute;c phong c&aacute;ch thời trang, c&aacute;c kiểu quần &aacute;o kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Th&ocirc;ng tin sản phẩm</strong></u></h2>\r\n\r\n<table cellspacing=\"0\" class=\"table table-bordered table-detail table-striped\" id=\"chi-tiet\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Velocity</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Xuất xứ thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">&Yacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Sản xuất tại</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Model</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">VL 5973 026</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Giới t&iacute;nh</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">K&iacute;ch cỡ</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Chiều d&agrave;i 16.5, chiều cao 7, chiều rộng 8.5 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '0', 118, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1.jpg', 'kinh-mat-nam-velocity-vl-5973-026-den-trong-mau-xanh-chinh-hang-1.jpg', 'kinh-mat-nam-velocity-vl-5973-026-den-trong-mau-xanh-chinh-hang-2.jpg', NULL, NULL, NULL, '480000', NULL, 0, 0, 0, 0, 'kinh-mat-nam-velocity-vl-5973-026-den-trong-mau-xanh-chinh-hang', 320000, 1489380452, '', '', 0, 5, 0, NULL, '', 1, NULL, 0, 'thanhdieu', 0, NULL, 0, NULL, NULL, NULL),
(3391, 'Kính Mát Nam Velocity VL 5973 001 - Đen Tròng Màu Khói Chính Hãng', '<p style=\"text-align: justify;\">Thiết kế đơn giản, kh&ocirc;ng đơn điệu, sở hữu t&ocirc;ng m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng.</p>\r\n', '<p style=\"text-align: justify;\">K&iacute;nh M&aacute;t Nam Velocity VL 5973 001 - Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng thiết kế trẻ trung, thanh lịch v&agrave; s&agrave;nh điệu. Tr&ograve;ng k&iacute;nh của k&iacute;nh m&aacute;t l&agrave;m từ chất liệu polycarbonat c&oacute; khả năng chống tia cực t&iacute;m, tia bức xạ, chống UV400. K&iacute;nh m&aacute;t l&agrave; d&ograve;ng phụ kiện kh&ocirc;ng thể của tất cả đối tượng.<br />\r\n.</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Đặc điểm nổi bật</strong></u></h2>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5973 001 - Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng thiết kế đơn giản, nhưng kh&ocirc;ng hề đơn điệu. Tr&ograve;ng k&iacute;nh vu&ocirc;ng vắn, tạo sự c&acirc;n đối v&agrave; h&agrave;i h&ograve;a cho gương mặt. Mặt k&iacute;nh hơi chếch về ph&iacute;a sau tạo n&eacute;t c&aacute; t&iacute;nh mạnh mẽ.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>Sản phẩm được gia c&ocirc;ng từ những chất liệu cao cấp bền đẹp. Tr&ograve;ng k&iacute;nh Polycarbonat si&ecirc;u nhẹ, c&oacute; khả năng chịu lực va đập cao. Lớp phủ chống tia UV 400, đạt ti&ecirc;u chuẩn chỉ số chống tia cực t&iacute;m theo khuyến c&aacute;o của c&aacute;c hiệp hội nh&atilde;n khoa thế giới. Gọng hợp kim cao cấp, c&oacute; phủ lớp xi mạ chống gỉ.</p>\r\n\r\n<p style=\"text-align: justify;\">- K&iacute;nh M&aacute;t Nam Velocity VL 5973 001 - Đen Tr&ograve;ng M&agrave;u Kh&oacute;i Ch&iacute;nh H&atilde;ng sở hữu t&ocirc;ng m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng. Đ&acirc;y cũng l&agrave; gam m&agrave;u gi&uacute;p bạn dễ d&agrave;ng phối hợp với c&aacute;c phong c&aacute;ch thời trang, c&aacute;c kiểu quần &aacute;o kh&aacute;c nhau.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Th&ocirc;ng tin sản phẩm</strong></u></h2>\r\n\r\n<table cellspacing=\"0\" class=\"table table-bordered table-detail table-striped\" id=\"chi-tiet\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Velocity</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Xuất xứ thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">&Yacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Sản xuất tại</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Model</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">VL 5973 001</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Giới t&iacute;nh</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">K&iacute;ch cỡ</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Chiều d&agrave;i 16.5, chiều cao 7, chiều rộng 8.5 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '0', 117, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1.jpg', 'kinh-mat-nam-velocity-vl-5973-001-den-trong-mau-khoi-chinh-hang-1.jpg', 'kinh-mat-nam-velocity-vl-5973-001-den-trong-mau-khoi-chinh-hang-2.jpg', NULL, NULL, NULL, '320000', NULL, 0, 0, 0, 0, 'kinh-mat-nam-velocity-vl-5973-001-den-trong-mau-khoi-chinh-hang', 320000, 1489388363, '', '', 0, 4, 0, NULL, '', 1, NULL, 0, 'thanhdieu', 0, NULL, 0, NULL, NULL, NULL),
(3392, 'Kính Mát Nam Velocity VL 5972 905 Đen Chính Hãng', '<p style=\"text-align: justify;\">Thiết kế đơn giản, kh&ocirc;ng đơn điệu, sở hữu t&ocirc;ng m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng.</p>\r\n', '<p style=\"text-align: justify;\">K&iacute;nh M&aacute;t Nam Velocity VL 5972 905 Đen Ch&iacute;nh H&atilde;ng l&agrave; lựa chọn l&yacute; tưởng cho những người hướng đến phong c&aacute;ch sang trọng, lịch thiệp, nhất l&agrave; khi bạn bước v&agrave;o độ tuổi 30. Kh&ocirc;ng chỉ đ&oacute;ng vai tr&ograve; bảo vệ mắt khỏi nắng , gi&oacute;, kh&oacute;i, bụi&hellip; h&agrave;ng ng&agrave;y, sản phẩm c&ograve;n l&agrave; m&oacute;n phụ kiện gi&uacute;p bạn thể hiện phong c&aacute;ch thời trang v&agrave; gu thẩm mỹ.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Đặc điểm nổi bật</strong></u></h2>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>K&iacute;nh M&aacute;t Nam Velocity VL 5972 905 Đen Ch&iacute;nh H&atilde;ng được thiết kế với kiểu gọng vu&ocirc;ng, tr&ograve;ng lớn. Sản phẩm dễ d&agrave;ng tạo điểm nhấn, sự h&agrave;i h&ograve;a cho gương mặt. Thiết kế đơn giản, kh&ocirc;ng cầu k&igrave;. T&iacute;nh thẩm mỹ thể hiện ở đường n&eacute;t h&agrave;i h&ograve;a, sắc sảo.Mặt k&iacute;nh hơi cong, chếch về ph&iacute;a sau, l&agrave;m tốt vai tr&ograve; bảo vệ mắt khỏi tia UV g&acirc;y hại. Những khớp nối được gia c&ocirc;ng tinh xảo, vừa đảm bảo độ bền, vừa thể hiện t&iacute;nh thẩm mỹ của một d&ograve;ng sản phẩm cao cấp</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>- </strong>Sản phẩm được gia c&ocirc;ng từ những chất liệu cao cấp, hạn chế t&aacute;c động của ngoại lực v&agrave; giữ được sự bền đẹp với thời gian. Gọng k&iacute;nh bằng hợp kim kh&ocirc;ng gỉ. Phần đệm sống mũi chế tạo từ silicon mềm, &ecirc;m. Tr&ograve;ng k&iacute;nh Polycarbonat cao cấp, c&oacute; khả năng chống trầy xước v&agrave; những va chạm kh&ocirc;ng mong muốn.</p>\r\n\r\n<p style=\"text-align: justify;\">- K&iacute;nh M&aacute;t Nam Velocity VL 5972 905 Đen Ch&iacute;nh H&atilde;ng sở hữu t&ocirc;ng m&agrave;u đen chủ đạo hạn chế ố m&agrave;u, vấy bẩn trong qu&aacute; tr&igrave;nh sử dụng. Đường viền tr&ograve;ng k&iacute;nh s&aacute;ng m&agrave;u tạo điểm nhấn ho&agrave;n hảo cho tổng thể thiết kế.<br />\r\n&nbsp;</p>\r\n\r\n<h2 style=\"text-align: justify;\"><u><strong>Th&ocirc;ng tin sản phẩm</strong></u></h2>\r\n\r\n<table cellspacing=\"0\" class=\"table table-bordered table-detail table-striped\" id=\"chi-tiet\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Velocity</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Xuất xứ thương hiệu</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">&Yacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Sản xuất tại</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Model</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">VL 5972 905</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">Giới t&iacute;nh</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align: justify;\">K&iacute;ch cỡ</td>\r\n			<td class=\"last\" style=\"text-align: justify;\">Chiều d&agrave;i 16.5, chiều cao 7, chiều rộng 8.5 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '29', 117, 0, 'bo-tap-an-chen-va-muong-pigeon-buoc-1.jpg', 'kinh-mat-nam-velocity-vl-5972-905-den-chinh-hang-1.jpg', NULL, NULL, NULL, NULL, '480000', NULL, 0, 0, 0, 0, 'kinh-mat-nam-velocity-vl-5972-905-den-chinh-hang', 480000, 1489389079, '', '', 0, 8, 0, NULL, '', 1, NULL, 0, 'thanhdieu', 0, NULL, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_provinces`
--

CREATE TABLE `mn_provinces` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `ship` int(11) DEFAULT NULL,
  `ticlock` int(11) DEFAULT NULL,
  `images` varchar(255) NOT NULL,
  `description_vn` text,
  `content_vn` longtext,
  `home` int(1) NOT NULL,
  `NoiBat` int(1) NOT NULL,
  `date` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_provinces`
--

INSERT INTO `mn_provinces` (`Id`, `title_vn`, `sort`, `parentid`, `ship`, `ticlock`, `images`, `description_vn`, `content_vn`, `home`, `NoiBat`, `date`, `alias`) VALUES
(65, 'Tài chính - Kế toán', 0, NULL, NULL, 0, 'PC-new2_jpg5.gif', '<p>des</p>\r\n', '<p>content</p>\r\n', 0, 0, 1546265001, 'tai-chinh-ke-toan'),
(66, 'Kỹ thuật điện tử - viễn thông', 0, NULL, NULL, 0, 'PC-new2_jpg4.gif', '<p>des</p>\r\n', '<p>content</p>\r\n', 0, 0, 1546265773, 'ky-thuat-dien-tu-vien-thong');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_reasoncancel`
--

CREATE TABLE `mn_reasoncancel` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_reasoncancel`
--

INSERT INTO `mn_reasoncancel` (`Id`, `title_vn`) VALUES
(1, 'Hủy vì gộp hoặc đặt trùng đơn hàng'),
(2, 'Hủy vì thời gian giao hàng lâu'),
(3, 'Hủy vì phí vận chuyển cao'),
(4, 'Hủy do cần thay đổi thông tin đơn hàng (địa chỉ nhận, màu sắc, kích cỡ, số lượng vv…)'),
(5, 'Hủy vì lý do khác');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_sessions`
--

CREATE TABLE `mn_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_sessions`
--

INSERT INTO `mn_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('3e2d9a3vql7d4a3hh14ia0nnqjqdl7ft', '::1', 1546354288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335343235343b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('grkihbcsm2okjdf9l23iqgo9q6gqe4ki', '::1', 1546353845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335333639383b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('eb634v3c34q673sru05ms223akmt1of3', '::1', 1546353568, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335333337343b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('mrq3p5r1bcq0v9mnamgmat6b2j4viche', '::1', 1546353011, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335323839303b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('20psohh94nq2qbohaacje997vjejuftb', '::1', 1546351884, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335313838323b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('k93cagf428oncniofa73igu2a3hgf3dg', '::1', 1546351483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335313433393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('rgplgp13tbn3nncf18p823hg8e6q1umn', '::1', 1546351306, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335313132323b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('mjnum9c2s0qb8t0242tft20vvsv1o0l9', '::1', 1546350807, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363335303637323b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('gefs9guvndoicit5sqlna01gl4bniv8t', '::1', 1546349645, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334393435303b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('ocbhgemebp5tn5a9avi5j3ifdjo17h2q', '::1', 1546349247, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334383939303b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('druafqveoh31i43p1uoca4k8tuaegl74', '::1', 1546340182, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333393838383b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('usmfclqvhpue9s2il2pi4gkrhsam2gi9', '::1', 1546339856, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333393538373b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('088qu5lqj46qpjojra0o3fbfhu4pb8jr', '::1', 1546339471, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333393235393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('5r2un4j766e53dneuk76io3n2rqgsfvi', '::1', 1546339064, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333383836393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('9op8404l04eje7lq6k4ogb3sd04s016r', '::1', 1546338847, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333383534393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('tc9uivq77g9jndbdpi3mnb8a5u4fv5ep', '::1', 1546338467, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333383231313b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('6jko6sc9r4u9ebjtkneht1k93olqm92l', '::1', 1546338189, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333373930393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('rlh1bqq3qjr85vcko4h556dl81993s4g', '::1', 1546337897, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333373630333b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('4cla0mrb8kesi2v8j74ut3k6872anevd', '::1', 1546337581, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333373239383b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('0ms1fthl5kqk97dld0equ1oq7teqpp5g', '::1', 1546337241, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333363939333b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('t7v230omemlcsooa5d0k6k2g068uhekb', '::1', 1546336960, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333363636303b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('qmrhgj3blhinrok6ljnkh4mafkni6uo3', '::1', 1546335427, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333353339373b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('1lf8iv3l8pici61ns29rfp7mfjicolki', '::1', 1546335272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333353032333b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('u88befh2cbg3g5qkvt30q00gbgtadkaq', '::1', 1546334983, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333343731393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('am4pet65bg7n8tm7tvbmuen6akm9cup6', '::1', 1546334694, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333343339353b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('l0aocpj3afhfcbmop010o3bh02f9uec5', '::1', 1546334166, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333343030373b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('qv6ds0rhspmvgn3bvooru30bcksio2ma', '::1', 1546348595, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334383337313b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('qokotm7bog1mcns09is5gvblqu0o7242', '::1', 1546348049, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334373938363b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('tj3u0q49tdf6465t3emk7n388ttdpmae', '::1', 1546347415, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334373336333b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('ke884psu7rncp2jepdhrfj5ku94gfbrp', '::1', 1546347954, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334373636393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('bammjbt9qti7shdr7tb8pppvf5eue4n4', '::1', 1546346834, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334363536393b),
('k035aj88df8s65jsi00584940rg0lgc4', '::1', 1546347155, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334363838363b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('475pt7m0nc3e438r4vsidv0jc3mt686o', '::1', 1546346527, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334363237363b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('7kbr2a1p2u9qqn19pblmkq6b13t057gt', '::1', 1546346276, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334363237353b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('hhvnj3i64mehn9udkj35rqf7q4lavofc', '::1', 1546346222, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334353936313b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('8n3qd9a1jrc3c8a9crjv6ksv74nf6qnp', '::1', 1546345772, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334353535373b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('17he2at6382ndfvo2d0kdlck75acuov9', '::1', 1546345391, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334353039373b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('b8f4pfktipg9rhqn13mv1suuaeb4s4og', '::1', 1546340360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363334303233323b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('8ehb1li9kp2sb314mlm3u44mi719ace5', '::1', 1546332845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333323732313b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('2p48fsffa4psbhomkjk761nv4vg9e0b8', '::1', 1546333526, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333333331363b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b),
('jpsuo6j8jhocn8ov2iqik0cuek6rc8rg', '::1', 1546333920, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363333333634393b6c6f67696e5f61646d696e5f69647c693a313b6c6f67696e5f61646d696e5f757365726e616d657c733a353a2261646d696e223b6c6f67696e5f61646d696e5f6c6576656c7c733a313a2231223b6c6f67696e5f61646d696e5f7569647c733a313a2231223b);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_size`
--

CREATE TABLE `mn_size` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_size`
--

INSERT INTO `mn_size` (`Id`, `title_vn`, `ticlock`, `sort`) VALUES
(2, 'M', 0, 0),
(3, 'S', 0, 0),
(4, 'L', 0, 0),
(5, 'XL', 0, 0),
(6, 'XXL', 0, 0),
(7, '35', 0, 0),
(8, '36', 0, 0),
(9, '37', 0, 0),
(10, '38', 0, 0),
(11, '39', 0, 0),
(12, '40', 0, 0),
(13, '41', 0, 0),
(14, '42', 0, 0),
(15, '43', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_student`
--

CREATE TABLE `mn_student` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_student`
--

INSERT INTO `mn_student` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `banggia`, `image_thumb`, `images`, `idcat`, `sort`, `ticlock`, `home`, `counter`, `NoiBat`, `date`, `tag`, `meta_keyword`, `meta_description`, `linkgoc`, `alias`) VALUES
(33, 'Test hd 1', NULL, '<p>Test hd 1</p>\r\n', NULL, '<p>Test hd 1</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362126.png', 26, 0, 0, 0, 0, 1, 1546224755, NULL, NULL, NULL, NULL, 'test-hd-1'),
(34, 'Test hd 2', NULL, '<p>Test hd 2</p>\r\n', NULL, '<p>Test hd 2</p>\r\n', NULL, '', NULL, 'noel_hutech-15456362127.png', 26, 0, 0, 0, 0, 1, 1546224774, NULL, NULL, NULL, NULL, 'test-hd-2');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_suppliers`
--

CREATE TABLE `mn_suppliers` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `postal_code` int(50) NOT NULL,
  `country` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_methods` int(11) NOT NULL,
  `discount_type` int(11) NOT NULL,
  `type_goods` int(11) NOT NULL,
  `discount_available` int(11) NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created` time NOT NULL,
  `modifield` time NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_support`
--

CREATE TABLE `mn_support` (
  `Id` int(11) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8,
  `date` datetime DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_support`
--

INSERT INTO `mn_support` (`Id`, `fullname`, `address`, `email`, `tel`, `content`, `date`, `iduser`) VALUES
(3, 'tieu de', NULL, NULL, NULL, 'noi dung', '2014-08-25 00:00:00', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_tags`
--

CREATE TABLE `mn_tags` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `titlepage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` tinyint(4) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `kodau` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_user`
--

CREATE TABLE `mn_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `avatar` varchar(255) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `idtinh` int(11) DEFAULT NULL,
  `iddistrict` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `ticlock` tinyint(4) NOT NULL DEFAULT '0',
  `lock` int(1) NOT NULL DEFAULT '0',
  `idfacebook` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `mn_user`
--

INSERT INTO `mn_user` (`id`, `username`, `password`, `email`, `fullname`, `birthday`, `phone`, `level`, `avatar`, `gender`, `address`, `idtinh`, `iddistrict`, `date`, `ticlock`, `lock`, `idfacebook`) VALUES
(131, 'colorstormcompany@gmail.com', '6bc9feb6ace59f83ccaebe46cae4ee64', 'colorstormcompany@gmail.com', 'Lê Hải Khương', NULL, '', 1, '', 1, '', 0, NULL, 1469427117, 0, 0, '0'),
(132, 'lehaikhuong@yahoo.com.vn', '6bc9feb6ace59f83ccaebe46cae4ee64', 'lehaikhuong@yahoo.com.vn', 'Lê Hải Khương', NULL, '', 1, '', 1, '', 0, NULL, 1469427147, 0, 0, '0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_voucher`
--

CREATE TABLE `mn_voucher` (
  `id` int(11) NOT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `start_day` int(11) NOT NULL,
  `end_day` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `modifeild` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_voucher`
--

INSERT INTO `mn_voucher` (`id`, `code`, `status`, `start_day`, `end_day`, `created`, `price`, `order_id`, `type`, `modifeild`) VALUES
(3, 'QIhnBoEokp', 0, 1469379600, 1472095588, 0, 100, 0, 0, 0),
(11, 'NUmNpMAz42', 0, 1469379600, 1472099163, 0, 20, 0, 1, 0),
(12, 'q9o0Z0GTyk', 0, 1469379600, 1472100584, 0, 100, 0, 0, 0),
(13, '3TXpRmw9LX', 0, 1469379600, 1472100656, 0, 20, 0, 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_voucher_type`
--

CREATE TABLE `mn_voucher_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  `modifeild` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `mn_voucher_type`
--

INSERT INTO `mn_voucher_type` (`id`, `price`, `status`, `type`, `created`, `modifeild`) VALUES
(1, '2000', 1, 0, 0, 0),
(2, '1000', 1, 0, 0, 0),
(3, '500', 1, 0, 0, 0),
(4, '200', 1, 0, 0, 0),
(5, '100', 1, 0, 0, 0),
(6, '20', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_weblink`
--

CREATE TABLE `mn_weblink` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) NOT NULL,
  `images` varchar(200) NOT NULL,
  `link` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `ticlock` int(11) NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `layout` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `iddeal` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_webplus`
--

CREATE TABLE `mn_webplus` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` int(1) NOT NULL DEFAULT '0',
  `date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mn_webplus`
--

INSERT INTO `mn_webplus` (`Id`, `title_vn`, `link`, `sort`, `ticlock`, `date`) VALUES
(24, 'School.com', 'www.com.vn', 0, 0, 1546250940),
(25, 'UniverS.com', 'UniverS.com', 0, 0, 1546252472);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mn_website`
--

CREATE TABLE `mn_website` (
  `id` int(11) NOT NULL,
  `description_vn` varchar(255) DEFAULT NULL,
  `keyword_vn` varchar(255) DEFAULT NULL,
  `title_vn` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleanalytics` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `enable` tinyint(4) DEFAULT NULL,
  `stamp` varchar(255) DEFAULT NULL,
  `hotline` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` text NOT NULL,
  `address` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `address3` text NOT NULL,
  `address4` text NOT NULL,
  `address5` text NOT NULL,
  `fax` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `mn_website`
--

INSERT INTO `mn_website` (`id`, `description_vn`, `keyword_vn`, `title_vn`, `message`, `email`, `googleanalytics`, `enable`, `stamp`, `hotline`, `footer`, `address`, `address1`, `address2`, `address3`, `address4`, `address5`, `fax`) VALUES
(1, '', '', 'School', '', 'email@gmail.com', '', 0, '0', '028 000 000', 'Trụ sở: 475A Điện Biên Phủ, P.25, Q.Bình Thạnh, TP.HCM\r\nCơ sở 475B: 475B Điện Biên Phủ, P.25, Q.Bình Thạnh, TP.HCM\r\nCơ sở Ung Văn Khiêm: 31/36 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM\r\nĐT: (028) 5445 7777 - Fax: (028) 5445 4444 - Email: hutech@hutech.edu.vn', 'Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM', 'Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM', '', '', '', '', '028 123 123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pro_color`
--

CREATE TABLE `pro_color` (
  `idpro` int(11) NOT NULL,
  `idcolor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pro_color`
--

INSERT INTO `pro_color` (`idpro`, `idcolor`) VALUES
(1962, 7),
(1962, 13),
(1962, 16),
(1962, 25),
(1962, 50),
(1589, 4),
(1589, 27),
(1589, 54),
(1643, 1),
(1643, 10),
(1643, 14),
(1643, 45),
(2089, 13),
(2089, 25),
(1655, 8),
(1655, 50),
(1893, 7),
(1893, 9),
(2041, 61),
(2038, 61),
(1874, 41),
(1874, 45),
(2334, 5),
(2343, 5),
(2400, 55),
(2502, 39),
(2023, 38),
(2022, 13),
(2510, 7),
(2508, 28),
(2507, 50),
(2506, 50),
(2505, 50),
(2500, 7),
(2499, 61),
(2497, 5),
(2496, 5),
(1931, 7),
(1931, 50),
(1931, 54),
(2331, 6),
(2330, 32),
(2330, 50),
(2329, 54),
(2469, 17),
(2449, 2),
(2448, 4),
(1654, 4),
(1654, 7),
(1654, 17),
(1654, 45),
(1654, 58),
(1654, 61),
(2247, 7),
(2247, 9),
(2374, 5),
(2373, 7),
(2372, 7),
(2371, 7),
(2370, 5),
(2369, 7),
(2368, 18),
(2367, 13),
(2366, 5),
(2365, 7),
(2364, 7),
(1858, 4),
(1858, 5),
(1858, 54),
(1858, 61),
(1860, 7),
(1860, 13),
(1860, 31),
(1860, 45),
(1856, 7),
(1856, 21),
(1856, 41),
(1853, 7),
(1853, 21),
(1848, 7),
(1848, 13),
(1848, 26),
(1848, 47),
(1846, 7),
(1846, 50),
(1838, 7),
(1838, 50),
(1838, 61),
(1835, 7),
(1835, 32),
(1835, 54),
(1835, 61),
(1827, 7),
(1827, 50),
(1827, 61),
(1824, 7),
(1824, 47),
(1909, 7),
(1909, 50),
(1909, 61),
(1909, 62),
(1908, 7),
(1908, 13),
(1908, 47),
(1908, 50),
(1907, 7),
(1907, 50),
(1907, 54),
(1907, 61),
(1904, 49),
(1904, 50),
(1904, 54),
(1904, 61),
(1902, 7),
(1902, 50),
(1902, 54),
(1900, 50),
(1900, 61),
(1900, 62),
(1898, 7),
(1898, 31),
(1878, 26),
(1867, 6),
(1867, 7),
(1867, 13),
(1867, 34),
(1867, 40),
(1865, 18),
(1865, 26),
(1864, 18),
(1864, 50),
(1864, 54),
(1864, 61),
(1864, 62),
(1953, 4),
(1953, 34),
(1953, 50),
(1953, 62),
(1952, 4),
(1952, 27),
(1952, 41),
(1952, 45),
(1952, 50),
(1952, 59),
(1952, 62),
(1951, 61),
(1948, 7),
(1948, 31),
(1944, 13),
(1944, 16),
(1943, 7),
(1943, 13),
(1943, 53),
(1942, 7),
(1942, 13),
(1942, 38),
(1937, 7),
(1937, 13),
(1937, 26),
(1932, 7),
(1932, 61),
(1930, 7),
(1930, 53),
(1929, 7),
(1929, 54),
(1929, 61),
(1920, 13),
(1920, 26),
(1918, 7),
(1918, 13),
(1918, 43),
(1918, 45),
(1918, 54),
(1917, 6),
(1917, 7),
(1917, 31),
(1912, 13),
(1912, 26),
(1912, 50),
(1912, 61),
(1912, 62),
(2011, 1),
(2011, 7),
(2011, 50),
(1995, 7),
(1995, 9),
(1990, 14),
(1990, 16),
(1990, 40),
(2047, 50),
(2046, 8),
(2046, 50),
(2040, 11),
(2040, 16),
(2039, 7),
(2347, 6),
(2346, 5),
(2341, 7),
(2328, 18),
(2328, 52),
(2327, 52),
(1871, 7),
(1871, 54),
(1871, 61),
(1871, 62),
(1861, 13),
(1861, 34),
(1861, 50),
(1861, 54),
(1861, 61),
(1852, 50),
(1852, 54),
(1852, 61),
(1852, 62),
(1845, 5),
(1845, 7),
(1845, 13),
(1845, 54),
(1841, 7),
(1841, 13),
(1841, 26),
(1841, 47),
(1837, 6),
(1837, 7),
(1837, 54),
(1837, 61),
(1821, 7),
(1821, 54),
(1821, 61),
(1820, 7),
(1820, 54),
(1820, 61),
(1816, 50),
(1816, 54),
(2239, 17),
(2239, 45),
(2238, 30),
(2535, 24),
(2535, 41),
(2538, 54),
(2537, 7),
(2539, 7),
(2536, 7),
(2542, 7),
(2546, 7),
(2543, 7),
(2541, 7),
(2540, 7),
(2548, 54),
(2549, 54),
(2551, 7),
(2553, 5),
(2556, 7),
(2558, 7),
(2555, 26),
(2552, 7),
(2550, 54),
(2559, 7),
(2557, 7),
(2560, 7),
(2561, 7),
(2568, 61),
(2566, 50),
(2565, 45),
(2564, 7),
(2569, 7),
(2570, 7),
(2572, 61),
(2571, 7),
(2574, 7),
(2576, 45),
(2573, 7),
(2578, 7),
(2579, 2),
(2581, 7),
(2583, 7),
(2584, 50),
(2585, 4),
(2587, 7),
(2588, 5),
(2589, 7),
(2590, 7),
(2592, 7),
(2593, 4),
(2595, 26),
(2597, 18),
(2599, 26),
(2601, 54),
(2600, 26),
(2603, 8),
(2605, 54),
(2604, 26),
(2607, 7),
(2359, 7),
(2358, 7),
(2357, 7),
(2355, 7),
(2354, 5),
(2353, 7),
(2348, 5),
(2351, 6),
(2349, 6),
(2628, 11),
(2626, 7),
(2624, 7),
(2624, 8),
(2624, 50),
(2624, 61),
(2622, 7),
(2620, 7),
(2618, 7),
(2621, 32),
(1965, 4),
(2613, 61),
(2639, 61),
(2642, 7),
(2649, 7),
(2650, 7),
(2661, 7),
(2661, 61),
(2676, 3),
(2676, 4),
(2676, 9),
(2676, 45),
(2676, 50),
(2676, 59),
(2676, 62),
(2677, 4),
(2677, 19),
(2679, 4),
(2679, 19),
(2681, 7),
(2706, 4),
(2706, 17),
(2706, 45),
(2706, 62),
(2717, 26),
(2716, 26),
(2709, 26),
(2722, 26),
(2729, 7),
(2733, 7),
(2736, 7),
(2741, 7),
(2751, 54),
(2707, 1),
(2563, 4),
(2562, 7),
(1922, 7),
(2378, 54),
(2379, 54),
(2380, 7),
(2377, 54),
(2381, 7),
(2382, 4),
(2383, 8),
(2385, 7),
(2386, 2),
(2388, 7),
(2389, 7),
(2390, 7),
(2391, 7),
(2392, 7),
(2393, 7),
(2395, 7),
(2397, 32),
(2398, 32),
(2399, 10),
(2401, 55),
(2402, 55),
(2406, 32),
(2375, 5),
(2612, 7),
(1935, 3),
(1935, 59),
(3073, 23),
(3073, 50),
(0, 7),
(0, 61),
(3106, 24),
(3104, 16),
(3102, 9),
(3100, 26),
(3096, 7),
(3109, 9),
(3131, 9),
(2376, 7),
(3092, 7),
(3092, 61),
(3091, 54),
(3088, 4),
(3088, 27),
(3088, 62),
(3093, 7),
(3093, 61),
(3085, 4),
(3083, 4),
(3083, 27),
(3083, 62),
(3081, 45),
(3081, 54),
(3081, 62),
(3080, 4),
(3080, 34),
(3080, 54),
(3080, 62),
(3077, 7),
(3084, 61),
(3087, 61),
(3078, 61),
(3089, 60),
(3090, 61),
(3200, 54),
(3198, 62),
(3196, 25),
(3195, 39),
(3193, 47),
(3191, 54),
(3189, 7),
(3187, 25),
(3185, 54),
(3184, 25),
(3181, 8),
(3177, 61),
(3175, 54),
(3173, 25),
(3169, 25),
(3167, 7),
(3166, 30),
(3161, 38),
(3159, 54),
(3157, 25),
(3154, 19),
(3152, 9),
(3150, 25),
(3149, 25),
(3147, 7),
(3145, 9),
(3140, 38),
(3138, 40),
(3136, 8),
(3134, 9),
(3128, 7),
(3094, 54),
(3126, 25),
(3124, 9),
(3121, 50),
(3212, 10),
(3119, 47),
(3114, 25),
(3208, 7),
(3107, 42),
(3101, 38),
(3101, 47),
(3099, 4),
(3204, 7),
(3197, 6),
(3194, 31),
(3192, 9),
(3190, 9),
(3211, 7),
(3186, 11),
(3183, 26),
(3182, 41),
(3178, 7),
(3176, 40),
(3165, 7),
(3163, 61),
(3160, 54),
(3158, 41),
(3155, 7),
(3153, 40),
(3144, 7),
(3143, 6),
(3142, 7),
(3139, 7),
(3137, 6),
(3135, 9),
(3133, 9),
(3123, 6),
(3120, 7),
(3111, 9);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pro_size`
--

CREATE TABLE `pro_size` (
  `idpro` int(11) NOT NULL,
  `idsize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pro_size`
--

INSERT INTO `pro_size` (`idpro`, `idsize`) VALUES
(2662, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `songuoi`
--

CREATE TABLE `songuoi` (
  `ID` bigint(20) NOT NULL,
  `NUMBER` bigint(20) DEFAULT NULL,
  `DATE` bigint(20) DEFAULT NULL,
  `NUMYES` int(11) NOT NULL,
  `NUMWEEK` int(11) NOT NULL,
  `WEEK` int(11) NOT NULL,
  `NUMMONTH` int(11) NOT NULL,
  `MONTH` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `songuoi`
--

INSERT INTO `songuoi` (`ID`, `NUMBER`, `DATE`, `NUMYES`, `NUMWEEK`, `WEEK`, `NUMMONTH`, `MONTH`) VALUES
(1, 22, 23, 27, 23, 0, 322, 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `spr_app_routes`
--

CREATE TABLE `spr_app_routes` (
  `id` int(11) NOT NULL,
  `key` varchar(250) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `ward_id` int(11) NOT NULL,
  `street_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `search_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `stats`
--

CREATE TABLE `stats` (
  `id` int(11) UNSIGNED NOT NULL,
  `s_time` int(10) DEFAULT '0',
  `s_id` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `stats`
--

INSERT INTO `stats` (`id`, `s_time`, `s_id`) VALUES
(318900, 1471485508, '80dae7820699fee1b52c8963fc999a27f19d7dce'),
(318899, 1471485163, '9689cf422116faca7be4958284944711be8a595c');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_counter`
--

CREATE TABLE `tbl_counter` (
  `hits` bigint(20) NOT NULL DEFAULT '0',
  `realhits` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tbl_counter`
--

INSERT INTO `tbl_counter` (`hits`, `realhits`) VALUES
(1464629, 1464628);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_admin`
--

CREATE TABLE `tv_admin` (
  `Id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(5) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_admin`
--

INSERT INTO `tv_admin` (`Id`, `username`, `password`, `email`, `level`, `time`, `fullname`, `note`, `address`) VALUES
(1, 'admin', '47c1b65b3fc3d0e6dfdad0082b2abc60', 'admin@mada.vn', 1, '2013-09-24 02:10:38', '', '', ''),
(5, 'puka', '4be0240804e443d97551129f588e3773', 'pubreson88@gmail.com', 1, '0000-00-00 00:00:00', 'Pu Bre Son', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_adminmenu`
--

CREATE TABLE `tv_adminmenu` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `parentid` int(11) DEFAULT NULL,
  `ticlock` tinyint(4) NOT NULL DEFAULT '0',
  `route` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_adminmenu`
--

INSERT INTO `tv_adminmenu` (`Id`, `title_vn`, `images`, `sort`, `parentid`, `ticlock`, `route`) VALUES
(1, 'Cấu hình site', 'icon-48-cauhinh.png', 0, 1, 0, 'admincp/website'),
(2, 'Admin', 'icon-48-user.png', 0, 1, 0, 'admincp/admin'),
(3, 'Banner', 'icon-48-image.png', 0, 1, 0, 'admincp/flash'),
(4, 'Thành viên', 'icon-48-nhom.png', 13, 3, 0, 'admincp/user'),
(13, 'Trang', 'icon-48-templates.png', 10, 3, 0, 'admincp/pagehtml'),
(18, 'Sản Phẩm', 'icon-48-product.png', 5, 3, 0, 'admincp/product'),
(19, 'Danh mục', 'icon-48-doituong.png', 4, 3, 0, 'admincp/catelog'),
(31, 'Hỏi đáp', 'icon-48-faq.png', 10, 3, 0, 'admincp/catnews'),
(26, 'Đơn hàng', 'icon-48-checklist.png', 9, 3, 0, 'admincp/payment'),
(32, 'Quote', 'icon-48-employee.png', 0, 3, 0, '/admincp/quote');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_catnews`
--

CREATE TABLE `tv_catnews` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_catnews`
--

INSERT INTO `tv_catnews` (`Id`, `title_vn`, `title_en`, `parentid`, `sort`, `ticlock`, `tag`, `meta_keyword`, `meta_description`, `alias`, `home`) VALUES
(2, 'Nguyên tắc hoạt động', '', 0, 2, '0', NULL, '', '', 'nguyen-tac-hoat-dong', 1),
(3, 'Hoa hồng & Thanh toán', '', 0, 1, '0', NULL, '', '', 'hoa-hong-thanh-toan', 1),
(4, 'Người mua cần biết', '', 0, 3, '0', NULL, '', '', 'nguoi-mua-can-biet', 1),
(5, 'Hỗ trợ khách hàng', '', 0, 4, '0', NULL, '', '', 'ho-tro-khach-hang', 1),
(8, 'Hướng dẫn', '', 0, 0, '0', NULL, '', '', 'Huong-dan', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_guid`
--

CREATE TABLE `tv_guid` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `banggia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcat` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `home` tinyint(1) NOT NULL,
  `counter` int(11) NOT NULL,
  `NoiBat` tinyint(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkgoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_manufacturer`
--

CREATE TABLE `tv_manufacturer` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `parentid` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_manufacturer`
--

INSERT INTO `tv_manufacturer` (`Id`, `title_vn`, `title_en`, `ticlock`, `parentid`, `sort`, `description_vn`) VALUES
(16, 'Cơ bản về Chương trình', NULL, '0', 0, 1, ''),
(17, 'Hoa Hồng & Phương Thức Thanh Toán', NULL, '0', 0, 2, ''),
(18, 'Công Cụ & Hỗ Trợ Kỹ Thuật', NULL, '0', 0, 3, ''),
(21, 'Chương trình CTV Mada.vn là gì?', NULL, '0', 16, 1, 'L&agrave; chương tr&igrave;nh hợp t&aacute;c chia sẻ % hoa hồng tr&ecirc;n mỗi đơn h&agrave;ng ph&aacute;t sinh do CTV giới thiệu đến bằng c&aacute;ch: đặt banner l&ecirc;n website, share - chia sẻ link tr&ecirc;n c&aacute;c trang mạng x&atilde; hội, diễn đ&agrave;n...&nbsp;'),
(22, 'Khi tham gia CTV của Mada.vn, tôi có phải đóng phí gì không?', NULL, '0', 16, 2, 'Kh&ocirc;ng. Chương tr&igrave;nh Th&agrave;nh vi&ecirc;n Affiliate Mada.vn&nbsp;l&agrave; ho&agrave;n to&agrave;n miễn ph&iacute;, bạn sẽ kh&ocirc;ng mất bất kỳ khoản chi ph&iacute; n&agrave;o. Khi tham gia chương tr&igrave;nh n&agrave;y, bạn chỉ việc kiếm tiền v&agrave; r&uacute;t về t&agrave;i khoản của bạn m&agrave; th&ocirc;i.'),
(23, 'Điều kiện đăng ký và cách đăng ký CTV Mada.vn như thế nào?', NULL, '0', 16, 3, '&nbsp;Cực kỳ đơn giản. Bạn chỉ cần l&agrave; c&ocirc;ng d&acirc;n Việt Nam từ 16 tuổi trở l&ecirc;n, c&oacute; th&ocirc;ng tin li&ecirc;n hệ r&otilde; r&agrave;ng ( Email, số điện thoại, địa chỉ) v&agrave; bạn y&ecirc;u th&iacute;ch c&ocirc;ng việc chia sẻ. Để đăng k&yacute; t&agrave;i khoản CTV Affiliate của Mada.vn, bạn chỉ cần nhấp v&agrave;o &lt;ĐĂNG K&Yacute;&gt; ph&iacute; tr&ecirc;n v&agrave; điền đầy đủ th&ocirc;ng tin, sau đ&oacute; x&aacute;c nhận Email của bạn v&agrave; bạn đ&atilde; c&oacute; thể kiếm tiền với Mada.vn&nbsp;rồi.'),
(24, 'Mada.vn chi trả hoa hồng cho CTV là bao nhiêu?', NULL, '0', 17, 1, '&nbsp;Hiện nay, Mada.vn&nbsp;đang &aacute;p dụng ch&iacute;nh s&aacute;ch chi trả hoa hồng cho CTV Affliate dựa theo từng ph&acirc;n loại danh mục sản phẩm với mức % hoa hồng giao động từ 10% - 50%. Xem chi tiết tại'),
(25, 'Tôi có bị giới hạn về thu nhập với Mada.vn hay không?', NULL, '0', 17, 2, '&nbsp;Ho&agrave;n to&agrave;n kh&ocirc;ng. Với chương tr&igrave;nh CTV li&ecirc;n kết của Mada.vn, bạn c&oacute; thể c&oacute; mức thu nhập l&agrave; kh&ocirc;ng giới hạn. Thu nhập của bạn ho&agrave;n to&agrave;n phụ thuộc v&agrave;o năng lực v&agrave; c&ocirc;ng sức m&agrave; bạn bỏ ra.'),
(27, 'Tôi sẽ nhận được thu nhập từ Mada.vn như thế nào?', NULL, '0', 17, 3, 'Sau 20 ng&agrave;y kể từ khi Mada.vn&nbsp;x&aacute;c nhận đơn h&agrave;ng được giới thiệu bởi bạn l&agrave; Giao h&agrave;ng th&agrave;nh c&ocirc;ng th&igrave; bạn c&oacute; quyền y&ecirc;u cầu r&uacute;t tiền hoa hồng về t&agrave;i khoản ng&acirc;n h&agrave;ng của bạn. Lưu &yacute;: Số tiền mỗi lượt r&uacute;t phải lớn hơn hoặc bằng 200.000đ. &nbsp;'),
(28, 'Tôi giới thiệu bạn bè tham gia CTV Mada.vn có được gì không?', NULL, '0', 16, 5, '&nbsp;Chắc chắn l&agrave; c&oacute;. Mada.vn hiện đang thực hiện ch&iacute;nh s&aacute;ch chia sẻ hoa hồng 10% tr&ecirc;n tổng thu nhập của người m&agrave; bạn giới thiệu. Khoản % hoa hồng n&agrave;y được hiểu l&agrave; Mada.vn biết ơn bạn đ&atilde; giới thiệu v&agrave; mang đến cho Mada.vn&nbsp;một cộng sự mới nhờ c&ocirc;ng của bạn.'),
(29, 'Mada.vn có hỗ trợ công cụ quảng cáo, bán hàng gì không?', NULL, '0', 18, 1, '&nbsp;Mada.vn&nbsp;hiện đang hỗ trợ CTV c&aacute;c c&ocirc;ng cụ như sau: Hệ thống Banner quảng c&aacute;o đa dạng, Hệ thống link giới thiệu trực tiếp, Hệ thống link giới thiệu th&ocirc;ng minh trong từng sản phẩm, Hệ thống Like, Share giới thiệu th&ocirc;ng minh...'),
(31, 'Hệ thống lưu trữ Cookie của Mada.vn có hiệu lực trong bao lâu?', NULL, '0', 18, 4, '&nbsp;Hệ thống lưu trữ Cookie của Mada.vn&nbsp;c&oacute; hiệu lực l&ecirc;n đến 30 ng&agrave;y. Tức l&agrave; kh&aacute;ch h&agrave;ng gh&eacute; thăm website th&ocirc;ng qua link giới thiệu của bạn th&igrave; trong v&ograve;ng 30 ng&agrave;y sau đ&oacute; c&oacute; ph&aacute;t sinh đơn h&agrave;ng th&igrave; đơn h&agrave;ng đ&oacute; vẫn được t&iacute;nh hoa hồng l&agrave; do bạn giới thiệu ( Trừ trường hợp kh&aacute;ch h&agrave;ng đ&oacute; đ&atilde; x&oacute;a Cookie hoặc nhấp v&agrave;o link giới thiệu của người kh&aacute;c). &nbsp;'),
(32, 'Tôi có được quyền tạo ra các quảng cáo, banner riêng để quảng bá Mada.vn không?', NULL, '0', 18, 2, '&nbsp;Tất nhi&ecirc;n l&agrave; được. Bạn ho&agrave;n to&agrave;n c&oacute; thể tạo ra c&aacute;c Banner quảng c&aacute;o, h&igrave;nh ảnh quảng c&aacute;o của ri&ecirc;ng bạn để giới thiệu về Mada.vn&nbsp;nhưng bạn phải đọc kỹ c&aacute;c điều khoản cấm tại , đặc biệt l&agrave; kh&ocirc;ng tạo nội dung sai lệch, đ&aacute;nh lừa kh&aacute;ch h&agrave;ng nhằm tạo giao dịch kh&ocirc;ng hợp lệ.'),
(34, 'Có bắt buộc phải có website thì mới tham gia CTV Mada.vn không?', NULL, '0', 16, 4, '&nbsp;Điều n&agrave;y l&agrave; kh&ocirc;ng bắt buộc. Nếu bạn đang sở hữu 1 website, đ&oacute; l&agrave; một lợi thế rất lớn để bắt đầu kiếm tiền tại Mada.vn. Nhưng nếu bạn kh&ocirc;ng c&oacute; website n&agrave;o, bạn vẫn c&oacute; thể đăng k&yacute; v&agrave; kiếm tiền với Mada.vn bằng c&aacute;ch chia sẻ link giới thiệu, link b&aacute;n h&agrave;ng tới bạn b&egrave;, người th&acirc;n, cộng đồng qua c&aacute;c phương tiện như: Mạng x&atilde; hội, diễn đ&agrave;n, chat...');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_menu`
--

CREATE TABLE `tv_menu` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `ticlock` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tv_menu`
--

INSERT INTO `tv_menu` (`Id`, `title_vn`, `link`, `images`, `sort`, `ticlock`, `hot`, `parentid`, `style`, `icon`) VALUES
(6, 'Đăng Nhập', '#6', '', 6, 1, NULL, 0, 0, NULL),
(2, 'Giới thiệu', 'nguyen-tac-hoat-dong.html', '', 1, 0, NULL, 0, 0, NULL),
(4, 'Hoa hồng & Thanh toán', 'hoa-hong-thanh-toan.html', '', 2, 0, NULL, 0, 0, NULL),
(5, 'Đăng Nhập', 'huong-dan.html', '', 3, 0, NULL, 0, 0, NULL),
(7, 'Đăng Ký', '#', '', 8, 1, NULL, 0, 0, NULL),
(8, 'Hỏi đáp', 'hoi-dap.html', '', 4, 0, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_pagehtml`
--

CREATE TABLE `tv_pagehtml` (
  `Id` int(11) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_vn` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `content_vn` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `ticlock` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_pagehtml`
--

INSERT INTO `tv_pagehtml` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `sort`, `ticlock`, `alias`) VALUES
(9, 'Quy Chế Hoạt Động', '', NULL, NULL, '<p style=\"margin-left: 40px;\"><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n\r\n<p style=\"text-align: center;\"><span style=\"font-size: 22px;\"><b><span style=\"line-height: 115%; font-family: \'Times New Roman\', serif;\">QUY CHẾ HOẠT ĐỘNG</span></b></span></p>\r\n\r\n<p>&nbsp;&nbsp;</p>\r\n\r\n<p align=\"center\" class=\"MsoNormal\" style=\"margin: 0in -13.95pt 10pt 80px; text-align: center; text-indent: -13.5pt;\"><b><span new=\"\" style=\"font-size:22.0pt;line-height:\r\n115%;font-family:\" times=\"\"><o:p></o:p></span></b></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 1: PHẠM VI QUY CHẾ</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; C&aacute;c Điều khoản v&agrave; Điều kiện chung cho Cộng T&aacute;c Vi&ecirc;n&nbsp;quảng c&aacute;o &aacute;p dụng đối với quan hệ hợp đồng giữa:</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; Đơn vị chủ quản Website TMĐT MADA.VN</strong>:</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>C&Ocirc;NG TY CP THƯƠNG MẠI V&Agrave; ĐẦU TƯ GNI</strong>&nbsp;( Sau đ&acirc;y gọi tắt l&agrave; &quot;<b>Mada.vn</b>&quot;)</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Số giấy ph&eacute;p kinh doanh: &nbsp; &nbsp; 0312828906 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cấp ng&agrave;y: &nbsp; &nbsp;23/06/2014</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Địa chỉ: 30 Nguyễn Minh Ho&agrave;ng, Phường 12, Quận T&acirc;n B&igrave;nh, Tp. Hồ Ch&iacute; Minh, Việt Nam.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; v&agrave; <strong>C&Aacute;C CỘNG T&Aacute;C VI&Ecirc;N&nbsp;</strong></span><span style=\"font-size: 14px;\"><strong>THAM GIA MẠNG LI&Ecirc;N KẾT TIẾP THỊ MADA.VN</strong>&nbsp;(sau đ&acirc;y gọi l&agrave; &quot;<b>CTV</b>&quot;).</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Điều khoản v&agrave; điều kiện của CTV&nbsp;đ&ograve;i hỏi phải c&oacute; được sự đồng &yacute; bằng văn bản của Mada.vn&nbsp;v&agrave; do đ&oacute; sẽ kh&ocirc;ng được &aacute;p dụng ngay cả khi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> kh&ocirc;ng phản đối t&iacute;nh hợp lệ của ch&uacute;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 2: HỢP ĐỒNG H&Igrave;NH TH&Agrave;NH</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; Hợp đồng giữa </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> v&agrave; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> về việc đặt vị tr&iacute; quảng c&aacute;o của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> được thực hiện độc quyền th&ocirc;ng qua hệ thống v&agrave; quy định của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. Trong trường hợp m&agrave; c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> gửi đơn xin tham gia v&agrave;o Chương tr&igrave;nh li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">, th&igrave; phải chấp nhận c&aacute;c Điều khoản chung v&agrave; điều kiện của Nh&agrave; li&ecirc;n kết.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; C&aacute;c biểu mẫu đăng k&yacute; c&ugrave;ng với c&aacute;c Điều khoản v&agrave; Điều kiện chung v&agrave; chấp nhận v&agrave;o chương tr&igrave;nh sẽ c&ugrave;ng nhau tạo th&agrave;nh một thỏa thuận khung giữa </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> (nh&agrave; quảng c&aacute;o) v&agrave; c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> (nh&agrave; li&ecirc;n kết).</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 3: CHỦ THỂ CỦA HỢP ĐỒNG</strong>&nbsp;</span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp;1)</strong> C&aacute;c chủ thể của hợp đồng n&agrave;y sẽ được tham gia v&agrave;o Chương tr&igrave;nh tiếp thị li&ecirc;n kết v&agrave; khuyến m&atilde;i của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> như một nh&agrave; li&ecirc;n kết. Để thực hiện điều n&agrave;y, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ lựa chọn c&aacute;c tư liệu quảng c&aacute;o (v&iacute; dụ như c&aacute;c banner quảng c&aacute;o, c&aacute;c n&uacute;t, li&ecirc;n kết văn bản, nguồn cấp dữ liệu sản phẩm, mẫu bản tin) d&agrave;nh cho đối t&aacute;c như một nh&agrave; quảng c&aacute;o th&ocirc;ng qua nền tảng ri&ecirc;ng của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; 2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> phải chịu tr&aacute;ch nhiệm cho việc đặt c&aacute;c tư liệu quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> tr&ecirc;n trang web của m&igrave;nh (sau đ&acirc;y gọi l&agrave; &quot;Trang web của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&quot;) đăng k&yacute; trong Chương tr&igrave;nh tiếp thị li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được tự do quyết định vị tr&iacute; v&agrave; thời gian để đặt c&aacute;c t&agrave;i liệu quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> tr&ecirc;n Website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; quyền loại bỏ c&aacute;c t&agrave;i liệu quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> bất kỳ l&uacute;c n&agrave;o. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> chỉ được ph&eacute;p đặt t&agrave;i liệu quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> tr&ecirc;n Website </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> chỉ sau khi đăng k&yacute; v&agrave; được </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> chấp thuận.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp;3)</strong> Đổi lại cho việc quảng b&aacute; v&agrave; m&ocirc;i giới th&agrave;nh c&ocirc;ng giao dịch, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ nhận được từ </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> một khoản hoa hồng m&ocirc;i giới phụ thuộc v&agrave;o mức độ v&agrave; gi&aacute; trị thực của dịch vụ.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp;4)</strong> Chương tr&igrave;nh tiếp thị li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ kh&ocirc;ng thiết lập bất kỳ mối quan hệ hợp đồng kh&aacute;c giữa c&aacute;c b&ecirc;n m&agrave; kh&ocirc;ng được đề cập trong hợp đồng n&agrave;y.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp;5)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> bảo lưu quyền sửa đổi c&aacute;c Điều khoản v&agrave; Điều kiện chung cho Nh&agrave; li&ecirc;n kết bất cứ l&uacute;c n&agrave;o. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được th&ocirc;ng b&aacute;o về bất kỳ thay đổi n&agrave;o qua e-mail. Nếu </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> kh&ocirc;ng đồng &yacute; với những thay đổi, th&igrave; c&oacute; quyền th&ocirc;ng b&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> biết trong thời hạn bốn tuần sau khi nhận được th&ocirc;ng b&aacute;o về sự thay đổi. Nếu </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> kh&ocirc;ng cung cấp th&ocirc;ng b&aacute;o như vậy trong thời gian n&agrave;y, những thay đổi sẽ được coi l&agrave; đ&atilde; được chấp nhận v&agrave; c&oacute; hiệu lực v&agrave;o cuối giai đoạn n&agrave;y.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Trong th&ocirc;ng b&aacute;o của m&igrave;nh về sự thay đổi, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> phải tư vấn cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> về tầm quan trọng sự thay đổi trong thời hạn bốn tuần.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 4: ĐỊNH NGHĨA V&Agrave; GIẢI TH&Iacute;CH</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong> -</strong> Chương tr&igrave;nh li&ecirc;n kiết tiếp thị: Thanh to&aacute;n chi ph&iacute; cho mỗi giao dịch.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong> -</strong> Giao dịch thất bại: C&aacute;c giao dịch m&agrave; kh&ocirc;ng đủ điều kiện thanh to&aacute;n hoa hồng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>&nbsp;-</strong> Nhấp chuột: Hoạt động của người d&ugrave;ng tr&ecirc;n một đường dẫn của c&aacute;c chương tr&igrave;nh li&ecirc;n kết, dẫn đến trang web của nh&agrave; quảng c&aacute;o.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong> -</strong> Kh&aacute;ch h&agrave;ng: Một người truy cập đến k&ecirc;nh quảng c&aacute;o của nh&agrave; li&ecirc;n kết hoặc trang web của nh&agrave; quảng c&aacute;o, v&agrave; đặt một đơn đặt h&agrave;ng tr&ecirc;n trang web của nh&agrave; quảng c&aacute;o .</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>-</strong> Li&ecirc;n kết: Một li&ecirc;n kết đến trang web của nh&agrave; quảng c&aacute;o theo h&igrave;nh thức URL ch&iacute;nh x&aacute;c, cung cấp th&ocirc;ng qua c&aacute;c chương tr&igrave;nh li&ecirc;n kết, để sử dụng bởi c&aacute;c nh&agrave; li&ecirc;n kết trong quảng c&aacute;o truyền th&ocirc;ng của m&igrave;nh (v&iacute; dụ như trang web đăng k&yacute;), để x&aacute;c định c&aacute;c Nh&agrave; li&ecirc;n kết.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp;<strong> -</strong> C&aacute;c b&ecirc;n: C&aacute;c thực thể tham gia v&agrave;o một thỏa thuận r&agrave;ng buộc với một hoặc nhiều b&ecirc;n k&yacute; kết kh&aacute;c v&agrave; do đ&oacute; chấp nhận những lợi &iacute;ch v&agrave; nghĩa vụ quy định trong đ&oacute;.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; <strong>- </strong>Nền tảng: C&aacute;c nền tảng trực tuyến, nơi c&aacute;c nh&agrave; li&ecirc;n kết c&oacute; thể t&igrave;m thấy tất cả c&aacute;c th&ocirc;ng tin cần thiết v&agrave; c&aacute;c t&agrave;i liệu l&agrave; một phần đ&oacute;ng g&oacute;p cho Chương tr&igrave;nh li&ecirc;n kết (theo hiệu quả của n&oacute;, lấy t&agrave;i liệu quảng c&aacute;o, tiếp cận với số lượng của hoa hồng, vv).</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp;<strong> - </strong></span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">: Một tổ chức/c&aacute; nh&acirc;n được xuất bản nội dung tr&ecirc;n internet, tham gia mạng li&ecirc;n kết tiếp thị của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; -</strong> Hoa hồng của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">: Phần tiền nhận được của một nh&agrave; xuất bản khi cung cấp một giao dịch hoặc một h&agrave;nh động thống nhất.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp;<strong> -</strong> Trang web của CTV: Trang web tr&ecirc;n m&agrave; CTV&nbsp;được xuất bản nội dung. Nội dung c&ocirc;ng bố c&oacute; thể bao gồm văn bản, h&igrave;nh ảnh, video v&agrave; c&aacute;c loại phương tiện truyền th&ocirc;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; <strong>&nbsp;-</strong> Giao dịch (c&ograve;n gọi l&agrave; đơn đặt h&agrave;ng): Việc b&aacute;n một sản phẩm hay dịch vụ v&agrave; trả bằng tiền của kh&aacute;ch h&agrave;ng th&ocirc;ng qua quảng c&aacute;o qua của người tham gia chương tr&igrave;nh.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; <strong>&nbsp;-</strong> Mẫu đăng k&yacute; th&agrave;nh vi&ecirc;n: H&igrave;nh thức Đăng k&yacute; c&oacute; thể truy cập th&ocirc;ng qua Chương tr&igrave;nh tiếp thị li&ecirc;n kết đăng k&yacute; Chương tr&igrave;nh li&ecirc;n kết.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; - </strong>Lượt xem (hoặc hiển thị): Số lần một quảng c&aacute;o được hiển thị tr&ecirc;n c&aacute;c phương tiện quảng c&aacute;o.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 5: NGHĨA VỤ CỦA C&Aacute;C CTV</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>1) </strong></span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;kh&ocirc;ng được sử dụng c&aacute;c t&agrave;i liệu quảng c&aacute;o c&oacute; nội dung sai lệch, g&acirc;y hiểu lầm... nhằm tạo giao dịch kh&ocirc;ng hợp lệ thu lợi bất ch&iacute;nh.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; <strong>2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> kh&ocirc;ng được ph&eacute;p sử dụng quảng c&aacute;o thư điện tử (EDM) để quảng c&aacute;o cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> m&agrave; kh&ocirc;ng c&oacute; sự chấp thuận trước bằng văn bản của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; <strong>3)</strong> C&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> phải đảm bảo rằng tất cả c&aacute;c địa chỉ e-mail đ&atilde; được tạo ra dựa tr&ecirc;n sự chấp thuận của người d&ugrave;ng, trong việc xem x&eacute;t tất cả c&aacute;c hạn chế cần thiết. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm cho tất cả c&aacute;c y&ecirc;u cầu của b&ecirc;n thứ ba trong trường hợp ph&aacute;t sinh c&aacute;c vấn đề do c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> gửi thư. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;đảm bảo rằng họ chịu tr&aacute;ch nhiệm trong trường hợp khiếu nại li&ecirc;n quan đến e-mail. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;kh&ocirc;ng được ph&eacute;p sử dụng thương hiệu &quot; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> &quot; trong địa chỉ e-mail, trong URL, b&ecirc;n trong m&atilde; nguồn, v&agrave; trong c&aacute;c chủ đề của e-mail. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;phải đảm bảo thể hiện r&otilde; rằng c&aacute;c e-mail đến từ&nbsp;</span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;chứ kh&ocirc;ng phải từ </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> trực tiếp. Email phải được ph&ecirc; duyệt bởi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> trước khi được gửi đi. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;phải bồi thường c&aacute;c chi ph&iacute;, trong trường hợp vi phạm thỏa thuận của b&ecirc;n thứ ba v&igrave; kh&ocirc;ng tu&acirc;n theo c&aacute;c luật đ&atilde; được thỏa thuận.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;<strong>&nbsp;4) </strong></span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;tự chịu tr&aacute;ch nhiệm về nội dung v&agrave; hoạt động của trang web của m&igrave;nh v&agrave; c&oacute; tr&aacute;ch nhiệm trong thời hạn của hợp đồng n&agrave;y, kh&ocirc;ng đăng c&aacute;c nội dung vi phạm luật ph&aacute;p, đạo đức c&ocirc;ng cộng, quyền của b&ecirc;n thứ ba l&ecirc;n website. Những nội dung sau đ&acirc;y bị cấm, bao gồm, nhưng kh&ocirc;ng giới hạn, c&aacute;c nội dung cổ x&uacute;y bạo lựa, nội dung v&agrave; h&igrave;nh ảnh khi&ecirc;u d&acirc;m, b&aacute;o c&aacute;o sai lệch nội dung hoặc ph&acirc;n biệt đối xử (v&iacute; dụ như đối với giới t&iacute;nh, chủng tộc, ch&iacute;nh trị, t&ocirc;n gi&aacute;o, quốc tịch hoặc khuyết tật). Những nội dung tr&ecirc;n kh&ocirc;ng được đề cập tr&ecirc;n trang web của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">, cũng như li&ecirc;n kết c&oacute; thể được tạo ra từ trang web của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; <strong>5)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ bị cấm duy tr&igrave; c&aacute;c trang web tr&ecirc;n internet m&agrave; c&oacute; thể l&agrave;m ph&aacute;t sinh một nguy cơ nhầm lẫn với sự hiện diện website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> kh&ocirc;ng được sử dụng c&aacute;c giao diện, kh&ocirc;ng sao ch&eacute;p đồ họa, văn bản hay những nội dung kh&aacute;c từ trang web </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. Nghi&ecirc;m cấm việc thu thập bất kỳ th&ocirc;ng tin n&agrave;o của c&aacute;c trang web của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. Đặc biệt, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> kh&ocirc;ng được tạo ra ấn tượng rằng trang web của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> l&agrave; một dự &aacute;n của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> hoặc người quản l&yacute; của n&oacute; l&agrave; chủ thể kinh tế c&oacute; li&ecirc;n quan đến </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> trong bất kỳ h&igrave;nh thức n&agrave;o b&ecirc;n ngo&agrave;i chương tr&igrave;nh li&ecirc;n kết quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> v&agrave; hợp đồng n&agrave;y. Bất kỳ việc sử dụng, bởi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">, c&aacute;c t&agrave;i liệu hoặc nội dung từ sự hiện diện web </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> hay logo hoặc thương hiệu bắt buộc phải c&oacute; văn bản được ph&ecirc; duyệt trước của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;&nbsp;<strong>6)</strong> C&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> chịu tr&aacute;ch nhiệm đảm bảo rằng quảng c&aacute;o th&ocirc;ng qua e-mail l&agrave; kh&ocirc;ng vi phạm trực tiếp hay gi&aacute;n tiếp quyền lợi của b&ecirc;n thứ ba ở trong nước v&agrave;/hoặc nước ngo&agrave;i v&agrave;/hoặc c&aacute;c quyền kh&aacute;c m&agrave; kh&ocirc;ng hưởng c&aacute;c sự bảo đảm đặc biệt kh&aacute;c.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;&nbsp;<strong>7)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ cam kết kh&ocirc;ng sử dụng t&ecirc;n &quot;</span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">&quot; trong c&aacute;c địa chỉ e-mail của người gửi hoặc trong d&ograve;ng chủ đề của e-mail, hoặc trong m&atilde; nguồn, v&agrave; phải đảm bảo rằng th&ocirc;ng tin của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> được thể hiện đầy đủ l&agrave; người gửi quảng c&aacute;o e-mail.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;&nbsp;<strong>8) </strong>Trong trường hợp c&oacute; vi phạm, tất cả doanh thu tạo ra từ quảng c&aacute;o tr&ecirc;n c&ocirc;ng cụ t&igrave;m kiếm sẽ bị hủy bỏ.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;&nbsp;<strong>9)</strong> Đơn h&agrave;ng chỉ được t&iacute;nh khi c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> phải đảm bảo rằng tất cả c&aacute;c lưu lượng truy cập th&ocirc;ng qua link giới thiệu của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> l&agrave; truy cập tự nhi&ecirc;n, kh&ocirc;ng &eacute;p buộc. Việc sử dụng layers, add-ons, iFrames, pop-up, pop-under, site-under, cookie dropping v&agrave; c&ocirc;ng nghệ postview đều bị ngăn cấm.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>10)</strong> Việc sử dụng dịch vụ, s&aacute;ng tạo hoặc t&ecirc;n thương hiệu cho bất kỳ trường hợp cạnh tranh hoặc xổ số đều bị nghi&ecirc;m cấm.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>11)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ loại bỏ c&aacute;c t&agrave;i liệu quảng c&aacute;o </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> nhanh ch&oacute;ng từ Website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> trong trường hợp </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> c&oacute; y&ecirc;u cầu.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>12)</strong> Nếu </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> bị kiện bởi b&ecirc;n thứ ba v&igrave; những sai phạm của người tham gia chương tr&igrave;nh của c&aacute;c nghĩa vụ hợp đồng, đặc biệt l&agrave; những quy định trong c&aacute;c khoản tr&ecirc;n 1 - 10, hoặc tr&ecirc;n t&agrave;i khoản vi phạm của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> của một điều khoản luật li&ecirc;n quan đến vị tr&iacute; quảng c&aacute;o của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; tr&aacute;ch nhiệm bồi thường cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> chống lại tất cả khiếu nại của b&ecirc;n thứ ba được khẳng định tr&ecirc;n t&agrave;i khoản của c&aacute;c h&agrave;nh vi vi phạm n&oacute;i tr&ecirc;n. Nếu, để bảo vệ quyền lợi ph&aacute;p luật, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> y&ecirc;u cầu </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> cung cấp th&ocirc;ng tin hoặc giải tr&igrave;nh, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; tr&aacute;ch nhiệm thực hiện kh&ocirc;ng chậm trễ v&agrave; nhanh ch&oacute;ng cung cấp hỗ trợ hợp l&yacute; cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> trong văn bản ph&aacute;p luật của n&oacute;.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong>13)</strong> Ngo&agrave;i ra, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> phải bồi thường </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> cho bất kỳ chi ph&iacute; ph&aacute;t sinh từ việc khiếu nại của b&ecirc;n thứ ba tr&ecirc;n t&agrave;i khoản của việc x&acirc;m phạm c&aacute;c quyền n&oacute;i tr&ecirc;n v&agrave;/hoặc nghĩa vụ chi trả chi ph&iacute; n&agrave;y, bao gồm ph&iacute; luật sư (500 EUR/ giờ), chi ph&iacute; t&ograve;a &aacute;n, đặc biệt l&agrave; chi ph&iacute; của thủ tục tố tụng độc lập để thu thập chứng cứ, thiệt hại v&agrave; bất lợi kh&aacute;c m&agrave; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> bị đ&oacute;.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 6: DỊCH VỤ CỦA MADA.VN</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp;<strong> 1)</strong> Sau khi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> đ&atilde; được chấp nhận tham gia chương tr&igrave;nh li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được cung cấp một loạt c&aacute;c t&agrave;i liệu quảng c&aacute;o, v&agrave; sẽ được điều chỉnh/cập nhật theo định kỳ ph&ugrave; hợp với c&aacute;c d&ograve;ng sản phẩm v&agrave; nhu cầu của kh&aacute;ch h&agrave;ng. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; thể y&ecirc;u cầu cung cấp c&aacute;c định dạng c&aacute; nh&acirc;n hay c&aacute;c mẫu bản tin từ </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> bất cứ l&uacute;c n&agrave;o.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; <strong>&nbsp;2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ hoạt động trang web v&agrave; c&aacute;c dịch vụ cung cấp th&ocirc;ng tin, chẳng hạn như việc cung cấp c&aacute;c nguồn cấp dữ liệu sản phẩm, trong phạm vi năng lực kỹ thuật c&oacute; sẵn của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> kh&ocirc;ng c&oacute; nghĩa vụ, trong giới hạn, phải cung cấp c&aacute;c th&ocirc;ng tin ch&iacute;nh x&aacute;c v&agrave; gi&aacute;n đoạn của trang web. Chất lượng v&agrave; t&iacute;nh ch&iacute;nh x&aacute;c của c&aacute;c sản phẩm, c&aacute;c file t&agrave;i liệu v&agrave; csv quảng c&aacute;o được cung cấp tr&ecirc;n nền tảng </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ nằm trong quyết định độc quyền của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; <strong>&nbsp;3)</strong> Tất cả c&aacute;c hoạt động của c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được đăng nhập th&ocirc;ng qua nền tảng hệ thống theo d&otilde;i m&agrave; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; thể truy cập th&ocirc;ng qua c&aacute;c thống k&ecirc; v&agrave; b&aacute;o c&aacute;o. Hoa hồng m&agrave; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> trả cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;mỗi th&aacute;ng một lần căn cứ v&agrave;o c&aacute;c đơn đặt h&agrave;ng trung gian theo gi&aacute; trị sản phẩm của giao dịch c&oacute; được trong th&aacute;ng v&agrave; hoa hồng từ thu nhập người được giới thiệu:</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>a</strong>. Hoa hồng b&aacute;n h&agrave;ng = </span><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">&nbsp;[ (Gi&aacute; trị sản phẩm - VA</span><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">T)</span><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">x tỷ lệ hoa hồng]</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong><span style=\"font-size: 14px;\">b</span>. </strong><span style=\"font-size: 14px;\">&nbsp;Tỷ lệ hoa hồng c&oacute; nhiều cấp độ 10%(sản phẩm th&ocirc;ng thường), từ 10% - 50%(sản phẩm đặc biệt_xem chi tiết sản phẩm)</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style=\"font-size: 14px;\"><b>c. </b>Hoa hồng giới thiệu</span><span style=\"color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px;\">&nbsp;tương ứng với 10% tr&ecirc;n thổng thu nhập từ hoa hồng b&aacute;n h&agrave;ng của người được giới thiệu.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;<strong>4)</strong> C&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> khi tham gia v&agrave;o chương tr&igrave;nh n&agrave;y v&agrave; ph&ugrave; hợp với c&aacute;c điều khoản v&agrave; điều kiện m&agrave; họ đ&atilde; đồng &yacute; với </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ nhận được hoa hồng từ </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> li&ecirc;n quan đến giao dịch được tạo ra, th&ocirc;ng qua việc quảng c&aacute;o cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> tr&ecirc;n Website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> đ&atilde; đăng k&yacute; với Chương tr&igrave;nh n&agrave;y, trong v&ograve;ng ba mươi ng&agrave;y sau.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 7: TR&Aacute;CH NHIỆM PH&Aacute;P L&Yacute;</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; <strong>1)</strong> Trong trường hợp c&oacute; h&agrave;nh vi vi phạm thường do sự sai phạm của việc vi phạm nhằm đạt được c&aacute;c mục đ&iacute;ch hợp đồng (t&agrave;i liệu nghĩa vụ hợp đồng), tr&aacute;ch nhiệm của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ được giới hạn trong số tiền thiệt hại m&agrave; c&oacute; thể dự đo&aacute;n v&agrave; ph&ugrave; hợp với c&aacute;c loại giao dịch trong c&acirc;u hỏi.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp;2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm hơn nữa cho c&aacute;c khoản tiền đ&atilde; tổn hại.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; <strong>3)</strong> Những hạn chế n&oacute;i tr&ecirc;n của tr&aacute;ch nhiệm ph&aacute;p l&yacute; cũng &aacute;p dụng đối với tr&aacute;ch nhiệm c&aacute; nh&acirc;n của người lao động </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">, đại diện v&agrave; cơ quan h&agrave;nh ph&aacute;p.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 8: HOA HỒNG CỦA NH&Agrave; LI&Ecirc;N KẾT</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; <strong>1)&nbsp;</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> đồng &yacute; chi trả một khoản hoa hồng dựa tr&ecirc;n doanh số của ph&aacute;t sinh từ lưu lượng truy cập do&nbsp;</span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;giới thiệu tới website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;<strong> 2)</strong>&nbsp; C&aacute;c khoản hoa hồng ph&aacute;t sinh kh&ocirc;ng bao gồm c&aacute;c đơn đặt h&agrave;ng bị trả, đơn h&agrave;ng bị hủy hoặc c&aacute;c trường hợp gian lận v&agrave; giao dịch kh&ocirc;ng hợp lệ.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; 3)</strong>&nbsp; Hoa hồng của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;được t&iacute;nh từ gi&aacute; trị sản phẩm:</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;*&nbsp;Gi&aacute; trị sản phẩm = gi&aacute; được ni&ecirc;m yết sản phẩm tại website của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;* Tỷ lệ hoa hồng c&oacute; nhiều cấp độ 10%(sản phẩm th&ocirc;ng thường), từ 10% - 50%(sản phẩm đặc biệt_xem chi tiết sản phẩm)</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;<strong> 4)</strong>&nbsp; Số tiền nhận được của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;= [ (Gi&aacute; trị sản phẩm - VA&nbsp;T)x tỷ lệ hoa hồng]</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; <strong>5) &nbsp;</strong>Cấu tr&uacute;c hoa hồng c&oacute; thể được sửa đổi bằng c&aacute;ch tăng th&ecirc;m cho c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;h&agrave;ng đầu, để khuyến kh&iacute;ch đẩy mạnh hiệu quả tốt nhất v&agrave; thưởng cho th&agrave;nh t&iacute;ch nổi bật.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;<strong>6)</strong>&nbsp; Hoa hồng của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;chưa bao gồm thuế gi&aacute; trị gia tăng (VAT) trong trường hợp </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">&nbsp;l&agrave; doanh nghiệp c&oacute; m&atilde; số thuế. Đối với c&aacute; nh&acirc;n, hoa hồng đ&atilde; bao gồm 10% thuế thu nhập c&aacute; nh&acirc;n (được trừ trực tiếp khi thanh to&aacute;n) theo quy định của ph&aacute;p luật Việt Nam.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 9: PHƯƠNG PH&Aacute;P THEO D&Otilde;I HIỆU QUẢ QUẢNG C&Aacute;O</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>1) </strong></span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> v&agrave; </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> đồng &yacute; việc theo d&otilde;i v&agrave; b&aacute;o c&aacute;o kết quả quảng c&aacute;o, tiếp thị được thực hiện bởi nền tảng của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> x&acirc;y dựng tr&ecirc;n cơ sở website </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> chỉ t&iacute;nh hoa hồng đơn h&agrave;ng cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> tạo n&ecirc;n ở lần nhấp chuột cuối c&ugrave;ng trong chuỗi nhiều lần nhấp chuột.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;3)</strong> Hệ thống lưu trữ Cookie của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> chỉ hiệu lực trong v&ograve;ng 30 ng&agrave;y v&agrave; chỉ khi kh&aacute;ch h&agrave;ng mở browser.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 10: THANH TO&Aacute;N</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>1)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; thể đăng nhập v&agrave;o hệ thống chương tr&igrave;nh tiếp thị li&ecirc;n kết của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> để xem hoa hồng t&iacute;ch lũy được t&iacute;nh trong v&ograve;ng 1 tiếng sau khi c&oacute; kh&aacute;ch h&agrave;ng đặt h&agrave;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> 2)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ cần thời gian 20 ng&agrave;y để x&aacute;c nhận t&igrave;nh trạng c&aacute;c đơn h&agrave;ng c&oacute; được trong mỗi th&aacute;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>3)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được thanh to&aacute;n trong v&ograve;ng 20 ng&agrave;y sau khi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> ho&agrave;n th&agrave;nh x&aacute;c nhận t&igrave;nh trạng đơn h&agrave;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>4)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> c&oacute; quyền giữ lại bất cứ thanh to&aacute;n dưới 200.000 đồng v&agrave; số tiền n&agrave;y sẽ được t&iacute;ch lũy v&agrave; chi trả trong kỳ thanh to&aacute;n tiếp theo (nếu số tiền đạt được 200.000 đồng v&agrave;o cuối th&aacute;ng sau).</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>5)</strong> Tất cả c&aacute;c thanh to&aacute;n được thực hiện từ c&aacute;c quảng c&aacute;o cho c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ được thực hiện theo h&igrave;nh thức chuyển khoản ng&acirc;n h&agrave;ng trực tiếp, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> sẽ chịu 100% c&aacute;c ph&iacute; ph&aacute;t sinh trong qu&aacute; tr&igrave;nh chuyển hoa hồng cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 11: ĐIỀU KHOẢN CỦA HỢP ĐỒNG V&Agrave; CHẤM DỨT</strong>&nbsp;</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>1)</strong> Thời hạn của hợp đồng n&agrave;y được căn cứ v&agrave;o thời hạn th&agrave;nh vi&ecirc;n của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> của Chương tr&igrave;nh tiếp thị li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>&nbsp;2)</strong> Sau khi chấm dứt hợp đồng, </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; tr&aacute;ch nhiệm, ngay cả khi kh&ocirc;ng được y&ecirc;u cầu, ngay lập tức x&oacute;a c&aacute;c th&ocirc;ng tin v&agrave; t&agrave;i liệu quảng c&aacute;o đ&atilde; được đệ tr&igrave;nh. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ kh&ocirc;ng c&oacute; quyền trong việc tiếp thục tham gia chương tr&igrave;nh n&agrave;y. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; tr&aacute;ch nhiệm, do </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> y&ecirc;u cầu cung cấp </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> c&oacute; x&aacute;c nhận bằng văn bản việc x&oacute;a.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>3)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> c&oacute; quyền thay đổi hoặc điều chỉnh cấu tr&uacute;c hoa hồng v&agrave;o ng&agrave;y đầu ti&ecirc;n của bất kỳ th&aacute;ng n&agrave;o. Trong trường hợp n&agrave;y, một th&ocirc;ng b&aacute;o được đăng tr&ecirc;n trang web </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">, v&agrave; một e-mail được gửi đến c&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\">, dựa tr&ecirc;n địa chỉ e-mail được cung cấp bởi </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> th&ocirc;ng qua c&aacute;c chương tr&igrave;nh tiếp thị li&ecirc;n kết </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">. Th&ocirc;ng b&aacute;o sẽ được đăng &iacute;t nhất 4 tuần trước khi xảy ra thay đổi. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm để đảm bảo rằng </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; thể nhận được th&ocirc;ng tin về những thay đổi trong chương tr&igrave;nh. Trong trường hợp kh&ocirc;ng đồng &yacute;, giải ph&aacute;p duy nhất của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> sẽ kết th&uacute;c sự tham gia của m&igrave;nh trong chương tr&igrave;nh.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>4)</strong> </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> c&oacute; quyền thay đổi hoặc chấm dứt bất kỳ xuất bản v&agrave;o ng&agrave;y đầu ti&ecirc;n của bất kỳ th&aacute;ng n&agrave;o, với 4 tuần trước khi th&ocirc;ng b&aacute;o cho Nh&agrave; li&ecirc;n kết.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 12: BẢO MẬT</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> 1) </strong>Th&ocirc;ng tin bảo mật bao gồm tất cả th&ocirc;ng tin v&agrave; t&agrave;i liệu được b&ecirc;n tương ứng đ&aacute;nh dấu l&agrave; bảo mật hoặc c&oacute; thể được coi l&agrave; bảo mật dựa v&agrave;o ho&agrave;n cảnh. Điều n&agrave;y sẽ bao gồm, đặc biệt l&agrave; th&ocirc;ng tin về c&aacute;c điều khoản v&agrave; điều kiện, th&ocirc;ng số kỹ thuật b&aacute;n h&agrave;ng v&agrave; c&aacute;c t&agrave;i liệu đ&aacute;nh gi&aacute; được thực hiện c&oacute; sẵn cho </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> của Th&agrave;nh vi&ecirc;n&nbsp;vi&ecirc;n </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>2)</strong> C&aacute;c b&ecirc;n sẽ đồng &yacute; duy tr&igrave; sự im lặng đối với th&ocirc;ng tin bảo mật. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> chỉ được ph&eacute;p c&ocirc;ng bố th&ocirc;ng tin bảo mật cho những nh&acirc;n vi&ecirc;n c&oacute; thẩm quyền cần phải nhận thức được n&oacute; để chỉnh sửa hợp đồng n&agrave;y. </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> bắt buộc phải n&oacute;i nh&acirc;n vi&ecirc;n biết duy tr&igrave; bảo mật đối với những th&ocirc;ng tin n&agrave;y trong v&agrave; sau hoạt động của họ.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>3)</strong> Tr&aacute;ch nhiệm kh&ocirc;ng tiết lộ được &aacute;p dụng trong một thời gian kh&ocirc;ng giới hạn vượt qu&aacute; thời hạn của hợp đồng n&agrave;y.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 13: CHUYỂN NHƯỢNG, QUYỀN DUY TR&Igrave;, TẮT CHƯƠNG TR&Igrave;NH</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>1) </strong>C&aacute;c </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">CTV</span><span style=\"font-size: 14px;\"> c&oacute; thể chuyển giao tuy&ecirc;n bố chống lại </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\"> dựa tr&ecirc;n hợp đồng n&agrave;y cho b&ecirc;n thứ ba chỉ với sự đồng &yacute; bằng văn bản của </span><span style=\"font-size: 14px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 14px;\">.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>2)</strong> Một trong c&aacute;c b&ecirc;n tham gia hợp đồng c&oacute; thể đặt ra hoặc thực hiện c&aacute;c quyền về việc lưu giữ chỉ li&ecirc;n quan đến c&aacute;c khoản phải thu của B&ecirc;n kia được phản b&aacute;c hoặc đ&atilde; trở th&agrave;nh y&ecirc;u cầu loại trừ.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 15px;\"><strong>ĐIỀU KHOẢN 14: ĐIỀU KHOẢN CUỐI C&Ugrave;NG</strong></span></p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>1)</strong> Hợp đồng n&agrave;y được điều chỉnh bởi ph&aacute;p luật của Việt Nam m&agrave; kh&ocirc;ng c&oacute; hiệu lực đối với luật ph&aacute;p quốc tế v&agrave; đa quốc gia, đặc biệt l&agrave; C&ocirc;ng ước Li&ecirc;n Hợp Quốc về hợp đồng mua b&aacute;n h&agrave;ng h&oacute;a quốc tế.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> 2)</strong> Cơ quan duy nhất c&oacute; thẩm quyền đối với bất kỳ tranh chấp ph&aacute;t sinh từ mối quan hệ hợp đồng l&agrave; T&ograve;a &Aacute;n Nh&acirc;n D&acirc;n cụ thể l&agrave; T&ograve;a &Aacute;n Kinh Tế.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>3)</strong> Sẽ kh&ocirc;ng c&oacute; những thỏa thuận bằng miệng cho hợp đồng n&agrave;y. Tất cả những sửa đổi đối với hợp đồng n&agrave;y sẽ được y&ecirc;u cầu bổ sung bằng văn bản viết tay. Điều n&agrave;y cũng &aacute;p dụng đối với việc sửa đổi hoặc hủy bỏ điều khỏan n&agrave;y. Những t&agrave;i liệu được cung cấp qua thư điện tử sẽ kh&ocirc;ng được tu&acirc;n thủ theo văn bản y&ecirc;u cầu được viết tay.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>4) </strong>Trong trường hợp c&oacute; một điều khoản trong hợp đồng n&agrave;y kh&ocirc;ng c&oacute; gi&aacute; trị hoặc kh&ocirc;ng thể thi h&agrave;nh, điều n&agrave;y kh&ocirc;ng ảnh hưởng đến hiệu lực của c&aacute;c quy định kh&aacute;c. C&aacute;c b&ecirc;n tham gia hợp đồng sẽ cố gắng thay thế c&aacute;c điều khoản sai tr&aacute;i hoặc kh&ocirc;ng thể thực hiện với một đ&aacute;p ứng tốt nhất c&aacute;c mục ti&ecirc;u hợp đồng trong điều kiện ph&aacute;p l&yacute; v&agrave; kinh tế. Điều n&agrave;y cũng được &aacute;p dụng trong trường hợp c&oacute; khiếm khuyết.</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: center;\"><span style=\"font-size: 14px;\">---o0o---</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n\r\n<p style=\"margin-left: 40px; text-align: justify;\"><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n', '', NULL, '0', 'quy-che-hoat-dong');
INSERT INTO `tv_pagehtml` (`Id`, `title_vn`, `title_en`, `description_vn`, `description_en`, `content_vn`, `content_en`, `sort`, `ticlock`, `alias`) VALUES
(8, 'Chính Sách Hoa Hồng', '', NULL, NULL, '<p style=\"text-align: center; margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: center; margin-left: 40px;\"><span style=\"font-size: 22px;\"><strong>CH&Iacute;NH S&Aacute;CH HOA HỒNG</strong></span>&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><b><span style=\"font-size: 15px;\">Khi tham gia l&agrave;m Th&agrave;nh vi&ecirc;n</span></b><span style=\"font-size: 15px;\"><b>&nbsp;li&ecirc;n kết với Mada.vn</b><strong>, </strong></span><strong>CTV</strong><span style=\"font-size: 15px;\"><strong> đư</strong><b>ợc hưởng 2 nguồn thu nhập ch&iacute;nh l&agrave;:</b></span></p>\r\n\r\n<p style=\"margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\"><strong>1. Hoa hồng b&aacute;n h&agrave;ng:</strong></span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Hoa hồng b&aacute;n h&agrave;ng l&agrave; hoa hồng t&iacute;nh tr&ecirc;n mỗi đơn h&agrave;ng th&agrave;nh c&ocirc;ng ph&aacute;t sinh từ lưu lượng truy cập th&ocirc;ng qua link giới thiệu của </span>CTV<span style=\"font-size: 15px;\">.&nbsp;</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- C&ocirc;ng thức t&iacute;nh hoa hồng được t&iacute;nh như sau:</span></p>\r\n\r\n<p style=\"text-align: center; margin-left: 40px;\"><span style=\"font-size: 15px;\">&nbsp;</span><em><span style=\"font-size: 15px;\">Hoa hồng b&aacute;n h&agrave;ng =</span></em><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">&nbsp;[ (Gi&aacute; trị sản phẩm - VA</span><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">T)</span><span style=\"font-size: 14px; line-height: 20.7999992370605px; text-align: justify;\">x tỷ lệ hoa hồng]</span></p>\r\n\r\n<p style=\"text-align: center; margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Tỷ lệ % hoa hồng mặc định cho tất cả sản phẩm tại Mada.vn&nbsp;l&agrave; 10%.</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Ngo&agrave;i ra, </span><span style=\"font-size: 15px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 15px;\"> c&ograve;n c&oacute; danh mục <strong>SẢN PHẨM ĐẶC BIỆT</strong> c&oacute; mức chiết khấu Cao, từ 10% - 50%(xem chi tiết sản phẩm) được cập nhật thường xuy&ecirc;n gi&uacute;p cho </span>CTV<span style=\"font-size: 15px;\"> gia tăng thu nhập một c&aacute;ch nhanh ch&oacute;ng.</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">&nbsp;</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\"><strong>2. Hoa hồng đội nh&oacute;m:</strong></span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Ngo&agrave;i khoản hoa hồng b&aacute;n h&agrave;ng, </span>CTV<span style=\"font-size: 15px;\"> c&ograve;n c&oacute; cơ hội tạo th&ecirc;m nguồn thu nhập bằng c&aacute;ch giới thiệu th&agrave;nh vi&ecirc;n mới tham gia chương tr&igrave;nh </span>CTV<span style=\"font-size: 15px;\"> với </span><span style=\"font-size: 15px; line-height: 20.8px; text-align: justify;\">Mada.vn</span><span style=\"font-size: 15px;\">. Khoản hoa hồng n&agrave;y tương ứng với 10% tr&ecirc;n tổng thu nhập từ hoa hồng b&aacute;n h&agrave;ng của người được giới thiệu.</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">&nbsp; &nbsp; &nbsp; <em>&nbsp;</em></span><em><span style=\"font-size: 15px;\">VD: </span>CTV<span style=\"font-size: 15px;\"> A giới thiệu B tham gia l&agrave;m </span>CTV<span style=\"font-size: 15px;\"> với Mada.vn, trong th&aacute;ng 11, </span>CTV<span style=\"font-size: 15px;\"> B kiếm được khoản hoa hồng b&aacute;n h&agrave;ng l&agrave; 10.000.000đ, như vậy </span>CTV<span style=\"font-size: 15px;\"> A sẽ c&oacute; th&ecirc;m 1 khoản hoa hồng đội nh&oacute;m l&agrave; 1.000.000đ ( Tương ứng 10% hoa hồng b&aacute;n h&agrave;ng của </span>CTV<span style=\"font-size: 15px;\"> B).</span></em></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Khoản hoa hồng n&agrave;y sẽ được hưởng trọn đời trong suốt qu&aacute; tr&igrave;nh th&agrave;nh vi&ecirc;n được giới thiệu c&ograve;n hoạt động.</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><span style=\"font-size: 15px;\">- Kh&ocirc;ng giới hạn th&agrave;nh vi&ecirc;n được giới thiệu.</span></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\"><strong><span style=\"font-size: 15px;\">&nbsp; &nbsp; Ngo&agrave;i ra, Mada.vn&nbsp;c&ograve;n thường xuy&ecirc;n tổ chức c&aacute;c chương tr&igrave;nh, sự kiện thi đua v&agrave; thưởng d&agrave;nh cho tất cả c&aacute;c th&agrave;nh vi&ecirc;n l&agrave;</span><span style=\"font-size: 15px;\"> </span>CTV</strong>&nbsp;<strong><span style=\"font-size: 15px;\">c&oacute; kết quả hoạt động tốt. N&agrave;o c&ugrave;ng đăng k&yacute; tham gia l&agrave;m </span>CTV</strong><strong><span style=\"font-size: 15px;\"> với Mada.vn&nbsp;ngay h&ocirc;m nay nh&eacute;!</span></strong></p>\r\n\r\n<p style=\"text-align: justify; margin-left: 40px;\">&nbsp;</p>\r\n', '', NULL, '0', 'chinh-sach-hoa-hong'),
(11, 'Hoa hồng và thanh toán', '', NULL, NULL, '<p><span style=\"font-size: 14px;\"><strong>&nbsp;Với ch&iacute;nh s&aacute;ch cực kỳ đơn giản v&agrave; tỷ lệ hoa hồng cao, chương tr&igrave;nh CTV</strong></span><span style=\"font-size: 14px;\"><strong>&nbsp;li&ecirc;n kết của Mada.vn chắc chắn sẽ mang lại 1 cơ hội kiếm th&ecirc;m thu nhập tuyệt vời cho bạn:</strong></span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Hoa hồng mặc định tất cả sản phẩm l&agrave; 10%. </span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Sản phẩm thuộc danh mục <strong>SẢN PHẨM ĐẶC BIỆT</strong> c&oacute; mức hoa hồng l&ecirc;n đến 50%.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Giới thiệu </span>CTV<span class=\"list-icon\" style=\"font-size: 14px;\"> mới tham gia hưởng 10% tr&ecirc;n thu nhập của người được giới thiệu.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Hệ thống lưu trữ Cookie đến 30 ng&agrave;y, tỷ lệ th&agrave;nh c&ocirc;ng cao.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Hoa hồng được to&aacute;n sau 20 ng&agrave;y sau khi Mada.vn</span><span style=\"color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;ho&agrave;n th&agrave;nh x&aacute;c nhận t&igrave;nh trạng đơn h&agrave;ng.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Hệ thống banner quảng c&aacute;o đa dạng, bắt mắt hỗ trợ tối đa việc quảng b&aacute; của CTV.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Sản phẩm đa dạng, thương hiệu, gi&aacute; th&agrave;nh cực kỳ cạnh tranh.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\"><span class=\"list-icon\" style=\"font-size: 14px;\">Nhiều chương tr&igrave;nh khuyến m&atilde;i, ưu đ&atilde;i sốc tăng doanh thu.</span></p>\r\n\r\n<p style=\"margin-left: 40px;\">&nbsp;</p>\r\n\r\n<p><span style=\"font-size: 14px;\">&nbsp;</span></p>\r\n', '', NULL, '0', 'hoa-hong-va-thanh-toan'),
(13, 'Thông tin footer', NULL, NULL, NULL, '<h3><strong><span style=\"font-size:14px\">C&Ocirc;NG TY CỔ PHẦN &nbsp;THƯƠNG MẠI V&Agrave; ĐẦU TƯ PPP</span></strong></h3>\r\n\r\n<p>M&atilde; Số Thuế : 0312828906 do Sở Kế Hoạch v&agrave; Đầu Tư TP. Hồ Ch&iacute; Minh</p>\r\n\r\n<p>cấp ng&agrave;y 23/06/2014&nbsp;</p>\r\n\r\n<p>Trụ sở ch&iacute;nh: 376 V&otilde; Văn Tần , P.5 , Q.3 TPHCM</p>\r\n\r\n<p>Điện thoại: (08) 3833 8888</p>\r\n\r\n<p>Email: info@mada.vn - cskh@mada.vn</p>\r\n', NULL, NULL, '', 'thong-tin-footer'),
(10, 'Hỗ trợ', NULL, NULL, NULL, '<h3><strong><span style=\"font-size:17px\">TRUNG T&Acirc;M HỖ TRỢ</span></strong><br />\r\n<span style=\"font-size:12px\"><span style=\"color:rgb(255, 0, 0)\"><strong>HOTLINE: (08) 6676 3888</strong></span></span><br />\r\n<span style=\"font-size:12px\"><strong>EMAIL: Ctv.mada@gmail.com</strong></span></h3>\r\n', NULL, NULL, '', 'ho-tro');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_quote`
--

CREATE TABLE `tv_quote` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quote` longtext COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticlock` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `modifield` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_quote`
--

INSERT INTO `tv_quote` (`id`, `fullname`, `quote`, `location`, `images`, `ticlock`, `created`, `modifield`) VALUES
(2, 'Puka', '<p>The world&#39;s leading conservation organization,WWF works in 100 countries and is supported by 1.2</p>\r\n', 'TP. HCM', 'avatar.jpg', 0, 1474522911, 0),
(3, 'Nguyễn Sơn', '<p>The world&#39;s leading conservation organization,WWF works in 100 countries and is supported by 1.2</p>\r\n', 'Đồng Nai', 'avatar1.jpg', 0, 1474522996, 0),
(4, 'Kim Hiền', '<p>The world&#39;s leading conservation organization,WWF works in 100 countries and is supported by 1.2</p>\r\n', 'Đà Lạt', 'avatar2.jpg', 0, 1474523046, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_support`
--

CREATE TABLE `tv_support` (
  `Id` int(11) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8,
  `date` datetime DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_tool`
--

CREATE TABLE `tv_tool` (
  `Id` int(11) NOT NULL,
  `location` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_vn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `style` tinyint(1) NOT NULL,
  `title_vn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticlock` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tv_tool`
--

INSERT INTO `tv_tool` (`Id`, `location`, `file_vn`, `file_en`, `link`, `width`, `height`, `sort`, `style`, `title_vn`, `ticlock`) VALUES
(1, '3', '1418017377_kiem-tien-online-voi-gumua.jpg', '', 'http://ctv.mada.vn', '300', '250', 0, 2, 'Chia sẻ là có tiền với Mada.vn', 0),
(2, '1', 'Hky2j1459216754.png', '', '', '', '', 0, 2, 'f', 0),
(3, '2', '1416998842_banner-ctv.jpg', '', '', '', '', 0, 2, NULL, 0),
(6, '5', '1417248888_banner.png', '', '', '', '', 0, 2, NULL, 0),
(7, '3', '1418029054_141206_160x600.jpg', '', 'http://ctv.mada.vn', '160', '600', 0, 2, 'Chia sẻ là có tiền với Mada.vn', 0),
(8, '3', '1418029121_141205_728x90.jpg', 'doi tac', 'http://ctv.mada.vn', '728', '90', 0, 2, 'Chia sẻ là có tiền với Mada.vn', 0),
(9, '3', '1418029136_141206_300x600.jpg', 'Chia sẻ là có tiền', 'http://ctv.mada.vn', '300', '600', 0, 2, 'Chia sẻ là có tiền với Mada.vn', 0),
(10, '3', '1418382168_giay-attom-160-600.jpg', '', 'http://mada.vn/giay-nam.html', '160', '600', 0, 2, 'Giày da ATTOM da thật 100%', 0),
(11, '3', '1418382327_giay-attom-300-250.jpg', '', 'http://mada.vn/giay-nam.html', '300', '250', 0, 2, 'Giày da ATTOM da thật 100%', 0),
(12, '3', '1418382363_giay-attom-300-600.jpg', '', 'http://mada.vn/giay-nam.html', '300', '600', 0, 2, 'Giày da ATTOM da thật 100%', 1),
(13, '3', '1418382402_giay-attom-728-90.jpg', '', 'http://mada.vn/giay-nam.html', '728', '90', 0, 2, 'Giày da ATTOM da thật 100%', 1),
(14, '3', '1419771298_141215_160x600.jpg', '', 'http://mada.vn/thuong-hieu/sakura', '160', '600', 1, 2, 'Mỹ Phẩm Sakura', 0),
(15, '3', '1419771320_141215_300x250.jpg', '', 'http://mada.vn/thuong-hieu/sakura', '300', '250', 2, 2, 'Mỹ Phẩm Sakura', 0),
(16, '3', '1419771336_141215_300x600.jpg', '', 'http://mada.vn/thuong-hieu/sakura', '300', '600', 3, 2, 'Mỹ Phẩm Sakura', 0),
(17, '3', '1419771352_141215_728x90.jpg', '', 'http://mada.vn/thuong-hieu/sakura', '728', '90', 4, 2, 'Mỹ Phẩm Sakura', 0),
(18, '3', '1419771404_141215_160x600_2.jpg', '', 'http://mada.vn/pin-sac-du-phong.html', '160', '600', 5, 2, 'Pin Dự Phòng', 0),
(19, '3', '1419771451_141215_300x600_2.jpg', '', 'http://mada.vn/pin-sac-du-phong.html', '300', '600', 6, 2, 'Pin Dự Phòng', 0),
(20, '3', '1419771463_141215_728x90_2.jpg', '', 'http://mada.vn/pin-sac-du-phong.html', '728', '90', 7, 2, 'Pin Dự Phòng', 0),
(21, '3', '1419771480_G-_JOB2_Gumua_141215_141215_300x250_2.jpg', '', 'http://mada.vn/pin-sac-du-phong.html', '300', '250', 8, 2, 'Pin Dự Phòng', 0),
(22, '3', '1419771553_141215_160x600_3.jpg', '', 'http://mada.vn/thuong-hieu/la-belle', '160', '600', 9, 2, 'Thời Trang Cao Cấp La Bella', 0),
(23, '3', '1419771566_141215_300x250_3.jpg', '', 'http://mada.vn/thuong-hieu/la-belle', '300', '250', 10, 2, 'Thời Trang Cao Cấp La Bella', 0),
(24, '3', '1419771579_141215_728x90_3.jpg', '', 'http://mada.vn/thuong-hieu/la-belle', '728', '90', 11, 2, 'Thời Trang Cao Cấp La Bella', 0),
(25, '3', '1419771604_G-_JOB2_Gumua_141215_141215_300x600_3.jpg', '', 'http://mada.vn/thuong-hieu/la-belle', '300', '600', 12, 2, 'Thời Trang Cao Cấp La Bella', 0),
(26, '3', '1419771964_141215_160x600_4.jpg', '', 'http://mada.vn/thuong-hieu/essance', '160', '600', 13, 2, 'Mỹ Phẩm Cao Cấp ESSANCE', 0),
(27, '3', '1419771978_141215_300x250_4.jpg', '', 'http://mada.vn/thuong-hieu/essance', '300', '250', 14, 2, 'Mỹ Phẩm Cao Cấp ESSANCE', 0),
(28, '3', '1419771991_141215_300x600_4.jpg', '', 'http://mada.vn/thuong-hieu/essance', '300', '600', 15, 2, 'Mỹ Phẩm Cao Cấp ESSANCE', 0),
(29, '3', '1419772003_141215_728x90_4.jpg', '', 'http://mada.vn/thuong-hieu/essance', '728', '90', 16, 2, 'Mỹ Phẩm Cao Cấp ESSANCE', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_user`
--

CREATE TABLE `tv_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `gioitinh` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `idtinh` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `ticlock` tinyint(4) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0',
  `idteam` int(11) DEFAULT NULL,
  `mutilevel` int(11) NOT NULL DEFAULT '0',
  `facebook` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `numbank` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `banking` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tongthunhap` int(11) DEFAULT '0',
  `hoahong` int(11) DEFAULT '0',
  `thunhap` int(11) DEFAULT '0',
  `codekichhoat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tongdonhang` int(11) DEFAULT '0',
  `tongdonhanggioithieu` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tv_user`
--

INSERT INTO `tv_user` (`id`, `username`, `password`, `email`, `fullname`, `birthday`, `phone`, `level`, `avatar`, `city`, `gioitinh`, `address`, `idtinh`, `date`, `code`, `ticlock`, `admin`, `idteam`, `mutilevel`, `facebook`, `website`, `numbank`, `account`, `banking`, `branch`, `tongthunhap`, `hoahong`, `thunhap`, `codekichhoat`, `tongdonhang`, `tongdonhanggioithieu`) VALUES
(1, 'demo', '5c173b601be14c6210b3115f8e7a6bf3', 'phuc@wincorp.vn', 'demo', '0000-00-00', '09878768686', NULL, NULL, NULL, NULL, '', NULL, 2016, '1', 0, 0, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '1VDkBvFmfI3Baj7mQ1ed7pDIOnZzTdka6noqMqnKgSbevvRkdA', 0, 0),
(4, 'vquyen', '47c1b65b3fc3d0e6dfdad0082b2abc60', 'vquyen2790@gmail.com', 'Phan van quyen', '0000-00-00', '0936443039', NULL, NULL, NULL, NULL, '', NULL, 2016, '4', 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 'HKRrK9RuO8MfTyA5nrJOV4e3vth7cQSYwXZWxrJLiUmCwywCYl', 0, 0),
(110, 'kubinss', 'c1afbfee94be5da708abafd58c45592e', 'abc@gmail.com', 'abc', NULL, '0933022222', 1, NULL, NULL, NULL, NULL, NULL, 1474692817, NULL, 1, 0, 106, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 'wi3NWD7AsPg4LJpNGquO0T36M1QEkxPtdLWBUujwKgwJKkehTs', 0, 0),
(106, 'kubin', 'c1afbfee94be5da708abafd58c45592e', 'pubreson88@gmail.com', 'sơn', NULL, '0933022900', 1, NULL, NULL, NULL, NULL, NULL, 1474598916, NULL, 0, 0, 76, 0, NULL, NULL, NULL, NULL, NULL, NULL, 133845, 1345, 132500, 'FKM83a5FGrqgYnqVeZ7oZDqQAGpbBQgTrnjS4GUqa8cwVYuwi5', 1, 0),
(107, 'sonk', 'c1afbfee94be5da708abafd58c45592e', 'ps_koho88@yahoo.com', 'sơn', NULL, '0933022901', 1, NULL, NULL, NULL, NULL, NULL, 1474601152, NULL, 1, 0, 106, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 'H3NjVFP0ThqIFAlfQa8z279XFnM7Tva3jXk88cHmJDYvHhaTAD', 0, 0),
(108, 'chimnon', 'c1afbfee94be5da708abafd58c45592e', 'son.giacongwebsite@gmail.com', 'chím', NULL, '09220292898', 1, NULL, NULL, NULL, NULL, NULL, 1474601286, NULL, 1, 0, 106, 0, NULL, NULL, NULL, NULL, NULL, NULL, 13450, 0, 13450, 'ccRoMCTsSCurHJITkQydGW9Rs959EluVHaCGzh4tHgtCDMVAL5', 1, 0),
(109, 'kubins', '30f01194f7a4fab51959ba1951672e1a', 'pubns@gmail.com', 'demos', NULL, '09330229001', 1, NULL, NULL, NULL, NULL, NULL, 1474692506, NULL, 1, 0, 106, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 'Z74eMpNhKWmpX5rS3Ht0zNYekKZt2MKaszh2AZHCuGjwE8siN0', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tv_website`
--

CREATE TABLE `tv_website` (
  `id` int(11) NOT NULL,
  `description_vn` varchar(255) DEFAULT NULL,
  `keyword_vn` varchar(255) DEFAULT NULL,
  `googleanalytics` varchar(255) DEFAULT NULL,
  `title_vn` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `percentdn` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tv_website`
--

INSERT INTO `tv_website` (`id`, `description_vn`, `keyword_vn`, `googleanalytics`, `title_vn`, `message`, `email`, `enable`, `percent`, `percentdn`) VALUES
(1, '', NULL, '', 'Mada Partner', '', 'info@wincorp.vn', 0, 10, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `mn_activities`
--
ALTER TABLE `mn_activities`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_admin`
--
ALTER TABLE `mn_admin`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_adminmenu`
--
ALTER TABLE `mn_adminmenu`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_admissions`
--
ALTER TABLE `mn_admissions`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_admonline`
--
ALTER TABLE `mn_admonline`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_bmenu`
--
ALTER TABLE `mn_bmenu`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_captcha`
--
ALTER TABLE `mn_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Chỉ mục cho bảng `mn_catactivities`
--
ALTER TABLE `mn_catactivities`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catadmissions`
--
ALTER TABLE `mn_catadmissions`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catadmonline`
--
ALTER TABLE `mn_catadmonline`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catalogue`
--
ALTER TABLE `mn_catalogue`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catcooperation`
--
ALTER TABLE `mn_catcooperation`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catdeal`
--
ALTER TABLE `mn_catdeal`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catelog`
--
ALTER TABLE `mn_catelog`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`,`alias`);

--
-- Chỉ mục cho bảng `mn_catlibrary`
--
ALTER TABLE `mn_catlibrary`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catmedia`
--
ALTER TABLE `mn_catmedia`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catnews`
--
ALTER TABLE `mn_catnews`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catpercent`
--
ALTER TABLE `mn_catpercent`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_catstudent`
--
ALTER TABLE `mn_catstudent`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_code`
--
ALTER TABLE `mn_code`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_code_discount`
--
ALTER TABLE `mn_code_discount`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_color`
--
ALTER TABLE `mn_color`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_comment`
--
ALTER TABLE `mn_comment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_comment_payment`
--
ALTER TABLE `mn_comment_payment`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_contact`
--
ALTER TABLE `mn_contact`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_cooperation`
--
ALTER TABLE `mn_cooperation`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_customer`
--
ALTER TABLE `mn_customer`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_customer_cart`
--
ALTER TABLE `mn_customer_cart`
  ADD PRIMARY KEY (`idcustomer`,`idpro`);

--
-- Chỉ mục cho bảng `mn_customer_reasoncancel`
--
ALTER TABLE `mn_customer_reasoncancel`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_deal`
--
ALTER TABLE `mn_deal`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_discount`
--
ALTER TABLE `mn_discount`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_district`
--
ALTER TABLE `mn_district`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_flash`
--
ALTER TABLE `mn_flash`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_follow`
--
ALTER TABLE `mn_follow`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_gift`
--
ALTER TABLE `mn_gift`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_guid`
--
ALTER TABLE `mn_guid`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_library`
--
ALTER TABLE `mn_library`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_location`
--
ALTER TABLE `mn_location`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_manufacturer`
--
ALTER TABLE `mn_manufacturer`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_media`
--
ALTER TABLE `mn_media`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_menu`
--
ALTER TABLE `mn_menu`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_news`
--
ALTER TABLE `mn_news`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_pagehtml`
--
ALTER TABLE `mn_pagehtml`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_partners`
--
ALTER TABLE `mn_partners`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_product`
--
ALTER TABLE `mn_product`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`,`idmanufacturer`,`idcat`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn` (`title_vn`,`content_vn`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn_2` (`title_vn`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn_3` (`title_vn`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn_4` (`title_vn`,`content_vn`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn_5` (`title_vn`,`content_vn`);
ALTER TABLE `mn_product` ADD FULLTEXT KEY `title_vn_6` (`title_vn`,`content_vn`);

--
-- Chỉ mục cho bảng `mn_provinces`
--
ALTER TABLE `mn_provinces`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_sessions`
--
ALTER TABLE `mn_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Chỉ mục cho bảng `mn_size`
--
ALTER TABLE `mn_size`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_student`
--
ALTER TABLE `mn_student`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_suppliers`
--
ALTER TABLE `mn_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_support`
--
ALTER TABLE `mn_support`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_tags`
--
ALTER TABLE `mn_tags`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_user`
--
ALTER TABLE `mn_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_voucher`
--
ALTER TABLE `mn_voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Chỉ mục cho bảng `mn_voucher_type`
--
ALTER TABLE `mn_voucher_type`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mn_weblink`
--
ALTER TABLE `mn_weblink`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_webplus`
--
ALTER TABLE `mn_webplus`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `mn_website`
--
ALTER TABLE `mn_website`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `songuoi`
--
ALTER TABLE `songuoi`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `spr_app_routes`
--
ALTER TABLE `spr_app_routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `key_2` (`key`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `ward_id` (`ward_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `street_id` (`street_id`),
  ADD KEY `city_id_2` (`city_id`),
  ADD KEY `key_3` (`key`),
  ADD KEY `key` (`key`);

--
-- Chỉ mục cho bảng `stats`
--
ALTER TABLE `stats`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tv_admin`
--
ALTER TABLE `tv_admin`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_adminmenu`
--
ALTER TABLE `tv_adminmenu`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_catnews`
--
ALTER TABLE `tv_catnews`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_guid`
--
ALTER TABLE `tv_guid`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_manufacturer`
--
ALTER TABLE `tv_manufacturer`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_menu`
--
ALTER TABLE `tv_menu`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_pagehtml`
--
ALTER TABLE `tv_pagehtml`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_quote`
--
ALTER TABLE `tv_quote`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tv_support`
--
ALTER TABLE `tv_support`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_tool`
--
ALTER TABLE `tv_tool`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tv_user`
--
ALTER TABLE `tv_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tv_website`
--
ALTER TABLE `tv_website`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `mn_activities`
--
ALTER TABLE `mn_activities`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT cho bảng `mn_admin`
--
ALTER TABLE `mn_admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT cho bảng `mn_adminmenu`
--
ALTER TABLE `mn_adminmenu`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT cho bảng `mn_admissions`
--
ALTER TABLE `mn_admissions`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT cho bảng `mn_admonline`
--
ALTER TABLE `mn_admonline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT cho bảng `mn_bmenu`
--
ALTER TABLE `mn_bmenu`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT cho bảng `mn_captcha`
--
ALTER TABLE `mn_captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=519;
--
-- AUTO_INCREMENT cho bảng `mn_catactivities`
--
ALTER TABLE `mn_catactivities`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT cho bảng `mn_catadmissions`
--
ALTER TABLE `mn_catadmissions`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT cho bảng `mn_catadmonline`
--
ALTER TABLE `mn_catadmonline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT cho bảng `mn_catalogue`
--
ALTER TABLE `mn_catalogue`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `mn_catcooperation`
--
ALTER TABLE `mn_catcooperation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT cho bảng `mn_catdeal`
--
ALTER TABLE `mn_catdeal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT cho bảng `mn_catelog`
--
ALTER TABLE `mn_catelog`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;
--
-- AUTO_INCREMENT cho bảng `mn_catlibrary`
--
ALTER TABLE `mn_catlibrary`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT cho bảng `mn_catmedia`
--
ALTER TABLE `mn_catmedia`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT cho bảng `mn_catnews`
--
ALTER TABLE `mn_catnews`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT cho bảng `mn_catpercent`
--
ALTER TABLE `mn_catpercent`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT cho bảng `mn_catstudent`
--
ALTER TABLE `mn_catstudent`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT cho bảng `mn_code`
--
ALTER TABLE `mn_code`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `mn_code_discount`
--
ALTER TABLE `mn_code_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `mn_color`
--
ALTER TABLE `mn_color`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT cho bảng `mn_comment`
--
ALTER TABLE `mn_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT cho bảng `mn_comment_payment`
--
ALTER TABLE `mn_comment_payment`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT cho bảng `mn_contact`
--
ALTER TABLE `mn_contact`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT cho bảng `mn_cooperation`
--
ALTER TABLE `mn_cooperation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT cho bảng `mn_customer`
--
ALTER TABLE `mn_customer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=418;
--
-- AUTO_INCREMENT cho bảng `mn_customer_reasoncancel`
--
ALTER TABLE `mn_customer_reasoncancel`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `mn_deal`
--
ALTER TABLE `mn_deal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT cho bảng `mn_discount`
--
ALTER TABLE `mn_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `mn_district`
--
ALTER TABLE `mn_district`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=716;
--
-- AUTO_INCREMENT cho bảng `mn_flash`
--
ALTER TABLE `mn_flash`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT cho bảng `mn_follow`
--
ALTER TABLE `mn_follow`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT cho bảng `mn_gift`
--
ALTER TABLE `mn_gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT cho bảng `mn_guid`
--
ALTER TABLE `mn_guid`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `mn_library`
--
ALTER TABLE `mn_library`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT cho bảng `mn_location`
--
ALTER TABLE `mn_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `mn_manufacturer`
--
ALTER TABLE `mn_manufacturer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT cho bảng `mn_media`
--
ALTER TABLE `mn_media`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT cho bảng `mn_menu`
--
ALTER TABLE `mn_menu`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT cho bảng `mn_news`
--
ALTER TABLE `mn_news`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT cho bảng `mn_pagehtml`
--
ALTER TABLE `mn_pagehtml`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT cho bảng `mn_partners`
--
ALTER TABLE `mn_partners`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `mn_product`
--
ALTER TABLE `mn_product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3393;
--
-- AUTO_INCREMENT cho bảng `mn_provinces`
--
ALTER TABLE `mn_provinces`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT cho bảng `mn_size`
--
ALTER TABLE `mn_size`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT cho bảng `mn_student`
--
ALTER TABLE `mn_student`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT cho bảng `mn_support`
--
ALTER TABLE `mn_support`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `mn_tags`
--
ALTER TABLE `mn_tags`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `mn_user`
--
ALTER TABLE `mn_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=850;
--
-- AUTO_INCREMENT cho bảng `mn_voucher`
--
ALTER TABLE `mn_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT cho bảng `mn_voucher_type`
--
ALTER TABLE `mn_voucher_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `mn_weblink`
--
ALTER TABLE `mn_weblink`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT cho bảng `mn_webplus`
--
ALTER TABLE `mn_webplus`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT cho bảng `mn_website`
--
ALTER TABLE `mn_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `songuoi`
--
ALTER TABLE `songuoi`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `spr_app_routes`
--
ALTER TABLE `spr_app_routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `stats`
--
ALTER TABLE `stats`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318901;
--
-- AUTO_INCREMENT cho bảng `tv_admin`
--
ALTER TABLE `tv_admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `tv_adminmenu`
--
ALTER TABLE `tv_adminmenu`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT cho bảng `tv_catnews`
--
ALTER TABLE `tv_catnews`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT cho bảng `tv_guid`
--
ALTER TABLE `tv_guid`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT cho bảng `tv_manufacturer`
--
ALTER TABLE `tv_manufacturer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT cho bảng `tv_menu`
--
ALTER TABLE `tv_menu`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT cho bảng `tv_pagehtml`
--
ALTER TABLE `tv_pagehtml`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT cho bảng `tv_quote`
--
ALTER TABLE `tv_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `tv_support`
--
ALTER TABLE `tv_support`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `tv_tool`
--
ALTER TABLE `tv_tool`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT cho bảng `tv_user`
--
ALTER TABLE `tv_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT cho bảng `tv_website`
--
ALTER TABLE `tv_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
