'use strict';
//var uiMask = require('angular-ui-mask');


//publish
var UrlDomainSite = 'http://ws.hutech.edu.vn/apituyensinh/Resources/json/';
var UrlDomainAPI = 'http://api.hutech.edu.vn/landingpage/';

//test
//var UrlDomainSite = 'http://test3.hutech.edu.vn/tuyensinh2018/';
//var UrlDomainAPI = 'http://test4.hutech.edu.vn/api/';

//local
//var UrlDomainSite = 'http://localhost:2047/js/TuyenSinh/';
//var UrlDomainAPI = 'http://test4.hutech.edu.vn/';



var appDangky = angular.module('appDangky', ['ui.mask', 'ui.bootstrap']);

var mySplit = function (string, nb) {
    var array = string.split(',');
    return array[nb];
}
appDangky.factory('loadingCounts', function () {
    return {
        enable_count: 0,
        disable_count: 0
    }
});
appDangky.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        return {
            'request': function (config) {
                $rootScope.$broadcast('loading-started');
                return config || $q.when(config);
            },
            'response': function (response) {
                $rootScope.$broadcast('loading-complete');
                return response || $q.when(response);
            },
            'responseError': function (rejection) {
                $rootScope.$broadcast('loading-complete');
                return $q.reject(rejection);
            }
        };
    });
});

appDangky.directive("loadingIndicator", function (loadingCounts, $timeout) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            scope.$on("loading-started", function (e) {
                loadingCounts.enable_count++;
                console.log("displaying indicator " + loadingCounts.enable_count);
                //only show if longer than one sencond
                $timeout(function () {
                    if (loadingCounts.enable_count > loadingCounts.disable_count) {
                        element.css({ "display": "" });
                    }
                }, 5000);  
            });
            scope.$on("loading-complete", function (e) {
                loadingCounts.disable_count++;
                console.log("hiding indicator " + loadingCounts.disable_count);
                if (loadingCounts.enable_count == loadingCounts.disable_count) {
                    element.css({ "display": "none" });
                }
            });
        }
    };
});

//check number có thập phân
appDangky.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                else if(val > 10)
                {
                    alert('Điểm số không vượt quá 10');
                    val = '';
                    return val;
                }
                var clean = val.replace(/[^0-9\.]/g, '');//only check number "val.replace(/[^0-9]+/g, '')"
                //check decimal 2 number
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }
                //end check decimal
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


appDangky.controller('mainController', ['$filter','$scope', '$http', '$window', function ($filter,$scope, $http,$window) {

    //$scope.URLFile='/LandingPageDangKy/';
    $scope.URLFile='';//local

    $scope.myDate = new Date();
      
    $scope.maxDate = new Date(
        $scope.myDate.getFullYear(),
        $scope.myDate.getMonth() + 12,
        $scope.myDate.getDate());
    
    $scope.NgaySinhTemp=null; 
    var resultList;
    $http.get(UrlDomainSite+"ThongTinXetTuyenHocBa.json") //load file json dữ liệu cấu hình
        .success(function (restult) {
            resultList = restult;
        
            //get các thông tin cấu hình view page
            $scope.dangky = {
                ListGioiTinh: resultList.ListGioiTinh,

                ListChuongTrinhDaoTao: resultList.ListChuongTrinhDaoTao,
                ListNamTotNghiep:resultList.ListNamTotNghiep, 
                ListNganhXetTuyen: resultList.ListNganhXetTuyen, //2019 - chuẩn nhật bản,
                ListNganhXetTuyenHocBa: resultList.ListNganhXetTuyenHocBa, //2019 - học bạ
                ListNganhXetTuyenThacSi: resultList.ListNganhXetTuyenThacSi,
                ListHeBacTotNghiep: resultList.ListHeBacTotNghiep,
                ListHeBacLienThong: resultList.ListHeBacLienThong,
                ListNganhLienThong: resultList.ListNganhLienThong,
                ListTrinhDo: resultList.ListTrinhDo,
                ListNganhDaoTaoTuXa: resultList.ListNganhDaoTaoTuXa,
                ListNganhVanBang2: resultList.ListNganhVanBang2,
                ListChuongTrinhDaoTaoThacSiQuocTe: resultList.ListChuongTrinhDaoTaoThacSiQuocTe,
                ListNganhXetTuyenCaoDangChinhQuy: resultList.ListNganhXetTuyenCaoDangChinhQuy, //2019 - ngành xét tuyển KS/CN thực hành
                ListNganhXetTuyenChuanTiengAnh: resultList.ListNganhXetTuyenChuanTiengAnh //2019 - ĐH chuẩn tiếng anh
            };

            $scope.getTruongDoiTac(resultList.ListChuongTrinhDaoTao[0]);
            $scope.getToHopMon(resultList.ListNganhXetTuyen[0]); //2019 - chuẩn nhật bản,
            $scope.getToHopMonHocBa(resultList.ListNganhXetTuyenHocBa[0]); //2019 - học bạ
            $scope.getTruongDoiTacThacSiQuocTe(resultList.ListChuongTrinhDaoTaoThacSiQuocTe[0]);
            //$scope.getToHopMonCaoDangChinhQuy(resultList.ListNganhXetTuyenCaoDangChinhQuy[0]);
            $scope.getToHopMonChuanTiengAnh(resultList.ListNganhXetTuyenChuanTiengAnh[0]); //2019 - ĐH chuẩn tiếng anh
        
        
            $scope.kq = {
                HoVaTen:"",
                GioiTinh:{
                    "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                    "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                },
                NamSinh:"",
                SoDienThoai:"",
                DiaChiLienHe:"",
                TruongTHPT:"",
                NamTotNghiep:{
                    "NamID": resultList.ListNamTotNghiep[0].NamID,
                    "NamName": resultList.ListNamTotNghiep[0].NamName
                },
                Email:"",
                ChuongTrinhDaoTao:{
                    "ChuongTrinhDaoTaoID": resultList.ListChuongTrinhDaoTao[0].ChuongTrinhDaoTaoID,
                    "ChuongTrinhDaoTaoName": resultList.ListChuongTrinhDaoTao[0].ChuongTrinhDaoTaoName
                },
                ChonTruongDoiTac:{
                    "TruongDoiTacID": resultList.ListChonTruongDoiTac[0].TruongDoiTacID,
                    "TruongDoiTacName": resultList.ListChonTruongDoiTac[0].TruongDoiTacName
                },
                NganhXetTuyen:{
                    "NganhXetTuyenID": resultList.ListNganhXetTuyen[0].NganhXetTuyenID,
                    "NganhXetTuyenName": resultList.ListNganhXetTuyen[0].NganhXetTuyenName
                },
                ChonToHopMon:{
                    "ToHopMonID": resultList.ListChonToHopMon[0].ToHopMonID,
                    "ToHopMonName": resultList.ListChonToHopMon[0].ToHopMonName
                },
                NganhXetTuyenHocBa:{
                    "NganhXetTuyenID": resultList.ListNganhXetTuyenHocBa[0].NganhXetTuyenID,
                    "NganhXetTuyenName": resultList.ListNganhXetTuyenHocBa[0].NganhXetTuyenName
                },
                ChonToHopMonHocBa:{
                    "ToHopMonID": resultList.ListChonToHopMonHocBa[0].ToHopMonID,
                    "ToHopMonName": resultList.ListChonToHopMonHocBa[0].ToHopMonName
                },
                NganhXetTuyenThacSi:{
                    "NganhXetTuyenID": resultList.ListNganhXetTuyenThacSi[0].NganhXetTuyenID,
                    "NganhXetTuyenName": resultList.ListNganhXetTuyenThacSi[0].NganhXetTuyenName
                },
                HeBacTotNghiep:{
                    "HeBacTotNghiepID": resultList.ListHeBacTotNghiep[0].HeBacTotNghiepID,
                    "HeBacTotNghiepName": resultList.ListHeBacTotNghiep[0].HeBacTotNghiepName
                },
                HeBacLienThong:{
                    "HeBacID": resultList.ListHeBacLienThong[0].HeBacID,
                    "HeBacName": resultList.ListHeBacLienThong[0].HeBacName
                },
                NganhLienThong:{
                    "NganhID": resultList.ListNganhLienThong[0].NganhID,
                    "NganhName": resultList.ListNganhLienThong[0].NganhName
                },
                TrinhDo:{
                    "TrinhDoID": resultList.ListTrinhDo[0].TrinhDoID,
                    "TrinhDoName": resultList.ListTrinhDo[0].TrinhDoName
                },
                NganhDaoTaoTuXa:{
                    "NganhID": resultList.ListNganhDaoTaoTuXa[0].NganhID,
                    "NganhName": resultList.ListNganhDaoTaoTuXa[0].NganhName
                },
                NganhVanBang2:{
                    "NganhID": resultList.ListNganhVanBang2[0].NganhID,
                    "NganhName": resultList.ListNganhVanBang2[0].NganhName
                },
                ChuongTrinhDaoTaoThacSiQuocTe:{
                    "ChuongTrinhDaoTaoID": resultList.ListChuongTrinhDaoTaoThacSiQuocTe[0].ChuongTrinhDaoTaoID,
                    "ChuongTrinhDaoTaoName": resultList.ListChuongTrinhDaoTaoThacSiQuocTe[0].ChuongTrinhDaoTaoName
                },
                ChonTruongDoiTacThacSiQuocTe:{
                    "TruongDoiTacID": resultList.ListChonTruongDoiTacThacSiQuocTe[0].TruongDoiTacID,
                    "TruongDoiTacName": resultList.ListChonTruongDoiTacThacSiQuocTe[0].TruongDoiTacName
                },
                //2019 - ngành xét tuyển KS/CN thực hành
                NganhXetTuyenCaoDangChinhQuy:{
                    "NganhXetTuyenID": resultList.ListNganhXetTuyenCaoDangChinhQuy[0].NganhXetTuyenID,
                    "NganhXetTuyenName": resultList.ListNganhXetTuyenCaoDangChinhQuy[0].NganhXetTuyenName
                },
                //ChonToHopMonCaoDangChinhQuy:{
                //    "ToHopMonID": resultList.ListChonToHopMonCaoDangChinhQuy[0].ToHopMonID,
                //    "ToHopMonName": resultList.ListChonToHopMonCaoDangChinhQuy[0].ToHopMonName
                //},
                NganhXetTuyenChuanTiengAnh:{
                    "NganhXetTuyenID": resultList.ListNganhXetTuyenChuanTiengAnh[0].NganhXetTuyenID,
                    "NganhXetTuyenName": resultList.ListNganhXetTuyenChuanTiengAnh[0].NganhXetTuyenName
                },
                ChonToHopMonChuanTiengAnh:{
                    "ToHopMonID": resultList.ListChonToHopMonChuanTiengAnh[0].ToHopMonID,
                    "ToHopMonName": resultList.ListChonToHopMonChuanTiengAnh[0].ToHopMonName
                },
                KetQuaKhaoSat: [],
                Capcha:""
            };
        });

    //xử lý tiêu chí khảo sát
    $scope.ListTieuChiKhaoSat = ['Facebook','Tìm kiếm Cốc cốc', 'Bài viết, tin tức báo mạng','Chương trình tư vấn trực tuyến trên truyền hình', 'Bài viết, tin tức trên báo giấy','Số tay Tuyển sinh', 'Các banner tuyển sinh trên báo mạng','Cẩm nang hướng nghiệp','Tìm kiếm trên Google','Website HUTECH'];
    $scope.toggleSelectionKhaoSat = function toggleSelectionKhaoSat(fruitName) {
        var idx = $scope.kq.KetQuaKhaoSat.indexOf(fruitName);
        if (idx > -1) {
            $scope.kq.KetQuaKhaoSat.splice(idx, 1);
        }
        else {
            $scope.kq.KetQuaKhaoSat.push(fruitName);
        }
    };
    
    //xử lý load dropdown sub từ event dropdown main
    $scope.getTruongDoiTac=function getTruongDoiTac(item)
    {
        var listTruongDoiTacView=[];
        var listID= item.CacTruongDoiTac;

        for(var i=0;i<listID.length;i++)  
        {
            for(var j=0;j<resultList.ListChonTruongDoiTac.length;j++)  
            {
                if(listID[i].id==resultList.ListChonTruongDoiTac[j].TruongDoiTacID)
                {
                    listTruongDoiTacView.push(resultList.ListChonTruongDoiTac[j]);
                    break;
                }
            }
        }
        $scope.dangky.ListChonTruongDoiTac=listTruongDoiTacView;
    }
    $scope.getToHopMon=function getToHopMon(item)
    {
        var listToHopMonView=[];
        var listID= item.ToHopMon;

        for(var i=0;i<listID.length;i++)  
        {
            for(var j=0;j<resultList.ListChonToHopMon.length;j++)  
            {
                if(listID[i].id==resultList.ListChonToHopMon[j].ToHopMonID)
                {
                    listToHopMonView.push(resultList.ListChonToHopMon[j]);
                    break;
                }
            }
        }
        $scope.dangky.ListChonToHopMon=listToHopMonView;
    }
    $scope.getToHopMonHocBa=function getToHopMonHocBa(item)
    {
        var listToHopMonView=[];
        var listID= item.ToHopMon;

        for(var i=0;i<listID.length;i++)  
        {
            for(var j=0;j<resultList.ListChonToHopMonHocBa.length;j++)  
            {
                if(listID[i].id==resultList.ListChonToHopMonHocBa[j].ToHopMonID)
                {
                    listToHopMonView.push(resultList.ListChonToHopMonHocBa[j]);
                    break;
                }
            }
        }
        $scope.dangky.ListChonToHopMonHocBa=listToHopMonView;
    }     
    $scope.getTruongDoiTacThacSiQuocTe=function getTruongDoiTacThacSiQuocTe(item)
    {

        var listTruongDoiTacView=[];
        var listID= item.CacTruongDoiTac;

        for(var i=0;i<listID.length;i++)  
        {
            for(var j=0;j<resultList.ListChonTruongDoiTacThacSiQuocTe.length;j++)  
            {
                if(listID[i].id==resultList.ListChonTruongDoiTacThacSiQuocTe[j].TruongDoiTacID)
                {
                    listTruongDoiTacView.push(resultList.ListChonTruongDoiTacThacSiQuocTe[j]);
                    break;
                }
            }
        }
        $scope.dangky.ListChonTruongDoiTacThacSiQuocTe=listTruongDoiTacView;
    }
    //$scope.getToHopMonCaoDangChinhQuy=function getToHopMonCaoDangChinhQuy(item)
    //{
    //    var listToHopMonView=[];
    //    var listID= item.ToHopMon;

    //    for(var i=0;i<listID.length;i++)  
    //    {
    //        for(var j=0;j<resultList.ListChonToHopMonCaoDangChinhQuy.length;j++)  
    //        {
    //            if(listID[i].id==resultList.ListChonToHopMonCaoDangChinhQuy[j].ToHopMonID)
    //            {
    //                listToHopMonView.push(resultList.ListChonToHopMonCaoDangChinhQuy[j]);
    //                break;
    //            }
    //        }
    //    }
    //    $scope.dangky.ListChonToHopMonCaoDangChinhQuy=listToHopMonView;
    //}
    $scope.getToHopMonChuanTiengAnh=function getToHopMonChuanTiengAnh(item)
    {
        var listToHopMonView=[];
        var listID= item.ToHopMon;

        for(var i=0;i<listID.length;i++)  
        {
            for(var j=0;j<resultList.ListChonToHopMonChuanTiengAnh.length;j++)  
            {
                if(listID[i].id==resultList.ListChonToHopMonChuanTiengAnh[j].ToHopMonID)
                {
                    listToHopMonView.push(resultList.ListChonToHopMonChuanTiengAnh[j]);
                    break;
                }
            }
        }
        $scope.dangky.ListChonToHopMonChuanTiengAnh=listToHopMonView;
    }     

    //xử lý ngày sinh
    $('#NamSinh').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });    
    $('#NamSinh').datepicker().on('changeDate', function (ev) {
        $scope.kq.NamSinh=$('#NamSinh').val();
    });
    $scope.toDate=function toDate(dateStr) {
        if(dateStr === null || dateStr ==="")
            return ""; //nếu null hoặc empty thì trả về empty
        const [day, month, year] = dateStr.split("/")
        return new Date(year, month - 1, day)
    }

    //xử lý capcha
    $scope.DrawBotBoot = function ()
    {
        var a = Math.ceil(Math.random() * 10);
        var b = Math.ceil(Math.random() * 10);       
        $scope.CapChaCheck = a + b;
        var text="Câu hỏi bảo mật "+ a + " + " + b +"=? ";
        $("#CauHoiCapCha").html(text);
    }    
    $scope.DrawBotBoot();

    //Xử lý tính tổng điểm
    $scope.updateTotal = function() {
        var total = parseFloat($scope.kq.DiemMon1 || 0) + parseFloat($scope.kq.DiemMon2 || 0) +
                    parseFloat($scope.kq.DiemMon3 || 0) ;
        $scope.kq.TongDiem = (total || 0);
    };

    //sự kiện submit form
    //2019    
    $scope.submitCaoDangChinhQuy = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divNganhXetTuyen').removeClass('has-error'); //cao đẳng chính quy
        //$('#divChonToHopMon').removeClass('has-error'); //cao đẳng chính quy
        var flag=true;
        if($scope.kq.NganhXetTuyen.NganhXetTuyenID=="") //cao đẳng chính quy
        {  $('#divNganhXetTuyen').addClass('has-error');flag=false; } 
        //if($scope.kq.ChonToHopMon.ToHopMonID=="") //cao đẳng chính quy
        //{  $('#divChonToHopMon').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //cao đẳng chính quy
                "sodienthoai":$scope.kq.SoDienThoai, //cao đẳng chính quy
                "email":$scope.kq.Email, //cao đẳng chính quy
                //"nganhdangkyhoc":$scope.kq.NganhXetTuyen.NganhXetTuyenID, //cao đẳng chính quy
                "nganhxettuyenid":$scope.kq.NganhXetTuyen.NganhXetTuyenID, //2019 - KS/CN thực hành
                "nganhxettuyenname":$scope.kq.NganhXetTuyen.NganhXetTuyenName, //2019 - KS/CN thực hành
                //"tohopmonxettuyen":$scope.kq.ChonToHopMon.ToHopMonID, //cao đẳng chính quy
                "tinhthanh":$scope.kq.TinhThanh, //cao đẳng chính quy
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //cao đẳng chính quy
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //cao đẳng chính quy
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        SoDienThoai:"", //cao đẳng chính quy
                        NamTotNghiep:{ //cao đẳng chính quy
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //cao đẳng chính quy
                        NganhXetTuyen:{ //cao đẳng chính quy
                            "NganhXetTuyenID": resultList.ListNganhXetTuyenCaoDangChinhQuy[0].NganhXetTuyenID,
                            "NganhXetTuyenName": resultList.ListNganhXetTuyenCaoDangChinhQuy[0].NganhXetTuyenName
                        },
                        //ChonToHopMon:{ //cao đẳng chính quy
                        //    "ToHopMonID": resultList.ListChonToHopMonCaoDangChinhQuy[0].ToHopMonID,
                        //    "ToHopMonName": resultList.ListChonToHopMonCaoDangChinhQuy[0].ToHopMonName
                        //},
                        TinhThanh:"", //cao đẳng chính quy
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };
    $scope.submitChuanTiengAnh = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;
        var diemchuan = 18;
        var diemchuanDuoc = 20;
        var manganhDuoc = '9';

        var tongdiem = 0;
        var diem1 = 0;
        var diem2 = 0;
        var diem3 = 0;
        var tohopmon = "";

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }       

       
        //validate control không có trong plugin
        $('#divNamTotNghiep').removeClass('has-error');  //đại học tiếng anh
        $('#divChonToHopMon').removeClass('has-error');  //đại học tiếng anh
        $('#divNganhXetTuyen').removeClass('has-error');  //đại học tiếng anh        
        //$('#divDiemTBChung').removeClass('has-error'); //2019 - đại học tiếng anh
        $('#divDiemTB').removeClass('has-error'); //2019 - đại học tiếng anh 
        var flag=true;
        if($scope.kq.NamTotNghiep.NamID=="")  //đại học tiếng anh
        {  $('#divNamTotNghiep').addClass('has-error');flag=false; } 
        //if($scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenID=="")  //đại học tiếng anh
        //{  $('#divNganhXetTuyen').addClass('has-error');flag=false; } 

        if($scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenID=="") //đại học tiếng anh
        {  
            $('#divNganhXetTuyen').addClass('has-error');
            flag=false; 
            alert("Vui lòng chọn Ngành đăng ký xét tuyển!");
            return;
        } 

        if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID=="") //đại học tiếng anh
        {
            $('#divChonToHopMon').addClass('has-error');
            flag=false;         
            alert("Vui lòng chọn Tổ hợp môn xét tuyển!");
            return;
        } 

        
        //if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID != "" && ($scope.kq.TongDiemChung !== undefined && $scope.kq.TongDiemChung != ""))
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    $('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    alert('Vui lòng chọn 1 trong 2 hình thức xét tuyển: Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    return;
        //}

        //if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID == "" && ($scope.kq.TongDiemChung === undefined || $scope.kq.TongDiemChung == ""))
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    $('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    alert('Vui lòng chọn hình thức xét tuyển theo Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    return;
        //}
        //else
        //{
            if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID != "" 
                && ($scope.kq.DiemMon1===undefined || $scope.kq.DiemMon2===undefined || $scope.kq.DiemMon3===undefined))
            {
                $('#divDiemTB').addClass('has-error');
                flag=false; 
                alert('Vui lòng nhập điểm trung bình lớp 12 (03 môn)!');
                return;
            }
        //}

        //if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID=="" && $scope.kq.TongDiemChung=="")  //2019 - đại học tiếng anh
        //{$('#divChonToHopMon').addClass('has-error');flag=false;        
        //    alert('Vui lòng chọn hình thức xét tuyển theo Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');} 

        if (form.$valid&&flag) {
            if($scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenID != manganhDuoc && $scope.kq.TongDiem < diemchuan)   
            {
                $("#modalRedirect").modal('show');   
                return;
            }

            if($scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenID == manganhDuoc && $scope.kq.TongDiem < diemchuanDuoc)    
            {
                $("#modalRedirectDuoc").modal('show');   
                return;
            }
            
            if($scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID != "" && $scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonID !== undefined)    
            {
                //alert("lưu to hop mon");
                tohopmon = $scope.kq.ChonToHopMonChuanTiengAnh.ToHopMonName
                tongdiem = $scope.kq.TongDiem;
                diem1 = $scope.kq.DiemMon1;
                diem2 = $scope.kq.DiemMon2;
                diem3 = $scope.kq.DiemMon3;
            }
            //else if($scope.kq.TongDiemChung != "" && $scope.kq.TongDiemChung !== undefined)  
            //{
            //    //alert("lưu diem tb");
            //    tongdiem = $scope.kq.TongDiemChung;
            //}

            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID,  //đại học tiếng anh
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"),  //đại học tiếng anh
                "sodienthoai":$scope.kq.SoDienThoai,  //đại học tiếng anh
                "diachilienhe":$scope.kq.DiaChiLienHe,  //đại học tiếng anh
                "truongtotnghiep":$scope.kq.TruongTHPT,  //đại học tiếng anh
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID,  //đại học tiếng anh
                "email":$scope.kq.Email,  //đại học tiếng anh
                "nganhxettuyenid":$scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenID, //2019 - đại học tiếng anh
                "nganhxettuyenname":$scope.kq.NganhXetTuyenChuanTiengAnh.NganhXetTuyenName, //2019 - đại học tiếng anh
                "tohopmonxettuyen":tohopmon,  //2019 - đại học tiếng anh
                "diemtb_mon1":diem1,  //2019 - đại học tiếng anh
                "diemtb_mon2":diem2,  //2019 - đại học tiếng anh
                "diemtb_mon3":diem3,  //2019 - đại học tiếng anh
                "diemtb_tong":tongdiem,  //2019 - đại học tiếng anh
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{  //đại học tiếng anh
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"",  //đại học tiếng anh
                        SoDienThoai:"",  //đại học tiếng anh
                        DiaChiLienHe:"",  //đại học tiếng anh
                        TruongTHPT:"",  //đại học tiếng anh
                        NamTotNghiep:{  //đại học tiếng anh
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"",  //đại học tiếng anh
                        NganhXetTuyen:{  //đại học tiếng anh
                            "NganhXetTuyenID": resultList.ListNganhXetTuyenChuanTiengAnh[0].NganhXetTuyenID,
                            "NganhXetTuyenName": resultList.ListNganhXetTuyenChuanTiengAnh[0].NganhXetTuyenName
                        },
                        ChonToHopMon:{  //đại học tiếng anh
                            "ToHopMonID": resultList.ListChonToHopMonChuanTiengAnh[0].ToHopMonID,
                            "ToHopMonName": resultList.ListChonToHopMonChuanTiengAnh[0].ToHopMonName
                        },
                        DiemMon1:"",  //đại học tiếng anh
                        DiemMon2:"",  //đại học tiếng anh
                        DiemMon3:"",  //đại học tiếng anh
                        TongDiem:"",  //đại học tiếng anh
                        //TongDiemChung:"", //2019 - đại học tiếng anh
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        }
    };  
    $scope.submitChuanNhatBan = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;
        var diemchuan = 18;
        
        var tongdiem = 0;
        var diem1 = 0;
        var diem2 = 0;
        var diem3 = 0;
        var tohopmon = "";

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }        
       
        //validate control không có trong plugin
        $('#divChonToHopMon').removeClass('has-error'); //đại học chuẩn nhật bản
        $('#divNganhXetTuyen').removeClass('has-error'); //đại học chuẩn nhật bản   
        //$('#divDiemTBChung').removeClass('has-error'); //2019 - đại học chuẩn nhật bản   
        $('#divDiemTB').removeClass('has-error'); //2019 - đại học chuẩn nhật bản   
        var flag=true;

        //if($scope.kq.NganhXetTuyen.NganhXetTuyenID=="") //đại học chuẩn nhật bản
        //{  $('#divNganhXetTuyen').addClass('has-error');flag=false; } 
                
        if($scope.kq.NganhXetTuyen.NganhXetTuyenID=="") //đại học chuẩn nhật bản
        {  
            $('#divNganhXetTuyen').addClass('has-error');
            flag=false; 
            alert("Vui lòng chọn Ngành đăng ký xét tuyển!");
            return;
        } 

        if($scope.kq.ChonToHopMon.ToHopMonID=="") //đại học chuẩn nhật bản
        {
            $('#divChonToHopMon').addClass('has-error');
            flag=false;         
            alert("Vui lòng chọn Tổ hợp môn xét tuyển!");
            return;
        } 

        //if($scope.kq.ChonToHopMon.ToHopMonID=="") //đại học chuẩn nhật bản
        //{$('#divChonToHopMon').addClass('has-error');flag=false; } 
        //if($scope.kq.ChonToHopMon.ToHopMonID != "" && ($scope.kq.TongDiemChung !== undefined && $scope.kq.TongDiemChung != ""))
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    $('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    alert('Vui lòng chọn 1 trong 2 hình thức xét tuyển: Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    return;
        //}

        //if($scope.kq.ChonToHopMon.ToHopMonID == "" && ($scope.kq.TongDiemChung === undefined || $scope.kq.TongDiemChung == ""))
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    $('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    alert('Vui lòng chọn hình thức xét tuyển theo Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    return;
        //}
        //else
        //{
        if($scope.kq.ChonToHopMon.ToHopMonID != "" 
            && ($scope.kq.DiemMon1===undefined || $scope.kq.DiemMon2===undefined || $scope.kq.DiemMon3===undefined))
        {
            $('#divDiemTB').addClass('has-error');
            flag=false; 
            alert('Vui lòng nhập điểm trung bình lớp 12 (03 môn)!');
            return;
        }
        //}

        if (form.$valid&&flag) {
            if($scope.kq.TongDiem < diemchuan)                                  
                $("#modalRedirect").modal('show');   
            else
            {                 
                if($scope.kq.ChonToHopMon.ToHopMonID != "" && $scope.kq.ChonToHopMon.ToHopMonID !== undefined)    
                {
                    //alert("lưu to hop mon");
                    tohopmon = $scope.kq.ChonToHopMon.ToHopMonName
                    tongdiem = $scope.kq.TongDiem;
                    diem1 = $scope.kq.DiemMon1;
                    diem2 = $scope.kq.DiemMon2;
                    diem3 = $scope.kq.DiemMon3;
                }
                //else if($scope.kq.TongDiemChung != "" && $scope.kq.TongDiemChung !== undefined)  
                //{
                //    //alert("lưu diem tb");
                //    tongdiem = $scope.kq.TongDiemChung;
                //}

                $http({
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                    },
                    method: "POST",
                    url: UrlDomainAPI+"api/dangkynhaphoc/create",
                    data: 
                {
                    "hovaten":$scope.kq.HoVaTen, //all
                    "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //đại học chuẩn nhật bản
                    "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //đại học chuẩn nhật bản
                    "sodienthoai":$scope.kq.SoDienThoai, //đại học chuẩn nhật bản
                    "diachilienhe":$scope.kq.DiaChiLienHe, //đại học chuẩn nhật bản
                    "email":$scope.kq.Email, //đại học chuẩn nhật bản
                    //"nganhdangkyhoc":$scope.kq.NganhXetTuyen.NganhXetTuyenID, //đại học chuẩn nhật bản
                    "nganhxettuyenid":$scope.kq.NganhXetTuyen.NganhXetTuyenID, //2019 - đại học chuẩn nhật bản
                    "nganhxettuyenname":$scope.kq.NganhXetTuyen.NganhXetTuyenName, //2019 - đại học chuẩn nhật bản
                    "tohopmonxettuyen":tohopmon,  //2019 - đại học chuẩn nhật bản
                    "diemtb_mon1":diem1,  //2019 - đại học chuẩn nhật bản
                    "diemtb_mon2":diem2,  //2019 - đại học chuẩn nhật bản
                    "diemtb_mon3":diem3,  //2019 - đại học chuẩn nhật bản
                    "diemtb_tong":tongdiem,  //2019 - đại học chuẩn nhật bản
                    "truongtotnghiep":$scope.kq.TruongTHPT, //đại học chuẩn nhật bản
                    "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //đại học chuẩn nhật bản
                    "bieumau":bieumau, //all
                    "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
                }
                })
                    .success(function (data, status, headers, config) {                    
                        $("#myModal").modal('show');     
                        $scope.kq = {
                            HoVaTen:"", //all
                            GioiTinh:{ //đại học chuẩn nhật bản
                                "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                                "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                            },
                            NamSinh:"", //đại học chuẩn nhật bản
                            SoDienThoai:"", //đại học chuẩn nhật bản
                            DiaChiLienHe:"", //đại học chuẩn nhật bản
                            TruongTHPT:"", //đại học chuẩn nhật bản
                            NamTotNghiep:{ //đại học chuẩn nhật bản
                                "NamID": resultList.ListNamTotNghiep[0].NamID,
                                "NamName": resultList.ListNamTotNghiep[0].NamName
                            },
                            Email:"", //đại học chuẩn nhật bản
                            NganhXetTuyen:{ //đại học chuẩn nhật bản
                                "NganhXetTuyenID": resultList.ListNganhXetTuyen[0].NganhXetTuyenID,
                                "NganhXetTuyenName": resultList.ListNganhXetTuyen[0].NganhXetTuyenName
                            },
                            ChonToHopMon:{ //đại học chuẩn nhật bản
                                "ToHopMonID": resultList.ListChonToHopMon[0].ToHopMonID,
                                "ToHopMonName": resultList.ListChonToHopMon[0].ToHopMonName
                            },
                            DiemMon1:"", //đại học chuẩn nhật bản
                            DiemMon2:"", //đại học chuẩn nhật bản
                            DiemMon3:"", //đại học chuẩn nhật bản
                            TongDiem:"", //đại học chuẩn nhật bản
                            //TongDiemChung:"", //2019 - đại học chuẩn nhật bản
                            KetQuaKhaoSat: [], //all
                            Capcha:"" //all                        
                        };
                    })
                    .error(function (data, status, headers, config) {
                        alert('Đã có lỗi xảy ra vui lòng thử lại!');
                    })
            }
        } 
    };   
    $scope.submitXetTuyenHocBa = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;
        
        var diemchuan = 18;
        var diemchuanDuoc = 20;
        var manganhDuoc = '1';
        
        var tongdiem = 0;
        var diem1 = 0;
        var diem2 = 0;
        var diem3 = 0;
        var tohopmon = "";

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divChonToHopMon').removeClass('has-error'); //học bạ
        $('#divNganhXetTuyenHocBa').removeClass('has-error'); //học bạ
        //$('#divDiemTBChung').removeClass('has-error'); //2019 - học bạ
        $('#divDiemTB').removeClass('has-error'); //2019 - học bạ
        var flag=true;

        if($scope.kq.NganhXetTuyenHocBa.NganhXetTuyenID=="") //học bạ
        {  
            $('#divNganhXetTuyenHocBa').addClass('has-error');
            flag=false; 
            alert("Vui lòng chọn Ngành đăng ký xét tuyển!");
            return;
        } 

        if($scope.kq.ChonToHopMonHocBa.ToHopMonID=="") //học bạ
        {
            $('#divChonToHopMon').addClass('has-error');
            flag=false;         
            alert("Vui lòng chọn Tổ hợp môn xét tuyển!");
            return;
        } 
        
        //if($scope.kq.ChonToHopMonHocBa.ToHopMonID != "" && ($scope.kq.TongDiemChung !== undefined && $scope.kq.TongDiemChung != ""))
        //if($scope.kq.ChonToHopMonHocBa.ToHopMonID != "")
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    //$('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    //alert('Vui lòng chọn 1 trong 2 hình thức xét tuyển: Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    ////alert('Vui lòng chọn Tổ hợp môn xét tuyển!');
        //    return;
        //}

        //if($scope.kq.ChonToHopMonHocBa.ToHopMonID == "" && ($scope.kq.TongDiemChung === undefined || $scope.kq.TongDiemChung == ""))
        //{
        //    $('#divChonToHopMon').addClass('has-error');
        //    $('#divDiemTBChung').addClass('has-error');
        //    flag=false; 
        //    alert('Vui lòng chọn hình thức xét tuyển theo Tổ hợp 3 môn hoặc Theo điểm TB chung cả năm!');
        //    return;
        //}
        //else
        //{

        if($scope.kq.ChonToHopMonHocBa.ToHopMonID != "" 
            && ($scope.kq.DiemMon1===undefined || $scope.kq.DiemMon2===undefined || $scope.kq.DiemMon3===undefined))
        {
            $('#divDiemTB').addClass('has-error');
            flag=false; 
            alert('Vui lòng nhập điểm trung bình lớp 12 (03 môn)!');
            return;
        }
        //}

        if (form.$valid && flag) {
            if($scope.kq.NganhXetTuyenHocBa.NganhXetTuyenID != manganhDuoc && $scope.kq.TongDiem < diemchuan)   
            {
                $("#modalRedirect").modal('show');   
                return;
            }

            if($scope.kq.NganhXetTuyenHocBa.NganhXetTuyenID == manganhDuoc && $scope.kq.TongDiem < diemchuanDuoc)    
            {
                $("#modalRedirectDuoc").modal('show');   
                return;
            }
                        
            if($scope.kq.ChonToHopMonHocBa.ToHopMonID != "" && $scope.kq.ChonToHopMonHocBa.ToHopMonID !== undefined)    
            {
                //alert("lưu to hop mon");
                tohopmon = $scope.kq.ChonToHopMonHocBa.ToHopMonName
                tongdiem = $scope.kq.TongDiem;
                diem1 = $scope.kq.DiemMon1;
                diem2 = $scope.kq.DiemMon2;
                diem3 = $scope.kq.DiemMon3;
            }
            //else if($scope.kq.TongDiemChung != "" && $scope.kq.TongDiemChung !== undefined)  
            //{
            //    //alert("lưu diem tb");
            //    tongdiem = $scope.kq.TongDiemChung;
            //}

            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //học bạ
                "sodienthoai":$scope.kq.SoDienThoai, //học bạ
                "email":$scope.kq.Email, //học bạ
                //"nganhdangkyhoc":$scope.kq.NganhXetTuyenHocBa.NganhXetTuyenID, //học bạ
                "nganhxettuyenid":$scope.kq.NganhXetTuyenHocBa.NganhXetTuyenID, //2019 - học bạ
                "nganhxettuyenname":$scope.kq.NganhXetTuyenHocBa.NganhXetTuyenName, //2019 - học bạ
                "tohopmonxettuyen":tohopmon,  //2019 - học bạ
                "diemtb_mon1":diem1,  //2019 - học bạ
                "diemtb_mon2":diem2,  //2019 - học bạ
                "diemtb_mon3":diem3,  //2019 - học bạ
                "diemtb_tong":tongdiem,  //2019 - học bạ
                "tinhthanh":$scope.kq.TinhThanh, //học bạ
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //học bạ
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //học bạ
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        SoDienThoai:"", //học bạ
                        NamTotNghiep:{ //học bạ
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //học bạ
                        NganhXetTuyenHocBa:{ //học bạ
                            "NganhXetTuyenID": resultList.ListNganhXetTuyenHocBa[0].NganhXetTuyenID,
                            "NganhXetTuyenName": resultList.ListNganhXetTuyenHocBa[0].NganhXetTuyenName
                        },
                        ChonToHopMonHocBa:{ //học bạ
                            "ToHopMonID": resultList.ListChonToHopMonHocBa[0].ToHopMonID,
                            "ToHopMonName": resultList.ListChonToHopMonHocBa[0].ToHopMonName
                        },
                        DiemMon1:"", //học bạ
                        DiemMon2:"", //học bạ
                        DiemMon3:"", //học bạ
                        TongDiem:"", //học bạ
                        //TongDiemChung:"", //2019 - học bạ
                        TinhThanh:"", //học bạ
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };   

    //2018
    $scope.submitCuNhanQuocTe = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }        
        
        //validate control không có trong plugin
        $('#divChonTruongDoiTac').removeClass('has-error'); //cử nhân quốc tế
        $('#divChuongTrinhDaoTao').removeClass('has-error'); //cử nhân quốc tế  
        var flag=true;
        if($scope.kq.ChuongTrinhDaoTao.ChuongTrinhDaoTaoID=="") //cử nhân quốc tế
        {  $('#divChuongTrinhDaoTao').addClass('has-error');flag=false; } 
        if($scope.kq.ChonTruongDoiTac.TruongDoiTacID=="") //cử nhân quốc tế
        {$('#divChonTruongDoiTac').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //cử nhân quốc tế
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //cử nhân quốc tế
                "sodienthoai":$scope.kq.SoDienThoai, //cử nhân quốc tế
                "diachilienhe":$scope.kq.DiaChiLienHe, //cử nhân quốc tế
                "email":$scope.kq.Email, //cử nhân quốc tế
                "chuongtrinhdaotao":$scope.kq.ChuongTrinhDaoTao.ChuongTrinhDaoTaoName, //2019 - cử nhân quốc tế
                "truongdoitac":$scope.kq.ChonTruongDoiTac.TruongDoiTacName, //2019 - cử nhân quốc tế
                "truongtotnghiep":$scope.kq.TruongTHPT, //cử nhân quốc tế
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //cử nhân quốc tế
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all
                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //cử nhân quốc tế
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"", //cử nhân quốc tế
                        SoDienThoai:"", //cử nhân quốc tế
                        DiaChiLienHe:"", //cử nhân quốc tế
                        TruongTHPT:"", //cử nhân quốc tế
                        NamTotNghiep:{ //cử nhân quốc tế
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //cử nhân quốc tế
                        ChuongTrinhDaoTao:{ //cử nhân quốc tế
                            "ChuongTrinhDaoTaoID": resultList.ListChuongTrinhDaoTao[0].ChuongTrinhDaoTaoID,
                            "ChuongTrinhDaoTaoName": resultList.ListChuongTrinhDaoTao[0].ChuongTrinhDaoTaoName
                        },
                        ChonTruongDoiTac:{ //cử nhân quốc tế
                            "TruongDoiTacID": resultList.ListChonTruongDoiTac[0].TruongDoiTacID,
                            "TruongDoiTacName": resultList.ListChonTruongDoiTac[0].TruongDoiTacName
                        },
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };       
    $scope.submitThacSiChinhQuy = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divNganhXetTuyen').removeClass('has-error'); //thạc sĩ chính quy
        var flag=true;
        if($scope.kq.NganhXetTuyenThacSi.NganhXetTuyenID=="") //thạc sĩ chính quy
        {  $('#divNganhXetTuyen').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //thạc sĩ chính quy
                "sodienthoai":$scope.kq.SoDienThoai, //thạc sĩ chính quy
                "diachilienhe":$scope.kq.DiaChiLienHe, //thạc sĩ chính quy
                "email":$scope.kq.Email, //thạc sĩ chính quy
                "noicongtac":$scope.kq.NoiCongTac, //thạc sĩ chính quy
                "nghenghiep":$scope.kq.NgheNghiep, //thạc sĩ chính quy
                "totnghiepnganh":$scope.kq.NganhTNDaiHoc, //thạc sĩ chính quy
                "truongtotnghiep":$scope.kq.TruongTNDaiHoc, //thạc sĩ chính quy
                //"nganhdangkyhoc":$scope.kq.NganhXetTuyenThacSi.NganhXetTuyenID, //thạc sĩ chính quy
                "nganhxettuyenid":$scope.kq.NganhXetTuyenThacSi.NganhXetTuyenID, //2019 - thạc sĩ chính quy
                "nganhxettuyenname":$scope.kq.NganhXetTuyenThacSi.NganhXetTuyenName, //2019 - thạc sĩ chính quy
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //thạc sĩ chính quy
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        NamSinh:"", //thạc sĩ chính quy
                        SoDienThoai:"", //thạc sĩ chính quy
                        DiaChiLienHe:"", //thạc sĩ chính quy
                        NamTotNghiep:{ //thạc sĩ chính quy
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //thạc sĩ chính quy
                        NoiCongTac:"", //thạc sĩ chính quy
                        NgheNghiep:"", //thạc sĩ chính quy
                        NganhTNDaiHoc:"", //thạc sĩ chính quy
                        TruongTNDaiHoc:"", //thạc sĩ chính quy
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };
    $scope.submitLienThongDaiHoc = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divHeBacTotNghiep').removeClass('has-error'); //liên thông đại học
        $('#divNamTotNghiep').removeClass('has-error'); //liên thông đại học
        $('#divHeBacLienThong').removeClass('has-error'); //liên thông đại học
        $('#divNganhLienThong').removeClass('has-error'); //liên thông đại học
        var flag=true;
        if($scope.kq.HeBacTotNghiep.HeBacTotNghiepID=="") //liên thông đại học
        {  $('#divHeBacTotNghiep').addClass('has-error');flag=false; } 
        if($scope.kq.NamTotNghiep.NamID=="") //liên thông đại học
        {  $('#divNamTotNghiep').addClass('has-error');flag=false; } 
        if($scope.kq.HeBacLienThong.HeBacID=="") //liên thông đại học
        {  $('#divHeBacLienThong').addClass('has-error');flag=false; } 
        if($scope.kq.NganhLienThong.NganhID=="") //liên thông đại học
        {  $('#divNganhLienThong').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //liên thông đại học
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //liên thông đại học
                "sodienthoai":$scope.kq.SoDienThoai, //liên thông đại học
                "diachilienhe":$scope.kq.DiaChiLienHe, //liên thông đại học
                "email":$scope.kq.Email, //liên thông đại học
                //"nganhdangkyhoc":$scope.kq.NganhLienThong.NganhID, //liên thông đại học
                "nganhxettuyenid":$scope.kq.NganhLienThong.NganhID, //2019 - liên thông đại học
                "nganhxettuyenname":$scope.kq.NganhLienThong.NganhName, //2019 - liên thông đại học
                "totnghiepnganh":$scope.kq.NganhTotNghiep, //liên thông đại học
                "truongtotnghiep":$scope.kq.TruongTotNghiep, //liên thông đại học
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //liên thông đại học
                "bactotnghiep":$scope.kq.HeBacTotNghiep.HeBacTotNghiepName, //2019 - liên thông đại học
                "baclienthong":$scope.kq.HeBacLienThong.HeBacName, //2019 - liên thông đại học
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //liên thông đại học
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"", //liên thông đại học
                        SoDienThoai:"", //liên thông đại học
                        DiaChiLienHe:"", //liên thông đại học
                        NamTotNghiep:{ //liên thông đại học
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //liên thông đại học
                        TruongTotNghiep:"", //liên thông đại học
                        NganhTotNghiep:"", //liên thông đại học
                        HeBacTotNghiep:{ //liên thông đại học
                            "HeBacTotNghiepID": resultList.ListHeBacTotNghiep[0].HeBacTotNghiepID,
                            "HeBacTotNghiepName": resultList.ListHeBacTotNghiep[0].HeBacTotNghiepName
                        },
                        HeBacLienThong:{ //liên thông đại học
                            "HeBacID": resultList.ListHeBacLienThong[0].HeBacID,
                            "HeBacName": resultList.ListHeBacLienThong[0].HeBacName
                        },
                        NganhLienThong:{ //liên thông đại học
                            "NganhID": resultList.ListNganhLienThong[0].NganhID,
                            "NganhName": resultList.ListNganhLienThong[0].NganhName
                        },
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };
    $scope.submitDaoTaoTuXa = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divTrinhDo').removeClass('has-error'); //đào tạo từ xa
        $('#divNganhDaoTaoTuXa').removeClass('has-error'); //đào tạo từ xa
        var flag=true;
        if($scope.kq.TrinhDo.TrinhDoID=="") //đào tạo từ xa
        {  $('#divTrinhDo').addClass('has-error');flag=false; } 
        if($scope.kq.NganhDaoTaoTuXa.NganhID=="") //đào tạo từ xa
        {  $('#divNganhDaoTaoTuXa').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //đào tạo từ xa
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //đào tạo từ xa
                "sodienthoai":$scope.kq.SoDienThoai, //đào tạo từ xa
                "diachilienhe":$scope.kq.DiaChiLienHe, //đào tạo từ xa
                "email":$scope.kq.Email, //đào tạo từ xa
                //"nganhdangkyhoc":$scope.kq.NganhDaoTaoTuXa.NganhID, //đào tạo từ xa
                "nganhxettuyenid":$scope.kq.NganhDaoTaoTuXa.NganhID, //2019 - đào tạo từ xa
                "nganhxettuyenname":$scope.kq.NganhDaoTaoTuXa.NganhName, //2019 - đào tạo từ xa
                "totnghiepnganh":$scope.kq.NganhTotNghiep, //đào tạo từ xa
                "truongtotnghiep":$scope.kq.TruongTotNghiep, //đào tạo từ xa
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //đào tạo từ xa
                "bactotnghiep":$scope.kq.TrinhDo.TrinhDoID, //đào tạo từ xa
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{  //đào tạo từ xa
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"", //đào tạo từ xa
                        SoDienThoai:"", //đào tạo từ xa
                        DiaChiLienHe:"", //đào tạo từ xa
                        NamTotNghiep:{ //đào tạo từ xa
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //đào tạo từ xa
                        TruongTotNghiep:"", //đào tạo từ xa
                        NganhTotNghiep:"", //đào tạo từ xa
                        TrinhDo:{ //đào tạo từ xa
                            "TrinhDoID": resultList.ListTrinhDo[0].TrinhDoID,
                            "TrinhDoName": resultList.ListTrinhDo[0].TrinhDoName
                        }, 
                        NganhDaoTaoTuXa:{ //đào tạo từ xa
                            "NganhID": resultList.ListNganhDaoTaoTuXa[0].NganhID,
                            "NganhName": resultList.ListNganhDaoTaoTuXa[0].NganhName
                        },
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };
    $scope.submitVanBangHai = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divNamTotNghiep').removeClass('has-error'); //văn bằng 2
        $('#divNganhVanBang2').removeClass('has-error'); //văn bằng 2
        var flag=true;
        if($scope.kq.NamTotNghiep.NamID=="") //văn bằng 2
        {  $('#divNamTotNghiep').addClass('has-error');flag=false; } 
        if($scope.kq.NganhVanBang2.NganhID=="") //văn bằng 2
        {  $('#divNganhVanBang2').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //văn bằng 2
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //văn bằng 2
                "sodienthoai":$scope.kq.SoDienThoai, //văn bằng 2
                "diachilienhe":$scope.kq.DiaChiLienHe, //văn bằng 2
                "email":$scope.kq.Email, //văn bằng 2
                //"nganhdangkyhoc":$scope.kq.NganhVanBang2.NganhID, //văn bằng 2
                "nganhxettuyenid":$scope.kq.NganhVanBang2.NganhID, //2019 - văn bằng 2
                "nganhxettuyenname":$scope.kq.NganhVanBang2.NganhName, //2019 - văn bằng 2
                "totnghiepnganh":$scope.kq.NganhTotNghiep, //văn bằng 2
                "truongtotnghiep":$scope.kq.TruongTotNghiep, //văn bằng 2
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //văn bằng 2
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //văn bằng 2
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"", //văn bằng 2
                        SoDienThoai:"", //văn bằng 2
                        DiaChiLienHe:"", //văn bằng 2
                        NamTotNghiep:{ //văn bằng 2
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //văn bằng 2
                        TruongTotNghiep:"", //văn bằng 2
                        NganhTotNghiep:"", //văn bằng 2
                        NganhVanBang2:{ //văn bằng 2
                            "NganhID": resultList.ListNganhVanBang2[0].NganhID,
                            "NganhName": resultList.ListNganhVanBang2[0].NganhName
                        },
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    };
    $scope.submitThacSiQuocTe = function (form) {
        //mỗi view đăng ký có id biểu mẫu riêng
        var bieumau=$scope.idbieumau;

        $("#val_CapCha").html("");
        if($scope.CapChaCheck!=$scope.kq.Capcha)
        {
            $("#val_CapCha").html("Câu trả lời bảo mật chưa đúng!");
            return;
        }
        
        //validate control không có trong plugin
        $('#divChuongTrinhDaoTaoThacSiQuocTe').removeClass('has-error'); //thạc sĩ quốc tế
        $('#divChonTruongDoiTacThacSiQuocTe').removeClass('has-error'); //thạc sĩ quốc tế
        var flag=true;
        if($scope.kq.ChuongTrinhDaoTaoThacSiQuocTe.ChuongTrinhDaoTaoID=="") //thạc sĩ quốc tế
        {  $('#divChuongTrinhDaoTaoThacSiQuocTe').addClass('has-error');flag=false; } 
        if($scope.kq.ChonTruongDoiTacThacSiQuocTe.TruongDoiTacID=="") //thạc sĩ quốc tế
        {  $('#divChonTruongDoiTacThacSiQuocTe').addClass('has-error');flag=false; } 

        if (form.$valid&&flag) {
            $http({
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'app-key':'MOBILE_HUTECH',
                    'Authorization': 'JWT undefined'
                },
                method: "POST",
                url: UrlDomainAPI+"api/dangkynhaphoc/create",
                data: 
            {
                "hovaten":$scope.kq.HoVaTen, //all
                "gioitinh":$scope.kq.GioiTinh.GioiTinhID, //thạc sĩ quốc tế
                "namsinh":$filter('date')($scope.toDate($scope.kq.NamSinh), "dd/MM/yyyy"), //thạc sĩ quốc tế
                "sodienthoai":$scope.kq.SoDienThoai, //thạc sĩ quốc tế
                "diachilienhe":$scope.kq.DiaChiLienHe, //thạc sĩ quốc tế
                "email":$scope.kq.Email, //thạc sĩ quốc tế
                "chuongtrinhdaotao":$scope.kq.ChuongTrinhDaoTaoThacSiQuocTe.ChuongTrinhDaoTaoName, //2019 - thạc sĩ quốc tế
                "truongdoitac":$scope.kq.ChonTruongDoiTacThacSiQuocTe.TruongDoiTacName, //2019 - thạc sĩ quốc tế
                "truongtotnghiep":$scope.kq.TruongTNDH, //thạc sĩ quốc tế
                "namtotnghiep":$scope.kq.NamTotNghiep.NamID, //thạc sĩ quốc tế
                "bieumau":bieumau, //all
                "tag_khaosat":$scope.kq.KetQuaKhaoSat //all                        
            }
            })
                .success(function (data, status, headers, config) {                    
                    $("#myModal").modal('show');     
                    $scope.kq = {
                        HoVaTen:"", //all
                        GioiTinh:{ //thạc sĩ quốc tế
                            "GioiTinhID": resultList.ListGioiTinh[0].GioiTinhID,
                            "GioiTinhName": resultList.ListGioiTinh[0].GioiTinhName
                        },
                        NamSinh:"", //thạc sĩ quốc tế
                        SoDienThoai:"", //thạc sĩ quốc tế
                        DiaChiLienHe:"", //thạc sĩ quốc tế
                        NamTotNghiep:{ //thạc sĩ quốc tế
                            "NamID": resultList.ListNamTotNghiep[0].NamID,
                            "NamName": resultList.ListNamTotNghiep[0].NamName
                        },
                        Email:"", //thạc sĩ quốc tế
                        ChuongTrinhDaoTaoThacSiQuocTe:{ //thạc sĩ quốc tế
                            "ChuongTrinhDaoTaoID": resultList.ListChonTruongDoiTacThacSiQuocTe[0].ChuongTrinhDaoTaoID,
                            "ChuongTrinhDaoTaoName": resultList.ListChonTruongDoiTacThacSiQuocTe[0].ChuongTrinhDaoTaoName
                        },
                        ChonTruongDoiTacThacSiQuocTe:{ //thạc sĩ quốc tế
                            "TruongDoiTacID": resultList.ListChonTruongDoiTacThacSiQuocTe[0].TruongDoiTacID,
                            "TruongDoiTacName": resultList.ListChonTruongDoiTacThacSiQuocTe[0].TruongDoiTacName
                        },
                        TruongTotNghiep:"", //thạc sĩ quốc tế
                        KetQuaKhaoSat: [], //all
                        Capcha:"" //all                        
                    };
                })
                .error(function (data, status, headers, config) {
                    alert('Đã có lỗi xảy ra vui lòng thử lại!');
                })
        } 
    }; 
}])