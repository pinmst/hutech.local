<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';

$route['index'] = 'home/index';
// $route['tin-noi-bat'] = 'home/listcat';
$route['search'] ='home/search';
$route['search/(:num)'] ='home/search/$1';

$route['he-thong-dao-tao/(:any)'] ='home/htdt/$1';
$route['event'] ='home/listevent';
$route['event/(:num)'] ='home/listevent/$1';
$route['event/(:any)'] ='home/event/$1';
$route['tin-noi-bat/(:any)/(:num)/(:any)'] = 'home/detail/$1/$2/$3';

// $route['search'] ="product/search";
$route['gioi-thieu'] = 'news/catelog/$1';
$route['gioi-thieu/(:any)'] ="news/detail/$1";

$route['khoa/(:any)'] = "faculty/index/$1";
$route['khoa/(:any)/(:any)'] = "faculty/listcat/$1/$2";
$route['khoa/(:any)/(:any)/(:num)'] = "faculty/listcat/$1/$2/$3";
$route['khoa/(:any)/(:any)/(:any)'] = "faculty/detail/$1/$2/$3";

$route['media'] = 'media/catelog/$1';
$route['media/(:any)'] ="media/listCat/$1";
$route['media/(:any)/(:num)'] ="media/listcat/$1/$2";
$route['media/(:any)/(:any)'] ="media/detail/$1/$2";

$route['hop-tac-doanh-nghiep/(:any)'] ="cooperation/listCat/$1";
$route['hop-tac-doanh-nghiep/(:any)/(:num)'] ="cooperation/listcat/$1/$2";
$route['hop-tac-doanh-nghiep/(:any)/(:any)'] ="cooperation/detail/$1/$2";

$route['thu-vien/(:any)'] ="library/listCat/$1";
$route['thu-vien/(:any)/(:num)'] ="library/listcat/$1/$2";
$route['thu-vien/(:any)/(:any)'] ="library/detail/$1/$2";

$route['hoat-dong'] = 'activities/catelog/$1';
$route['hoat-dong/(:any)'] ="activities/listCat/$1";
$route['hoat-dong/(:any)/(:num)'] ="activities/listcat/$1/$2";
$route['hoat-dong/(:any)/(:any)'] ="activities/detail/$1/$2";

$route['tuyen-sinh'] = 'admissions/catelog/$1';
$route['tuyen-sinh/tin-tuc-tuyen-sinh'] = 'admissions/news/$1';
$route['tuyen-sinh/nganh-dao-tao'] = 'admissions/nganhdt/$1';
$route['tuyen-sinh/nganh-dao-tao/(:num)'] = 'admissions/nganhdt/$1';
$route['tuyen-sinh/nganh-dao-tao/(:any)'] = 'admissions/nganhdt_detail/$1';
$route['tuyen-sinh/thong-bao'] = 'admissions/thongbao/$1';
$route['tuyen-sinh/dang-ky/xet-diem-hoc-ba'] = 'admissions/xethocba';
$route['tuyen-sinh/dang-ky/xet-diem-thi-thpt'] = 'admissions/xetthpt';
$route['tuyen-sinh/tra-cuu/ket-qua-tuyen-sinh'] = 'admissions/ketqua';
$route['tuyen-sinh/(:any)'] ="admissions/listCat/$1";
$route['tuyen-sinh/(:any)/(:num)'] ="admissions/listcat/$1/$2";
$route['tuyen-sinh/(:any)/(:any)'] ="admissions/detail/$1/$2";

$route['sinh-vien'] = 'student/catelog/$1';
$route['sinh-vien/(:any)'] ="student/listCat/$1";
$route['sinh-vien/(:any)/(:num)'] ="student/listcat/$1/$2";
$route['sinh-vien/(:any)/(:any)'] ="student/detail/$1/$2";


$route['admincp'] ="admincp/login";
$route['404.html'] ="home/notfoud";
$route['translate_uri_dashes'] = FALSE;
$route['404_override'] = 'home/notfound';

