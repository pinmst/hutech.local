<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admissions extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('menu_model');
		$this->load->model('news_model');
		$this->load->model('flash_model');
		$this->load->model('media_model');
		$this->load->model('admissions_model');
		$this->load->model('admonline_model');
		$this->load->model('provinces_model');
		$this->load->model('tohop_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');

		$this->load->model('catnews_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	public function guidon(){
		
	}

	public function xethocba(){

		$temp['data']['nganh'] = $this->provinces_model->list_data();
		$temp['data']['tohop'] = $this->tohop_model->list_data();

		$data['fullname'] = $fullname = $this->input->post('fullname');
		$data['gender'] = $gender = $this->input->post('gender');
		$data['email'] = $email = $this->input->post('email');
		$data['phone'] = $phone = $this->input->post('phone');
		$data['graduation_year'] = $graduation_year = $this->input->post('graduation_year');
		$data['city'] = $city = $this->input->post('city');
		$data['facebook'] = $facebook = $this->input->post('facebook');
		$data['zalo'] = $zalo = $this->input->post('zalo');
		$data['idprovin'] = $idprovin = $this->input->post('idprovin');
		$data['tohop'] = $tohop = $this->input->post('tohop');
		$data['score1'] = $score1 = $this->input->post('score1');
		$data['score2'] = $score2 = $this->input->post('score2');
		$data['score3'] = $score3 = $this->input->post('score3');
		$data['score_sum'] = $score_sum = $score1 + $score2 + $score3;
		$data['dtb12'] = $dtb12 = $this->input->post('dtb12');
		$data['idcat'] = $idcat = $this->input->post('idcat');
		$data['code_reg'] = $code_reg = $this->page->getCode($this->page->rand_number());
		if($fullname=="") { /*$this->session->set_flashdata('message_error', 'Có lỗi xảy ra, vui lòng thử lại.');*/ }
		else {
			$data = array(
				"code_reg" =>$code_reg,
				"fullname" =>$fullname,
				"phone" =>$phone,
				"gender"=> $gender,
				"email" =>$email,
				"graduation_year" => $graduation_year,
				"city" => $city,
				"facebook" => $facebook,
				"zalo" => $zalo,
				"idprovin" => $idprovin,
				"tohop" => $tohop,
				"score1" => $score1,
				"score2" => $score2,
				"score3" => $score3,
				"score_sum" => $score_sum,
				"dtb12" => $dtb12,
				"idcat" => $idcat,
				"ticlock" => '0',
				"status" => '4',
				"date"	=>time()
			);
			$this->admonline_model->add($data);
			$this->session->set_flashdata('message_success', 'Bạn đã đăng ký xét tuyển danh bạ thành công.');
			$temp['template']='default/admissions/form_xethocba';
		}
		// redirect(current_url());

		$temp['template']='default/admissions/form_xethocba';
		$this->load->view("default/layout_form",$temp); 
	}
	public function xetthpt(){
		
		$temp['data']['nganh'] = $this->provinces_model->list_data();

		$data['fullname'] = $fullname = $this->input->post('fullname');
		$data['gender'] = $gender = $this->input->post('gender');
		$data['email'] = $email = $this->input->post('email');
		$data['phone'] = $phone = $this->input->post('phone');
		$data['graduation_year'] = $graduation_year = $this->input->post('graduation_year');
		$data['city'] = $city = $this->input->post('city');
		$data['facebook'] = $facebook = $this->input->post('facebook');
		$data['zalo'] = $zalo = $this->input->post('zalo');
		$data['idprovin'] = $idprovin = $this->input->post('idprovin');
		$data['sbd'] = $sbd = $this->input->post('sbd');
		$data['barcode'] = $barcode = $this->input->post('barcode');
		$data['idcat'] = $idcat = $this->input->post('idcat');
		$data['code_reg'] = $code_reg = $this->page->getCode($this->page->rand_number());
		if($fullname=="") { }
		else {
			$data = array(
				"code_reg" =>$code_reg,
				"fullname" =>$fullname,
				"phone" =>$phone,
				"gender"=> $gender,
				"email" =>$email,
				"graduation_year" => $graduation_year,
				"city" => $city,
				"facebook" => $facebook,
				"zalo" => $zalo,
				"idprovin" => $idprovin,
				"sbd" => $sbd,
				"barcode" => $barcode,
				"ticlock" => '0',
				"status" => '4',
				"idcat" => $idcat,
				"date"	=>time()
			);
			$this->admonline_model->add($data);
			$this->session->set_flashdata('message_success', 'Bạn đã đăng ký xét tuyển điểm thi THPT thành công.');
			$temp['template']='default/admissions/form_xetthpt';
		}
		$temp['template']='default/admissions/form_xetthpt';
		$this->load->view("default/layout_form",$temp); 
	}

	public function ketqua(){
		$temp['data']['nganh'] = $this->provinces_model->list_data();
		$temp['data']['idcat'] = $idcat = $this->input->post('idcat');
		$temp['data']['key'] = $key = $this->input->post('key');
		$order  = "Id DESC";
		if(!empty($idcat) || !empty($key)){
			$sql = "SELECT * FROM mn_admonline  WHERE (idcat= ".$idcat." ) AND (( email like  '%".$key."%') OR (  phone like  '%".$key."%') OR ( code_reg like  '%".$key."%')) ORDER BY ".$order;
			$numrow = 50;
			$temp['data']['info_search'] = $this->get_query($sql,$numrow);
			
		}else{ 
			$temp['data']['info_search'] = '';
		}

		$temp['template']='default/admissions/ketqua';
		$this->load->view("default/layout_form",$temp); 
	}

	function download($filename = NULL) {
	    // load download helder
	    $this->load->helper('download');
	    // read file contents
	    $data = file_get_contents(base_url('./data/Document/'.$filename));
	    force_download($filename, $data);
	}

	public function detail($alias, $alias_sub){
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';

		$temp['data']['info'] = $info = $this->catadmissions_model->get_list(array('alias'=>$alias));
		$temp['data']['info_detail'] = $info_detail = $this->admissions_model->get_list(array('alias'=>$alias_sub));
		if(!$alias || empty($info)) redirect(base_url('404.html'));

		//lay cung loai
		$info_cat = $this->catadmissions_model->get_list(array('alias'=>$alias));
		$temp['data']['info_cl'] = $this->admissions_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",'','');
		
		$temp['template']='default/admissions/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function catelog($alias,$p=0){
		//menu
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();
		// $temp['data']['sidebar'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/'; 

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';

		// new all sort desc
		$sql = "SELECT mn_admissions.* , mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.ghim = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 9;
		$temp['data']['news_lester']=  $this->get_query($sql,$numrow);
		$temp['data']['news_lester_alias']= 'thong-bao/';

		// data home mn_catadmissions
		$sql ="SELECT mn_catadmissions.Id,mn_catadmissions.title_vn,mn_catadmissions.alias
				 FROM  mn_catadmissions
				 WHERE mn_catadmissions.ticlock = 0 AND mn_catadmissions.home = 1
			 	 ORDER BY mn_catadmissions.Id DESC";
		$numrow = 3;
		$temp['data']['admissions_home_info']=  $this->get_query($sql,$numrow);
		$temp['data']['admissions_home_alias']=  'tuyen-sinh/';

		$temp['data']['admissions_home'] = array();
		if (!empty($temp['data']['admissions_home_info'])) {
			$sql ="SELECT mn_catadmissions.title_vn,mn_catadmissions.alias,mn_admissions.title_vn,mn_admissions.content_vn,mn_admissions.images,mn_admissions.alias FROM  mn_catadmissions
				 LEFT JOIN mn_admissions ON ".$temp['data']['admissions_home_info']['0']['Id']."= mn_admissions.idcat
			 	 WHERE mn_catadmissions.ticlock = 0 AND mn_catadmissions.home = 1
			 	 ORDER BY mn_admissions.Id DESC";
			$numrow = 9;
			$temp['data']['admissions_home']=  $this->get_query($sql,$numrow);
		}

		$temp['data']['detail'] = $this->admissions_model->list_data();
		$temp['data']['info'] = $this->catadmissions_model->get_metoru();
		$temp['template']='default/admissions/index'; 
		$this->load->view("default/layout",$temp);
		
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function listCat($alias,$p=0){
		//use menu
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();
		// $temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';

		$info_cat = $this->catadmissions_model->get_list(array('alias'=>$alias));
		$config['base_url']	=	base_url('tuyen-sinh/'.$info_cat[0]['alias']);
		
		$config['total_rows'] = $this->admissions_model->count_where(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']));
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->admissions_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",$config['per_page'],$p);

		$temp['data']['sub'] = $alias;
		$temp['data']['main_vn'] = $info_cat;
		$temp['template']='default/admissions/listcat'; 
		$this->load->view("default/layout",$temp);
		
	}

	public function thongbao($alias,$p=0){
		//use menu
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';

		// new all sort desc
		$sql = "SELECT mn_admissions.* , mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['info']=  $this->get_query($sql,$numrow);
		$temp['data']['news_lester_alias']= 'thong-bao/';

		// $info_cat = $this->catadmissions_model->get_list(array('alias'=>$alias));
		$config['base_url']	=	base_url('tuyen-sinh/thong-bao');
		$config['total_rows'] = $this->admissions_model->count_where(array("ticlock"=>0));
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->admissions_model->get_list(array("ticlock"=>0),"sort DESC, Id DESC",$config['per_page'],$p);

		$temp['data']['sub'] = 'thong-bao';
		$temp['data']['main_vn']['0']['title_vn'] = 'Thông báo';
		$temp['template']='default/admissions/notice'; 
		$this->load->view("default/layout",$temp);
	}

	public function nganhdt($p=0){
		//use menu
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';


		// $info_cat = $this->catadmissions_model->get_list(array('alias'=>$alias));
		$config['base_url']	=	base_url('tuyen-sinh/nganh-dao-tao');
		
		$config['total_rows'] = $this->provinces_model->count_all();
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->provinces_model->get_list(array("ticlock"=>0),"sort DESC, Id DESC",$config['per_page'],$p);
		// $temp['data']['info'] = $this->provinces_model->list_data();

		$temp['data']['sub'] = 'nganh-dao-tao';
		$temp['data']['main_vn']['0']['title_vn'] = 'Ngành đào tạo';
		$temp['template']='default/admissions/listndt'; 
		$this->load->view("default/layout",$temp);
	}

	public function nganhdt_detail($alias){
		$temp['data']['menu']['sub_alias'] = '/tuyen-sinh';
		$temp['data']['menu']['sub_title'] = 'Trang tuyển sinh';
		$temp['data']['menu']['tuyen_sinh'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['sub_alias_ts'] = '/nganh-dao-tao';
		$temp['data']['menu']['sub_title_ts'] = 'Ngành đào tạo';
		$temp['data']['menu']['ts_nganh'] = $this->provinces_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_admissions.*, mn_catadmissions.alias as alias_cat
				FROM mn_admissions LEFT JOIN mn_catadmissions ON mn_admissions.idcat = mn_catadmissions.Id
				WHERE mn_admissions.ticlock = 0 AND mn_admissions.home = 1
    			ORDER BY mn_admissions.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'tuyen-sinh';

		$temp['data']['info_detail'] = $info_detail = $this->provinces_model->get_list(array('alias'=>$alias));
		if(!$alias || empty($info_detail)) redirect(base_url('404.html'));

		//lay cung loai
		$temp['data']['info_cl'] = $this->provinces_model->get_list(array("ticlock"=>0),"sort DESC, Id DESC",'','');
		$temp['info']['0']['alias'] = 'nganh-dao-tao';
		$temp['template']='default/admissions/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function detail_hot()
	{
		$detail_hot = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id FROM  mn_activities
				    UNION  
				    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id FROM  mn_student
				    UNION
				    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id FROM  mn_media
				    UNION
				    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id FROM  mn_cooperation
				    UNION
				    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id FROM  mn_admissions
				    UNION
				    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id FROM  mn_faculty
				) A 
				WHERE A.NoiBat = 1
    			ORDER BY date DESC";
		return $detail_hot;	
	}
}
