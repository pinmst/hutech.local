<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catfaculty extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('catfaculty_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/catfaculty/index'; 
		$temp['idmenu']=48;
		$bdata = NULL;
		$config['base_url']	=	base_url('admincp/catfaculty/index');
		$temp['data']['total'] = $config['total_rows']	=	$this->catfaculty_model->count_all();
		$config['per_page']	=	50;
		$config['num_links'] = 10;
		
		$this->pagination->initialize($config);
		$bdatas = $this->catfaculty_model->list_data($config['per_page'],$this->uri->segment(4));
		if(!empty($bdatas)){
			foreach($bdatas as $rows)
			{
				$bdata[] = $rows;
			}
		}
		$temp['data']['info'] =$bdata;
	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 48;
		$temp['data']['map_title']  = "Thêm mới";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$config['upload_path'] = './data/Faculty/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '1000';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload()){
					$arr =  $this->upload->data();
					if($arr['file_name'] !=""){
						$data['banner'] = $arr['file_name'];
					}
				}else{
					$error = $this->upload->display_errors();
				}
				
				$result = $this->catfaculty_model->add($data);
				$url = base_url('admincp/catfaculty');
				redirect($url);
			}
		}
		$temp['data']['listcat'] = $this->catfaculty_model->list_data(50,0);
		$temp['template']='admincp/catfaculty/add'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->catfaculty_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 48;
		$temp['data']['map_title']  = "Sửa";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$config['upload_path'] = './data/Faculty/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '1000';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload()){
					$arr =  $this->upload->data();
					if($arr['file_name'] !=""){
						$data['banner'] = $arr['file_name'];
					}
				}else{
					$error = $this->upload->display_errors();
				}
				
				$result = $this->catfaculty_model->update($id,$data,true);
				redirect(base_url('admincp/catfaculty'));
			}
		}
		$temp['data']['listcat'] = $this->catfaculty_model->list_data(50,0);
		$temp['template']='admincp/catfaculty/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $this->catfaculty_model->delete($id);
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$this->catfaculty_model->delete($v);
				}
			}
		}
		redirect(base_url('admincp/catfaculty'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->catfaculty_model->update($k,$data);
				}
			}
		}
		redirect(base_url('admincp/catfaculty'));
	}
}
