<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admissions extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('admissions_model');
		 $this->load->model('catadmissions_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/admissions/index'; 
		$temp['idmenu']=45; 
		$tukhoa =$this->input->get('tukhoa', TRUE)?$this->input->get('tukhoa', TRUE):-1;

		$idcat =$this->input->get('idcat', TRUE)?$this->input->get('idcat', TRUE):0;

		if($idcat > 0 || $tukhoa !=-1){
			$p =$this->input->get('p', TRUE)?str_replace("/","",$this->input->get('p', TRUE)):0;

			$sql= "SELECT * FROM mn_admissions WHERE (idcat= ".$idcat." OR ".$idcat." =0 ) AND ( title_vn LIKE '%".$tukhoa."%' OR '".$tukhoa."' = -1) ORDER BY Id DESC"; 
			$temp['data']['info'] = $this->admissions_model->get_query2($sql,500,0);
			$temp['data']['total'] = count($temp['data']['info']);
		}else{ 
			$config['base_url']	=	base_url('admincp/admissions/index');
			$temp['data']['total'] = $config['total_rows']	=	$this->admissions_model->count_all();
			$config['per_page']	=	50;
			$config['num_links'] = 10;

			$this->pagination->initialize($config);

			$temp['data']['info'] = $this->admissions_model->list_data($config['per_page'],$this->uri->segment(4));

		}
		$temp['data']['listcat'] = $this->pagehtml_model->get_catelog_ad(0); 
	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 45;
		$temp['data']['map_title']  = "Thêm mới";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_rules('idcat','Chủ đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');

		if($this->input->post('save')) 
		{
			if($this->form_validation->run() == TRUE  )
			{
				$data = array();
				$tenhinh = $this->page->strtoseo($this->input->post('title_vn'));
				$data['images'] = $this->uploadimages("images",$tenhinh);
				// $data['dinhkem'] = $this->uploadfile("dinhkem",$tenhinh);

				$result = $this->admissions_model->add($data);
				$url = base_url('admincp/admissions');
				redirect($url);
			}
		}
		$temp['data']['listcat']= $this->catadmissions_model->list_data(50,0);
		$temp['template']='admincp/admissions/add'; 
		$this->load->view("admincp/layout",$temp); 
	}

	public function uploadimages($file,$tenhinh){
		$this->load->library('upload');
		$this->load->library('image_lib');
		$dir = './data/News/';
		$config = array(
			'allowed_types'     => 'jpg|jpeg|gif|png', 
			'overwrite'			=> TRUE,
			// 'max_size'          => 0,//2048000, //2MB max
			'max_size'			=> 	50*1024,
			'max_width'  		=> 	1024*100,
			'max_height'  		=> 	1024*100,
			'upload_path'       => $dir,
			'file_name' =>  $tenhinh, 
		);
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		 // unset($upload_data);
		if($this->upload->do_upload($file)) {
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];			
		}
		return $file_name;
	}

	public function uploadfile($file,$tenfile){
		$this->load->library('upload');
		$dir = './data/Document/';
		$config = array(
			'allowed_types'     => 'pdf|txt|doc|xls|xlsx', 
			'overwrite'			=> TRUE,
			'max_size'          => 0,//2048000, //2MB max
			'max_width'			=> 0,
			'max_height'		=> 0,
			'upload_path'       => $dir,
			'file_name' =>  $tenfile, 
		);
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		 // unset($upload_data);
		if($this->upload->do_upload($file)) {
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];
		}
		return $file_name;
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->admissions_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 45;
		$temp['data']['map_title']  = "Sửa";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_rules('idcat','Chủ đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{ 
				$data  =array();
				$tenhinh = $this->page->strtoseo($this->input->post('title_vn'));				
				if($_FILES['images']['name'] != ''){
					$data['images'] = $this->uploadimages("images",$tenhinh);
				}
				if($_FILES['dinhkem']['name'] != ''){
					$data['dinhkem'] = $this->uploadfile("dinhkem",$tenhinh);
				}
				$result = $this->admissions_model->update($id,$data,true);
				redirect(base_url('admincp/admissions'));
			}
		}
		$temp['data']['listcat']= $this->catadmissions_model->list_data(50,0);
		$temp['template']='admincp/admissions/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $this->admissions_model->delete($id);
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$this->admissions_model->delete($v);
				}
			}
		}
		redirect(base_url('admincp/admissions'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->admissions_model->update($k,$data);
				}
			}
		}
		redirect(base_url('admincp/admissions'));
	}
}
