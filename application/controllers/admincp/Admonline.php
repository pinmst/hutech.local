<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admonline extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('admonline_model');
		 $this->load->model('pagehtml_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/admonline/index'; 
		$temp['idmenu']=45;
		$order  = "Id DESC";
		$tukhoa = $this->input->get('tukhoa', TRUE);
		$idcat =$this->input->get('idcat', TRUE)?$this->input->get('idcat', TRUE):0;
		$status =$this->input->get('status', TRUE)?$this->input->get('status', TRUE):0;

		if($idcat > 0 || $status != '' || empty($tukhoa)){
			$sql = "SELECT * FROM mn_admonline  WHERE (idcat= ".$idcat." OR ".$idcat." =0 ) AND (status= ".$status." OR ".$status." =0) AND (( email like  '%".$tukhoa."%') OR (  phone like  '%".$tukhoa."%') OR ( fullname like  '%".$tukhoa."%')) ORDER BY ".$order;
		   
			$sql_toal = "SELECT COUNT(mn_admonline.Id) AS total FROM mn_admonline WHERE ( email like  '%".$tukhoa."%') OR ( phone like  '%".$tukhoa."%') OR ( fullname like  '%".$tukhoa."%') "; 
			
			$config['base_url']	=	base_url('admincp/admonline/index?tukhoa='.$tukhoa.'&p=');
			//*---------------pagination------------------
			$p =$this->input->get('p', TRUE)?str_replace("/","",$this->input->get('p', TRUE)):0;
			$numrow = 50;
			$div = 10;
			$skip = $p * $numrow;
			$temp['data']['info'] = $this->admonline_model->get_query($sql,$numrow,$skip);
			$temp['data']['total'] = $this->admonline_model->count_query($sql_toal); 
			$temp['data']['page']= $this->page->divPage($temp['data']['total'],$p,$div,$numrow,$link);
		}else{ 
			$p = $this->uri->segment(4);
			$numrow = 50;
			$div = 10;
			$skip = $p * $numrow;
			$link  = base_url("admincp/admonline/index/");
			//*---------------pagination------------------
			$sql = "SELECT * FROM mn_admonline  ORDER BY ".$order;
			
			$sql_total = "SELECT  COUNT(Id) AS total FROM mn_admonline ";
			$temp['data']['total'] = $this->admonline_model->count_query($sql_total); 
			$temp['data']['info'] = $this->admonline_model->get_query($sql,$numrow,$skip);
			$temp['data']['page']= $this->page->divPage($temp['data']['total'],$p,$div,$numrow,$link);
		}
	
		$this->load->model('provinces_model');
	    $this->load->view("admincp/layout",$temp); 
	}

	public function export_excel(){
		$this->load->library('phpexcel');
		$sheet = $this->phpexcel->getActiveSheet(); 
			$objWorkSheet = $this->phpexcel->createSheet(0);	
			$objWorkSheet->setCellValue('A1', 'STT');
			$objWorkSheet->setCellValue('B1', 'Họ tên');
			$objWorkSheet->setCellValue('C1', 'Email');
			$objWorkSheet->setCellValue('D1', 'SĐt');
			$objWorkSheet->setCellValue('E1', 'Hình thức xét tuyển');
			$objWorkSheet->setCellValue('F1', 'Năm tốt nghiệp');
			$objWorkSheet->setCellValue('G1', 'Địa chỉ');
			$objWorkSheet->setCellValue('H1', 'Giới tính');
			$objWorkSheet->setCellValue('I1', 'Ngành đăng ký');
			$objWorkSheet->setCellValue('J1', 'Tổ hợp xét tuyển');
			$objWorkSheet->setCellValue('K1', 'Điểm môn 1');
			$objWorkSheet->setCellValue('L1', 'Điểm môn 2');
			$objWorkSheet->setCellValue('M1', 'Điểm môn 3');
			$objWorkSheet->setCellValue('N1', 'Điểm tổng');
			$objWorkSheet->setCellValue('O1', 'Điểm TB lớp 12');
			$objWorkSheet->setCellValue('P1', 'Số báo danh');
			$objWorkSheet->setCellValue('Q1', 'Mã vạch kết quả thi');
			$objWorkSheet->setCellValue('R1', 'Ngày đăng ký');
			$objWorkSheet->setCellValue('S1', 'Zalo');
			$objWorkSheet->setCellValue('T1', 'Facebook');
			$objWorkSheet->setCellValue('U1', 'Trình trạng hồ sơ');

			//-------------------------------
		$order  = "Id DESC";
		$idcat =$this->input->get('idcat', TRUE)?$this->input->get('idcat', TRUE):0;
		$status =$this->input->get('status', TRUE)?$this->input->get('status', TRUE):100;

		// if($idcat > 0 || $status != '' || empty($tukhoa)){
		// 	$sql = "SELECT * FROM mn_admonline  WHERE (idcat= ".$idcat." OR ".$idcat." =0 ) AND (status= ".$status." OR ".$status." =100 ) AND (( email like  '%".$tukhoa."%') OR (  phone like  '%".$tukhoa."%') OR ( fullname like  '%".$tukhoa."%')) ORDER BY ".$order;
		   
		// 	$sql_toal = "SELECT COUNT(mn_admonline.Id) AS total FROM mn_admonline WHERE ( email like  '%".$tukhoa."%') OR ( phone like  '%".$tukhoa."%') OR ( fullname like  '%".$tukhoa."%') "; 
		// 	$temp['data']['export'] = $this->admonline_model->get_query($sql,500,0);
		// }else{ 
		// 	$p = $this->uri->segment(4);
		// 	$sql = "SELECT * FROM mn_admonline  ORDER BY ".$order;
		// 	$sql_total = "SELECT  COUNT(Id) AS total FROM mn_admonline ";
		// 	$temp['data']['export'] = $this->admonline_model->get_query($sql,500,0);
		// }
		
		// $p = $this->uri->segment(4);
		// 	$sql = "SELECT * FROM mn_admonline  ORDER BY ".$order;
		// 	$temp['data']['export'] = $this->admonline_model->get_query($sql,500,0);
		// print_r($temp['data']['export']);

		// $sql="SELECT * FROM mn_admonline where idcat = ".$idcat." AND status = '2' order by Id desc";

		$sql = "SELECT * FROM mn_admonline  WHERE (idcat= ".$idcat." OR ".$idcat." =0 ) AND (status= ".$status." OR ".$status." =100 ) ORDER BY ".$order;
		$empInfo = $this->admonline_model->get_all_data($sql);

		$nganh = $this->pagehtml_model->get_provinces(array('ticlock'=>0,'parentid'=>0),1000);
		$tohop = $this->pagehtml_model->get_tohop(array('ticlock'=>0,'parentid'=>0),1000);
		
		if(!empty($empInfo)){
			$k=1;
			$j=0;
			 foreach ($empInfo as $item) { 
			 $k++;
			 $j++;

			 if ($item['idcat'] == '24') { $htxt = 'Xét điểm học bạ';}
			 if ($item['idcat'] == '25') { $htxt = 'Xét điểm thi THPT';}

			 if ($item['gender'] == '0') { $gender = 'Nam';}
			 if ($item['gender'] == '1') { $gender = 'Nữ';}

			 if ($item['status'] == '4') { $status = 'Hồ sơ mới';}
			 if ($item['status'] == '1') { $status = 'Đang xử lý';}
			 if ($item['status'] == '2') { $status = 'Đậu';}
			 if ($item['status'] == '3') { $status = 'Không đủ điều kiện';}

			foreach($nganh as $item_ng){
				if($item['idprovin'] == $item_ng['Id']){
				 	$nganhdk = $item_ng['title_vn'];
				}
			}

			foreach($tohop as $item_th){
			    if($item['tohop'] == $item_th['Id']){
			    	$tohopdk = $item_th['title_vn'].' - '.$item_th['content_vn'];
			    }
			}

				$objWorkSheet->setCellValue('A'.$k, $j);
				$objWorkSheet->setCellValue('B'.$k, $item['fullname']);
				$objWorkSheet->setCellValue('C'.$k, $item['email']);
				$objWorkSheet->setCellValue('D'.$k, $item['phone']);
				$objWorkSheet->setCellValue('E'.$k, $htxt);
				$objWorkSheet->setCellValue('F'.$k, $item['graduation_year']);
				$objWorkSheet->setCellValue('G'.$k, $item['city']);
				$objWorkSheet->setCellValue('H'.$k, $gender);
				$objWorkSheet->setCellValue('I'.$k, $nganhdk);
				$objWorkSheet->setCellValue('J'.$k, $tohopdk);
				$objWorkSheet->setCellValue('K'.$k, $item['score1']);
				$objWorkSheet->setCellValue('L'.$k, $item['score2']);
				$objWorkSheet->setCellValue('M'.$k, $item['score3']);
				$objWorkSheet->setCellValue('N'.$k, $item['score_sum']);
				$objWorkSheet->setCellValue('O'.$k, $item['dtb12']);
				$objWorkSheet->setCellValue('P'.$k, $item['sbd']);
				$objWorkSheet->setCellValue('Q'.$k, $item['barcode']);
				$objWorkSheet->setCellValue('R'.$k, date('d-m-Y', $item['date']));
				$objWorkSheet->setCellValue('S'.$k, $item['zalo']);
				$objWorkSheet->setCellValue('T'.$k, $item['facebook']);
				$objWorkSheet->setCellValue('U'.$k, $status);
			 }
		}
			//------------
		$objWorkSheet->setTitle('Danh sách xét tuyển');
		$this->phpexcel->setActiveSheetIndex(0);
		$filename='danh_sach_xet_tuyen.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel5');  
		$objWriter->save('php://output');

		$this->load->view("admincp/layout",$temp); 
	}
	
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->admonline_model->get_Id($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 45;
		$temp['data']['map_title']  = "Xem";
		// $this->admonline_model->update($id,array('view'=>1));
		if($this->input->post('save')){
			$arr = array(
                "status"=>$this->input->post('status')
            );    
			$this->admonline_model->update($id,$arr);
			redirect(base_url('admincp/admonline'));
		}

		$temp['template']='admincp/admonline/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $this->admonline_model->delete($id);
			
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$this->admonline_model->delete($v);
					
				}
			}
		}
		redirect(base_url('admincp/admonline'));
	}

	public function save()
	{
		
		if($this->input->post('price_sale')) {
			$checked = $this->input->post("price_sale");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sale_price'] = str_replace(".","",$v);
					$this->admonline_model->update($k,$data);
				}
			}
		}

		redirect(base_url('admincp/admonline'));
	}
	
	public function createXLS1() {
		// create file name
        $fileName = 'data-'.time().'.xlsx';  
		// load excel library
        $this->load->library('PHPExcel');
        $empInfo = $this->admonline->employeeList();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Ho va ten');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Dien thoai');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ngành đăng ký');       
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['fullname']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['phone']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['idprovin']);
            // $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['idcat']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
		// download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(HTTP_UPLOAD_IMPORT_PATH.$fileName);        
    }
}

