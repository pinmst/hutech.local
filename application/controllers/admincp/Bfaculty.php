<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bfaculty extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('bmenu_model');
		 $this->load->model('catfaculty_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/bfaculty/index'; 
		$temp['idmenu']=48;
		$config['base_url']	=	base_url('admincp/bfaculty/index');
		$total = $this->bmenu_model->count_bfaculty();
		// $temp['data']['total'] = $config['total_rows'] = count($total);

		$parentid =$this->input->get('parentid', TRUE)?$this->input->get('parentid', TRUE):0;
		if($parentid != 0){
			$p =$this->input->get('p', TRUE)?str_replace("/","",$this->input->get('p', TRUE)):0;
			$sql= "SELECT * FROM mn_bmenu WHERE (parentid= ".$parentid.") ORDER BY Id DESC"; 
			$temp['data']['info'] = $this->bmenu_model->get_query2($sql,500,0);
			$temp['data']['total'] = $config['total_rows'] = count($temp['data']['info']);
		}else{ 
			$config['base_url']	=	base_url('admincp/activities/index');
			$temp['data']['total'] = $config['total_rows'] = count($total);
			$config['per_page']	=	50;
			$config['num_links'] = 10;
			$this->pagination->initialize($config);
			$temp['data']['info'] = $this->bmenu_model->get_bfaculty($config['per_page'],$this->uri->segment(4));
		}
			 
		// $temp['data']['info'] = $this->bmenu_model->get_bfaculty($config['per_page'],$this->uri->segment(4));
		$temp['data']['listcat'] = $this->pagehtml_model->get_catelog_faculty(0); 
	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 48;
		$temp['data']['map_title']  = "Thêm mới";

		$this->form_validation->set_message('required','Vui lòng chọn %s');
		$this->form_validation->set_rules('parentid','Khoa hiển thị','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');

		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{	
				$config['upload_path'] = './data/Banner/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '2000';
				$config['encrypt_name'] = TRUE; 
				$config['file_name'] = $this->page->rand_string(30);
				$this->load->library('upload', $config);
				if ($this->upload->do_upload()){
					$arr =  $this->upload->data();
					$data['images'] = $arr['file_name'];
				}
				$result = $this->bmenu_model->add($data);
				$url = base_url('admincp/bfaculty');
				redirect($url);
			}
		}
		$temp['data']['catlist']= $this->pagehtml_model->get_catelog_faculty(0); 
		$temp['template']='admincp/bfaculty/add'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->bmenu_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 48;
		$temp['data']['map_title']  = "Sửa";

		$this->form_validation->set_message('required','Vui lòng chọn %s');
		$this->form_validation->set_rules('parentid','Khoa hiển thị','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{	
				$config['upload_path'] = './data/Banner/';
				$config['file_name'] = $this->page->rand_string(30);
				$config['encrypt_name'] = TRUE;
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '1000';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload()){
					$arr =  $this->upload->data();
					$data['images'] = $arr['file_name'];
				}
				$result = $this->bmenu_model->update($id,$data,true);
				redirect(base_url('admincp/bfaculty'));
			}
		}
		$temp['data']['catlist']= $this->pagehtml_model->get_catelog_faculty(0); 
		$temp['template']='admincp/bfaculty/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			$info = $this->bmenu_model->get_where($id); 
			$this->bmenu_model->delete($id);
			if(file_exists('./data/Banner/'.$info[0]['images']))
						unlink('./data/Banner/'.$info[0]['images']);
	
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$info = $this->bmenu_model->get_where($v); 
					$this->bmenu_model->delete($v);
					if(file_exists('./data/Banner/'.$info[0]['images']))
						unlink('./data/Banner/'.$info[0]['images']);
				}
			}
		}
		redirect(base_url('admincp/bfaculty'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->bmenu_model->update($k,$data);
				}
			}
		}
		redirect(base_url('admincp/bfaculty'));
	}
}
