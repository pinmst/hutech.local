<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tohop extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('tohop_model');
		 // $this->load->model('provinces_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/tohop/index'; 
		$temp['idmenu']=45;
		$config['base_url']	=	base_url('admincp/tohop/index');
		$temp['data']['total'] = $config['total_rows']	=	$this->tohop_model->count_all();
		$config['per_page']	=	50;
		$config['num_links'] = 10;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->tohop_model->list_data($config['per_page'],$this->uri->segment(4));
	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 45;
		$temp['data']['map_title']  = "Thêm mới";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');

		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				
				$result = $this->tohop_model->add();
				$url = base_url('admincp/tohop');
				redirect($url);
			}
		}
		// $temp['data']['listcat'] = $this->provinces_model->getdata(array('ticlock'=>0));
		$temp['template']='admincp/tohop/add'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->tohop_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 45;
		$temp['data']['map_title']  = "Sửa";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$result = $this->tohop_model->update($id,NULL,true);
				redirect(base_url('admincp/tohop'));
			}
		}
				// $temp['data']['listcat'] = $this->provinces_model->getdata(array('ticlock'=>0));
		$temp['template']='admincp/tohop/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $this->tohop_model->delete($id);
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$this->tohop_model->delete($v);
				}
			}
		}
		redirect(base_url('admincp/tohop'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->tohop_model->update($k,$data);
				}
			}
		}
		redirect(base_url('admincp/tohop'));
	}
    
    function ajax_get() {
        $idcat = $this->input->post('idcat');
        $tohop = array('Id' => 0, 'title_vn' => '--Quận/Huyện--');
        $tohops = $this->db->query("SELECT Id,title_vn FROM mn_tohop WHERE idcat='".$idcat."'")->result();
        $response[] = (object)$tohop;
        foreach($tohops as $tohop) {
            $response[] = $tohop;
        }
        echo json_encode($response);
    }
}
