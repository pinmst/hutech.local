<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('menu_model');
		 $this->load->model('catelog_model');
		 $this->load->model('catnews_model');
		$this->load->model('catactivities_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catstudent_model');
		$this->load->model('catfaculty_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/menu/index'; 
		$temp['idmenu']=1;
		$bdata = NULL;

		$temp['data']['total'] = count($this->get_query($this->list_menu(),0));
		$temp['data']['info']=  $this->get_query($this->list_menu(),0);
		// print_r($temp['data']['info']);
	
	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 1;
		$temp['data']['map_title']  = "Thêm mới";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$result = $this->menu_model->add(NULL);
				$url = base_url('admincp/menu');
				redirect($url);
			}
		}
		$temp['data']['listcat'] = $this->menu_model->list_data(10000,0);
		$temp['template']='admincp/menu/add'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->menu_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 1;
		$temp['data']['map_title']  = "Sửa";
		$this->form_validation->set_message('required','Vui lòng nhập %s');
		$this->form_validation->set_rules('title_vn','Tiêu đề','required');
		$this->form_validation->set_error_delimiters('<span class="input-error ">', '</span>');
		if($this->input->post('save'))
		{
			if($this->form_validation->run() == TRUE  )
			{
				$data = NULL;
				$config['upload_path'] = './data/Menu/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '1000';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload()){
					$arr =  $this->upload->data();
					if($arr['file_name'] !=""){
						$data['images'] = $arr['file_name'];
					}
				}else{
					$error = $this->upload->display_errors();
				}
				$result = $this->menu_model->update($id,$data,true);
				redirect(base_url('admincp/menu'));
			}
		}
		$temp['data']['listcat'] = $this->pagehtml_model->get_menu(0);
		$temp['template']='admincp/menu/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $this->menu_model->delete($id);
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$this->menu_model->delete($v);
				}
			}
		}
		redirect(base_url('admincp/menu'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->menu_model->update($k,$data);
				}
			}
		}
		redirect(base_url('admincp/menu'));
	}

	public function list_menu()
	{
		$list_menu = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.sort,mn_news.alias,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_catactivities.title_vn,mn_catactivities.sort,mn_catactivities.alias,mn_catactivities.Id FROM  mn_catactivities
				    UNION  
				    SELECT mn_catstudent.title_vn,mn_catstudent.sort,mn_catstudent.alias,mn_catstudent.Id FROM  mn_catstudent
				    UNION
				    SELECT mn_catmedia.title_vn,mn_catmedia.sort,mn_catmedia.alias,mn_catmedia.Id FROM  mn_catmedia
				    UNION
				    SELECT mn_catcooperation.title_vn,mn_catcooperation.sort,mn_catcooperation.alias,mn_catcooperation.Id FROM  mn_catcooperation
				    UNION
				    SELECT mn_catadmissions.title_vn,mn_catadmissions.sort,mn_catadmissions.alias,mn_catadmissions.Id FROM  mn_catadmissions
				    UNION
				    SELECT mn_catfaculty.title_vn,mn_catfaculty.sort,mn_catfaculty.alias,mn_catfaculty.Id FROM  mn_catfaculty
				) A 
				WHERE 0 = 0
    			ORDER BY Id DESC";
		return $list_menu;	
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}
}
