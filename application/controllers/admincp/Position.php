<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Controller  {

	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('adminmenu_model');
		 $this->load->model('weblink_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		 $controller = $this->router->fetch_class();
		 $act = $this->router->fetch_method();
		 $this->permission->checkAdmin($controller,$act);
	}
	public function index()
	{
		$temp['template']='admincp/position/index'; 
		$temp['idmenu']=1;
		$temp['data']['total'] = $config['total_rows']	=	$this->weblink_model->count_position();

		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->weblink_model->get_listposition();

		$temp['data']['listcol']=  $this->get_query($this->list_menu(),0);

	    $this->load->view("admincp/layout",$temp); 
	}
	public function add()
	{
		$id = $this->uri->segment(4);
		$temp['idmenu'] = 1;
		$temp['data']['map_title']  = "Thêm mới";

		if($this->input->post('save'))
		{
				$data['style'] = $this->input->post('style');
				$data['sort'] = (int)$this->input->post('sort');
				$data['ticlock'] = $this->input->post('ticlock')?$this->input->post('ticlock'):0;
				$data['link'] = $this->input->post('link');
				$result = $this->weblink_model->add($data);
				
				$url = base_url('admincp/position');
				$this->page->redirect($url);
		
		}
		$temp['template']='admincp/position/add'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function edit($id)
	{
		$id = $this->uri->segment(4);
		$info = $this->weblink_model->get_where($id);
		$temp['data']['info'] = $info[0];
		$temp['idmenu'] = 1;
		$temp['data']['map_title']  = "Sửa";
		if($this->input->post('save'))
		{
			
				$result = $this->weblink_model->update($id,$data,true);
				$this->page->redirect(base_url('admincp/position'));
			
		}
		$temp['template']='admincp/position/edit'; 
		$this->load->view("admincp/layout",$temp); 
	}
	public function delete()
	{
		$id = $this->uri->segment(4);
		if($id>0){
			 $info = $this->weblink_model->get_where($id); 
			 $this->weblink_model->delete($id);
			 if(file_exists('./data/Banner/'.$info[0]['images']))
						unlink('./data/Banner/'.$info[0]['images']);
		}
		if($this->input->post('check_list')) {
			$checked = $this->input->post("check_list");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$info = $this->weblink_model->get_where($v); 
					$this->weblink_model->delete($v);
					if(file_exists('./data/Banner/'.$info[0]['images']))
						unlink('./data/Banner/'.$info[0]['images']);
				}
			}
		}
		$this->page->redirect(base_url('admincp/position'));
	}
	public function save()
	{
		if($this->input->post('sort')) {
			$checked = $this->input->post("sort");
			if(!empty($checked)){
				foreach($checked as $k=>$v){
					$data['sort'] = $v;
					$this->weblink_model->update($k,$data);
				}
			}
		}
		$this->page->redirect(base_url('admincp/weblink'));
	}

	public function list_menu()
	{
		$list_menu = "SELECT * FROM 
				(
				    SELECT mn_catactivities.title_vn,mn_catactivities.home,mn_catactivities.alias,mn_catactivities.Id FROM  mn_catactivities
				    UNION  
				    SELECT mn_catstudent.title_vn,mn_catstudent.sort,mn_catstudent.home,mn_catstudent.Id FROM  mn_catstudent
				    UNION
				    SELECT mn_catcooperation.title_vn,mn_catcooperation.home,mn_catcooperation.alias,mn_catcooperation.Id FROM  mn_catcooperation
				    UNION
				    SELECT mn_catadmissions.title_vn,mn_catadmissions.home,mn_catadmissions.alias,mn_catadmissions.Id FROM  mn_catadmissions
				) A 
				WHERE A.home = 1
    			ORDER BY Id DESC";
		return $list_menu;	
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}
}
