<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('flash_model');
		$this->load->model('menu_model');
		$this->load->model('news_model');
		$this->load->model('media_model');
		$this->load->model('admissions_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');

		$this->load->model('catnews_model');
		$this->load->model('catactivities_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catstudent_model');
		$this->load->model('catfaculty_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}
	
	public function detail($alias){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();
		$temp['data']['sidebar_notice'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		$temp['data']['info'] = $info = $this->news_model->get_list(array('alias'=>$alias));
		if(!$alias || empty($info)) redirect(base_url('404.html'));
		$temp['template']='default/news/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function catelog($alias,$p=0){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();
		$temp['data']['sidebar_notice'] = 'off';

		$temp['data']['info'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['detail'] = $this->news_model->get_list(array('ticlock'=>'0'));
		$temp['template']='default/news/catelog'; 
		$this->load->view("default/layout",$temp);	
	}

	public function detail_hot()
	{
		$detail_hot = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id FROM  mn_activities
				    UNION  
				    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id FROM  mn_student
				    UNION
				    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id FROM  mn_media
				    UNION
				    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id FROM  mn_cooperation
				    UNION
				    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id FROM  mn_admissions
				    UNION
				    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id FROM  mn_faculty
				) A 
				WHERE A.NoiBat = 1
    			ORDER BY date DESC";
		return $detail_hot;	
	}
}
