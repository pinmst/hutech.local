<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	protected $arrowmap = " | ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('flash_model');
		$this->load->model('menu_model');
		$this->load->model('news_model');
		$this->load->model('media_model');
		$this->load->model('admissions_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');

		$this->load->model('catnews_model');
		$this->load->model('catactivities_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catstudent_model');
		$this->load->model('catfaculty_model');

		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	public function changeviews($id){
		$this->session->set_userdata('gridviews',$id);
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function index()
	{
		// data home mn_catadmissions
		$sql ="SELECT mn_catadmissions.Id,mn_catadmissions.title_vn,mn_catadmissions.alias
				 FROM  mn_catadmissions
				 WHERE mn_catadmissions.ticlock = 0 AND mn_catadmissions.home = 1
			 	 ORDER BY mn_catadmissions.Id DESC";
		$numrow = 1;
		$temp['data']['admissions_home_info']=  $this->get_query($sql,$numrow);
		$temp['data']['admissions_home_alias']=  'tuyen-sinh/';

		if (!empty($temp['data']['admissions_home_info'])) {
			$sql ="SELECT mn_catadmissions.title_vn,mn_catadmissions.alias,mn_admissions.title_vn,mn_admissions.content_vn,mn_admissions.images,mn_admissions.alias FROM  mn_catadmissions
				 LEFT JOIN mn_admissions ON ".$temp['data']['admissions_home_info']['0']['Id']."= mn_admissions.idcat
			 	 WHERE mn_catadmissions.ticlock = 0 AND mn_catadmissions.home = 1
			 	 ORDER BY mn_admissions.Id DESC";
			$numrow = 10;
			$temp['data']['admissions_home']=  $this->get_query($sql,$numrow);
		}

		// data home activities
		// $sql ="SELECT mn_catactivities.Id,mn_catactivities.title_vn,mn_catactivities.alias
		// 		 FROM  mn_catactivities
		// 		 WHERE mn_catactivities.ticlock = 0 AND mn_catactivities.home = 1
		// 	 	 ORDER BY mn_catactivities.Id DESC";
		// $numrow = 1;
		// $temp['data']['activities_home_info']=  $this->get_query($sql,$numrow);
		$temp['data']['activities_home_alias']=  'hoat-dong/';
		$sql ="SELECT mn_catactivities.title_vn,mn_catactivities.alias as catalias,mn_activities.title_vn,mn_activities.content_vn,mn_activities.images,mn_activities.alias,mn_activities.description_vn FROM  mn_catactivities
					 LEFT JOIN mn_activities ON mn_catactivities.Id = mn_activities.idcat AND mn_activities.ticlock = 0 
				 	 WHERE mn_catactivities.ticlock = 0 AND mn_catactivities.home = 1
				 	 ORDER BY mn_activities.date DESC";
		$numrow = 10;
		$temp['data']['activities_home']=  $this->get_query($sql,$numrow);

		// data home student
		// $sql ="SELECT mn_catstudent.Id,mn_catstudent.title_vn,mn_catstudent.alias
		// 		 FROM  mn_catstudent
		// 		 WHERE mn_catstudent.ticlock = 0 AND mn_catstudent.home = 1
		// 	 	 ORDER BY mn_catstudent.Id DESC";
		// $numrow = 1;
		// $temp['data']['student_home_info']=  $this->get_query($sql,$numrow);
		$temp['data']['student_home_alias']=  'sinh-vien/';
		$sql ="SELECT mn_catstudent.title_vn,mn_catstudent.alias as catalias,mn_student.title_vn,mn_student.content_vn,mn_student.images,mn_student.alias,mn_student.description_vn FROM  mn_catstudent
					 LEFT JOIN mn_student ON mn_catstudent.Id = mn_student.idcat AND mn_student.ticlock = 0 
				 	 WHERE mn_catstudent.ticlock = 0 AND mn_catstudent.home = 1
				 	 ORDER BY mn_student.date DESC";
		$numrow = 9;
		$temp['data']['student_home']=  $this->get_query($sql,$numrow);

		/*// data home media
		$sql ="SELECT mn_catmedia.title_vn,mn_catmedia.alias as alias_cat,mn_media.title_vn,mn_media.content_vn,mn_media.images,mn_media.alias FROM  mn_catmedia
				 LEFT JOIN mn_media ON mn_catmedia.Id = mn_media.idcat
			 	 WHERE mn_catmedia.ticlock = 0 AND mn_catmedia.home = 1
			 	 ORDER BY mn_media.Id DESC";
		$numrow = 4;
		$temp['data']['media_home']=  $this->get_query($sql,$numrow);
		$temp['data']['media_home_alias']=  'media/';

		// data home cooperation
		$sql ="SELECT mn_catcooperation.Id,mn_catcooperation.title_vn,mn_catcooperation.alias
				 FROM  mn_catcooperation
				 WHERE mn_catcooperation.ticlock = 0 AND mn_catcooperation.home = 1
			 	 ORDER BY mn_catcooperation.Id DESC";
		$numrow = 1;
		$temp['data']['cooperation_home_info']=  $this->get_query($sql,$numrow);
		$temp['data']['cooperation_home_alias']=  'hop-tac-doanh-nghiep/';

		if (!empty($temp['data']['cooperation_home_info'])) {
			$sql ="SELECT mn_catcooperation.title_vn,mn_catcooperation.alias,mn_cooperation.title_vn,mn_cooperation.content_vn,mn_cooperation.images,mn_cooperation.alias FROM  mn_catcooperation
					 LEFT JOIN mn_cooperation ON ".$temp['data']['cooperation_home_info']['0']['Id']." = mn_cooperation.idcat
				 	 WHERE mn_catcooperation.ticlock = 0 AND mn_catcooperation.home = 1
				 	 ORDER BY mn_cooperation.Id DESC";
			$numrow = 9;
			$temp['data']['cooperation_home']=  $this->get_query($sql,$numrow);
		}*/

		// Tin library
		$temp['data']['library_home_alias'] = 'thu-vien/';
		$sql ="SELECT mn_catlibrary.title_vn,mn_catlibrary.alias as catalias,mn_library.title_vn,mn_library.content_vn,mn_library.images,mn_library.alias,mn_library.description_vn FROM  mn_catlibrary
					 LEFT JOIN mn_library ON mn_catlibrary.Id = mn_library.idcat AND mn_library.ticlock = 0 
				 	 WHERE mn_catlibrary.ticlock = 0
				 	 ORDER BY mn_library.date DESC";
		$numrow = 7;
		$temp['data']['news_home']=  $this->get_query($sql,$numrow);

		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off'; 
		
		$temp['template']='default/home/index'; 
		$this->load->view("default/layout",$temp); 
	}

	public function detail($text,$Id,$date){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();	
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';
		$temp['data']['breadcrumb']=  'Tin nổi bật';

		$sql = "SELECT * FROM
				(SELECT * FROM 
					(
						SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id,mn_news.content_vn FROM  mn_news
					    UNION  
					    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id,mn_activities.content_vn FROM  mn_activities
					    UNION  
					    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id,mn_student.content_vn FROM  mn_student
					    UNION
					    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id,mn_media.content_vn FROM  mn_media
					    UNION
					    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id,mn_cooperation.content_vn FROM  mn_cooperation
					    UNION
					    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id,mn_admissions.content_vn FROM  mn_admissions
					    UNION
					    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id,mn_faculty.content_vn FROM  mn_faculty
					) A 
					WHERE A.NoiBat = 1) B
	    		WHERE B.date = ".$date." AND B.Id = ".$Id."
	    		";
		$numrow = 0;
		$temp['data']['detail']=  $this->get_query($sql,$numrow);

		if(!$date || empty($temp['data']['detail'])) redirect(base_url('404.html'));

		$temp['template']='default/home/detail';
		$this->load->view("default/layout",$temp);
	}

	public function htdt($alias){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();	
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_activities.*, mn_catactivities.alias as alias_cat
				FROM mn_activities LEFT JOIN mn_catactivities ON mn_activities.idcat = mn_catactivities.Id
				WHERE mn_activities.ticlock = 0 AND mn_activities.home = 1
    			ORDER BY mn_activities.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'he-thong-dao-tao';

		$sql = "SELECT * FROM mn_webplus
				WHERE kind = 2 AND alias = '".$alias."'
	    		ORDER BY Id";
		$numrow = 10;
		$temp['data']['detail'] = $info = $this->get_query($sql,$numrow);
		if(!$alias || empty($info)) redirect(base_url('404.html'));

		//lay cung loai
		$sql = "SELECT * FROM mn_webplus
				WHERE kind = 2 AND alias != '".$alias."'
	    		ORDER BY Id";
		$numrow = 10;
		$temp['data']['info_cl'] = $info = $this->get_query($sql,$numrow);

		$temp['template']='default/home/detail';
		$this->load->view("default/layout",$temp);
	}

	public function event($alias){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		// $temp['data']['htdt'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_activities.*, mn_catactivities.alias as alias_cat
				FROM mn_activities LEFT JOIN mn_catactivities ON mn_activities.idcat = mn_catactivities.Id
				WHERE mn_activities.ticlock = 0 AND mn_activities.home = 1
    			ORDER BY mn_activities.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'he-thong-dao-tao';

		$sql = "SELECT * FROM mn_webplus
				WHERE kind = 3 AND alias = '".$alias."'
	    		ORDER BY Id";
		$numrow = 10;
		$temp['data']['detail'] = $info = $this->get_query($sql,$numrow);
		if(!$alias || empty($info)) redirect(base_url('404.html'));

		//lay cung loai
		$sql = "SELECT * FROM mn_webplus
				WHERE kind = 3 AND alias != '".$alias."'
	    		ORDER BY Id";
		$numrow = 10;
		$temp['data']['info_cl'] = $info = $this->get_query($sql,$numrow);
		$temp['data']['menu']['sub_alias']= 'event';

		$temp['data']['breadcrumb']=  "<a href='event' > Sự kiện hoạt động media</a>";
		$temp['template']='default/home/detail';
		$this->load->view("default/layout",$temp);
	}

	public function listevent($p=0){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		// $sql = "SELECT * FROM mn_webplus
		// 		WHERE kind = 3 AND alias != '".$alias."'
	 	//    		ORDER BY date DESC";
		// $numrow = 10;
		// $temp['data']['info']=  $this->get_query($sql,$numrow);

		$config['base_url']	=	base_url('event/'.$info_cat[0]['alias']);
		$config['total_rows'] = $this->webplus_model->count_where(array("ticlock"=>0,"kind"=>"3"));
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->webplus_model->get_page(array("ticlock"=>0,"kind"=>"3"),"sort DESC, Id DESC",$config['per_page'],$p);
		$temp['data']['title_main']= 'Sự kiện hoạt động media';
		$temp['data']['sub_alias']= 'event/';

		$temp['template']='default/home/listevent';
		$this->load->view("default/layout",$temp);
	}

	public function listcat($p=0){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		$temp['template']='default/home/listcat';
		$this->load->view("default/layout",$temp);
	}

	public function search($p=0){
		$temp['data']['menu']['menu_news'] = $this->pagehtml_model->get_newsidcat(25,0,0);
		$temp['data']['menu']['menu_activities'] = $this->catactivities_model->get_metoru();
		$temp['data']['menu']['menu_cooperation'] = $this->catcooperation_model->get_metoru();
		$temp['data']['menu']['menu_library'] = $this->catlibrary_model->get_metoru();
		$temp['data']['menu']['menu_media'] = $this->catmedia_model->get_metoru();
		$temp['data']['menu']['menu_student'] = $this->catstudent_model->get_metoru();
		$temp['data']['menu']['menu_admissions'] = $this->catadmissions_model->get_metoru();
		$temp['data']['menu']['menu_faculty'] = $this->catfaculty_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//search
		$temp['data']['s'] =$s  = $this->input->get('s', TRUE);
		$keysearch = $this->page->strtoseo($s);
		$sql = "SELECT * FROM 
		(
			SELECT mn_news.title_vn,mn_news.images,mn_news.alias,mn_news.date,mn_news.Id,mn_catnews.alias as alias_cat FROM mn_news
					LEFT JOIN mn_catnews ON mn_catnews.Id = mn_news.idcat
		    UNION  
		    SELECT mn_activities.title_vn,mn_activities.images,mn_activities.alias,mn_activities.date,mn_activities.Id,mn_catactivities.alias as alias_cat FROM  mn_activities
		    		LEFT JOIN mn_catactivities ON mn_catactivities.Id = mn_activities.idcat
		    UNION  
		    SELECT mn_student.title_vn,mn_student.images,mn_student.alias,mn_student.date,mn_student.Id,mn_catstudent.alias as alias_cat FROM  mn_student
		    		LEFT JOIN mn_catstudent ON mn_catstudent.Id = mn_student.idcat
		    UNION
		    SELECT mn_media.title_vn,mn_media.images,mn_media.alias,mn_media.date,mn_media.Id,mn_catmedia.alias as alias_cat FROM  mn_media
		    		LEFT JOIN mn_catmedia ON mn_catmedia.Id = mn_media.idcat
		    UNION
		    SELECT mn_cooperation.title_vn,mn_cooperation.images,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id,mn_catcooperation.alias as alias_cat FROM  mn_cooperation
		    		LEFT JOIN mn_catcooperation ON mn_catcooperation.Id = mn_cooperation.idcat
		) A 
		WHERE A.alias LIKE '%".$keysearch."%'
		ORDER BY A.date DESC";

		$temp['data']['total'] = $this->get_query($sql,0);

		$total = count($temp['data']['total']);
		$p = (int)$this->input->get('page', TRUE);
		$numrow = 10;
		$div = 5;
		$skip = $p * $numrow;

    	$sql = "SELECT * FROM 
		(
			SELECT mn_news.title_vn,mn_news.images,mn_news.alias,mn_news.date,mn_news.Id,mn_catnews.alias as alias_cat FROM mn_news
					LEFT JOIN mn_catnews ON mn_catnews.Id = mn_news.idcat
		    UNION  
		    SELECT mn_activities.title_vn,mn_activities.images,mn_activities.alias,mn_activities.date,mn_activities.Id,mn_catactivities.alias as alias_cat FROM  mn_activities
		    		LEFT JOIN mn_catactivities ON mn_catactivities.Id = mn_activities.idcat
		    UNION  
		    SELECT mn_student.title_vn,mn_student.images,mn_student.alias,mn_student.date,mn_student.Id,mn_catstudent.alias as alias_cat FROM  mn_student
		    		LEFT JOIN mn_catstudent ON mn_catstudent.Id = mn_student.idcat
		    UNION
		    SELECT mn_media.title_vn,mn_media.images,mn_media.alias,mn_media.date,mn_media.Id,mn_catmedia.alias as alias_cat FROM  mn_media
		    		LEFT JOIN mn_catmedia ON mn_catmedia.Id = mn_media.idcat
		    UNION
		    SELECT mn_cooperation.title_vn,mn_cooperation.images,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id,mn_catcooperation.alias as alias_cat FROM  mn_cooperation
		    		LEFT JOIN mn_catcooperation ON mn_catcooperation.Id = mn_cooperation.idcat
		) A 
		WHERE A.alias LIKE '%".$keysearch."%'
		ORDER BY A.date DESC LIMIT ".$skip." , ".$numrow."";

		// $this->pagination->initialize($config);
    	$temp['data']['info_search'] = $this->get_pagination($sql);
		$params = $this->input->get();
		$params['page'] = '';
		$temp['data']['linked'] = BASE_URL."search?".http_build_query($params);
		$temp['data']['page']= $this->page->divPageF($total,$p,$div,$numrow,$temp['data']['linked']);

		$temp['template']='default/home/listsearch';
		$this->load->view("default/layout",$temp);
	}

	public function get_pagination($sql)
	{
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function detail_hot()
	{
		$detail_hot = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id FROM  mn_activities
				    UNION  
				    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id FROM  mn_student
				    UNION
				    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id FROM  mn_media
				    UNION
				    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id FROM  mn_cooperation
				    UNION
				    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id FROM  mn_admissions
				    UNION
				    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id FROM  mn_faculty
				) A 
				WHERE A.NoiBat = 1
    			ORDER BY date DESC";
		return $detail_hot;	
	}

	/*_______________________________________________________*/
	
	public function page($id){ 
		$this->load->model('flash_model');
		$temp['data']['idcat'] = $id; 
		$temp['data']['about']  = $this->pagehtml_model->get_public($id);
		$temp['data']['breadcrumb'] =  $this->map_title .$this-> arrowmap . '<a href = "'.base_url($temp['data']['about'][0]['alias'].".html").'" >'.$temp['data']['about'][0]['title_vn'].'</a>';
		$temp['template']='default/home/'; 
			$this->load->view("default/layout",$temp);
	}

	public function contact(){
		$this->load->model('contact_model');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$mess = "Nhập thông tin cần thiết";
		
		if($fullname=="") { $mess = "Nhập họ tên"; $key = "#fullname-contact"; }
		else if($email=="") { $mess = "Nhập email"; $key = "#email-contact";  }
		else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $mess = "Email không đúng định dạng"; $key = "#email-contact"; }
		else if($title=="") { $mess = "Nhập tiêu đề";   $key = "#title-contact";} 
		else if($content==""){  $mess = "Nhập nội dung cần gửi"; $key = "#content-contact"; }
		else {
			$arr = array(
				"title" => $title,
				"email" =>$email,
				"fullname" =>$fullname,
				"content_vn"=> $content,
				"date"	=>time(),
			);
			$this->contact_model->add($arr);
			$mess = "Gửi thông tin liên hệ thành công. <br />Chúng tôi sẽ sớm liên hệ với bạn !";
			die(json_encode(array('err'=>"false","mess"=>$mess)));
		}
		die(json_encode(array('err'=>"true","mess"=>$mess,"key"=>$key)));
	}
	
	function email(){
		$email = $this->input->post('email');
		if($email ==""){
			die(json_encode(array('mess'=>"Địa chỉ email không được bỏ trống!")));
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			die(json_encode(array('mess'=>"Email không đúng định dạng")));
		}else{
			$this->load->model('contact_model');
			$arr = array(
				"email" =>$email,
				'date'=>time(),
				'ticlock' =>0,
				'iduser' => $this->session->userdata('login_user_id'),
			);
			$this->contact_model->add($arr);
			die(json_encode(array('mess'=>"Đăng ký email thành công")));
		}
		
	}
	public function notfound(){
		$temp['data'] = NULL;
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';
		$temp['data']['banner_off'] = 'off';
		$temp['template']='default/home/notfound'; 
		$this->load->view("default/layout",$temp); 
	}
	
	public function excel()
	{
		$sql = "SELECT *,(SELECT title_vn FROM mn_catelog WHERE mn_catelog.Id= mn_product.idcat ) AS danhmuc,(SELECT title_vn FROM mn_manufacturer WHERE mn_manufacturer.Id= mn_product.idmanufacturer) AS manu FROM mn_product WHERE ticlock=0 AND trash=0";
		$data['info'] =  $this->product_model->get_query($sql,9999,0);
		$this->load->view("default/home/excel",$data); 
	}
	
}
