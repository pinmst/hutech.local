<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('menu_model');
		$this->load->model('student_model');
		$this->load->model('flash_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');

		$this->load->model('catnews_model');
		$this->load->model('catactivities_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catstudent_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}
	
	public function detail($alias, $alias_sub){
		$temp['data']['menu']['sub_alias'] = '/sinh-vien';
		$temp['data']['menu']['sub_title'] = 'Sinh viên';
		$temp['data']['menu']['only'] = $this->catstudent_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_student.*, mn_catstudent.alias as alias_cat
				FROM mn_student LEFT JOIN mn_catstudent ON mn_student.idcat = mn_catstudent.Id
				WHERE mn_student.ticlock = 0 AND mn_student.home = 1
    			ORDER BY mn_student.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'sinh-vien';

		$temp['data']['info'] = $info = $this->catstudent_model->get_list(array('alias'=>$alias));
		$temp['data']['info_detail'] = $info_detail = $this->student_model->get_list(array('alias'=>$alias_sub));
		if(!$alias || empty($info)) redirect(base_url('404.html'));

		//lay cung loai
		$info_cat = $this->catstudent_model->get_list(array('alias'=>$alias));
		$temp['data']['info_cl'] = $this->student_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",'','');
		
		$temp['template']='default/student/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function catelog($alias,$p=0){
		//menu
		$temp['data']['menu']['sub_alias'] = '/sinh-vien';
		$temp['data']['menu']['sub_title'] = 'Sinh viên';
		$temp['data']['menu']['only'] = $this->catstudent_model->get_metoru();
		// $temp['data']['sidebar'] = 'off';

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_student.*, mn_catstudent.alias as alias_cat
				FROM mn_student LEFT JOIN mn_catstudent ON mn_student.idcat = mn_catstudent.Id
				WHERE mn_student.ticlock = 0 AND mn_student.home = 1
    			ORDER BY mn_student.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'sinh-vien';

		$temp['data']['detail'] = $this->student_model->list_data();
		$temp['data']['info'] = $this->catstudent_model->get_metoru();
		$temp['template']='default/student/catelog'; 
		$this->load->view("default/layout",$temp);
		
	}

	public function listCat($alias,$p=0){
		//menu
		$temp['data']['menu']['sub_alias'] = '/sinh-vien';
		$temp['data']['menu']['sub_title'] = 'Sinh viên';
		$temp['data']['menu']['only'] = $this->catstudent_model->get_metoru();

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_student.*, mn_catstudent.alias as alias_cat
				FROM mn_student LEFT JOIN mn_catstudent ON mn_student.idcat = mn_catstudent.Id
				WHERE mn_student.ticlock = 0 AND mn_student.home = 1
    			ORDER BY mn_student.date DESC";
		$numrow = 10;
		$temp['data']['news_hot']=  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'sinh-vien';

		$info_cat = $this->catstudent_model->get_list(array('alias'=>$alias));
		$config['base_url']	=	base_url('sinh-vien/'.$info_cat[0]['alias']);
		$config['total_rows'] = $this->student_model->count_where(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']));
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->student_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",$config['per_page'],$p);
	
		$temp['data']['sub'] = $alias;
		$temp['data']['main_vn'] = $info_cat;

		$temp['template']='default/student/listcat'; 
		$this->load->view("default/layout",$temp);
	}

	public function detail_hot()
	{
		$detail_hot = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id FROM  mn_activities
				    UNION  
				    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id FROM  mn_student
				    UNION
				    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id FROM  mn_media
				    UNION
				    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id FROM  mn_cooperation
				    UNION
				    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id FROM  mn_admissions
				    UNION
				    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id FROM  mn_faculty
				) A 
				WHERE A.NoiBat = 1
    			ORDER BY date DESC";
		return $detail_hot;	
	}
}
