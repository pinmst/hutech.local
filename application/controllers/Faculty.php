<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('menu_model');
		$this->load->model('news_model');
		$this->load->model('flash_model');
		$this->load->model('media_model');
		$this->load->model('admissions_model');
		$this->load->model('admonline_model');
		$this->load->model('provinces_model');
		$this->load->model('tohop_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');
		$this->load->model('faculty_model');

		$this->load->model('catnews_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catadmissions_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catfaculty_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	public function get_query($sql,$limit = 1)
	{
		if($limit>0)
			$sql  .=" LIMIT ".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function index($alias){
		// menu
		$idcat = array();
		$check = $this->catfaculty_model->get_alias($alias);
		$idcat[] = $check['0']['Id'];
		$sql= "SELECT * FROM mn_bmenu WHERE parentid = ".$check['0']['Id']." AND style='4' AND ticlock='0' ORDER BY Id DESC"; 
		$temp['data']['bfaculty'] = $this->bmenu_model->get_query2($sql,500,0);

		$temp['data']['menu']['sub_alias'] = '/khoa/'.$alias;
		$temp['data']['menu']['sub_title'] = 'KHOA';
		$temp['data']['menu']['only'] = $this->catfaculty_model->get_metoru1($check['0']['Id']);
		$temp['data']['menu']['only_sub2'] = $subcat = $this->catfaculty_model->get_metoru2();
		foreach($temp['data']['menu']['only_sub2'] as $item)
        {
        	if($item['parentid'] != '0' && $item['parentid'] == $check['0']['Id']){
            	$idstr = $idstr.','.$item['Id'];
            }
        } 

        $condition = $check['0']['Id'].$idstr;

		$temp['data']['faculty']['only'] = $this->catfaculty_model->get_where($check['0']['Id']);
		if(empty($check)) redirect(base_url('404.html'));
		
		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.home = 1
    			ORDER BY mn_faculty.date DESC";
		$numrow = 10;
		$temp['data']['news_hot'] =  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'khoa';

		// new all sort desc
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.ghim = 1 AND mn_faculty.idcat IN (".$condition.")
    			ORDER BY mn_faculty.date DESC";
		$numrow = 9;
		$temp['data']['news_lester']=  $this->get_query($sql,$numrow);
		$temp['data']['news_lester_alias']= 'thong-bao/';

		$temp['data']['detail'] = $this->faculty_model->list_data();
		$temp['data']['info'] = $temp['data']['menu']['only'];
		$temp['template']='default/faculty/index'; 
		$this->load->view("default/layout",$temp);
	}

	public function detail($alias,$alias_sub,$alias_child){
		$check_cat = $this->catfaculty_model->get_alias($alias);
		$check_sub = $this->catfaculty_model->get_alias($alias_sub);
		$temp['data']['info_detail'] = $info_detail = $this->faculty_model->get_list(array('alias'=>$alias_child));
		// $sql= "SELECT * FROM mn_bmenu WHERE parentid = ".$check_cat['0']['Id']." AND style='4' AND ticlock='0' ORDER BY Id DESC"; 
		// $temp['data']['bfaculty'] = $this->bmenu_model->get_query2($sql,500,0);

		$temp['data']['menu']['sub_alias'] = '/khoa/'.$alias;
		$temp['data']['menu']['sub_title'] = 'KHOA';
		$temp['data']['menu']['only'] = $this->catfaculty_model->get_metoru1($check_cat['0']['Id']);
		$temp['data']['menu']['only_sub2'] = $subcat = $this->catfaculty_model->get_metoru2();

		if(empty($check_cat) || empty($check_sub) || empty($info_detail)) redirect(base_url('404.html'));

		//banner and footer for faculty
		$temp['data']['faculty']['only'] = $this->catfaculty_model->get_where($check_cat['0']['Id']);

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.home = 1
    			ORDER BY mn_faculty.date DESC";
		$numrow = 10;
		$temp['data']['news_hot'] =  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'khoa';

		// new all sort desc
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.ghim = 1
    			ORDER BY mn_faculty.date DESC";
		$numrow = 10;
		$temp['data']['news_lester']=  $this->get_query($sql,$numrow);
		$temp['data']['news_lester_alias']= 'thong-bao/';

		foreach($subcat as $item)
        {
        	if($item['parentid'] != '0' && $item['parentid'] == $check_sub['0']['Id']){
            	$idstr = $idstr.','.$item['Id'];
            }
        }
        $condition = $check_sub['0']['Id'].$idstr;

		$sql = "SELECT *
				FROM mn_faculty
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.idcat IN (".$condition.")
    			ORDER BY mn_faculty.date DESC";
    	$temp['data']['info_cl'] =  $this->get_query($sql,10);

    	$temp['data']['sub'] = '/khoa/'.$alias.'/'.$alias_sub;
    	
    	$temp['data']['sub_breacrum'] = '/khoa/'.$alias;
    	$temp['data']['title_breacrum'] = $check_sub;
		$temp['template']='default/faculty/detail'; 
		$this->load->view("default/layout",$temp);
	}

	public function listcat($alias,$alias_sub,$p=0){
		$check_cat = $this->catfaculty_model->get_alias($alias);
		$check_sub = $this->catfaculty_model->get_alias($alias_sub);
		$sql= "SELECT * FROM mn_bmenu WHERE parentid = ".$check_cat['0']['Id']." AND style='4' AND ticlock='0' ORDER BY Id DESC"; 
		$temp['data']['bfaculty'] = $this->bmenu_model->get_query2($sql,500,0);

		$temp['data']['menu']['sub_alias'] = '/khoa/'.$alias;
		$temp['data']['menu']['sub_title'] = 'KHOA';
		$temp['data']['menu']['only'] = $this->catfaculty_model->get_metoru1($check_cat['0']['Id']);
		$temp['data']['menu']['only_sub2'] = $subcat = $this->catfaculty_model->get_metoru2();
		if(empty($check_cat) || empty($check_sub)) redirect(base_url('404.html'));

		//banner and footer for faculty
		$temp['data']['faculty']['only'] = $this->catfaculty_model->get_where($check_cat['0']['Id']);

		// Tin noi bat
		$numrow = 10;
		$temp['data']['news_home']=  $this->get_query($this->detail_hot(),$numrow);
		$temp['data']['news_home_alias']=  'tin-noi-bat/';

		//new of cat
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.home = 1
    			ORDER BY mn_faculty.date DESC";
		$numrow = 10;
		$temp['data']['news_hot'] =  $this->get_query($sql,$numrow);
		$temp['data']['news_hot_alias']=  'khoa';

		// new all sort desc
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.ghim = 1
    			ORDER BY mn_faculty.date DESC";
		$numrow = 10;
		$temp['data']['news_lester']=  $this->get_query($sql,$numrow);
		$temp['data']['news_lester_alias']= 'thong-bao/';

		foreach($subcat as $item)
        {
        	if($item['parentid'] != '0' && $item['parentid'] == $check_sub['0']['Id']){
            	$idstr = $idstr.','.$item['Id'];
            }
        }
        $condition = $check_sub['0']['Id'].$idstr;
		$info_cat = $this->catfaculty_model->get_list(array('alias'=>$alias_sub));

		$sql = "SELECT count(mn_faculty.Id) as total
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.idcat IN (".$condition.")
    			ORDER BY mn_faculty.date DESC";
    	// print_r($sql);
    	$total =  $this->get_query($sql,0);
    	$config['base_url']	=	base_url('/khoa/'.$alias.'/'.$alias_sub);
    	$config['total_rows'] = $total['0']['total'];
		$config['per_page']	= 10;
		$config['num_links'] = 5;
		$num = $config['per_page'] * $p;
		
		$this->pagination->initialize($config);
		$sql = "SELECT mn_faculty.*, mn_catfaculty.alias as alias_cat
				FROM mn_faculty LEFT JOIN mn_catfaculty ON mn_faculty.idcat = mn_catfaculty.Id
				WHERE mn_faculty.ticlock = 0 AND mn_faculty.idcat IN (".$condition.") 
    			ORDER BY mn_faculty.sort ASC,mn_faculty.date DESC LIMIT ".$num." , ".$config['per_page']."";
    	$temp['data']['info'] = $this->get_pagination($sql);

    	$temp['data']['sub'] = '/khoa/'.$alias.'/'.$alias_sub;

    	$temp['data']['main_vn'] = $info_cat;
    	$temp['data']['sub_breacrum'] = '/khoa/'.$alias;
		$temp['template']='default/faculty/listcat'; 
		$this->load->view("default/layout",$temp);
	}

	public function get_pagination($sql)
	{
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function detail_hot()
	{
		$detail_hot = "SELECT * FROM 
				(
					SELECT mn_news.title_vn,mn_news.NoiBat,mn_news.alias,mn_news.date,mn_news.Id FROM  mn_news
				    UNION  
				    SELECT mn_activities.title_vn,mn_activities.NoiBat,mn_activities.alias,mn_activities.date,mn_activities.Id FROM  mn_activities
				    UNION  
				    SELECT mn_student.title_vn,mn_student.NoiBat,mn_student.alias,mn_student.date,mn_student.Id FROM  mn_student
				    UNION
				    SELECT mn_media.title_vn,mn_media.NoiBat,mn_media.alias,mn_media.date,mn_media.Id FROM  mn_media
				    UNION
				    SELECT mn_cooperation.title_vn,mn_cooperation.NoiBat,mn_cooperation.alias,mn_cooperation.date,mn_cooperation.Id FROM  mn_cooperation
				    UNION
				    SELECT mn_admissions.title_vn,mn_admissions.NoiBat,mn_admissions.alias,mn_admissions.date,mn_admissions.Id FROM  mn_admissions
				    UNION
				    SELECT mn_faculty.title_vn,mn_faculty.NoiBat,mn_faculty.alias,mn_faculty.date,mn_faculty.Id FROM  mn_faculty
				) A 
				WHERE A.NoiBat = 1
    			ORDER BY date DESC";
		return $detail_hot;	
	}
}