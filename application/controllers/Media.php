<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {
	protected $arrowmap = " > ";
	protected $map_title = '<a href="/">Trang chủ</a>';
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('user_model');
		$this->load->model('bmenu_model');
		$this->load->model('menu_model');
		$this->load->model('flash_model');
		$this->load->model('news_model');
		$this->load->model('media_model');
		$this->load->model('pagehtml_model');
		$this->load->model('webplus_model');

		$this->load->model('catnews_model');
		$this->load->model('catactivities_model');
		$this->load->model('catcooperation_model');
		$this->load->model('catlibrary_model');
		$this->load->model('catmedia_model');
		$this->load->model('catstudent_model');
		 $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	public function detail($alias, $alias_sub){
		$temp['data']['menu']['sub_alias'] = '/media';
		$temp['data']['menu']['sub_title'] = 'Media';
		$temp['data']['menu']['only'] = $this->catmedia_model->get_metoru();
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';

		$temp['data']['info'] = $info = $this->catmedia_model->get_list(array('alias'=>$alias));
		$temp['data']['info_detail'] = $info_detail = $this->media_model->get_list(array('alias'=>$alias_sub));
		if(!$alias || empty($info)) redirect(base_url('404.html'));

		//lay cung loai
		$temp['data']['detail'] = $this->media_model->list_data();
		
		$temp['template']='default/media/detail';
		$this->load->view("default/layout",$temp); 
	}

	public function catelog($alias,$p=0){
		$temp['data']['menu']['sub_alias'] = '/media';
		$temp['data']['menu']['sub_title'] = 'Media';
		$temp['data']['menu']['only'] = $this->catmedia_model->get_metoru();
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';

		$temp['data']['detail'] = $this->media_model->list_data();
		$temp['data']['info'] = $this->catmedia_model->get_metoru();
		$temp['template']='default/media/catelog'; 
		$this->load->view("default/layout",$temp);
		
	}

	public function listCat($alias,$p=0){
		//use menu
		$temp['data']['menu']['sub_alias'] = '/media';
		$temp['data']['menu']['sub_title'] = 'Media';
		$temp['data']['menu']['only'] = $this->catmedia_model->get_metoru();
		$temp['data']['sidebar'] = 'off';
		$temp['data']['sidebar_notice'] = 'off';
		$temp['data']['side_htdt'] = 'off';
		
		$info_cat = $this->catmedia_model->get_list(array('alias'=>$alias));
		$config['base_url']	=	base_url('media/'.$info_cat[0]['alias']);
		
		$config['total_rows'] = $this->media_model->count_where(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']));
		$config['per_page']	= 12;
		$config['num_links'] = 5;
		
		$this->pagination->initialize($config);
		$temp['data']['info'] = $this->media_model->get_list(array("ticlock"=>0,"idcat"=>$info_cat[0]['Id']),"sort DESC, Id DESC",$config['per_page'],$p);

		$temp['data']['sub'] = $alias;
		$temp['data']['main_vn'] = $info_cat;
		$temp['template']='default/media/listcat'; 
		$this->load->view("default/layout",$temp);
		
	}
}
