<?php 
class Webplus_model extends CI_Model {
	public $title;
	public $content;
	public $date;

	public function __construct()
	{
		 parent::__construct();
		$this->load->database();
	}


	public function list_data($limit,$skip)
	{
		$query = $this->db->get('mn_webplus',$limit,$skip);
		return $query->result();
	}
	public function count_all()
	{
		return $this->db->count_all('mn_webplus');	

	}
	
	public function get_kind($where,$order = "sort ASC, Id DESC",$limit=1,$skip=0)
	{
		$this->db->order_by($order);
		$query = $this->db->get_where('mn_webplus',$where,$limit,$skip);
		return $query->result_array();
	}

	public function get_page($where,$order = "sort ASC, Id DESC",$limit=1,$skip=0)
	{
		//$this->db->cache_on();
		$this->db->order_by($order);
		$query = $this->db->get_where('mn_webplus',$where,$limit,$skip);
		return $query->result_array();
	}

	public function count_where($where)
	{
		$query = $this->db->get_where('mn_webplus',$where);
		$result = $query->num_rows();
		return $result;
	}

	public function get_list()
	{ 
		$idcache = md5('webplus_home');
		$result = $this->cache->get($idcache);
		if(!$result){
			$query = $this->db->get_where('mn_webplus',array('ticlock'=>'0'));
			$result =  $query->result_array();
			$this->cache->save($idcache, $result, 60*60*2);
		}
		return $result;
	}
	public function get_where($id)
	{
		$query = $this->db->get_where('mn_webplus',array("Id"=>$id));
		return $query->result_array();
	}
	public function get_Arr($arr)
	{
		$query = $this->db->get_where('mn_webplus',$arr);
		return $query->result_array();
	}
	public function add($data){
		$data['title_vn'] = $this->input->post('title_vn');
		$data['sort'] = (int)$this->input->post('sort');
		$data['ticlock'] = $this->input->post('ticlock')?$this->input->post('ticlock'):0;
		$data['link'] = $this->input->post('link');
		$data['date'] = time();
		// $data['meta_keyword'] = $this->input->post('meta_keyword');
		// $data['meta_description'] = $this->input->post('meta_description');
		return $this->db->insert('mn_webplus', $data);
	}
	public function delete($id){
		
		return $this->db->delete('mn_webplus', array('Id' => $id)); 
	}
	public function update($id,$data,$option = false)
	{
		if($option==true){
			$data['title_vn'] = $this->input->post('title_vn');
			$data['sort'] = (int)$this->input->post('sort');
			$data['ticlock'] = $this->input->post('ticlock')?$this->input->post('ticlock'):0;
			$data['link'] = $this->input->post('link');
			// $data['meta_keyword'] = $this->input->post('meta_keyword');
			// $data['meta_description'] = $this->input->post('meta_description');
		}
		$this->db->where('Id', $id);
		$result = $this->db->update('mn_webplus', $data); 
		return $result;
	}
}