<?php 
class Bmenu_model extends CI_Model {
	public function __construct()
	{
		 parent::__construct();
		$this->load->database();
	}

	public function list_data($limit,$skip)
	{
		$query = $this->db->get('mn_bmenu',$limit,$skip);
		return $query->result();
	}
	public function count_all()
	{
		$query = $this->db->get_where('mn_bmenu',array('parentid'=>'0','style'=>'3'));
		return $query->result_array();
	}
	public function get_list()
	{
		$query = $this->db->get_where('mn_bmenu',array('style'=>'3'));
		$result =  $query->result_array();
		return $result;
	}
	public function get_listhome()
	{
		$query = $this->db->get_where('mn_bmenu',array('ticlock'=>'0','style'=>'3'));
		$result =  $query->result_array();
		return $result;
	}
	
	public function get_bfaculty()
	{
		$query = $this->db->get_where('mn_bmenu',array('style'=>'4'));
		$result =  $query->result_array();
		return $result;
	}
	public function count_bfaculty()
	{
		$query = $this->db->get_where('mn_bmenu',array('ticlock'=>'0','style'=>'4'));
		return $query->result_array();
	}
	public function get_query2($sql,$limit = 1,$skip = 0)
	{
		if($skip=="") $skip = 0;
		$sql  .=" LIMIT ".$skip.",".$limit;
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function get_where($id)
	{
		$query = $this->db->get_where('mn_bmenu',array("Id"=>$id));
		return $query->result_array();
	}
	public function get_Arr($arr)
	{
		$this->db->order_by("sort ASC, Id ASC"); 
		$query = $this->db->get_where('mn_bmenu',$arr);
		return $query->result_array();
	}
	public function add($data){
		$data['parentid'] = $this->input->post('parentid');
		$data['style'] = $this->input->post('style');
		$data['sort'] = (int)$this->input->post('sort');
		$data['ticlock'] = $this->input->post('ticlock')?$this->input->post('ticlock'):0;
		$data['link'] = $this->input->post('link');
		return $this->db->insert('mn_bmenu', $data);
	}
	public function delete($id){
		
		return $this->db->delete('mn_bmenu', array('Id' => $id)); 
	}
	public function update($id,$data,$option = false)
	{
		if($option==true){
			$data['parentid'] = $this->input->post('parentid')?$this->input->post('parentid'):0;
			$data['style'] = $this->input->post('style');
			$data['sort'] = (int)$this->input->post('sort');
			$data['ticlock'] = $this->input->post('ticlock')?$this->input->post('ticlock'):0;
			$data['link'] = $this->input->post('link');
		}
		$this->db->where('Id', $id);
		$result = $this->db->update('mn_bmenu', $data); 
		return $result;
	}
}