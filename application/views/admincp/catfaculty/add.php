<div class="main_area">
    <div class="breakcrumb">
    <table border="0">
    <tbody>
    <tr>
    <td width="25">
    <i class="fa icon-23 fa-windows"></i>
    </td>
    <td> Nội dung / Chủ đề bài viết / <?php echo $map_title ?></td>
    </tr>
    </tbody>
    </table>
    </div>
</div>
<div class="content">
<div class="content_i">
<form method="post" name="add-new" action="" enctype="multipart/form-data">
<table>
<tr>
    <td class = 'title_td' width="100">Khoa </td>
    <td>
         <select name = 'parentid'>
            <option value = ''>- - Chọn Khoa - -</option>
            <?php echo $this->page->TreeCat($listcat,0,"","","");?>
        </select>
    </td>
</tr>
<tr>
	<td class = 'title_td' >Tiêu đề</td>
    <td> <input type="text" name="title_vn" value="<?php echo set_value('title_vn','') ?>"  style="width:400px">
   		 <?php echo form_error('title_vn'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Banner top</td>
    <td> <input type="file" name="userfile"   >
     </td>
</tr>
<tr>
    <td class = 'title_td' >Alias</td>
    <td> <input type="text" name="alias" value="<?php echo set_value('alias','') ?>"  style="width:400px"> 
        <?php echo form_error('alias'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td'> Nội dung footer</td>
    <td><textarea name="footer" id="editor1" ><?php echo set_value('footer','') ?></textarea>
     <?php echo form_error('footer'); ?>
    </td>
</tr>
<tr>
	<td class = 'title_td' >Sắp xếp</td>
    <td> <input type="text" name="sort" value="<?php echo set_value('sort','') ?>"  style="width:200px">
   		 <?php echo form_error('sort'); ?>
     </td>
</tr>
<tr>
	<td class = 'title_td' >Khóa</td>
    <td> <input type="checkbox" value="1" name="ticlock"  <?php echo set_checkbox('ticlock', '1'); ?>/> </td>
</tr>
<tr>
<td  ></td>
    <th align = 'left'>
       <button type = 'submit' name="save" value="save"  class="button" >Thêm mới</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <input type = 'reset' value = 'Làm lại' class="button">
    </th>
</tr>	
</table>
</form> 
</div>
</div> 
<script type="text/javascript">
if (typeof CKEDITOR == 'undefined') {
    document.write('CKEditor');
}else {
    var editorContent = CKEDITOR.replace('editor1'); 
}
</script>  