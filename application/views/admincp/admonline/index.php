<div class="main_area">
    <div class="breakcrumb">
    <table border="0">
    <tbody>
    <tr>
    <td width="25">
    	<i class="fa icon-23 fa-windows"></i>
    </td>
    <td> Nội dung / Thông tin xét tuyển</td>
    </tr>
    </tbody>
    </table>
    </div>
</div>
<div class="content">
<div class="list_button">

<form action="<?php echo base_url('admincp/admonline/index'); ?>" method="get">
<table cellpadding="0" style="float:left">
<tbody>
<td>
    <select name="idcat">
    <option value="0">-- Chọn phương thức --</option>
    <?php
    $idcat = $this->input->get('idcat', TRUE);
    $status = $this->input->get('status', TRUE);
    $catelog = $this->pagehtml_model->get_catadmonline(array('ticlock'=>0,'parentid'=>0),1000);
    if(!empty($catelog)){
        foreach($catelog as $item){
            $sub = $this->pagehtml_model->get_catadmonline(array('ticlock'=>0,'parentid'=>$item['Id']),100);
    ?>
    <option value="<?php echo $item['Id'] ?>" <?php if($idcat==$item['Id']) echo "selected"; ?>><?php echo $item['title_vn'] ?></option>
    <?php if(!empty($sub)){
        foreach($sub as $row){ ?>
        <option value="<?php echo $row['Id'] ?>" <?php if($idcat==$row['Id']) echo "selected"; ?>> <?php echo $row['title_vn'] ?></option>
    <?php }}}} ?>
</select>
</td>
<td>
    <select id="style" name="status" >
        <option value="0">-- Chọn tình trạng --</option>
        <option value="4" <?php if ($status == '4') {echo "selected";} ?>>Hồ sơ mới</option>
        <option value="1" <?php if ($status == '1') {echo "selected";} ?>>Nhận hồ sơ, Đang xử lý</option>
        <option value="2" <?php if ($status == '2') {echo "selected";} ?>>Đậu</option>
        <option value="3" <?php if ($status == '3') {echo "selected";} ?>>Không đủ điều kiện</option>
    </select>
</td>
<td> 
<input type="text" value="" placeholder="Tìm tiêu đề" size="40" name="tukhoa" /> </td>
<td> <input type="submit" value="Tìm kiếm" name="btntimkiem" class="button" /></td>
</tbody>
</table>
</form>

<div style="float: right;">
    <form action="<?php echo base_url('admincp/admonline/export_excel'); ?>" method="get">
        <select name="idcat" style="display: ;">
            <option value="0">-- Chọn phương thức --</option>
            <?php
            $idcat = $this->input->get('idcat', TRUE);
            $status = $this->input->get('status', TRUE);
            $catelog = $this->pagehtml_model->get_catadmonline(array('ticlock'=>0,'parentid'=>0),1000);
            if(!empty($catelog)){
                foreach($catelog as $item){
                    $sub = $this->pagehtml_model->get_catadmonline(array('ticlock'=>0,'parentid'=>$item['Id']),100);
            ?>
            <option value="<?php echo $item['Id'] ?>" <?php if($idcat==$item['Id']) echo "selected"; ?>><?php echo $item['title_vn'] ?></option>
            <?php if(!empty($sub)){
                foreach($sub as $row){ ?>
                <option value="<?php echo $row['Id'] ?>" <?php if($idcat==$row['Id']) echo "selected"; ?>> <?php echo $row['title_vn'] ?></option>
            <?php }}}} ?>
        </select>
        <select id="style" name="status" name="idcat" style="display: ;">
            <option value="0">-- Chọn tình trạng --</option>
            <option value="4" <?php if ($status == '4') {echo "selected";} ?>>Hồ sơ mới</option>
            <option value="1" <?php if ($status == '1') {echo "selected";} ?>>Nhận hồ sơ, Đang xử lý</option>
            <option value="2" <?php if ($status == '2') {echo "selected";} ?>>Đậu</option>
            <option value="3" <?php if ($status == '3') {echo "selected";} ?>>Không đủ điều kiện</option>
        </select>
        <button type="submit" class="button" style="float: right;">Export danh sách</button>
    </form>
</div>

</div>
<form action = '<?php echo base_url('admincp/admonline/delete');  ?>' method = 'post'  name="rowsForm" id="rowsForm">
<table class="view">
	<tr>
		<th><input type="checkbox" name="Check_ctr" id = 'Check_ctr' value="yes" onClick="Check(document.rowsForm.check_list)"></th>
		<th>ID</th>
        <th>Họ tên</th>
        <th>Phương thức</th>
        <th>Email</th>
        <th>Mã đăng ký</th>
        <th>Tình trạng</th>
        <th>Ngày</th>
		<th width="130" colspan="2">Hành động</th> 
	</tr>
    <?php
	if(empty($info)){
	?>
    <tr>
		<td colspan = '15' class = 'emptydata'>Không có dữ liệu</td>
	</tr>
    <?php }else{
		 foreach ($info as $item) { 
			$imgdel = ADMIN_PATH_IMG."b_drop.png";
			$imgedit = ADMIN_PATH_IMG."b_edit.png";
			$imgnondefault = ADMIN_PATH_IMG."icon-16-nondefault.png";
			$imgdefault = ADMIN_PATH_IMG."icon-16-default.png";
			$imgremove = ADMIN_PATH_IMG."icon-16-remove.png";
			$imgtick = ADMIN_PATH_IMG."icon-16-tick.png";
			$urledit = base_url('admincp/admonline/edit/'.$item['Id']);
			$urldel = base_url('admincp/admonline/delete/'.$item['Id']); 
	?>
      <tr <?php if($item['view']==0) echo 'style="font-weight:bold"'; ?> >
			<td align = 'center'><input type="checkbox" id = 'check_list' name="check_list[]" value="<?php echo $item['Id'];?>"><br></td>
			<td><?php echo $item['Id']; ?></td>
			<td><a href="<?php echo $urledit; ?>"><?php echo $item['fullname']; ?></a></td>
           	<td><?php if ($item['idcat'] == 24) { echo "Xét học bạ";}elseif ($item['idcat'] == 25) {echo "Xét điểm THPT";}else{echo "";} ?></td>
            <td><?php echo $item['email']; ?></td>
            <td><?php echo $item['code_reg']; ?></td>
            <td>
                <?php if ($item['status'] == '4') {echo "Hồ sơ mới";} ?>
                <?php if ($item['status'] == '1') {echo "Đang xử lý";} ?>
                <?php if ($item['status'] == '2') {echo "Đạt";} ?>
                <?php if ($item['status'] == '3') {echo "Không đủ điều kiên";} ?>
            </td>
            <td align = 'center'><?php echo date("d-m-Y",$item['date']); ?></td>	
            <td align = 'center'  width="30">
            <a href = '<?php echo $urledit; ?>' title = 'Sửa' ><img src = '<?php echo $imgedit; ?>'></a>
            </td>
            <td align = 'center'  width="30">
            <a href = '<?php echo $urldel; ?>' title = 'Xóa' onclick = 'javascript:return thongbao("Bạn có chắc xóa không");'><img src = '<?php echo ADMIN_PATH_IMG;?>b_drop.png'></a>
            </td>
		</tr>
    <?php }} ?>
</table>
<div class="frm-paging">
<span class="total">Tổng số: <b><?php echo $total; ?></b> </span>
<?php 
if(!empty($page)) echo $page;
?>
</div>
<div class="list_button">
<a href = 'javascript:CheckAll(document.rowsForm.check_list)'  class="button">Check all</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href = 'javascript:UnCheckAll(document.rowsForm.check_list)'  class="button">Uncheck all</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="javascript:confirmDelete('Bạn thật sự muốn xóa',document.rowsForm.check_list)"  class="button">Delete</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="javascript:confirmSave('Bạn có chắc muốn lưu lại','<?php echo  base_url('admincp/admonline/save');?>')" class="button">Save</a>
</div>
</form>
</div>
<script type="text/javascript">
function sortOrder(str,val){
	document.frmSort.sortitem.value=str;
	document.frmSort.sorvalue.value=val;
	document.frmSort.submit();
}
</script>
<form name="frmSort" action="" method="post" >
<input type="hidden" name="sortitem" value=""  />
<input type="hidden" name="sorvalue" value=""  />
</form>