<div class="main_area">
    <div class="breakcrumb">
    <table border="0">
    <tbody>
    <tr>
    <td width="25">
    <i class="fa icon-23 fa-windows"></i>
    </td>
    <td> Nội dung / Thông tin học viên / <?php echo $map_title ?></td>
    </tr>
    </tbody>
    </table>
    </div>
</div>
<div class="content">
<div class="content_i">
<form method="post" name="add-new" action="" enctype="multipart/form-data">
<table>

<tr>
    <td class = 'title_td' >Cập nhật trình trạng hồ sơ</td>
    <td>
        <select id="style" name="status" >
            <option value="4" <?php if ($info['status'] == '4') {echo "selected";} ?>>Hồ sơ mới</option>
            <option value="1" <?php if ($info['status'] == '1') {echo "selected";} ?>>Nhận hồ sơ, Đang xử lý</option>
            <option value="2" <?php if ($info['status'] == '2') {echo "selected";} ?>>Đạt</option>
            <option value="3" <?php if ($info['status'] == '3') {echo "selected";} ?>>Không đủ điều kiện</option>
        </select>  
         <?php echo form_error('status'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Mã đăng ký</td>
    <td> <input type="text" name="code_reg" value="<?php echo set_value('code_reg',$info['code_reg']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('code_reg'); ?>
     </td>
</tr>

<tr>
	<td class = 'title_td' >Họ tên</td>
    <td> <input type="text" name="fullname" value="<?php echo set_value('fullname',$info['fullname']);  ?>"  style="width:400px" readonly="">
   		 <?php echo form_error('title_vn'); ?>
     </td>
</tr>
<tr>
	<td class = 'title_td' >Giới tính</td>
    <td> <input type="text" name="gender" value="<?php if($info['gender'] == 0) echo 'Nam'; else if($info['gender'] == 1) echo 'Nữ'; else echo "Khác"?>"  style="width:400px" readonly="">
   		 <?php echo form_error('gender'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Email</td>
    <td> <input type="text" name="email" value="<?php echo set_value('email',$info['email']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('email'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điện thoại</td>
    <td> <input type="text" name="phone" value="<?php echo set_value('phone',$info['phone']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('phone'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Năm tốt nghiệp</td>
    <td> <input type="text" name="graduation_year" value="<?php echo set_value('graduation_year',$info['graduation_year']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('graduation_year'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Địa chỉ</td>
    <td> <input type="text" name="city" value="<?php echo set_value('city',$info['city']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('city'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Zalo</td>
    <td> <input type="text" name="zalo" value="<?php echo set_value('zalo',$info['zalo']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('zalo'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Ngành đăng ký</td>
    <?php 
        $nganh = $this->pagehtml_model->get_provinces(array('ticlock'=>0,'parentid'=>0),1000);
        foreach($nganh as $item){
            if($info['idprovin'] == $item['Id']) $nganhdk = $item['title_vn'];
        }
    ?>
    <td> <input type="text" name="idprovin" value="<?php echo $nganhdk;  ?>"  style="width:400px" readonly="">
         <?php echo form_error('idprovin'); ?>
     </td>
</tr>
<?php if ($info['idcat'] == '24') { ?>
<tr>
    <td class = 'title_td' >Tổ hợp xét tuyển</td>
    <?php 
        $tohop = $this->pagehtml_model->get_tohop(array('ticlock'=>0,'parentid'=>0),1000);
        foreach($tohop as $item){
            if($info['tohop'] == $item['Id']) $tohopdk = $item['title_vn'].' - '.$item['content_vn'];
        }
    ?>
    <td> <input type="text" name="tohop" value="<?php echo $tohopdk;  ?>"  style="width:400px" readonly="">
         <?php echo form_error('tohop'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điểm môn 1</td>
    <td> <input type="text" name="score1" value="<?php echo set_value('score1',$info['score1']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('score1'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điểm môn 2</td>
    <td> <input type="text" name="score2" value="<?php echo set_value('score2',$info['score2']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('score2'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điểm môn 3</td>
    <td> <input type="text" name="score3" value="<?php echo set_value('score3',$info['score3']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('score3'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điểm tổng</td>
    <td> <input type="text" name="score_sum" value="<?php echo set_value('score_sum',$info['score_sum']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('score_sum'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Điểm trung bình lớp 12 </td>
    <td> <input type="text" name="dtb12" value="<?php echo set_value('dtb12',$info['dtb12']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('dtb12'); ?>
     </td>
</tr>
<?php } ?>
<?php if ($info['idcat'] == '25') { ?>
<tr>
    <td class = 'title_td' >Số báo danh </td>
    <td> <input type="text" name="sbd" value="<?php echo set_value('sbd',$info['sbd']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('sbd'); ?>
     </td>
</tr>
<tr>
    <td class = 'title_td' >Mã vạch kết quả thi </td>
    <td> <input type="text" name="barcode" value="<?php echo set_value('barcode',$info['barcode']);  ?>"  style="width:400px" readonly="">
         <?php echo form_error('barcode'); ?>
     </td>
</tr>
<?php } ?>
<tr>
	<td class = 'title_td' >Ngày đăng ký</td>
    <td> <input type="text" name="email" value="<?php echo date("d-m-Y H:i:s",$info['date']);  ?>"  style="width:400px" readonly="">
   		 <?php echo form_error('title_vn'); ?>
     </td>
</tr>			
	
<tr>
<td  ></td>
    <th align = 'left'>
       <button type = 'submit' name="save" value="save"  class="button" >Cập nhật</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <input type = 'reset' value = 'Làm lại' class="button">
    </th>
</tr>	
</table>
</form>
</div>
</div>  
 