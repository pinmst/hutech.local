<style>
body{text-align:left}
ol.vertical {
  margin: 0 0 9px 0;
  text-align:left;
  min-height: 10px; }
  /* line 13, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
  ol.vertical li {
    display: block;
    margin: 5px;
    padding: 5px;
    border: 1px solid #cccccc;
    color: #0088cc;
    background: #eeeeee; }
  /* line 20, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
  ol.vertical li.placeholder {
    position: relative;
    margin: 0;
    padding: 0;
    border: none; }
    /* line 25, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder:before {
      position: absolute;
      content: "";
      width: 0;
      height: 0;
      margin-top: -5px;
      left: -5px;
      top: -4px;
      border: 5px solid transparent;
      border-left-color: red;
      border-right: none; }
	  .category-list,
	  .category-list ul{
		  list-style:none
	  }
	  .category-list li label{
		  padding:5px;
		  display: block;
		  margin-bottom:5px;
		  color:#0088cc;
		  background: #eee;
		  border:1px solid #cccccc
	  }
	  .category-list li label input{float:left}
</style>
<div class="main_area">
    <div class="breakcrumb">
    <table border="0">
    <tbody>
    <tr>
    <td width="25">
    	<i class="fa icon-23 fa-windows"></i>
    </td>
    <td> Hệ thống / Menu Ngang  </td>
    </tr>
    </tbody>
    </table>
    </div>
</div>
<div class="content">
<div class="list_button">
</div>

<form method="post" name="add-new" action="admincp/menu/add" enctype="multipart/form-data">
<input type="text" name="title_vn" value="<?php echo set_value('title_vn','Media') ?>"  style="width:400px">
<input type="text" name="link" value="<?php echo set_value('link','media') ?>"  style="width:400px"> 
<button type = 'submit' name="save" value="save"  class="button" >Thêm mới</button> &nbsp;&nbsp;&nbsp;&nbsp;
</form>

	<table style="width:100%;">
		<tr>
			<td style="width:300px;vertical-align:top">
			<h3>Danh mục</h3>
			<ul class="category-list">
				<?php
				$media = $this->catmedia_model->get_metoru();
				echo "<p>Danh mục media</p>";
				?>
				<form method="post" name="add-new" action="admincp/menu/add" enctype="multipart/form-data">
					<li>
						<input type="text" name="title_vn" value="Media"  style="display: block;">
						<input type="text" name="link" value="media"  style="display: block;">
						<input type="text" name="parentid" value="0"  style="display: none;">
						<input type="text" name="ticlock" value="0"  style="display: none;">
						<input type="text" name="sort" value="0"  style="display: none;">
						<label for="media">Media</label>
						<button class="button" submit' name="save" value="save"  style="float:right">Thêm</button>	
					</li>
				</form>

				<?php foreach($media as $level1) { ?>
					<form method="post" name="add-new" action="admincp/menu/add" enctype="multipart/form-data">
						<li>
							<input type="text" name="title_vn" value="<?=$level1['title_vn']?>"  style="display: block;">
							<input type="text" name="link" value="media/<?=$level1['alias']?>"  style="display: block;">
							<input type="text" name="parentid" value="0"  style="display: none;">
							<input type="text" name="ticlock" value="0"  style="display: none;">
							<input type="text" name="sort" value="0"  style="display: none;">
							<label for="media"><?=$level1['title_vn']?></label>
							<!-- <button class="button" submit' name="save" value="save"  style="float:right">Thêm vào menu</button>	 -->
							<!-- <label for="media<?=$level1['Id']?>">
								<input id="media<?=$level1['Id']?>" type="checkbox" name="media[]" value="<?=$level1['Id']?>"><?=$level1['title_vn']?>
							</label> -->
							<?php
							$level2_media = $this->catmedia_model->get_list(array('parentid' => $level1['Id']));
							?>
							<ul>
								<?php foreach($level2_media as $level2) {?>
								<!-- <li><label for="media<?=$level2['Id']?>"><input id="media<?=$level2['Id']?>" type="checkbox" name="media[]" value="<?=$level2['Id']?>"><?=$level2['title_vn']?></label>
								</li> -->
								<?php } ?>
							</ul>
						</li>
						<button class="button" submit' name="save" value="save"  style="float:right">Thêm</button>	
					</form>
				<?php } ?>
			</ul>
			
			<br>

			<ul class="category-list">
				<?php
				$cooperation = $this->catcooperation_model->get_metoru();
				echo "<hr><p>Danh mục hợp tác doanh nghiệp</p>";
				foreach($cooperation as $level1) { ?>
					<li>
						<label for="cooperation<?=$level1['Id']?>">
							<input id="cooperation<?=$level1['Id']?>" type="checkbox" name="cooperation[]" value="<?=$level1['Id']?>"><?=$level1['title_vn']?>
						</label>
						<?php
						$level2_cooperation = $this->catcooperation_model->get_list(array('parentid' => $level1['Id']));
						?>
						<ul>
							<?php foreach($level2_cooperation as $level2) {?>
							<li><label for="cooperation<?=$level2['Id']?>"><input id="cooperation<?=$level2['Id']?>" type="checkbox" name="cooperation[]" value="<?=$level2['Id']?>"><?=$level2['title_vn']?></label>
							</li>
							<?php } ?>
						</ul>
					</li>
				<?php } ?>
			</ul>
			<button class="button" type="submit" style="float:right">Thêm vào menu</button>
			<br>
			<ul class="category-list">
				<?php
				$activities = $this->catactivities_model->get_metoru();
				echo "<hr><p>Danh mục hoạt động Khoa, Trung tâm</p>";
				foreach($activities as $level1) { ?>
					<li>
						<label for="activities<?=$level1['Id']?>">
							<input id="activities<?=$level1['Id']?>" type="checkbox" name="activities[]" value="<?=$level1['Id']?>"><?=$level1['title_vn']?>
						</label>
						<?php
						$level2_activities = $this->catactivities_model->get_list(array('parentid' => $level1['Id']));
						?>
						<ul>
							<?php foreach($level2_activities as $level2) {?>
							<li><label for="activities<?=$level2['Id']?>"><input id="activities<?=$level2['Id']?>" type="checkbox" name="activities[]" value="<?=$level2['Id']?>"><?=$level2['title_vn']?></label>
							</li>
							<?php } ?>
						</ul>
					</li>
				<?php } ?>
			</ul>
			<button class="button" type="submit" style="float:right">Thêm vào menu</button>
			</td>
			<td>

		<div class='span4'>
		<?php
		$menus = $this->menu_model->get_list(array('parentid' => 0));
		?>
              <ol class='serialization vertical'>
			  <?php foreach($menus as $level1) { ?>
                <li data-id='<?=$level1['Id']?>' data-name='<?=$level1['title_vn']?>'>
                	
                	<!-- loock -->
                	<?php $urldel = base_url('admincp/menu/delete/'.$level1['Id']); ?>
           			<a href = '<?php echo $urldel;?>' style="float:right">del</a>
		   
                  <?=$level1['title_vn']?>
				  <?php
					$menus_level2 = $this->menu_model->get_list(array('parentid' => $level1['Id']));
					?>
					<ol>
						<?php foreach($menus_level2 as $level2) { ?>
						<li data-id='<?=$level2['Id']?>' data-name='<?=$level2['title_vn']?>'><?=$level2['title_vn']?>
						
						<?php $urldel = base_url('admincp/menu/delete/'.$level1['Id']); ?>
           				<a href = '<?php echo $urldel;?>' style="float:right">del</a>
						
						</li>
						<?php } ?>
					</ol>
                </li>
			  <?php } ?>
                
              </ol>
			  <a href="javascript:;" id="btnSave" class="button" style="float:right">Save</a>
            </div>
			</td>
		</tr>
	</table>
	<pre id="serialize_output2"></pre>
<div class="list_button">

</div>
</div>
<script src="<?=base_url('public/template/admin/js/jquery-sortable.js')?>"></script>
<script>
var group = $("ol.serialization").sortable({
    group: 'serialization',
    delay: 500,
    onDrop: function ($item, container, _super) {
      var data = group.sortable("serialize").get();
      _super($item, container);
    }
  });
  $(function(){
	  $('#btnSave').on('click', function(){
		  var data = group.sortable("serialize").get();
		  var jsonString = JSON.stringify(data, null, ' ');
		  $('#serialize_output2').text(jsonString);
	  })
  })
</script>

