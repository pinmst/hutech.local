<!-- <div class="col-sm-3 nopadding pull-right">
    <div class="block-content nopadding">    
        <div class="header-block">
            <img class="icon-he-thong-dao-tao" src="template/images/img_blank.gif">
            <span class="title-block text-blue">Lịch sự kiện</span>
        </div>
        <div class="content-block he-thong-dao-tao">
            <?php foreach($event as $item){ ?>
                <div class="col-xs-12 nopadding event">
                    <div class="col-xs-4 nopadding event-left">
                        <?php
                            $scheduled_day = $item['day_open'];
                            $days = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4','Thứ 5','Thứ 6', 'Thứ 7'];
                            $day = date('w',strtotime($scheduled_day));
                            $scheduled_day = $days[$day];
                        ?>
                        <div class="w-full back-orange text-white text-bold"><?php echo $scheduled_day; ?></div>
                        <div class="w-full"><?php echo $item['day_open'] ?></div>
                    </div>
                    <div class="col-xs-8 no-padding-right-2 event-right">
                        <div class="text-justify">
                            <a href="<?php echo base_url('event/'.$item['alias']) ?>">
                                <?php echo $item['title_vn'] ?>
                            </a>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
</div> -->

<div class="block-nopadding wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
    <div class="content noidung back-white border-radius">
        <div class="header-block" style="text-align: center;display: block;padding: 10px 0">
            <div class="title-line-right title-line-orange">
                <a href="<?php echo base_url('event'); ?>" class="title-block text-blue">SỰ KIỆN HOẠT ĐỘNG MEDIA</a>
            </div>
        </div>
        <div class="content-block">
            <div class="col-sm-12 nopadding wow slideInRight animated animated" style="visibility: visible; animation-name: slideInRight;">
                <?php foreach($event as $item){ ?>
                    <div class="col-xs-6 col-md-3 col-sm-3 thumb-video" style="height: unset;">
                        <a href="<?php echo base_url('event/'.$item['alias']) ?>" class="text-bold">
                            <div class="img">
                                <img class="img-responsive thumb" src="<?php echo PATH_IMG_BANNER.$item['images'] ?>">
                                <!-- <img data-brackets-id="18211" class="icon-media-play" src="template/images/img_blank.gif"> -->
                            </div>
                            <?php echo $item['title_vn']; ?>
                        </a>
                        <div class="col-xs-12 nopadding event">
                        <div class="col-xs-4 nopadding event-left">
                            <?php
                                $scheduled_day = $item['day_open'];
                                $days = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4','Thứ 5','Thứ 6', 'Thứ 7'];
                                $day = date('w',strtotime($scheduled_day));
                                $scheduled_day = $days[$day];
                            ?>
                            <div class="w-full back-blue text-white text-bold"><?php echo $scheduled_day; ?></div>
                            <div class="w-full"><?php echo $item['day_open'] ?></div>
                        </div>
                        <div class="col-xs-8 no-padding-right-2 event-right" style="border-bottom:unset;">
                            <div class="text-orange">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <?php echo $item['location'] ?>
                            </div>
                        </div>
                    </div>
                    </div>

                <?php } ?>
                </div>
        </div>
    </div>
</div>