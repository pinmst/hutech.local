<div class="main col1-layout container">
  <!-- ESI START DUMMY COMMENT [] -->
  <!-- ESI URL DUMMY COMMENT -->

  <!-- ESI END DUMMY COMMENT [] -->
  <div id="main" class="col-main row">
      <div class="breadcrumbs">
          <ul itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li class="home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                  <a itemprop="item" href="" title="Go to Home Page">Home</a>
                  <meta itemprop="name" content="Home" />
                  <meta itemprop="position" content="1" />
                  <span></span>
              </li>
              <li class="product" itemprop="itemListElement" itemscope itemty<?php echo $prod[0]['title_vn'] ?>pe="http://schema.org/ListItem">
                  <strong><?php echo $prod[0]['title_vn'] ?></strong>
                  <meta itemprop="name" content="<?php echo $prod[0]['title_vn'] ?>" />
                  <meta itemprop="position" content="2" />
              </li>
          </ul>
      </div>
      <script type="text/javascript">
          var optionsPrice = new Product.OptionsPrice([]);
      </script>
      <div id="messages_product_view">
          <!-- ESI START DUMMY COMMENT [] -->
          <!-- ESI URL DUMMY COMMENT -->

          <!-- ESI END DUMMY COMMENT [] -->
      </div>
      <div class="product-view" itemscope itemtype="http://schema.org/Product">
          <div class="row">
              <div class="product-view-left col-lg-9">
            <!-- <form  action="<?php echo base_url('addcart_fromdetail'); ?>" method="post" id="product_addtocart_form"> -->
              <form action="<?php echo base_url('addcart_fromdetail'); ?>" method="post" class="cart" id="" enctype='multipart/form-data'/>
                <input name="form_key" type="hidden" value="mnXHFyRvKORnlU3c" />
                <div class="no-display">
                    <input type="hidden" name="product" value="5468" />
                    <input type="hidden" name="related_product" id="related-products-field" value="" />
                    <span itemprop="url"></span>
                    <span itemprop="manufacturer"></span>
                </div>
                <div class="product-essential">
                    <div class="row">
                        <div class="product-img-box col-sm-5 hidden-xs">
                            <p class="product-image product-image-zoom">
                                    <img itemprop="image" id="image" src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" title="<?php echo $prod[0]['title_vn'] ?>" /> 
                            </p>
                          <div  id="slider3">
                            <ul>
                              <?php if($prod[0]['images']!="") { ?>
                              <li>
                              <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" class="trueimages" > 
                                <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                              </a>
                              </li>
                              <?php }else {
                              echo "";
                              } ?>
                              <?php  if($prod[0]['images1']!="") { ?>
                              <li>
                              <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images1'] ?>" class="trueimages" > 
                                <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images1'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                              </a>
                              </li>
                              <?php }else {
                              echo "";
                              } ?>
                              <?php  if($prod[0]['images2']!="") { ?>
                              <li>
                              <a href="<?php echo PATH_IMG_PRODUCT.$prod[0]['images2'] ?>" class="trueimages" > 
                                <img src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images2'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" style="max-width: 80px;">
                              </a>
                              </li>
                              <?php }else {
                              echo "";
                              } ?>

                            </ul>
                          </div>
                            <div class="product-share-icons">
                                <span>Share:</span>&nbsp;
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                    <a class="addthis_button_facebook"></a>
                                    <a class="addthis_button_twitter"></a>
                                    <a class="addthis_button_pinterest_share"></a>
                                    <a class="addthis_button_google_plusone" g:plusone:count="false"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <!-- AddThis Button END -->
                            </div>
                        </div>

                        <div class="product-shop col-xs-12 col-sm-7">
                          
                            <div class="product-main-info">
                                <div class="hidden-lg hidden-md hidden-sm">
                                    <div class="product-img-box">
<p class="product-image">
                                            <a href="javascript: void(0)" title="<?php echo $prod[0]['title_vn'] ?>" class="abs show-on-big" id="main-image" onclick="showStaticPopup('large_img', 'popupContentImage');">
                                                <img id="image" class="lazy" data-src="<?php echo PATH_IMG_PRODUCT.$prod[0]['images'] ?>" alt="<?php echo $prod[0]['title_vn'] ?>" title="<?php echo $prod[0]['title_vn'] ?>" /> </a>
                                        </p>                                                
                                    </div>
                                    <div class="product-share-icons">
                                        <span>Share:</span>&nbsp;
                                        <!-- AddThis Button BEGIN -->
                                        <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                            <a class="addthis_button_facebook"></a>
                                            <a class="addthis_button_twitter"></a>
                                            <a class="addthis_button_pinterest_share"></a>
                                            <a class="addthis_button_google_plusone" g:plusone:count="false"></a>
                                            <a class="addthis_button_compact"></a>
                                            <a class="addthis_counter addthis_bubble_style"></a>
                                        </div>
                                        <!-- AddThis Button END -->
                                    </div>
                                </div>

                                <div class="product-type-data">
                                  <div class="product-type-data-inner">
                                      <h1 itemprop="name" id="product_general" class="product-name">
                                        <span class="brand-name"></span>
                                          <strong class="display-name"><?php echo $prod[0]['title_vn'] ?></strong></h1>
                                        
                                        <p class="product-ids"></p>

                                        <div>
                                            <ul class="grouped-items-list">
                                                <li class="item productspricelist" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
                                                    <div class="row">
                                                        <div class="data-size col-xs-12 col-md-3"></div>

                                                        <div class="data-price col-xs-12 col-md-5">

                                                            <div class="price-box">
                                                                <p class="special-price">
                                                      <span class="price" >
                                                          <span class="price-numbers">
                                                            <span class="price"><?php echo $this->page->bsVndDot($prod[0]['sale_price']) ?> <sub>(đ)</sub></span>
                                                          </span>
                                                        </span>
                                                      <?php if($prod[0]['price']){ ?>
                                                      <p class="price">Giá gốc:
                                                        <span class="old-price"><?php echo $this->page->bsVndDot($prod[0]['price'])."đ" ?></span> 
                                                        <span class="reduced-to">-
                                                        <?php
                                                          if($prod[0]['price']>0){
                                                          if($prod[0]['price']>0) 
                                                          $pt = 100-floor(($prod[0]['sale_price']/ $prod[0]['price'])*100); else  $pt=0;
                                                        ?>
                                                        <i>
                                                        <?php  echo $pt?>%</i>
                                                        <?php } ?>
                                                        </span> </p>
                                                      <?php } ?>
                                                      <?php
                                                      if($prod[0]['price']>0){
                                                      if($prod[0]['price']>0) 
                                                      $pt = 100-floor(($prod[0]['sale_price']/ $prod[0]['price'])*100); else  $pt=0;
                                                      ?>

                                                      <?php } ?>
                              
                                                                    <!--<span class="price-label">-->
                                                                    <!--</span>-->
                                                                    
                                                                </p>
                                                            </div>
                                                        </div>
                              <input type="hidden" name="add-to-cart" value="<?php echo  $prod[0]['Id']; ?>" />
                              <input type="hidden" value="<?php echo $prod[0]['Id'] ?>" name="idpro" />
                              <input type="hidden" value="<?php echo $prod[0]['sale_price'] ?>" name="shop_price" id="sale_price" />
                              <input type="hidden" name="name_pro" value="<?php echo $prod[0]['title_vn']; ?>" />
                              <input type="hidden" name="alias" value="<?php echo $prod[0]['alias']; ?>" />
                            </form>
                                <div class="data-qty col-xs-12 col-md-4">
                                    <link itemprop="availability" href="http://schema.org/InStock" />
                                    <div class="qty-counter">
                                        <i title="" class="icon-thin-arrow-right plus fa fa-plus "></i>
                                          <input type="number" pattern="[0-9\.]*" name="quanty" data-price="<?php echo $prod[0]['sale_price'] ?>"  class="input-text qty" value="1" maxlength="12" id="qty">
                                        <i title="Decrease Quantity" class="icon-thin-arrow-left minus fa fa-minus"></i>
                           
                                    </div>
                                </div>
                            </div>
                                                      </li>
                                                  </ul>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="mobile-highlights">
                          <div class="product-collateral">
                              <div class="product-tab-area">

                                  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#1" data-toggle="tab"><p>Thông tin sản phẩm</p></a></li>
                                    <li><a href="#2" data-toggle="tab"><p>Thành phần chính</p></a></li>
                                    <li><a href="#2" data-toggle="tab"><p>Hướng dẫn sử dụng</p></a></li>
                                    <li><a href="#2" data-toggle="tab"><p>Thông số sản phẩm</p></a></li>
                                    <li><a href="#2" data-toggle="tab"><p>Đánh giá</p></a></li>
                                    <li><a href="#2" data-toggle="tab"><p>Hỏi đáp</p></a></li>
                                  </ul>
                                  <div class="product-tabs-container" style="color: black">
                                      <div class="row">
                                        <div class="tab-content ">
                                          <div class="tab-pane active" id="1">
                                            11111111111111
                                            <?php echo stripcslashes($prod[0]['content_vn']);?>
                                          </div>
                                            <div class="tab-pane" id="2">
                                              <div id="review_form_wrapper">
                                                2222222222222
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>

                              <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                              <script type="text/javascript">
                                  //<![CDATA[
                                  function doTabs() {
                                      var productTabs = new Varien.Tabs('.nav-tabs');
                                      //    $('product-review-link').observe('click', function (e) { productTabs.switchTab($('nav-tabs'), $('reviews')); });
                                  }
                                  // do not run this when in print mode
                                  if (!isPrintMode()) {
                                      doTabs();
                                  }
                                  //]]>
                              </script>
                          </div>
                          <div id="sli_recommender_pdp" class="box-collateral box-up-sell col-sm-12" data-sku="LSAP3-G" data-variant="currency:(đ)"></div>
                          <div class="row hidden-lg high_confidence">
                              <div class="product-highlights col-xs-12 col-sm-6 col-md-6">
                                  <div class="add-to-box">
                                      <div class="add-to-cart">
                                          <div class="row actions">
                                              <button type="button" title="Add to Cart" class="button btn-cart ajaxcartview" ><span><span>Mua Hàng</span></span>
                                              </button>
                                              <div class="rewards-product-view-points">

                                                  <script type="text/javascript">
                                                      var elems = $$("table#super-product-table tbody tr td input.qty");
                                                      elems.each(function(item) {
                                                          Event.observe(item, 'change', function() {
                                                              var param = {};
                                                              elems.each(function(i) {
                                                                  key = i.name.slice(12, -1);
                                                                  param[key] = parseInt(i.value);
                                                              });
                                                              param["product_id"] = 5468;
                                                              new Ajax.Updater("points-total", 'rewards/product_view_distribution/updatePoints/', {
                                                                  method: 'post',
                                                                  parameters: param
                                                              }, {
                                                                  asynchronous: false
                                                              });
                                                          });
                                                      });
                                                  </script>
                                                  <script type="text/javascript">
                                                      document.observe("dom:loaded", function() {
                                                          if ($('bundleSummary') != undefined) {
                                                              if ($('bundleSummary').getElementsBySelector('.add-to-cart').first() != undefined) {
                                                                  $('bundleSummary').getElementsBySelector('.add-to-cart').first().insert({
                                                                      after: $('product-view-points')
                                                                  });
                                                              } else if ($('bundleSummary').getElementsBySelector('.price-box').first() != undefined) {
                                                                  $('bundleSummary').getElementsBySelector('.price-box').first().insert({
                                                                      after: $('product-view-points')
                                                                  });
                                                              }
                                                          }
                                                      });
                                                  </script>
                                              </div>
                                          </div>
                                      </div>
                                      <script type="text/javascript">
                                          $$('.product-essential .icon-thin-arrow-left').each(function(element) {
                                              $(element).observe('click', function() {
                                                  var quantityInputBox = this.previous();
                                                  if (parseInt(quantityInputBox.value) == 1) {
                                                      quantityInputBox.value = 0;
                                                  } else if (parseInt(quantityInputBox.value) > 1) {
                                                      quantityInputBox.value = parseInt(quantityInputBox.value) - 1;
                                                  }
                                              });
                                          });
                                          $$('.product-essential .icon-thin-arrow-right').each(function(element) {
                                              $(element).observe('click', function() {
                                                  var quantityInputBox = this.next();
                                                  if (parseInt(quantityInputBox.value) < 100) {
                                                      quantityInputBox.value = parseInt(quantityInputBox.value) + 1;
                                                  }
                                              });
                                          });
                                      </script>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>

              <div class="col-lg-3 col-xs-12">
                    <div class="product-highlights col-lg-12">
                        <div class="add-to-box">
                              <button type="button" title="Add to Cart" class="button btn-cart ajaxcartview" style="width: unset;height: unset;"><span>Thêm Vào Giỏ Hàng</span>
                              </button>

                            <br>
                            <p style="color: #0c0c54;font-size: 18px;font-weight: bold;margin-top: 30px;"><a href="tel:<?php echo $web['hotline'] ?>" data-track="true" data-category="Footer" data-action="Get in touch" data-label="Call <?php echo $web['hotline'] ?>"><?php echo $web['hotline'] ?></a></p>
                            <p style="color: red;font-size: 15px;font-weight: bold;">Gọi đặt hàng nhanh | Ship COD</p>
                              <script type="text/javascript">
                                  $$('.product-essential .icon-thin-arrow-left').each(function(element) {
                                      $(element).observe('click', function() {
                                          var quantityInputBox = this.previous();
                                          if (parseInt(quantityInputBox.value) == 1) {
                                              quantityInputBox.value = 0;
                                          } else if (parseInt(quantityInputBox.value) > 1) {
                                              quantityInputBox.value = parseInt(quantityInputBox.value) - 1;
                                          }
                                      });
                                  });
                                  $$('.product-essential .icon-thin-arrow-right').each(function(element) {
                                      $(element).observe('click', function() {
                                          var quantityInputBox = this.next();
                                          if (parseInt(quantityInputBox.value) < 100) {
                                              quantityInputBox.value = parseInt(quantityInputBox.value) + 1;
                                          }
                                      });
                                  });
                              </script>
                          </div>
                    </div>

                    <hr style="border-color: #aaa;width: 100%;">
                    <div class="box-title dk-blue amcatlist_title col-xs-12" style="padding: unset;border: unset;">
                          <h2>Cùng Danh Mục</h2>
                      <div style="margin-top: 10px">
                      <ul>
                        <?php $i=0; ?>
                           <?php if(!empty($prod_cl)){ 
                              foreach($prod_cl as $item) { ?>
                                <?php if($item['alias'] === $prod[0]['alias']){ ?>
                                  <?php echo ""; ?>
                                <?php }else{ ?>
                                <li style="border: .1px solid #aaa;padding: 10px 2px;">
                                  <img src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" style="float: left;max-width: 80px">
                                  <?php
                                    if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                                        ?>
                                        <span class="price"style="color: #50352f;text-decoration: line-through;"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                                  <?php } ?>
                                  <br>
                                  <?php if ((int)$item['sale_price'] != 0 ) {?>
                                      <span class="price" id="" style="color: #ff5b35">
                                          <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                                      </span>
                                  <?php }else{ ?>
                                      <span class="price" id="">
                                          <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                                      </span>
                                  <?php } ?> 
                                  <p style="font-size: 12px;color: black;"><?php echo $item['title_vn'] ?></p>
                                </li> 
                            <?php $i++; ?>
                        <?php }}}else{ ?>
                            <div class="alert alert-danger">Sản phẩm đang update.</div>
                        <?php } ?>
                      </ul>
                    </div>
                      </div>
                  
              </div>
          </div>
      </div>
  </div>
</div>
<div class="container">
<h2 class="title_related">Sản phẩm liên quan</h2>

       <?php $i=0; ?>
       <?php if(!empty($prod_cl)){ 
          foreach($prod_cl as $item) { ?>
            <?php if($item['alias'] === $prod[0]['alias']){ ?>
              <?php echo ""; ?>
            <?php }else{ ?>
            <?php if ($i == 4) break; ?>
            <div class="col-sm-3 col-xs-6 " style="padding-top: 5px;"> 
              <div class="home-item home-item--product-grid products-grid__item only-grid only-grid-24">
                  <div class="product-grid__top">
                      <div class="product-images">
                          <a href="<?php echo base_url($item['alias'].'.html'); ?>" title="<?php echo $item['title_vn'] ?>" class="product-image" data-track="tru" data-label="<?php echo $item['title_vn'] ?>">
                              <div class="product-promos">
                                  <?php $price = (int)$item['price'];$sale_price = $item['sale_price'];
                              if ($price != 0 && $price != $sale_price) {
                                  $pt = 100-floor(($sale_price/ $price)*100) ?>
                                  <i style="margin-left: 5px"> -<?php  echo $pt?>%</i>
                              <?php }else{ echo "";} ?>
                              </div>
                              <img data-src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" width="100%" alt="" class="lazy" src="<?php echo PATH_IMG_PRODUCT.$item['images']."?v=".time();?>" data-loaded="true" style="max-height: 144px">
                          </a>
                      </div>
                      <div class="product-info" style="height: auto;">
                          <div class="product-brand">
                              <a href=""></a>
                          </div>
                          <div class="product-name" style="min-height: 44px">
                              <a href="" title="" class="product-title" data-track="true" data-category="Homepage We Recommend" data-action="<?php echo $item['title_vn'] ?>" data-label="<?php echo $item['title_vn'] ?>"><?php echo $item['title_vn'] ?></a>
                          </div>
                         <div class="color-black"></div>
                      </div>
                  </div>

                  <div class="product-grid__middle" style="max-height: 80px;">
                      <div class="product-grid__middle__left">
                     <div class="price-box">
                          <p class="minimal-price special-price">
                              <?php
                                if ((int)$item['sale_price'] != 0) {?>
                                  <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                      <?php echo bsVndDot($item['sale_price']); ?><sub>(đ)</sub>
                                  </span>
                              <?php }else{ ?>
                                  <span class="price" id="product-minimal-price-5468-widget-new-grid">
                                      <?php echo bsVndDot($item['price']); ?><sub>(đ)</sub>
                                  </span>
                              <?php } ?>                                                        
                          </p>
                          <p class="you-save">
                              <span class="price msrp linemiddle">
                                  <?php
                                    if ((int)$item['price'] != 0 && (int)$item['price'] != $item['sale_price']) {
                                        ?>
                                        <span class="price"><?php echo bsVndDot($item['price']); ?> <sub>(đ)</sub></span>
                                  <?php } ?>
                              </span>
                          </p>
                      </div>
                  </div>

                  <div class="product-grid__middle__right">
                      <div class="ratings" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                              
                      </div>
                  </div>
                  </div>
                  <div class="product-grid__bottom">
                      <div class="width-haft">
                          <button type="button" title="Add to Cart" class="button btn-cart ajaxcartpost" data-id="24">
                              <span style="font-size: 12px;">Mua Hàng</span></button>
                      </div>
                  </div>
              </div>
          </div>
        <?php $i++; ?>
    <?php }}}else{ ?>
        <div class="alert alert-danger">Sản phẩm đang update.</div>
    <?php } ?>
</div>
            <!-- ESI START DUMMY COMMENT [] -->
            <!-- ESI URL DUMMY COMMENT -->
     <script type="text/javascript" >(function($) {
     $('.trueimages').click(function(e){
            e.preventDefault();
            var fullimages = $(this).attr('href');
            $('.product-image-zoom').find('img').attr('src',fullimages);
    })
        
  })(jQuery);
    </script>


<style type="text/css">
  .title_related{
    width: 100%;
    float: left;
    padding: 8px 0;
    margin: 10px 0;
    background: #EEE;
    font-size: 16px;
    text-transform: uppercase;
    text-indent: 10px;
    text-align: left;
  }
  .row{
    margin-left: unset;
    margin-right: unset;
  }
  h2{
    padding-top: unset;
  }
  .product-tab-area .nav-tabs > li{
    margin: unset;
    margin-right: unset;
    margin-bottom: unset;
  }
  .product-tab-area .nav-tabs {
    border-bottom: .1px solid #4FA6BC;
  }
  .product-tab-area .nav-tabs > li p{
    color: #fff;
  }
  .product-tab-area .nav-tabs > li.active p {
    color: #4C4C4C;
  }
  
</style>