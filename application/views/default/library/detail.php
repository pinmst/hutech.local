<?php 
    $brea_alias1 = base_url();
    $brea_alias2 = base_url($menu['sub_alias'].'/'.$info[0]['alias']);
    $brea_title2 = $info[0]['title_vn'];
?>

<div class="col-sm-9 no-padding-left padding-right-5">
    <p class="breacrumb">
        <a href="<?php echo $brea_alias1 ?>">Trang chủ</a> >> <a href="<?php echo $brea_alias2; ?>"><?php echo $brea_title2; ?></a> >> <?php echo $info_detail['0']['title_vn']; ?>
    </p>
    <div class="detail-news">
        <div class="content-block">
            <div class="col-sm-12 no-padding-left padding-bottom">
                <span class="title-block text-blue nopadding"><?php echo $info_detail['0']['title_vn']; ?></span>
            </div>
            <div class="col-sm-12 nopadding news-info">
                <div class="col-sm-6 nopadding">
                    <div class="article-tools clearfix">
                        <dl style="width: auto;" class="article-info">
                            <dd class="create"><i class="fa fa-calendar" aria-hidden="true"></i> <span><?php echo date("d-m-Y",$info_detail['0']['date']) ?></span></dd>
                        </dl>
                    </div>
                </div>
            </div>
            
            <panel id="contentnews">
                <?php echo $info_detail[0]['content_vn'];?>
            </panel>
            <panel id="endNews"></panel>
            <hr>
            <div class="extranews_box">
                <h2><strong>Các tin khác</strong></h2>
                <div class="extranews_older">
                    <ul class="older">
                    <?php foreach ($info_cl as $item){ ?> 
                        <?php if ($info_detail['0']['Id'] === $item['Id']) { ?> 
                            <?php echo ""; ?>
                        <?php }else{ ?>
                            <li class="wow slideInUp animated animated" style="visibility: visible; animation-name: slideInUp;">
                                <a href="<?php echo base_url($menu['sub_alias'].'/'.$info['0']['alias'].'/'.$item['alias']) ?>"><?php echo $item['title_vn']; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
