
<div id="ja-container" class="wrap" style="text-align: center;">
        <div class="main clearfix">
            <div class="container">
                <form name="form" method="post" action="<?php echo current_url(); ?>" class="ng-invalid ng-invalid-required ng-dirty ng-valid-parse ng-valid-minlength">
                    <div class="container blockContent">
                        <div class="row">
                            <div class="content">
                                <div class="blockTitle">
                                    <span style="font-size: 20px;">KẾT QUẢ XÉT TUYỂN</span>
                                </div>
                                <div class="syllView">
                                    <div class="row">
                                        <div class="col-sm-3 textinput" id="divNganhXetTuyenHocBa">
                                            <div class="input-group input-group-custom validate-inputgroup">
                                                <span class="input-group-addon input-group-addon-custom">Hình thức xét tuyển</span>
                                                <input type="hidden" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 textinput">
                                            <select id="idcat" class="form-control ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched" name="idcat" required="">
                                                <option label="-Chọn ngành xét tuyển-" value="" selected="selected">-Chọn hình thức xét tuyển-</option>
                                                <option label="Xét tuyển học bạ" value="24" <?php if($data['idcat'] == '24') echo "selected" ?> >Xét tuyển học bạ</option>
                                                <option label="Xét tuyển điểm thi THPT" value="25" <?php if($data['idcat'] == '25') echo "selected" ?> >Xét tuyển điểm thi THPT</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 textinput">
                                            <div class="input-group input-group-custom validate-inputgroup">
                                                <span class="input-group-addon input-group-addon-custom">Thông tin tra cứu</span>
                                                <input type="hidden" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 textinput">
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="key" name="key" value="<?php if(!empty($data['key'])) echo $data['key'] ?>" type="text" placeholder="Mã đăng ký, Số điện thoại hoặc Email" valid-number="" required="" maxlength="50">
                                        </div>
                                    </div>
                                    <div class="block">
                                        <div class="text-center">

                                            <button type="submit" id="btnSaveThongTinNhapHocOnline" class="btn btn-info">
                                                Xem kết quả
                                            </button>
                                        </div>
                                    </div>
                                    <span id="content">
                                        
                                            <?php
                                            if(empty($data['info_search'])){
                                            ?>
                                                <p style="color: red;">Không tìm thất kết quả nào, vui lòng kiểm tra lại thông tin.</p>
                                            <?php }else{ ?>
                                                <?php foreach ($data['info_search'] as $item) {  ?>
                                                    <div class="">
                                                        <h2>Kết quả tra cứu</h2>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Họ tên</th>
                                                                    <th>Ngành đăng ký</th>
                                                                    <th>Ngày đăng ký</th>
                                                                    <th>Kết quả</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><?php echo $item['fullname']; ?></td>
                                                                    <?php 
                                                                        foreach($nganh as $item1){

                                                                            if($item['idprovin'] == $item1->Id){
                                                                                $nganhdk = $item1->title_vn;
                                                                            }
                                                                        }
                                                                    ?>
                                                                    <td><?php echo $nganhdk;  ?></td>
                                                                    <td><?php echo date("d/m/Y",$item['date']);?></td>
                                                                    <td>
                                                                        <?php if ($item['status'] == '4') {echo "Đang chờ xử lý";} ?>
                                                                        <?php if ($item['status'] == '1') {echo "Đã tiếp nhận";} ?>
                                                                        <?php if ($item['status'] == '2') {echo "Đạt";} ?>
                                                                        <?php if ($item['status'] == '3') {echo "Không đủ điều kiên";} ?>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    
                                            <?php }} ?>
                                        
                                    </span>
                                    <br>
                                    <p align="center">
                                        <i>Nếu không xem được, vui lòng liên hệ trực tiếp đến trường để được hỗ trợ</i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
