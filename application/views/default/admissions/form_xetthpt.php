<?php if ($this->session->flashdata('message_success')) { ?>
    <?php $message = $this->session->flashdata('message_success');
       echo '<div style="text-align: center;padding 20px 0"><a href="echo base_url()" class="button wc-forward">Trang chủ</a><i class="fa fa-home" aria-hidden="true"></i> '.$message.'</div>'; 
    ?>
<?php } ?>
<?php if ($this->session->flashdata('message_error')) { ?>
    <?php $message = $this->session->flashdata('message_error');
       echo '<div style="text-align: center;padding 20px 0">'.$message.'</div>'; 
    ?>
<?php } ?>

<form name="form" method="post" action="<?php echo current_url(); ?>" class="ng-invalid ng-invalid-required ng-dirty ng-valid-parse ng-valid-minlength">
    <div class="container blockContent">
        <div class="row">
            <div class="content">

                <div class="blockTitle">
                    <img class="img-responsive" src="template/LandingPageDangKy/image/nhaphoc2018/thong-tin-thi-sinh.png">
                </div>
                <div class="syllView">
                    <input id="idcat" name="idcat" type="hidden" value="25" >

                    <div class="row">
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup" >
                                <span class="input-group-addon "><label for="fullname">Họ và tên</label></span>
                                <input class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-minlength" id="fullname" required="" name="fullname" value="" type="text" placeholder="Nhập tên">
                            </div>
                        </div>
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom">
                                <span class="input-group-addon"><label for="gender">Giới tính</label></span>
                                <select id="gender" class="form-control   ng-pristine ng-untouched ng-valid ng-not-empty" name="gender">
                                    <option label="-Chọn-" value="" selected="selected">-Chọn-</option>
                                    <option label="Nam" value="0">Nam</option><option label="Nữ" value="1">Nữ</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup" >
                                <span class="input-group-addon"><label for="email">Email</label></span>
                                <input class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-minlength" required="" id="email" name="email" value="" type="email" placeholder="Nhập email">
                            </div>
                        </div>
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup has-error">
                                <span class="input-group-addon"><label for="phone">Điện thoại</label></span>
                                <input class="form-control ng-pristine ng-empty ng-invalid ng-invalid-required ng-valid-minlength ng-touched" required="" id="phone" name="phone" value="" type="text" placeholder="Nhập số điện thoại" maxlength="12" minlength="9">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom">
                                <span class="input-group-addon"><label for="DDL_NamTotNghiep">Năm tốt nghiệp</label></span>
                                <select class="form-control   ng-pristine ng-valid ng-not-empty ng-touched" name="graduation_year" id="graduation_year">
                                    <option label="-Chọn-" value="" selected="selected">-Chọn-</option>
                                    <option label="2000" value="2000">2000</option>
                                    <option label="2001" value="2001">2001</option>
                                    <option label="2002" value="2002">2002</option>
                                    <option label="2003" value="2003">2003</option>
                                    <option label="2004" value="2004">2004</option>
                                    <option label="2005" value="2005">2005</option>
                                    <option label="2006" value="2006">2006</option>
                                    <option label="2007" value="2007">2007</option>
                                    <option label="2008" value="2008">2008</option>
                                    <option label="2009" value="2009">2009</option>
                                    <option label="2010" value="2010">2010</option>
                                    <option label="2011" value="2011">2011</option>
                                    <option label="2012" value="2012">2012</option>
                                    <option label="2013" value="2013">2013</option>
                                    <option label="2014" value="2014">2014</option>
                                    <option label="2015" value="2015">2015</option>
                                    <option label="2016" value="2016">2016</option>
                                    <option label="2017" value="2017">2017</option>
                                    <option label="2018" value="2018">2018</option>
                                    <option label="2019" value="2019">2019</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup has-error">
                                <span class="input-group-addon"><label for="city">Tỉnh thành</label></span>
                                <input class="form-control ng-touched ng-dirty ng-valid-parse ng-invalid ng-empty ng-invalid-required ng-valid-minlength" required="" id="city" name="city" value="" type="text" placeholder="Nhập tỉnh thành">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom">
                                <span class="input-group-addon "><label for="facebook">Facebook</label></span>
                                <input class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-minlength" id="facebook" name="facebook" value="" type="text" placeholder="Nhập facebook">
                            </div>
                        </div>
                        <div class="col-sm-6 textinput">
                            <div class="input-group input-group-custom" >
                                <span class="input-group-addon "><label for="zalo">Zalo</label></span>
                                <input class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-minlength" id="zalo" name="zalo" value="" type="text" placeholder="Nhập zalo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="note">
                            <b class="text-danger">*Lưu ý:</b> Sinh viên bắt buộc điền đầy đủ những mục có dấu (<span class="text-danger">*</span>)
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="blockTitle">
                    <img class="img-responsive" src="template/LandingPageDangKy/image/nhaphoc2018/thong-tin-dang-ky.png">
                </div>
                <div class="syllView">
                    <div class="row">
                        <div class="col-sm-3 textinput" id="divNganhXetTuyenHocBa">
                            <div class="input-group input-group-custom validate-inputgroup">
                                <span class="input-group-addon input-group-addon-custom">Ngành xét tuyển:</span>
                                <input type="hidden" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 textinput">
                            <select id="idprovin" class="form-control ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched" name="idprovin" required="">
                                <option label="-Chọn ngành xét tuyển-" value="" selected="selected">-Chọn ngành xét tuyển-</option>

                                <?php if(!empty($nganh)){ ?>
                                    <?php foreach ($nganh as $item){ ?>
                                        <option label="<?php echo $item->title_vn; ?>" value="<?php echo $item->Id; ?>"><?php echo $item->title_vn; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 textinput" id="divChonToHopMon">
                            <div class="input-group input-group-custom validate-inputgroup">
                                <span class="input-group-addon input-group-addon-custom">Số báo danh</span>
                                <input type="hidden" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup">
                                <input class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="sbd" name="sbd" value="" type="text" placeholder="Nhập số báo danh" valid-number="" required="" maxlength="20">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 textinput" id="divChonToHopMon">
                            <div class="input-group input-group-custom validate-inputgroup">
                                <span class="input-group-addon input-group-addon-custom">Mã vạch kết quả thi</span>
                                <input type="hidden" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 textinput">
                            <div class="input-group input-group-custom validate-inputgroup">
                                <input class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="barcode" name="barcode" value="" type="text" placeholder="Nhập mã vạch kết quả thi" valid-number="" required="" maxlength="50">
                            </div>
                        </div>
                    </div>
                   
                    <div class="note"><b class="text-danger">*Lưu ý:</b> Sinh viên bắt buộc điền đầy đủ những mục có dấu (<span class="text-danger">*</span>)</div>
                    <br>
                </div>
            </div>

            <div class="block">
                <div class="text-center">

                    <button type="submit" id="btnSaveThongTinNhapHocOnline" class="btn btn-danger">
                        Đăng ký
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>