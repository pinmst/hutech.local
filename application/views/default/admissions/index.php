<div class="block-nopadding slideshow">
    <div class="content noidung">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                      <?php if ($sl==0) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>" class="active"></li>
                      <?php }else{ ?>
                         <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>"></li>
                      <?php } ?>
                      <?php $sl++; ?>
                <?php } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                    <?php if ($sl==0) { ?>
                      <div class="active item">
                        <a href="<?php echo base_url($item['link']); ?>">
                          <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%">
                        </a>
                      </div>
                    <?php }else{ ?>
                        <div class="item">
                          <a href="<?php echo base_url($item['link']); ?>">
                            <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%" >
                          </a>
                        </div>
                    <?php } ?>
                    <?php $sl++; ?>
                <?php } ?>

            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<div class="col-sm-9 no-padding-left padding-right-5">
<?php if (!empty($news_lester)): ?>
    <div class="block-nopadding">
    <div class="content noidung">
        <div class="col-sm-12 col-md-12 no-padding-left padding-right-5">
            <div class="block-content nopadding">    
                <div class="header-block">
                    <img class="icon-tin-tuyen-sinh" src="template/images/img_blank.gif">
                    <a href="<?php echo base_url('tuyen-sinh') ?>" class="title-block text-blue">
                        Tin tức mới
                    </a>
                </div>
                <div class="content-block">
                    <div class="col-sm-12 nopadding">
                    <?php $d=0; ?>
                    <?php foreach($news_lester as $item){ ?>
                        <?php if ($d == 0) { ?>
                            <div class="col-md-9 col-sm-12 news-block-left">
                                <div class="image-news-home">
                                    <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" class="link-img">
                                        <img class="img-responsive" alt="" src="<?php echo PATH_IMG_NEWS.$item['images'] ?>">
                                    </a>
                                </div>
                                <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" class="news-title text-blue">
                                    <?php echo $item['title_vn']; ?>
                                </a>
                                <p class="des_news">
                                    <?php echo $item['title_vn']; ?>
                                </p>
                            </div>
                        <?php }else{ ?>
                            <?php if ($d == 1) { ?>
                                <div class="col-sm-12 col-md-3 news-block-right" style="display: block !important;">
                                    <div style="max-height: 100px;overflow: hidden;">
                                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" class="link-img news-block-right" style="border-left:unset;padding: unset;">
                                            <img class="img-responsive" alt="" src="<?php echo PATH_IMG_NEWS.$item['images'] ?>">
                                        </a>
                                    </div>
                                    <a class="title wow slideInUp animated animated" title="<?php echo $item['title_vn']; ?>" href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" style="visibility: visible; animation-name: slideInUp;"><?php echo $item['title_vn']; ?></a>
                                </div>
                            <?php }else{ ?>
                                <?php if ($d < 5) { ?>
                                <div class="col-sm-12 col-md-3 news-block-right" style="display: block !important;">
                                    <a class="title wow slideInUp animated animated" title="<?php echo $item['title_vn']; ?>" href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" style="visibility: visible; animation-name: slideInUp;"><?php echo $item['title_vn']; ?></a>
                                </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php $d++; ?>
                    <?php } ?>
                    </div>
                    <?php if (count($news_lester) > 5) { ?>
                        <div class="col-sm-12 col-md-12 news-block-bottom">
                            <?php $d=0; ?>
                            <?php foreach($news_lester as $item){ ?>
                                <?php if ($d > 4) { ?>
                                    <div class="col-sm-12 col-md-3 link-img  title ">
                                    <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']); ?>" >
                                        <div class="img">
                                            <img class="img-responsive" alt="" src="<?php echo PATH_IMG_NEWS.$item['images'] ?>">
                                        </div>
                                        <?php echo $item['title_vn']; ?>
                                    </a></div>
                                <?php } ?>
                                <?php $d++; ?> 
                            <?php } ?>  
                        </div>
                    <?php } ?>  
                   <!--  <div class="col-sm-12 text-right padding">
                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat']); ?>" class="text-red text-bold">
                            <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>

<?php $i=0; ?>
    <?php foreach($info as $item){ ?>
        <?php if ($item['home_sub'] === '1'){ ?>
        <div class="col-sm-12 block-content nopadding">
            <div class="header-block">
                <img class="icon-tin-tuyen-sinh" src="template/images/img_blank.gif">
                    <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']) ?>" class="title-block text-blue">
                        <?php echo $item['title_vn'] ?>
                    </a>
            </div>

            <div class="content-block">
                <div class="col-sm-12 nopadding">
                    <?php $d=0; ?>
                    <?php foreach($detail as $item_dt){ ?>
                        <?php if ($item['Id'] === $item_dt->idcat){ ?>
                            <?php if ($d == 5) break; ?>
                            <?php if ($d == 0) { ?>
                            <div class="col-sm-9 news-block-left">
                                <div class="image-news-home">
                                    <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" class="link-img">
                                        <img class="img-responsive" alt="" src="<?php echo PATH_IMG_NEWS.$item_dt->images ?>">
                                    </a>
                                </div>
                                <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" class="news-title text-blue">
                                    <?php echo $item_dt->title_vn; ?>
                                </a>
                                <p class="des_news">
                                    <?php echo $item_dt->content_vn; ?>
                                </p>
                            </div>

                        <?php }else{ ?>
                            <?php if ($d == 1) { ?>
                                <div class="col-sm-3 news-block-right">
                                    <div style="max-height: 100px;overflow: hidden;">
                                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" class="link-img">
                                            <img class="img-responsive" alt="" src="<?php echo PATH_IMG_NEWS.$item_dt->images ?>">
                                        </a>
                                    </div>
                                    <a class="title wow slideInUp animated animated" title="<?php echo $item_dt->title_vn; ?>" href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" style="visibility: visible; animation-name: slideInUp;"><?php echo $item_dt->title_vn; ?></a>
                                </div>
                            <?php }else{ ?>
                            <div class="col-sm-3 news-block-right">
                                <a class="title wow slideInUp animated animated" title="<?php echo $item_dt->title_vn; ?>" href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" style="visibility: visible; animation-name: slideInUp;"><?php echo $item_dt->title_vn; ?></a>
                            </div>
                            <?php } ?>
                        <?php } ?>
                        <?php $d++; ?>
                    <?php } ?>
                    <?php } ?>
                </div>

                <div class="col-sm-12 text-right padding">
                    <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>" class="text-red text-bold">
                        <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                    </a>
                </div>
            
            </div>

        </div>
        <?php } ?>
    <?php $i++; ?>
<?php } ?>
</div>