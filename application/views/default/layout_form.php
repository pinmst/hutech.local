<?php
$data['web'] = $web  =$this->pagehtml_model->get_website(1);
$data['logo'] = $logo = $this->flash_model->list_data();
$data['webplus'] = $webplus = $this->webplus_model->list_data(10,$this->uri->segment(4));

?>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi-vn" lang="vi-vn" >
  
<head profile="http://www.w3.org/1999/xhtml/vocab">
    <base href="<?php echo BASE_URL ?>" />
    <meta name="google-site-verification" content="V7AOP6PwUqe36rcBVz5C2xpAb16Q9eBbCKi6w07RfUQ" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />      
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    
    <title><?php echo $web['title_vn']; ?></title>
    <meta name="keywords" content="<?php echo $web['keyword_vn']; ?>" />
    <meta name="description" content="<?php echo $web['description_vn']; ?>" />
    <meta name="copyright" content="<?php echo $web['title_vn']; ?>" />
    <meta name="author" content="<?php echo $web['title_vn']; ?>" />
    <meta http-equiv="audience" content="General" />
    <meta name="resource-type" content="Document" />
    <meta name="distribution" content="Global" />
    <meta name="GENERATOR" content="<?php echo $web['title_vn']; ?>" />
    <link rel="publisher" href="https://plus.google.com/+hutech/posts" />
    <link rel="author" href="https://plus.google.com/+hutech/posts" />
    <meta property="og:title" content="<?php echo $web['title_vn']; ?>" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="<?php echo base_url(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="vi_VN" />
    
    <link rel="shortcut icon" href="favicon.html" type="image/x-icon"/>
            
   <!--  <script type="text/javascript" src="template/khaosat/js/angular.min.js"></script>
    <script type="text/javascript" src="template/khaosat/js/ui-mask.js"></script>
    <script src="template/khaosat/js/ui-bootstrap-tpls-1.3.3.js"></script>

    <link rel="stylesheet" href="template/minify/site/default/bootstrap.min.css">
    <script src="template/khaosat/js/jquery.min.js"></script>
    <script src="template/khaosat/js/bootstrap.min.js"></script> -->

    <link href="template/LandingPageDangKy/fonts/glyphicons/css/glyphicons.css" rel="stylesheet" />
    <link href="template/LandingPageDangKy/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="template/LandingPageDangKy/css/bootstrap.css" rel="stylesheet" />
    <link href="template/LandingPageDangKy/css/custom-style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="template/LandingPageDangKy/plugins/datapicker/datepicker3.css" rel="stylesheet" />
    <!-- <script type="text/javascript" src="template/LandingPageDangKy/plugins/angular.js"></script> -->

    <!-- <script type="text/javascript" src="template/LandingPageDangKy/js/ui-mask.js"></script> -->
    <!-- <script src="template/LandingPageDangKy/plugins/ui/ui-bootstrap-tpls.min.js"></script> -->

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <script type="text/javascript" src="template/LandingPageDangKy/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="template/LandingPageDangKy/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- <script type="text/javascript" src="template/LandingPageDangKy/js/TuyenSinh/appDangky.js"></script> -->
    <script type="text/javascript" src="template/LandingPageDangKy/js/bootstrap.min.js"></script>

</head>
<body ng-controller="mainController" class="ng-scope">
    <!-- <div class="wrap-back-to-top">
        <a href="#0" class="cd-top"><img src="template/images/go-to-top.png" /></a>
    </div> -->
    
    <header class="row">
        <div class="content">
            <div class="col-sm-6" id="logo">
                <a href="<?php echo base_url(); ?>">
                    <img id="" src="<?php echo PATH_IMG_FLASH.$logo[0]->file_vn?>" style="max-height: 150px;">
                </a>
            </div>
            <div id="slogan" class="col-sm-6 text-center">
                <div class="logan"><?php echo $web['title_vn']; ?></div>
            </div>
        </div>
    </header>
            
    <?php $this->load->view($template,$data); ?>


    <div class="row footer">
        <div class="content">
            <div class="col-sm-6 padingtext">
                <h3>Thông tin trường</h3>
                <div><b class="truso">Trụ sở:</b> <?php echo $web['address']; ?></div>
                <?php if (!empty($web['address1'])) {?>
                    <div class="coso"><b>Cơ sở 1:</b> <?php echo $web['address1']; ?></div>
                <?php } ?>
                <?php if (!empty($web['address2'])) {?>
                    <div class="coso"><b>Cơ sở 2:</b> <?php echo $web['address2']; ?></div>
                <?php } ?>
                <?php if (!empty($web['address3'])) {?>
                    <div class="coso"><b>Cơ sở 3:</b> <?php echo $web['address3']; ?></div>
                <?php } ?>
                <?php if (!empty($web['address4'])) {?>
                    <div class="coso"><b>Cơ sở 4:</b> <?php echo $web['address4']; ?></div>
                <?php } ?>
                <?php if (!empty($web['address5'])) {?>
                    <div class="coso"><b>Cơ sở 5:</b> <?php echo $web['address5']; ?></div>
                <?php } ?>                
                <div>ĐT:  <?php echo $web['hotline']; ?> - Fax:  <?php echo $web['fax']; ?> - Email:  <?php echo $web['email']; ?></div>

                <div>
                    <!-- <a href="#">
                        <img class="icon-lien-he-facebook" src="template/images/img_blank.gif">
                    </a>
                    <a href="#" target="_blank">
                        <img class="icon-lien-he-youtube" src="template/images/img_blank.gif">
                    </a>
                    <a href="#" target="_blank">
                        <img class="icon-lien-he-google" src="template/images/img_blank.gif">
                    </a>
                    <a href="#" target="_blank">
                        <img class="icon-lien-he-twice" src="template/images/img_blank.gif">
                    </a>
                    <a href="#" target="_blank">
                        <img class="icon-lien-he-blog" src="template/images/img_blank.gif">
                    </a>
                    <a href="#" target="_blank">
                        <img class="icon-lien-he-tum" src="template/images/img_blank.gif">
                    </a> -->
                    <br> Bản quyền 
                    <span style="font-size: small;">
                        <a href="https://www.hutech.edu.vn/admin"><strong>©</strong></a>
                    </span> <script> document.write(new Date().getFullYear())</script> | All rights reserved  <a rel="author" href=""><?php echo $web['title_vn']; ?></a>
                </div>
            </div>

            <div class="col-sm-6 text-center  box-footer-3">
                <div class="col-sm-12 footer-right" style="text-align: left;">
                    <h3>Trang liên kết</h3>
                    <panel id="contentnews">
                        <ul>
                            <?php foreach($webplus as $item){ ?>
                                <li><a target="_blank" href="//<?php echo $item->link ?>"><?php echo $item->title_vn ?></a></li>
                            <?php } ?>
                        </ul>
                    </panel>
                </div>
            </div>
        </div>
    </div>

   <!--  <script type="text/javascript">window._sbzq||function(e){e._sbzq=[];var t=e._sbzq;t.push(["_setAccount",61705]);var n=e.location.protocol=="https:"?"https:":"http:";var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//static.subiz.com/public/js/loader.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)}(window);</script> -->
<script>
    (function($) {
        $(document).ready(function() {
        try{
            $.slidebars();
        } catch(e){}
        });
        }) (jQuery);
</script>
</body>
</html>