<?php 
    $brea_alias1 = base_url('media');
    $brea_alias2 = base_url($menu['sub_alias'].'/'.$info[0]['alias']);
    $brea_title2 = $info[0]['title_vn'];
?>

<p class="breacrumb">
    <a href="<?php echo $brea_alias1 ?>">Trang chủ media</a> >> <a href="<?php echo $brea_alias2; ?>"><?php echo $brea_title2; ?></a> >> <?php echo $info_detail['0']['title_vn']; ?>
</p>
<div class="block no-padding-bottom back-gray">
    <div class="content noidung desc-video">
        <div class="content-block nomargin">
            <div class="col-sm-8 nopadding contentvideo" url="<?php echo $info_detail['0']['content_vn']; ?>">
                <iframe width="100%" height="350" src="https://www.youtube.com/embed/<?php echo $info_detail['0']['content_vn']; ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe>
            </div>
            <div class="col-sm-4 right text-justify">
                <div class="header-block">
                    <img class="icon-kham-pha-hutech" src="template/images/img_blank.gif">
                    <span class="title-block text-orange"><?php echo $info['0']['title_vn'] ?></span>
                </div>
                <div>
                    <span class="title text-blue">
                        <?php echo $info_detail['0']['title_vn']; ?>
                    </span>
                    <span class="time">
                        <i data-brackets-id="1745" class="fa fa-clock-o" aria-hidden="true"></i>
                        <?php echo date("d-m-Y",$info_detail['0']['date']) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $i=0; ?>
<?php foreach($menu['only'] as $item){ ?>

<div class="block back-gray">
    <div class="content noidung block-content ">
        <div class="header-block">
            <div class="header-block title-line-right title-line-no">
                <img class="icon-video-lien-quan" src="template/images/img_blank.gif">
                <span class="title-block text-gray"><?php echo $item['title_vn']; ?></span>
            </div>
        </div>
        <div class="content-block">
            <div class="col-sm-12 nopadding wow slideInRight animated animated" style="visibility: visible; animation-name: slideInRight;">
                <?php $d=0; ?>
                <?php foreach($detail as $item_dt){ ?>
                    <?php if ($item['Id'] === $item_dt->idcat){ ?>
                        <?php if ($d == 4) break; ?>
                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>">
                            <div class="col-xs-6 col-md-3 col-sm-3 no-padding-right no-padding-left thumb-video" style="padding: 0 5px">
                                <div class="img">
                                    <img class="img-responsive thumb" alt="<?php echo $item_dt->title_vn; ?>" src="<?php echo PATH_IMG_NEWS.$item_dt->images ?>">
                                    <img class="icon-media-play" src="template/images/img_blank.gif">
                                </div>
                                <span class="text-bold"><?php echo $item_dt->title_vn; ?></span>
                            </div>
                        </a>   
                        <?php $d++; ?>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="col-sm-12 nopadding text-right">
                <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>" class="text-red text-bold">
                    <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                </a>
            </div>
        </div>
    </div>
</div>
<?php $i++; ?>
<?php } ?>