<?php 
    $brea_alias1 = base_url('media');
    $brea_alias2 = $main_vn['0']['title_vn'];
?>

<div class="block">
    <div class="content noidung block-content">
        <p class="breacrumb"><a href="<?php echo $brea_alias1 ?>">Trang chủ media</a> >> <?php echo $brea_alias2; ?></p>
        <div class="header-block">
            <div class="">
                <img class="icon-hutech-tv" src="template/images/img_blank.gif">
                <span class="title-block text-orange"><?php echo $main_vn['0']['title_vn']; ?></span>
            </div>
        </div>
        <div class="content-block">
            <div class="col-sm-12 nopadding">
                <?php foreach ($info as $item){ ?>
                    <div class="col-xs-6 col-md-3 col-sm-3 no-padding-right  thumb-video">
                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$sub.'/'.$item['alias']) ?>">
                            <div class="img">
                                <img class="img-responsive thumb" alt="<?php echo $item['title_vn'] ?>" src="<?php echo PATH_IMG_NEWS.$item['images'] ?>">
                                <img class="icon-media-play" src="template/images/img_blank.gif">
                            </div>
                            <span class="text-bold"><?php echo $item['title_vn'] ?></span>
                        </a>
                    </div>
                <?php } ?>
            </div>

            <div class="frm-paging">
            <?php 
                echo $this->pagination->create_links();
            ?>
            </div>
        </div>
    </div>
</div>