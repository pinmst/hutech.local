<div class="block-nopadding slideshow">
    <div class="content noidung">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                      <?php if ($sl==0) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>" class="active"></li>
                      <?php }else{ ?>
                         <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>"></li>
                      <?php } ?>
                      <?php $sl++; ?>
                <?php } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                    <?php if ($sl==0) { ?>
                      <div class="active item">
                        <a href="<?php echo base_url($item['link']); ?>">
                          <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%">
                        </a>
                      </div>
                    <?php }else{ ?>
                        <div class="item">
                          <a href="<?php echo base_url($item['link']); ?>">
                            <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%" >
                          </a>
                        </div>
                    <?php } ?>
                    <?php $sl++; ?>
                <?php } ?>

            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<?php $i=0; ?>
<?php foreach($info as $item){ ?>
<div class="block-nopadding" style="padding: 5px 0">
    <div class="content noidung block-content nopadding">
                                                                             
        <div class="header-block">
            <div class="title-line-orange">
                <img class="icon-kham-pha-hutech" src="template/images/img_blank.gif">
                <span class="title-block text-orange"><?php echo $item['title_vn'] ?></span>
            </div>
        </div>

        <div class="content-block">
            <?php $d=0; ?>
            <?php foreach($detail as $item_dt){ ?>
                <?php if ($item['Id'] === $item_dt->idcat){ ?>
                    <?php if ($d == 5) break; ?>
                    <?php if ($d == 0) { ?>
                        <div class="col-sm-6 nopadding thumb-video-new">
                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>" class="text-bold">
                                <div class="img-new">
                                    <img class="img-responsive thumb" alt="<?php echo $item->title_vn ?>" src="<?php echo PATH_IMG_NEWS.$item_dt->images ?>" style="width: 200%">
                                    <img class="icon-media-play2" src="template/images/img_blank.gif">
                                </div>
                                <?php echo $item_dt->title_vn; ?>
                            </a>
                        </div>
                    <?php }else{ ?>
                        <div class="col-sm-3 nopadding d-inline-block">
                            <div class="col-xs-12 no-padding-right thumb-video">
                                <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_dt->alias) ?>">
                                    <div class="img">
                                        <img class="img-responsive thumb" alt="<?php echo $item->title_vn ?>" src="<?php echo PATH_IMG_NEWS.$item_dt->images ?>">
                                        <img class="icon-media-play" src="template/images/img_blank.gif">
                                    </div>
                                    <?php echo $item_dt->title_vn; ?>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                    <?php $d++; ?>
                <?php } ?>
                
            <?php } ?>

            <div class="col-xs-12 nopadding text-right">
                <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']) ?>" class="text-red text-bold">
                    <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                </a>
            </div>
        </div>
        
    </div>
</div>
<?php $i++; ?>
<?php } ?>