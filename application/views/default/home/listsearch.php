<div class="col-sm-9 no-padding-left padding-right-5">
<p class="breacrumb"><a href="<?php echo base_url() ?>">Trang chủ</a> >> Tìm kiếm</p>                                       
<!-- Template Start: site/news/WrapListNews -->
    <div class="list-news block-content">
        
        <div class="content-block">
            <div class="col-sm-12 no-padding-left padding-bottom">
                <span class="title-block text-blue nopadding">Kêt quả tìm kiếm</span>
            </div>

            <?php foreach ($info_search as $item){ ?>
                <div class="item">
                    <!-- <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias_cat'].'/'.$item['alias']) ?>" class="news-title"></a> -->
                    <?php echo $item['title_vn'] ?>
                </div>
            <?php } ?>

        </div>
        <?php if(!empty($page)) echo '
            <nav class="div-pagi">
              <div class="pagination" style="">'.$page.' </div>
            </nav>
        '; ?>
    </div>
</div>


<style>
.div-pagi{
    text-align: center;
}
.pagination{
    margin: auto;
    box-sizing: border-box;
    height: 40px;
    padding: 8px 10px;
    display: block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
}
.pagination a{
    display: inline-block;
    padding: 6px 10px;
    background-color: #eee;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    color: #686868;
    margin: 0 2px;
    text-decoration: none!important;
}
.pagination a:hover{
    color: #fff;
    background-color: #e74847;
}
.pagination strong {
    display: inline-block;
    padding: 6px 10px;
    text-align: center;
    box-sizing: border-box;
    font-size: 12px;
    margin: 0 2px;
    text-decoration: none;
    color: #fff;
    background-color: #e74847;
}
#lengthValue,
#widthValue {
    color: blue
}
#areaValue {
    color: red
}
</style>