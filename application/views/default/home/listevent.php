<div class="col-sm-9 no-padding-left padding-right-5">
<p class="breacrumb"><a href="<?php echo base_url() ?>">Trang chủ</a> >> <?php echo $title_main; ?></p>                               
    <div class="list-news block-content">
        <div class="content-block">
            <div class="col-sm-12 no-padding-left padding-bottom">
                <span class="title-block text-blue nopadding"><?php echo $title_main; ?></span>
            </div>

            <?php foreach ($info as $item){ ?>
                <div class="item">
                    <div class="col-sm-3 no-padding-left">
                        <a href="<?php echo base_url($sub_alias.$item['alias']) ?>">
                            <img class="img-responsive img-rescat" alt="" src="<?php echo PATH_IMG_BANNER.$item['images'] ?>"  onerror="this.src=template/img/news/default.jpg">
                        </a>
                    </div>
                    <div class="col-sm-9 no-padding-left text-justify">
                        <a href="<?php echo base_url($sub_alias.$item['alias']) ?>" class="news-title">
                            <?php echo $item['title_vn'] ?></a>
                        <div class="text-orange">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <?php echo $item['location'] ?>
                        </div>
                         <div class="w-full"><?php echo $item['day_open'] ?></div>
                    </div>
                </div>
            <?php } ?>
            
            <div class="frm-paging">
            <?php 
                echo $this->pagination->create_links();
            ?>
            </div>
        </div>
    </div>
</div>