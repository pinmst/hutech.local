
<div class="block-nopadding slideshow">
    <div class="content noidung">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                      <?php if ($sl==0) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>" class="active"></li>
                      <?php }else{ ?>
                         <li data-target="#myCarousel" data-slide-to="<?php echo $sl ?>"></li>
                      <?php } ?>
                      <?php $sl++; ?>
                <?php } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $sl=0; ?>
                <?php foreach ($banner as $item){ ?>
                    <?php if ($sl==0) { ?>
                      <div class="active item">
                        <a href="<?php echo base_url($item['link']); ?>">
                          <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%">
                        </a>
                      </div>
                    <?php }else{ ?>
                        <div class="item">
                          <a href="<?php echo base_url($item['link']); ?>">
                            <img src="<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>" alt="<?php echo $item['images'] ?>" width="100%" >
                          </a>
                        </div>
                    <?php } ?>
                    <?php $sl++; ?>
                <?php } ?>

            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<main id="content" class="content-home" >
    <div class="container" style="padding: 0">
        <div class="shome-feature box-white">
            <div class="box-left">
                <h2 class="cate-title"><img src="template/forhome/images/icon-cate.png" alt="" /><a href="<?php echo base_url('hoat-dong'); ?>">Bản tin hoạt động khoa - trung tâm</a> </h2>
                <div class="boxleft-inner list-inline">
                    <?php $d=0; ?>
                    <?php foreach($activities_home as $item){ ?>
                        <?php $link_alias = base_url($activities_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                        <?php if ($d == 0) { ?>
                            <div class="item-inline article-feature">
                                <a href="<?php echo $link_alias; ?>">
                                    <img src="<?php echo PATH_IMG_NEWS.$item['images'] ?>" alt=""  style="width: 100%"/>
                                </a>
                                <div class="info">
                                    <h3><a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?></a></h3>
                                    <div class="description"><?php echo $item['description_vn']; ?></div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $d++; ?>
                    <?php } ?>
                    <?php $d=0; ?>
                    <div class="item-inline article-list">
                        <ul>
                        <?php foreach($activities_home as $item){ ?>
                            <?php $link_alias = base_url($activities_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                            <?php if ($d == 1) { ?>
                                <li class="featured">
                                    <a href="<?php echo $link_alias; ?>">
                                        <img src="<?php echo PATH_IMG_NEWS.$item['images'] ?>" alt="" style="width: 100%"/>
                                        <b><?php echo $item['title_vn']; ?></b>
                                    </a>
                                </li>
                            <?php }if($d > 1 && $d < 5){ ?>
                                <li><a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?></a></li>
                            <?php } ?>
                            <?php $d++; ?>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-right">
                <h2 class="cate-title cate-other"><img src="template/forhome/images/icon-admissions.png" alt="" /> Tuyển sinh</h2>
                <ul class="admissions-list">
                    <?php foreach($webplus as $item){ ?>
                        <li><a href="<?php if($item['link'] != ''){ echo '//'.$item['link']; } else { echo "#"; } ?>" style="background-image:url(<?php echo PATH_IMG_BANNER.$item['images']."?v=".time();?>)"><?php echo $item['title_vn']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div><!--end box featured-->
        <div class="box-white box-newssupport">
            <ul class="list-inline">
                <?php $d=0; ?>
                <?php foreach($activities_home as $item){ ?>
                    <?php $link_alias = base_url($activities_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                    <?php if ($d >= 5) { ?>
                    <li class="item-inline">
                        <a href="<?php echo $link_alias; ?>"><img src="<?php echo PATH_IMG_NEWS.$item['images']."?v=".time();?>" alt="" style=";max-height: 140px"/></a>
                        <h3><a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?></a></h3>
                    </li>
                    <?php } ?>
                    <?php $d++; ?>
                <?php } ?>
            </ul>
        </div>
        <!--end box news support-->
        <?php if (!empty($banner_rightbar)): ?>
        <div class="block-nopadding" style="margin: 5px 0">
            <div class="content noidung block-content nopadding">
                <div class="content-block">
                <a href="<?php echo $banner_rightbar['0']['link'] ?>">
                    <img src="<?php echo './data/Banner/'.$banner_rightbar['0']['images'] ?>" style="width: 100%">
                </a>
                </div>
            </div>
        </div>
        <?php endif ?>

        <div class="shome-feature box-white">
            <div class="box-left">
                <h2 class="cate-title"><img src="template/forhome/images/icon-cate2.png" alt="" /><a href="<?php echo base_url('sinh-vien') ?>"> Bản tin sinh viên</a></h2>
                <div class="boxleft-inner list-inline">
                    <?php $d=0; ?>
                    <?php foreach($student_home as $item){ ?>
                        <?php $link_alias = base_url($student_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                        <?php if ($d == 0) { ?>
                            <div class="item-inline article-feature">
                                <a href="<?php echo $link_alias; ?>">
                                    <img src="<?php echo PATH_IMG_NEWS.$item['images'] ?>" alt=""  style="width: 100%"/>
                                </a>
                                <div class="info">
                                    <h3><a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?></a></h3>
                                    <div class="description"><?php echo $item['description_vn']; ?></div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $d++; ?>
                    <?php } ?>
                    <?php $d=0; ?>
                    <div class="item-inline article-list">
                        <ul>
                        <?php foreach($student_home as $item){ ?>
                            <?php $link_alias = base_url($student_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                            <?php if ($d == 1) { ?>
                                <li class="featured">
                                    <a href="<?php echo $link_alias; ?>">
                                        <img src="<?php echo PATH_IMG_NEWS.$item['images'] ?>" alt="" style="width: 100%"/>
                                        <b><?php echo $item['title_vn']; ?></b>
                                    </a>
                                </li>
                            <?php }if($d > 1 && $d < 5){ ?>
                                <li><a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?></a></li>
                            <?php } ?>
                            <?php $d++; ?>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-right">
                <h2 class="cate-title cate-other"><img src="template/forhome/images/icon-cate3.png" alt="" /> Thư viện - Cà Phê Sách</h2>
                <ul class="news-list-sidebar">
                    <?php foreach($news_home as $item){ ?>
                        <?php $link_alias = base_url($library_home_alias.$item['catalias'].'/'.$item['alias']); ?>
                        <?php if($item['title_vn'] != null): ?>                              
                        <li>
                            <a href="<?php echo $link_alias; ?>"><?php echo $item['title_vn']; ?> <img src="template/forhome/images/icon-new.png" alt="" class="status-new" /></a>
                        </li>
                        <?php endif ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div><!--end box featured-->

        <div class="box-white box-media">
            <h2 class="cate-title title-media"><a href="<?php echo base_url('event') ?>">SỰ KIỆN HOẠT ĐỘNG MEDIA</a></h2>
            <ul class="list-inline">
                <?php foreach($event as $item){ ?>
                    <li class="item-inline">
                        <a href="<?php echo base_url('event/'.$item['alias']) ?>">
                            <img src="<?php echo PATH_IMG_BANNER.$item['images'] ?>" alt="" style="width: 100%"/>
                        </a>
                        <?php
                            $scheduled_day = $item['day_open'];
                            $days = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4','Thứ 5','Thứ 6', 'Thứ 7'];
                            $day = date('w',strtotime($scheduled_day));
                            $scheduled_day = $days[$day];
                        ?>
                        <div class="info-media">
                            <!-- <span><img src="template/forhome/images/icon-view.png" alt="" /> 12</span> -->
                            <!-- <span><img src="template/forhome/images/icon-heart.png" alt="" /> 345</span> -->
                            <!-- <span class="it-date"><img src="template/forhome/images/icon-clock.png" alt="" /> <?php echo $item['day_open'] ?></span> -->
                        </div>
                        <h3><a href="<?php echo base_url('event/'.$item['alias']) ?>"><?php echo $item['title_vn'] ?></a></h3>
                        <div class="time-place">
                            <div class="it-time">
                                <span><?php echo $scheduled_day; ?></span>
                                <p><?php echo $item['day_open'] ?></p>
                            </div>
                            <p class="it-place" style="margin: 20px"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $item['location'] ?></p>
                        </div>

                    </li>
                <?php } ?>
            </ul>
        </div> <!-- end box media -->
    </div>
</main>


<?php if(!empty($event)){ ?>
    <?php /*$this->load->view("default/event",$data);*/ ?>
<?php } ?> 

<?php if (!empty($media_home)): ?>
<!-- <div class="block-nopadding wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
    <div class="content noidung back-white border-radius">
        <div class="header-block">
            <div class="title-line-right title-line-orange">
                <img class="icon-media" src="template/images/img_blank.gif">
                <a href="<?php echo base_url($media_home_alias.$media_home_info['0']['alias']); ?>" class="title-block text-orange">MEDIA</a>
            </div>
        </div>
        <div class="content-block">
            <div class="col-sm-12 nopadding wow slideInRight animated animated" style="visibility: visible; animation-name: slideInRight;">
                <?php foreach($media_home as $item){ ?>
                    <div class="col-xs-6 col-md-3 col-sm-3 thumb-video">
                        <a href="<?php echo base_url($media_home_alias.$item['alias_cat'].'/'.$item['alias']); ?>" class="text-bold">
                            <div class="img">
                                <img class="img-responsive thumb" src="<?php echo PATH_IMG_NEWS.$item['images'] ?>">
                                <img data-brackets-id="18211" class="icon-media-play" src="template/images/img_blank.gif">
                            </div>
                            <?php echo $item['title_vn']; ?>
                        </a>
                    </div>
                <?php } ?>
                </div>
        </div>
    </div>
</div> -->
<?php endif ?>