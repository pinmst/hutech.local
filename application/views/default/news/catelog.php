<?php 
    $brea_alias1 = base_url('tuyen-sinh');
    $brea_alias2 = $main_vn['0']['title_vn'];
?>

<div class="col-sm-9 no-padding-left padding-right-5">
    <p class="breacrumb"><a href="<?php echo $brea_alias1 ?>">Trang chủ</a> >> Giới thiệu trường</p> 
    <div class="detail-news">
        <div class="content-block">
            <div class="col-sm-12 no-padding-left padding-bottom">
                <span class="title-block text-blue nopadding">Giới thiệu trường</span>
                <panel id="contentnews">
                    <ul style="padding-top: 20px;">
                        <?php foreach($info as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('gioi-thieu/'.$item['alias']); ?>" style="">
                                    <?php echo $item['title_vn'] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </panel>
            </div>
            
            
        </div>
    </div>
</div>