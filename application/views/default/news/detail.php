<?php 
    $brea_alias1 = base_url();
    $brea_alias2 = base_url('gioi-thieu');
    $brea_title2 = 'Giới thiệu trường';
?>

<div class="col-sm-9 no-padding-left padding-right-5">
    <p class="breacrumb">
        <a href="<?php echo $brea_alias1 ?>">Trang chủ</a> >> <a href="<?php echo $brea_alias2; ?>"><?php echo $brea_title2; ?></a> >> <?php echo $info['0']['title_vn']; ?>
    </p>
    <div class="detail-news">
    <div class="content-block">
        <div class="col-sm-12 no-padding-left padding-bottom">
            <span class="title-block text-blue nopadding"><?php echo $info['0']['title_vn']; ?></span>
        </div>
        <div class="col-sm-12 nopadding news-info">
            <div class="col-sm-6 nopadding">
                <div class="article-tools clearfix">
                    <dl style="width: auto;" class="article-info">
                        <dd class="create"><i class="fa fa-calendar" aria-hidden="true"></i> <span><?php echo date("d-m-Y",$info['0']['date']) ?></span></dd>
                    </dl>
                </div>
            </div>
        </div>
        
        <panel id="contentnews">
            <?php echo $info[0]['content_vn'];?>
        </panel>
        <panel id="endNews"></panel>
        </div>
    </div>
</div>