<div class="menu-main">
    <a href="" class="toggle-menu">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </a>
    <nav class="" id="menu-main">
        <div class="container">
        <ul class="table autoLayout" id="ja-cssmenu">
            <li>
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home fa-1-5" aria-hidden="true"></i></a>
                <?php if (!empty($menu['sub_title'])): ?>
                <ul id="menu_child">
                    <li>
                        <a href="<?php echo base_url(); ?>"><span class="menu-title">Trang chủ</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url($menu['sub_alias']); ?>"><span class="menu-title"><?php echo $menu['sub_title']; ?></span>
                        </a>
                    </li>
                </ul>
                <?php endif ?>
            </li>

            <?php if (!empty($menu['menu_news'])): ?>
                <li id="">
                    <a href="#">Giới thiệu</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_news'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('gioi-thieu/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_library'])): ?>
                <li id="">
                    <a href="#">Thư Viện - Cà phê sách</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_library'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('thu-vien/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>
            
            <!-- menu khoa -->
            <?php if (!empty($menu['menu_faculty'])): ?>
                <li id="">
                    <a href="#">Khoa</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_faculty'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('khoa/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>
            
            <?php if (!empty($menu['menu_media'])): ?>
                <li id="">
                    <a href="<?php echo base_url('media'); ?>">Media</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_media'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('media/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_cooperation'])): ?>
                <li id="">
                    <a href="#">Hợp tác doanh nghiệp</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_cooperation'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('hop-tac-doanh-nghiep/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_activities'])): ?>
                <li id="">
                    <a href="<?php echo base_url('hoat-dong'); ?>">Hoạt động Khoa - Trung tâm</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_activities'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('hoat-dong/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_admissions'])): ?>
                <li id="">
                    <a href="<?php echo base_url('tuyen-sinh'); ?>">Tuyển sinh</a>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_student'])): ?>
                <li id="">
                    <a href="<?php echo base_url('sinh-vien'); ?>">Sinh viên</a>
                    <ul id="menu_child">
                        <?php foreach ($menu['menu_student'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('sinh-vien/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <!-- menu sub only -->
            <?php if (!empty($menu['only'])): ?>
                <?php foreach ($menu['only'] as $item){ ?>
                    <li>
                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>"><?php echo $item['title_vn']; ?></a>
                        <?php if (!empty($menu['only_sub2'])): ?>
                            <ul id="menu_child">
                                <?php foreach ($menu['only_sub2'] as $item_sub){ ?>
                                    <?php if($item_sub['parentid'] == $item['Id'] && $item_sub['parentid'] != '0'): ?>
                                        <li>
                                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item_sub['alias']); ?>">
                                                <?php echo $item_sub['title_vn']; ?>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                <?php } ?>
                            </ul>
                        <?php endif ?>
                    </li>
                <?php } ?>
            <?php endif ?>

            <!-- menu sub only tuyen sinh-->
            <?php if (!empty($menu['tuyen_sinh'])): ?>
                <?php if (!empty($menu['sub_alias_ts'])): ?>
                <li>
                    <a href="<?php echo base_url($menu['sub_alias'].$menu['sub_alias_ts']); ?>"><?php echo $menu['sub_title_ts']; ?></a>
                    <ul id="nganh-dao-tao" class="menu_child">
                        <?php foreach ($menu['ts_nganh'] as $item){ ?>
                            <li class="havechild">
                                <a href="<?php echo base_url($menu['sub_alias'].$menu['sub_alias_ts'].'/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php endif ?>
                <?php foreach ($menu['tuyen_sinh'] as $item){ ?>
                    <?php if ($item['parentid'] === '0'): ?>
                        <li>
                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>"><?php echo $item['title_vn']; ?></a>
                            <?php foreach ($menu['tuyen_sinh'] as $item_child){ ?>
                                <?php if (!empty($item_child['parentid'] != '0') && $item_child['parentid'] === $item['Id']): ?>
                                    
                                <?php endif ?>
                            <?php } ?>
                            <ul id="menu_child">
                                <?php foreach ($menu['tuyen_sinh'] as $item_child){ ?>
                                    <?php if (!empty($item_child['parentid'] != '0') && $item_child['parentid'] === $item['Id']): ?>
                                        <li>
                                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_child['alias']); ?>"><span class="menu-title"><?php echo $item_child['title_vn']; ?></span></a>
                                        </li>
                                    <?php endif ?>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php endif ?>
                <?php } ?>
            <?php endif ?>

        </ul>
        </div>
    </nav>
</div> 

