<?php 
    $brea_alias1 = base_url($sub_breacrum);
    $brea_alias2 = $main_vn['0']['title_vn'];
?>

<div class="col-sm-9 no-padding-left padding-right-5">   
<p class="breacrumb"><a href="<?php echo $brea_alias1 ?>">Trang chủ Khoa</a> >> <?php echo $brea_alias2; ?></p>

    <div class="list-news block-content">
        <div class="content-block">
            <div class="col-sm-12 no-padding-left padding-bottom">
                <span class="title-block text-blue nopadding"><?php echo $main_vn['0']['title_vn']; ?></span>
            </div>
            <?php $$PATH_IMG = './data/Faculty/'; ?>
            <?php foreach ($info as $item){ ?>
                <div class="item">
                    <div class="col-sm-3 no-padding-left">
                        <a href="<?php echo base_url($sub.'/'.$item['alias']) ?>">
                            <img class="img-responsive img-rescat" alt="" src="<?php echo $$PATH_IMG.$item['images'] ?>" onerror="this.src=template/img/news/default.jpg">
                        </a>
                    </div>
                    <div class="col-sm-9 no-padding-left text-justify">
                        <a href="<?php echo base_url($sub.'/'.$item['alias']) ?>" class="news-title">
                            <?php echo $item['title_vn'] ?></a>
                        <span class="des_news">
                            <?php echo $item['description_vn'] ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
            
            <div class="frm-paging">
            <?php 
                echo $this->pagination->create_links();
            ?>
            </div>
        </div>
    </div>
</div>