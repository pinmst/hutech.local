<?php
$data['web'] = $web  =$this->pagehtml_model->get_website(1);
$data['logo'] = $logo = $this->flash_model->list_data(1,0);
$data['banner'] = $banner =  $this->bmenu_model->get_listhome();
$data['webplus'] = $webplus = $this->webplus_model->get_kind(array("kind"=>"1",'ticlock'=>'0'),2,0);
$data['htdt'] = $htdt = $this->webplus_model->get_kind(array("kind"=>"2",'ticlock'=>'0'),1,0);
$data['event'] = $event = $this->webplus_model->get_kind(array("kind"=>"3",'ticlock'=>'0'),1,0);
$data['banner_rightbar'] = $banner_rightbar = $this->pagehtml_model->get_on_list_weblink(array('style'=>6, 'ticlock'=>'0'),1);
$data['banner_bottom'] = $banner_bottom = $this->pagehtml_model->get_on_list_weblink(array('style'=>4, 'ticlock'=>'0'),12);

?>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi-vn" lang="vi-vn" >

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head profile="http://www.w3.org/1999/xhtml/vocab">
    <base href="<?php echo BASE_URL ?>" />
    <meta name="google-site-verification" content="V7AOP6PwUqe36rcBVz5C2xpAb16Q9eBbCKi6w07RfUQ" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />      
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    
    <title><?php echo $web['title_vn']; ?></title>
    <meta name="keywords" content="<?php echo $web['keyword_vn']; ?>" />
    <meta name="description" content="<?php echo $web['description_vn']; ?>" />
    <meta name="copyright" content="<?php echo $web['title_vn']; ?>" />
    <meta name="author" content="<?php echo $web['title_vn']; ?>" />
    <meta http-equiv="audience" content="General" />
    <meta name="resource-type" content="Document" />
    <meta name="distribution" content="Global" />
    <meta name="GENERATOR" content="<?php echo $web['title_vn']; ?>" />
    <meta property="og:title" content="<?php echo $web['title_vn']; ?>" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="<?php echo base_url(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="vi_VN" />
    
    <link rel="shortcut icon" href="<?php echo PATH_IMG_NEWS.$web['message']; ?>" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="themes/media/js/bxslider/jquery.bxslider.css" media="all" />
	<link rel="stylesheet" type="text/css" href="themes/media/js/slick/slick.css" media="all" />
	<link rel="stylesheet" type="text/css" href="themes/media/js/slick/slick-theme.css" media="all" />        
    <script type="text/javascript" src="template/khaosat/js/angular.min.js"></script>
    <script type="text/javascript" src="template/khaosat/js/ui-mask.js"></script>
    <script src="template/khaosat/js/ui-bootstrap-tpls-1.3.3.js"></script>

    <!-- <link rel="stylesheet" href="template/minify/site/default/bootstrap.min.css"> -->
    <script src="template/khaosat/js/jquery.min.js"></script>
    <script src="template/khaosat/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

      
    <link rel="stylesheet" type="text/css" href="template/minify/site/default/0.2.5.css" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700&amp;subset=latin-ext" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="template/forhome/fonts/font-awesome.min.css" media="all" />
    <link rel="stylesheet" type="text/css" href="template/forhome/css/mcs.reset.css" media="all" />
    <!-- <link rel="stylesheet" type="text/css" href="template/forhome/js/bxslider/jquery.bxslider.css" media="all" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="template/forhome/js/slick/slick.css" media="all" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="template/forhome/js/slick/slick-theme.css" media="all" /> -->
    <link rel="stylesheet" type="text/css" href="template/forhome/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="template/forhome/css/responsive.css" media="all" />
        
        <script type='text/javascript'>
            var url_root = "index.html", url_img, url_static;
            var token = "7bf47e1862ea79d3f39f7626b092ebc6";
            url_img = "s-img/index.html";
            url_static = "index.html";
        </script>
        <script type="text/javascript" src="template/minify/site/default/home/Index/0.2.8.js"></script>
          <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-37324204-1', 'auto');
            ga('send', 'pageview');
          </script>
        
            <!-- Facebook Pixel Code -->
                <script>
                !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','../connect.facebook.net/en_US/fbevents.js');

                fbq('init', '662238160536354');
                fbq('track', 'PageView');
                </script>
                <noscript><img height="1" width="1" style="display:none"
                src="http://www.facebook.com/tr?id=662238160536354&amp;ev=PageView&amp;noscript=1"
                /></noscript>
            <!-- End Facebook Pixel Code -->
        <style>
            .no-padding-home {
                    padding: 0 !important;
            }
            .frm-paging{
                text-align: center;
                margin: auto;
                box-sizing: border-box;
                height: 40px;
                padding: 8px 10px;
                display: block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .frm-paging a{
                    display: inline-block;
                padding: 6px 10px;
                background-color: #eee;
                text-align: center;
                box-sizing: border-box;
                font-size: 12px;
                color: #686868;
                margin: 0 2px;
                text-decoration: none!important;
            }
            .frm-paging a:hover{
                color: #fff;
                background-color: #e74847;
            }
            .frm-paging strong{
                display: inline-block;
                padding: 6px 10px;
                text-align: center;
                box-sizing: border-box;
                font-size: 12px;
                margin: 0 2px;
                text-decoration: none;
                color: #fff;
                background-color: #e74847;
            }
            .breacrumb{
                padding: 10px 0;font-style: italic;font-size: 12px;
            }
            .img-rescat{
                max-height: 130px;
            }

        </style>
</head>
<body>
    <script type="text/javascript" src="template/libs/menu/ddaccordion.js"></script>

    <div class="the-header">
        <header id="header">
            <div class="container">
                <a href="<?php echo base_url(); ?>" class="logo">
                    <?php if(!empty($data['faculty']['only']['0']['banner'])){ ?>
                        <img src="<?php echo "./data/faculty/".$data['faculty']['only']['0']['banner'] ?>" alt="" style="max-width: 100%" />
                    <?php }else{ ?>
                        <img src="<?php echo PATH_IMG_FLASH.$logo[0]->file_vn?>" alt="" style="max-width: 100%" />
                    <?php } ?>
                        
                </a>
                <div class="header-right">
                    <form method="get" action="<?php echo base_url().'search' ?>" id="searchForm" class="pc">
                        <input type="text" name="s" id="input_searchword" placeholder="Tìm kiếm" /> 
                        <button id="btnsearch" type="submit" class="btn"></button>
                    </form>
                    <div class="banner-image" style="background-image:url('template/forhome/images/picture/bannerimg.png')"></div>
                </div>
                <div class="clear"></div>
            </div>
        </header>
    </div>

    <?php $this->load->view("default/nav", $data); ?>

    <div id="sb-site">
    <div class="wrap-back-to-top">
        <a href="#0" class="cd-top"><img src="template/images/go-to-top.png" /></a>
    </div>


    <section class="col-sm-12 content nopadding">
        <div class="container nopadding">

                    
            <div class="block-nopadding" style="padding: 5px 0;min-height: 300px">
                <div class="content noidung">
                    
                    <!-- content data -->
                    <?php $this->load->view($template,$data); ?>
                    
                    <?php if ($data['side_htdt'] != 'off'): ?>
                        <?php if(!empty($data['htdt'])): ?>
                            <?php $this->load->view("default/htdt",$data); ?>
                        <?php endif ?>
                    <?php endif ?>
                    <?php if ($data['sidebar'] != 'off'): ?>
                        <?php if(!empty($data['news_home'])): ?>
                            <?php $this->load->view("default/sidebar_right_top",$data); ?>
                        <?php endif ?>
                    <?php endif ?>
                    <!-- <div class="block-nopadding"></div> -->
                    <?php if ($data['sidebar_notice'] != 'off'): ?>
                        <?php if(!empty($data['news_hot'])): ?>
                            <?php $this->load->view("default/sidebar_notice",$data); ?>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
    
	<?php if ($data['banner_off'] != 'off'): ?>
	<div class="block-nopadding" id="">
			<div class="content noidung">
			<div class="box-white box-partner">
				<h2 class="cate-title title-media">CÁC TRƯỜNG ĐỐI TÁC & LIÊN THÔNG</h2>
				<ul class="list-inline">
					<?php $d=0; ?>
                    <?php foreach ($banner_bottom as $item){ ?>
					<li class="item-inline"><a href="<?php echo $item['link'] ?>"><img src="<?php echo './data/Banner/'.$item['images'] ?>" alt="" /></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>		
    <?php endif ?>
	<!--end main-->

	<footer id="footer">		
		<a href="" class="logo"><img src="template/images/favicon.png" alt="" /></a>
		<div class="top-footer">
			<div class="content noidung container">
				<div class="info-contact item-inline">
					<h3>TRƯỜNG ĐẠI HỌC TÀI CHÍNH - KẾ TOÁN</h3>
                    <?php if(!empty($data['faculty']['only']['0']['footer'])){ ?>
                        <p><?php echo $data['faculty']['only']['0']['footer']; ?></p>
                    <?php }else{ ?>
                        <ul class="list-contact">
                            <li><img src="template/images/ic-home.png" alt="" />Địa chỉ: <?php echo $web['address']; ?></li>
                            <li><img src="template/images/ic-phone.png" alt="" /> Điện thoại: <?php echo $web['hotline']; ?></li>
                            <li><img src="template/images/icon-mail.png" alt="" /> Email: <?php echo $web['email']; ?></li>
                        </ul>
                    <?php } ?>
				</div>

				<ul class="menu-footer item-inline">
					<li><a href="gioi-thieu">Giới thiệu</a></li>
					<li><a href="<?php echo base_url('thu-vien/sach-tuoi-tre') ?>">Thư viện - cà phê sách</a></li>
					<li><a href="<?php echo base_url('media') ?>">Media</a></li>
					<li><a href="#">Hợp tác doanh nghiệp</a></li>
					<li><a href="<?php echo base_url('hoat-dong') ?>">Hoạt động khoa - trung tâm</a></li>
					<li><a href="<?php echo base_url('tuyen-sinh') ?>">Tuyển sinh</a></li>
					<li><a href="<?php echo base_url('sinh-vien') ?>">Sinh Viên</a></li>
					<li><a href="#">Liên hệ</a></li>
				</ul>
				<img class="human" src="template/images/footer-human.png" alt="" />
			</div>
		</div>	
		<div class="footer-bottom">
            <div class="container">
                <p class="copy-right">All Right Reserved <script> document.write(new Date().getFullYear())</script> © Bản quyền Trường Đại học Tài chính - Kế toán</p>
                <div class="list-social-network" style="margin-top:unset;float: unset;">
                    <a href="<?php echo $web['address4']; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="<?php echo $web['address2']; ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a href="<?php echo $web['address5']; ?>" target="_blank"><i class="fa fa-google-plus"></i></i></a>
                    <a href="<?php echo $web['address3']; ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
        </div>
	</footer>
</div>
</section>
</div>
<script type="text/javascript" src="template/libs/menu/slidebars.js"></script>
<?php $this->load->view("default/nav_mobile", $data); ?>

<script>
    (function($) {
        $(document).ready(function() {
        try{
            $.slidebars();
        } catch(e){}
        });
        }) (jQuery);
</script>
<script type="text/javascript">
		jQuery(function($) {
			$('#slider-main .bxslider').bxSlider({
				auto: true,
				controls: false
			});
			$('.box-partner .list-inline').slick({
				infinite: true,
				autoplay: true,
				slidesToShow: 9,
				slidesToScroll: 1,
				responsive: [
					{
					  breakpoint: 992,
					  settings: {
						slidesToShow: 7,
			   			slidesToScroll: 1,
					  }
					},
					{
					  breakpoint: 768,
					  settings: {
						slidesToShow: 5,
			   			slidesToScroll: 1,
					  }
					},
					{
					  breakpoint: 576,
					  settings: {
						slidesToShow: 3,
			   			slidesToScroll: 1,
					  }
					},
					{
					  breakpoint: 414,
					  settings: {
						slidesToShow: 2,
			   			slidesToScroll: 1,
					  }
					},
				]
			});
		});
	</script>
	<script type="text/javascript" src="themes/media/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="themes/media/js/bxslider/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="themes/media/js/slick/slick.min.js"></script>
	<script type="text/javascript" src="themes/media/js/mcs.custom.js"></script>
</body>
</html>