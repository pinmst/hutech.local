<div class="sb-slidebar sb-left">
    <div id="wrap">
        <ul class="menu" style="margin-top:0px;">
            <li>
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home fa-1-5" aria-hidden="true"></i></a>
                <?php if (!empty($menu['sub_title'])): ?>
                <ul>
                    <li>
                        <a href="<?php echo base_url(); ?>"><span class="menu-title">Trang chủ</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url($menu['sub_alias']); ?>"><span class="menu-title"><?php echo $menu['sub_title']; ?></span>
                        </a>
                    </li>
                </ul>
                <?php endif ?>
            </li>

            <?php if (!empty($menu['menu_news'])): ?>
                <li id="">
                    <a href="#"><span class="menu-title">Giới thiệu</span></a>
                    <ul id="">
                        <?php foreach ($menu['menu_news'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('gioi-thieu/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if ($menu['menu_library'] === 'show'): ?>
                <li id="">
                    <a href="#" ><span class="menu-title">Thư Viện - Cafe sách</span></a>
                    
                </li>
            <?php endif ?>
            
            <!-- menu khoa -->
            <?php if (!empty($menu['menu_faculty'])): ?>
                <li id="">
                    <a href="#"><span class="menu-title">Khoa</span></a>
                    <ul id="">
                        <?php foreach ($menu['menu_faculty'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('khoa/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>
            
            <?php if (!empty($menu['menu_media'])): ?>
                <li id="">
                    <a href="<?php echo base_url('media'); ?>"><span class="menu-title">Media</span></a>
                    <ul id="">
                        <?php foreach ($menu['menu_media'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('media/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_cooperation'])): ?>
                <li id="">
                    <a href="#"><span class="menu-title">Hợp tác doanh nghiệp</span></a>
                    <ul id="">
                        <?php foreach ($menu['menu_cooperation'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('hop-tac-doanh-nghiep/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_activities'])): ?>
                <li id="">
                    <a href="<?php echo base_url('hoat-dong'); ?>"><span class="menu-title">Hoạt động Khoa, Trung tâm, Viện</span></a>
                    <ul id="noi-bo">
                        <?php foreach ($menu['menu_activities'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('hoat-dong/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_admissions'])): ?>
                <li id="">
                    <a href="<?php echo base_url('tuyen-sinh'); ?>"><span class="menu-title">Tuyển sinh</span></a>
                </li>
            <?php endif ?>

            <?php if (!empty($menu['menu_student'])): ?>
                <li id="">
                    <a href="<?php echo base_url('sinh-vien'); ?>"><span class="menu-title">Sinh viên</span></a>
                    <ul id="noi-bo">
                        <?php foreach ($menu['menu_student'] as $item){ ?>
                            <li>
                                <a href="<?php echo base_url('sinh-vien/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php endif ?>

            <!-- menu sub only -->
            <?php if (!empty($menu['only'])): ?>
                <?php foreach ($menu['only'] as $item){ ?>
                    <li>
                        <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span>
                        </a>
                        <?php if (!empty($menu['only_sub2'])): ?>
                            <ul id="noi-bo">
                                <?php foreach ($menu['only_sub2'] as $item_sub){ ?>
                                    <?php if($item_sub['parentid'] == $item['Id'] && $item_sub['parentid'] != '0'): ?>
                                        <li>
                                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item_sub['alias']); ?>">
                                                <span class="menu-title"><?php echo $item_sub['title_vn']; ?></span>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                <?php } ?>
                            </ul>
                        <?php endif ?>
                    </li>
                <?php } ?>
            <?php endif ?>

            <!-- menu sub only tuyen sinh-->
            <?php if (!empty($menu['tuyen_sinh'])): ?>
                <?php if (!empty($menu['sub_alias_ts'])): ?>
                <li>
                    <a href="<?php echo base_url($menu['sub_alias'].$menu['sub_alias_ts']); ?>"><span class="menu-title"><?php echo $menu['sub_title_ts']; ?></span>
                    </a>
                    <ul id="nganh-dao-tao">
                        <?php foreach ($menu['ts_nganh'] as $item){ ?>
                            <li class="havechild">
                                <a href="<?php echo base_url($menu['sub_alias'].$menu['sub_alias_ts'].'/'.$item['alias']); ?>"><span class="menu-title"><?php echo $item['title_vn']; ?></span></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php endif ?>
                <?php foreach ($menu['tuyen_sinh'] as $item){ ?>
                    <?php if ($item['parentid'] === '0'): ?>
                        <li>
                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias']); ?>">
                                <span class="menu-title"><?php echo $item['title_vn']; ?></span>
                            </a>
                            <?php if (!empty($item['parentid'] != '0')): ?>
                            <ul id="">
                                <?php foreach ($menu['tuyen_sinh'] as $item_child){ ?>
                                    <?php if (!empty($item_child['parentid'] != '0')): ?>
                                        <?php if ($item_child['parentid'] === $item['Id']): ?>
                                        <li>
                                            <a href="<?php echo base_url($menu['sub_alias'].'/'.$item['alias'].'/'.$item_child['alias']); ?>"><span class="menu-title"><?php echo $item_child['title_vn']; ?></span></a>
                                        </li>
                                        <?php endif ?>
                                    <?php endif ?>
                                <?php } ?>
                            </ul>
                            <?php endif ?>
                        </li>
                    <?php endif ?>
                <?php } ?>
            <?php endif ?>
        </ul>
    </div>
</div>