<?php 
global $ticketid,$orderid;
if($_POST['confirm_pticket']) {
	$user_current = wp_get_current_user();
	$total = get_field('ticket_total','option');
	update_field('field_58e5fbc85db54',$total+1,'option');
	$title = 'SVP'.($total+1);
	$ticket_post = array(
		'post_title'    => $title,
		'post_status'   => 'pending',
		'post_type'		=> 've'
	);
	$ticketid = wp_insert_post($ticket_post);
	if($ticketid) {
		update_field('field_58cff6b497c48',$_POST['pticket_type'],$ticketid);
		if($_POST['pticket_htgb'] == 3 || !$_POST['pticket_price'] || $_POST['pticket_price'] =='') update_field('field_58cef83004a76',0,$ticketid);
		else update_field('field_58cef83004a76',$_POST['pticket_price'],$ticketid);
		update_field('field_58cef89604a78',$user_current->ID,$ticketid);
		update_field('field_58cef76504a74',$_POST['pticket_godate'].' '.$_POST['pticket_gotime'],$ticketid);
		update_field('field_58deb72ef03b2',$_POST['pticket_gocode'],$ticketid);
		update_field('field_58cef81504a75',$_POST['pticket_backdate'].' '.$_POST['pticket_backtime'],$ticketid);
		update_field('field_58deb73ef03b3',$_POST['pticket_backcode'],$ticketid);
		update_field('field_58de5e11da55a',$_POST['pticket_htgb'],$ticketid);
		update_field('field_58de5ecbda55c',$_POST['pticket_note'],$ticketid);
		update_field('field_58de5f13da55e',$_POST['pticket_addnote'],$ticketid);
		update_field('field_58cef85404a77',0,$ticketid);
		update_field('field_58d015df90aa7',0,$ticketid);
		update_field('field_591f33afe8531',0,$ticketid);
		update_field('field_591f3d4889980',0,$ticketid);
		$ticket_list_member = get_field('ticket_list_member',$ticketid);
		$array_update = array();
		if($_POST['ticket_name']) {
			foreach($_POST['ticket_name'] as $tname) {
				$value = array(
					'member_name'	=> $tname
				);
				if($ticket_list_member) {
					$array_update = $ticket_list_member;
				}
				$array_update[] = $value;
			}
		}
		update_field( 'field_58e619f51d9bc', $array_update, $ticketid);
		$diterms = get_terms( 'diem_di', array(
			'hide_empty' => false,
			'slug' => $_POST['pticket_from']
		) );
		$denterms = get_terms( 'diem_den', array(
			'hide_empty' => false,
			'slug' => $_POST['pticket_to']
		) );
		$ddi = array($diterms[0]->term_id);
		$dden = array($denterms[0]->term_id);
		$airline = array($_POST['pticket_airline']);
		wp_set_post_terms( $ticketid, $ddi, 'diem_di' );
		wp_set_post_terms( $ticketid, $dden, 'diem_den' );
		wp_set_post_terms( $ticketid, $airline, 'hang_hang_khong' );
		/* Bell */
		$bell_post = array(
			'post_title'    => 'Đăng vé thành công',
			'post_status'   => 'publish',
			'post_type'		=> 'thong_bao'
		);
		$bellid = wp_insert_post($bell_post);
		if($bellid) {
			update_field('field_5903cbf6ddd44',$user_current->ID,$bellid);
			update_field('field_5903cd2bddd47','Chưa đọc',$bellid);
			wp_set_post_terms( $bellid, 50, 'loai_thong_bao' );
		}
		/* Send mail booking ticket */
		$locationfrom = $_POST['pticket_from_text'];
		$locationto = $_POST['pticket_to_text'];
		$email = get_field('email_manager_ticket','option');
		$subject = '[Sanve.com.vn] Thông báo có khách hàng đăng vé';
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Sanve.com.vn <hotro@sanve.com.vn>' . "\n";
		ob_start();
		INCLUDE(TEMPLATEPATH .'/email/admin-post.html');
		$emailbody = ob_get_clean();
		wp_mail($email,$subject,$emailbody,$headers);
	}
	wp_redirect(get_permalink($pageid));
}
/* Booking ticket */
if($_POST['confirm_bticket']) {
	$user_current = wp_get_current_user();
	$total = get_field('ticket_total_order','option');
	update_field('field_58f00be7e92b1',$total+1,'option');
	$title = 'SVB'.($total+1);
	$ticket_post = array(
		'post_title'    => $title,
		'post_status'   => 'publish',
		'post_type'		=> 'don_hang'
	);
	$orderid = wp_insert_post($ticket_post);
	if($orderid) {
		update_field('field_58f97cd50589e',$_POST['ticket_sale'],$orderid);
		update_field('field_58eff568548c4',$user_current->ID,$orderid);
		update_field('field_58eff873548c5',$_POST['ticket_id'],$orderid);
		$book_current = get_field('ticket_book',$_POST['ticket_id']);
		update_field('field_58d015df90aa7',$book_current+1,$_POST['ticket_id']);
		update_field('field_58eff892548c6',$_POST['ticket_number'],$orderid);
		$ticket_list_member = get_field('oticket_list_member',$orderid);
		$array_update = array();
		$elmember = '';
		if($_POST['ticket_name']) {
			$tdate = $_POST['ticket_date'];
			$i = 0;
			foreach($_POST['ticket_name'] as $tname) {
				$value = array(
					'oticket_name'	=> $tname,
					'oticket_date'	=> $tdate[$i]
				);
				if($ticket_list_member) {
					$array_update = $ticket_list_member;
				}
				$array_update[] = $value;
				$elmember .= '<p style="margin:0 0 5px;"><b>+ '.$tname.' - '.$tdate[$i].'</b></p>';
				$i++;
			}
		}
		update_field( 'field_58eff8c1548c7', $array_update, $orderid);
		update_field('field_58f97e7cd427a',$_POST['ticket_payment'],$orderid);
		update_field('field_58effaaa548ca',$_POST['ticket_add'],$orderid);
		update_field('field_58effb2c548cb',$_POST['ticket_note'],$orderid);
		/* Send mail booking ticket */
		$edear = get_field('pro_name','user_'.$_POST['ticket_sale']);
		$email_to = get_userdata($_POST['ticket_sale'])->user_email;
		$form_terms = wp_get_post_terms($_POST['ticket_id'],'diem_di');
		$efrom = $form_terms[0]->name;
		$to_terms = wp_get_post_terms($_POST['ticket_id'],'diem_den');
		$eto = $to_terms[0]->name;
		$ename = get_field('pro_name','user_',$user_current->ID);
		$enumber = $_POST['ticket_number'];
		$epay = 'Giao dịch an toàn thông qua SANVE';
		$esv = '<p style="margin:15px 0 0;">Phí giao dịch: <b>50.000 VNĐ</b></p>';
		if($_POST['ticket_payment'] == 2) {
			$epay = 'Giao dịch trực tiếp với khách hàng';
			$esv = '<p style="margin:15px 0 0;">Thông tin khách hàng: </p>';
			$esv .= '<p style="margin:15px 0 0;">Họ tên: <b>'.get_field('pro_name','user_'.$user_current->ID).'</b></p>';
			$esv .= '<p style="margin:15px 0 0;">SĐT: <b>'.$user_current->user_login.'</b></p>';
			$esv .= '<p style="margin:15px 0 0;">Email: <b>'.$user_current->user_email.'</b></p>';
		}
		$elnote = '';
		if($_POST['ticket_add']) {
			foreach($_POST['ticket_add'] as $tadd) {
				if($tadd == 1) $taddtext = 'Mua thêm hành lý';
				if($tadd == 2) $taddtext = 'Có trẻ em, em bé đi cùng';
				if($tadd == 3) $taddtext = 'Mua thêm hành lý';
				$elnote .= '<p style="margin:0 0 5px;"><b>+ '.$taddtext.'</b></p>';
			}
		}
		$enote = $_POST['ticket_note'];
		
		$subject = '[Sanve.com.vn] Thông báo có khách hàng đặt vé';
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Sanve.com.vn <hotro@sanve.com.vn>' . "\n";
		ob_start();
		INCLUDE(TEMPLATEPATH .'/email/sale-booking.html');
		$emailbody = ob_get_clean();
		wp_mail($email_to,$subject,$emailbody,$headers);
		/* Send mail admin booking ticket */
		$email_admin = get_field('email_manager_ticket','option');
		
		$memberem = '<p style="margin:15px 0 0;">Họ tên: <b>'.get_field('pro_name','user_'.$user_current->ID).'</b></p>';
		
		ob_start();
		INCLUDE(TEMPLATEPATH .'/email/admin-booking.html');
		$emailbody2 = ob_get_clean();
		wp_mail($email_admin,$subject,$emailbody2,$headers);
		/* Bell */
		$bell_post = array(
			'post_title'    => 'Đặt vé thành công',
			'post_status'   => 'publish',
			'post_type'		=> 'thong_bao'
		);
		$bellid = wp_insert_post($bell_post);
		if($bellid) {
			update_field('field_5903cbf6ddd44',$user_current->ID,$bellid);
			update_field('field_5903cd2bddd47','Chưa đọc',$bellid);
			wp_set_post_terms( $bellid, 51, 'loai_thong_bao' );
		}
		$bell_post = array(
			'post_title'    => 'Thông báo đặt vé',
			'post_status'   => 'publish',
			'post_type'		=> 'thong_bao'
		);
		$bellid = wp_insert_post($bell_post);
		if($bellid) {
			update_field('field_5903cbf6ddd44',$_POST['ticket_sale'],$bellid);
			update_field('field_5903cd2bddd47','Chưa đọc',$bellid);
			ob_start();
			?>
			<p style="margin: 0 0 20px;">Xin chào <i><?php echo $edear; ?>,</i></p>
			<p style="margin:0 0 5px;">Vừa có khách hàng <b><?php echo $ename; ?></b> đặt vé chuyến bay <a href="#"><?php echo $efrom; ?> - <?php echo $eto; ?> (mã số: <?php echo $title; ?>)</a></p>
			<p style="margin:0 0 5px;">Thông tin chi tiết như sau:</p>
			<p style="margin:0 0 5px;">- Số lượng vé: </b><?php echo $enumber; ?></b></p>
			<p style="margin:0 0 5px;">- Họ tên hành khách: </p>
			<?php echo $elmember; ?>
			<p style="margin:0 0 5px;">- Ghi chú: </p>
			<?php echo $elnote; ?>
			<p style="margin:0 0 5px;">Ghi chú thêm: <b><?php echo $enote; ?></b></p>
			<p style="margin:15px 0 0;">Loại hình giao dịch: <b><?php echo $epay; ?></b></p>
			<?php echo $esv; ?>
			<p style="margin:20px 0 0;">Chúc bạn một ngày tốt lành.</p>
			<?php
			$cmessage = ob_get_clean();
			update_field('field_590459b2ef7c0',$cmessage,$bellid);
		}
	}
	wp_redirect(get_permalink($pageid));
}
/* Post Bell */
if($_POST['content_bell']) {
	if($_POST['user_bell'][0] == 'all') {
		$list_ubell = get_users();
		if($list_ubell) {
		foreach($list_ubell as $ubell) {
			$bell_post = array(
				'post_title'    => $_POST['title_bell'],
				'post_status'   => 'publish',
				'post_type'		=> 'thong_bao'
			);
			$bellid = wp_insert_post($bell_post);
			if($bellid) {
				update_field('field_5903cbf6ddd44',$ubell->ID,$bellid);
				update_field('field_5903cd2bddd47','Chưa đọc',$bellid);
				update_field('field_590459b2ef7c0',$_POST['content_bell'],$bellid);
				
			}
		} }
	} else {
		$list_ubell = $_POST['user_bell'];
		if($list_ubell) {
		foreach($list_ubell as $ubell) {
			$bell_post = array(
				'post_title'    => $_POST['title_bell'],
				'post_status'   => 'publish',
				'post_type'		=> 'thong_bao'
			);
			$bellid = wp_insert_post($bell_post);
			if($bellid) {
				update_field('field_5903cbf6ddd44',$ubell,$bellid);
				update_field('field_5903cd2bddd47','Chưa đọc',$bellid);
				update_field('field_590459b2ef7c0',$_POST['content_bell'],$bellid);
				
			}
		} }
	}
	wp_redirect(get_permalink($pageid));
}
?>